<?php
/*
	Template Name: Offers
*/	
?>
<?php
	get_header();

    $show_page_title = get_post_meta( $post->ID, '_show_page_title_meta_key', true );
?>
    <div class="notice-wrapper noticeproduct white">
        <div class="notice-container">
            <div class="notice-inner">
                <div class="noticeproduct-left">
                    <div class="noticeproduct-image">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/templateoffers-sample/noticeproduct.jpg" alt="">
                    </div>
                    <div class="noticeproduct-name">
                        バタフライ
                    </div>
                </div>
                <div class="noticeproduct-right">
                    <a href="#" title="">
                        <div class="std-button">
                            <?php _e( 'カート', 'ambientlounge' ); ?>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>


    
				
						

    <div class="content-area">

        <?php
            if ( !is_front_page() && !is_checkout() && !is_account_page() && ( $show_page_title == 'yes' || $show_page_title == '' ) )
            {
                ?>
                    <div class="page-heading-wrapper">
                        <div class="entry-container">
                            <?php
                                if ( generate_show_title() ) {
                                    $params = generate_get_the_title_parameters();

                                    the_title( $params['before'], $params['after'] );
                                }
                            ?>
                        </div>
                    </div>
                <?php				
            }		
        ?>

        <div class="entry-content">        
            <?php
                the_content();
            ?>
        </div>
    </div>

<?php
	get_footer();
?>