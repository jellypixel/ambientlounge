/* 
* Search Page
*/



(function($) {
	// For search page only
	if ( $('body.search').length ) {
		var initialCatLoad = initialTagsLoad = false;
		var urlParams = new URLSearchParams(window.location.search);

		$(document).ready(function() {
			$("select[name='categories[]']").select2({
				placeholder: "All Categories"
			});

			$("select[name='tags[]']").select2({
				placeholder: "All Tags"
			});

			/** Initialize **/

			// Set content-type
			if ( urlParams.has('content-type') ) {
				$('select[name="content-type"]').val(urlParams.get('content-type'));
				$("select[name='content-type']").trigger('change');

				initialCatLoad = true;
				initialTagsLoad = true;
			}
		});

		// On content-type change
		$(document).on('change', 'select[name="content-type"]', function() {
			var contentType = $(this).val();
			var ajaxUrl = HC.ajaxurl;

			// Create an $.post request with the action: advanced_search_fetch_categories
			$.post(ajaxUrl, {
				action: 'advanced_search_fetch_categories',
				nonce: HC.nonce,
				content_type: contentType
			}, function(response) {
				// Destroy the select2 for categories
				$("select[name='categories[]']").select2('destroy');

				// Replace the select2 with the new data
				$("select[name='categories[]']").empty();
				$("select[name='categories[]']").select2({
					placeholder: "All Categories",
					data: response.data
				});

				if ( !initialCatLoad ) return;

				// Set categories
				if ( urlParams.has('categories[]') ) {
					var categories = urlParams.getAll('categories[]');
					// Select2 - preselect the categories
					$('select[name="categories[]"]').val(categories).trigger('change');
				}

				initialCatLoad = false; // reset
			}, 'json');

			// Create an $.post request with the action: advanced_search_fetch_tags
			$.post(ajaxUrl, {
				action: 'advanced_search_fetch_tags',
				nonce: HC.nonce,
				content_type: contentType
			}, function(response) {
				// Destroy the select2 for categories
				$("select[name='tags[]']").select2('destroy');

				// Replace the select2 with the new data
				$("select[name='tags[]']").empty();
				$("select[name='tags[]']").select2({
					placeholder: "All Tags",
					data: response.data
				});

				if ( !initialTagsLoad ) return;

				// Set categories
				if ( urlParams.has('tags[]') ) {
					var tags = urlParams.getAll('tags[]');
					// Select2 - preselect the tags
					$('select[name="tags[]"]').val(tags).trigger('change');
				}

				initialTagsLoad = false; // reset
			}, 'json');

		});
	}
})(jQuery);

/* 
* Others
*/

var $j = jQuery.noConflict();

var vpHeight = $j( window ).height();
var headerHeight = 0;
var headerEl;
var footerTop = $j( '#footer-widgets' ).position().top;
var flexslider = { vars:{} };

$j(function(){	
	// Search
		$j( '.search-field' ).attr( 'placeholder', 'Search...' );

	// Megamenu 		
		var submenuContentWrapper = $j( '.submenu-content-wrapper' );

		$j( '.submenu-title' ).click( function(){
			var submenuContentTargetID = $j( this ).attr( 'data-target' );

			$j( this ).toggleClass( 'active' )
				.siblings()
					.removeClass( 'active' );
			
			submenuContentWrapper.find( $j( submenuContentTargetID ) )
				.addClass( 'active')
					.siblings()
						.removeClass( 'active' );
		})		

	// Mobile Menu
		// Root Menu - Tabbing
		var primaryMenu = $j( '.desktop-primary-menu' );

		$j( '.mobile-menu-title' ).click( function() {
			var menuContentTargetID = $j( this ).attr( 'data-target' );

			$j( this ).toggleClass( 'active' )
				.siblings()
					.removeClass( 'active' );
			
			primaryMenu.find( $j( menuContentTargetID ) )
				.addClass( 'active')
					.siblings()
						.removeClass( 'active' );
		});

		// Sub Menu (Open)
		$j( '.submenu-accordion-title' ).click( function() {
			var thisEl = $j( this ).parent( '.submenu-accordion-wrapper' );
			/*var activeSibling;
			if ( thisEl.hasClass( 'active' ) ) 
			{
				thisEl.find( '.submenu-accordion-content' ).slideUp( 400, function() {
					thisEl.removeClass( 'active' );
				});
			}
			else 
			{
				activeSibling = thisEl.siblings( '.active' );
				if ( activeSibling.length ) 
				{
					activeSibling.find( '.submenu-accordion-content' ).slideUp( 400, function() {
						activeSibling.removeClass( 'active' );

						thisEl.find( '.submenu-accordion-content' ).slideDown( 400 );
						thisEl.addClass( 'active' );
					});				
				}
				else
				{
					thisEl.find( '.submenu-accordion-content' ).slideDown( 400 );
					thisEl.addClass( 'active' );
				}
			}*/
			thisEl.siblings( '.active' ).removeClass( 'active' );
			thisEl.toggleClass( 'active' );
		});

		// Sub Menu (Close)
		$j( '.submenu-accordion-content > .submenu-accordion-title' ).click( function() {
			var thisEl = $j( this ).parents( '.submenu-accordion-wrapper' );

			thisEl.siblings( '.active' ).removeClass( 'active' );
			thisEl.toggleClass( 'active' );
		});

		// Sub Menu Level 2 - Accordion
		$j( '.sub-submenu-accordion-title' ).click( function() {
			var thisEl = $j( this ).parent( '.sub-submenu-accordion-wrapper' );
			
			if ( thisEl.hasClass( 'active' ) ) 
			{
				thisEl.find( '.sub-submenu-accordion-content' ).slideUp( 400, function() {
					thisEl.removeClass( 'active' );
				});
			}
			else 
			{
				thisEl.find( '.sub-submenu-accordion-content' ).slideDown( 400 );
				thisEl.addClass( 'active' );
			}
		});

		// Burger bar on click		
		$j( '.burger-wrapper' ).click( function() {
			$j( this )
				.toggleClass( 'active' )
				.siblings( '#site-navigation' ).toggleClass( 'active' );
		});

		// Reset first submenu tab on entering mobile menu
		resetMobileMenu();

		/*var nav = $j( '.menu-wrapper' );
		var headerWrapper = $j( '.header-wrapper' );
		var slidingOverlay = $j( '.slidingmenu-overlay' );
	
		// Burger bar on click
		$j( '.nav-toggler' ).click( function() {
			// Sliding Menu
			if ( nav.hasClass( 'slidingmenu' ) )
			{
				// Close Sliding Menu
				if ( headerWrapper.hasClass( 'open' ) )
				{
					nav.animate({ 'left' : '-220px' }, function() {
						nav.hide();	
						slidingOverlay.fadeOut(400);
					});
				}
				// Open Sliding Menu
				else
				{
					nav.show();
					nav.animate({ 'left' : '0' }, 400);
					slidingOverlay.fadeIn(400);
				}
			}
			// Dropdown Menu
			else
			{	
				nav.slideToggle( 400 );
			}
			
			headerWrapper.toggleClass( 'open' );
		});
		
		// Close Sliding Menu on Overlay Click
		slidingOverlay.click( function() {
			nav.animate({ 'left' : '-220px' }, function() {
				nav.hide();	
				slidingOverlay.fadeOut(400);
				
				headerWrapper.toggleClass( 'open' );
			});
		});*/
		
	// Sticky
		headerEl = $j( 'header' );
		if( headerEl.length && headerEl.hasClass( 'stickyheader' ) ) 
		{
			var topBar = $j( '.topbar-wrapper' ).outerHeight();			
			var headerBar = $j( '.header-wrapper' ).outerHeight();
			topBar = topBar ? topBar : 0;
			headerBar = headerBar ? headerBar : 0;
			headerHeight = topBar + headerBar;
			
			if( $j( '.stickyheader:not(.transparentheader)' ).length ) {			
				$j( 'body' ).css({ 'padding-top' : headerHeight });
			}
		}
		
	// Search
		$j( 'body' ).on( 'click', '.search-wrapper svg', function() {
			headerEl.toggleClass( 'searchopen' );
			
			$j( '.search-input' ).focus();
		});
		
	// Gutenberg Block Image - Fit Container
		$j( '.is-style-fit-container' ).parent( '.wp-block-column' ).css({ 'position' : 'relative' });
	
	// To Top	
		$j( '#toTop, .toTop' ).click( function(e) {
			e.preventDefault();
			$j( 'html, body' ).animate({
				scrollTop: 0
			}, 2000);
		});
		
	// Category and Year Filter
		$j( '#cat' ).change( function(e) {			
			e.preventDefault();
			//$j( this ).parent( 'form' ).submit();			
			window.location.href = $j( this ).val();	
		});
		
	// Areas of Work Carousel
		$j('.carousel-wrapper').flexslider({
			animation: "slide",
			animationLoop: false,
			itemWidth: 210,
			itemMargin: 50,
			minItems: getGridSize(), 
			maxItems: getGridSize(),
			prevText: '',
			nextText: '',
			controlNav: false,
			directionNav: true
		});
		
	// Filter List - Convert from Gutenberg
		if ( $j( '.faculty-sidebar' ).length ) {
			var thisEl = $j( '.faculty-sidebar' );	
			thisEl.addClass( 'filter-wrapper' );
			
			thisEl.children().not( 'h4' ).wrapAll( '<div class="filter-content"></div>' );
			//thisEl.children( 'h4' ).wrap( '<div class="filter-header"></div>' );
			//thisEl.children( '.filter-header' ).append( '<div class="filter-icon"></div>' );
			thisEl.prepend( '<div class="filter-header"><h2>' + thisEl.children( 'h4' ).html() +'</h2><div class="filter-icon"></div></div>' );
			thisEl.children( 'h4' ).remove();
		}
		
	// Filter List - General
		$j( 'body' ).on( 'click', '.filter-icon', function() {
			var filterWrapper = $j( this ).parents( '.filter-wrapper' );
			var filterContent = $j( '.filter-content' );
			
			if ( filterWrapper.hasClass( 'open' ) ) {
				filterContent.slideUp(200, function() {
					filterWrapper.removeClass( 'open' );	
				});				
			}
			else {
				filterContent.slideDown(400, function() {
					filterWrapper.addClass( 'open' );	
				});	
			}
		});

	

	
		
	//
	/*var searchItem = $j( '.search-item > a ' );
	var searchContent = searchItem.html();
	var searchAria = searchItem.attr( 'aria-label' );
	searchItem.replaceWith( '<div aria-label="' + searchAria + '">' + searchContent + '</div>' );*/
	
	// WAVE Fix
		$j( '.wp-block-cover' ).each( function() {
			var hasBackgroundColorEl = $j( this ).find( '.has-background-color' );
			var hasForegroundColorEl = $j( this ).find( '.has-foreground-color' );
			
			if ( hasBackgroundColorEl.length ) {
				$j( this ).css({ 'background-color' : '#000' });
			}
			else if ( hasForegroundColorEl.length ) {
				$j( this ).css({ 'background-color' : '#fff' });
			}
		});
	
		$j( '.flex-prev' ).attr({ 'aria-label' : 'Previous Slides' });
		$j( '.flex-next' ).attr({ 'aria-label' : 'Next Slides' });
	
	
	
	/*$j( '.search-field' ).attr({ 'id' : 's' });
	$j( '.navigation-search' ).prepend( '<label for="s" style="display:none;">Search</label>' );*/
	
	$j( '.screen-reader-text' ).removeAttr( 'title' );
	
	$j( '.gutentor-post-module .posted-on a' ).each( function() {
		var postedOn = $j( this ).parent( '.posted-on' );
		var dateEl = $j( this ).html();
		
		$j( this ).remove();
		postedOn.html( dateEl );
		
	});
	
	$j( '.post-list-wrapper .post-list .post-box .post-content .post-excerpt a, .blog-stage-wrapper .post-sticky .post-sticky-row .post-sticky-rest .post-sticky-rest-box .post-sticky-rest-content .post-sticky-rest-excerpt a' ).remove();

	// Home - Category
		$j( '.blog-category-wrapper .blog-category' ).click( function(e) {
			$j( this ).toggleClass( 'active' ).siblings( '.blog-category' ).removeClass( 'active' );
			$j( '#cat' ).val( $j( this ).attr( 'cat' ) );

			$j( this ).parents( 'form' ).submit();
		})

	// Styling Input File
		$j( '#photo-attachment-trigger' ).each( function()
		{
			var attachmentFileInput = $j( this ).find( '#photo-attachment' );
			var attachmentLabel	= $j( this ).siblings( '#photo-attachment-label' );
			var attachmentLabelValue = attachmentLabel.children( 'span' ).html();

			attachmentFileInput.on( 'change', function( e ) {
				var fileName = '';

				if( this.files && this.files.length > 1 ) 
				{
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				}
				else if( e.target.value )
				{
					fileName = e.target.value.split( '\\' ).pop();
				}

				if( fileName ) 
				{
					attachmentLabel.html( '<span>' + fileName + '</span><span id="photo-attachment-remove"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" aria-hidden="true" focusable="false"><path d="M12 13.06l3.712 3.713 1.061-1.06L13.061 12l3.712-3.712-1.06-1.06L12 10.938 8.288 7.227l-1.061 1.06L10.939 12l-3.712 3.712 1.06 1.061L12 13.061z"></path></svg></span>' );

					var reader = new FileReader();				
					reader.onload = function (e) {
						$j( '#photo-attachment-preview' ).html( '<img src="' + e.target.result + '">').addClass( 'file-added' );
					}

					reader.readAsDataURL( this.files[0] );
				}
				else 
				{
					attachmentLabel.html( attachmentLabelValue );
				}
			});

			// FF fix
			attachmentFileInput
				.on( 'focus', function(){ attachmentFileInput.addClass( 'has-focus' ); })
				.on( 'blur', function(){ attachmentFileInput.removeClass( 'has-focus' ); });
		});

		$j( 'body' ).on( 'click', '#photo-attachment-remove', function() {
			var triggerEl = $j( this ).parent( '#photo-attachment-label' ).siblings( '#photo-attachment-trigger' );

			triggerEl.find( '#photo-attachment-preview' )
				.removeClass( 'file-added' )
				.empty();

			triggerEl.find( '#photo-attachment' ).val( '' );

			$j( this ).siblings( 'span' ).html( 'No file chosen' );
			$j( this ).remove();
		})

	// Styling Input Number 
		$j( '.inputqty-wrapper .input-minus' ).click( function() {
			var parentEl = $j( this ).parent( '.inputqty-wrapper' );
			var numberEl = parentEl.find( 'input[type=number]' );
			var finalNumber = parseInt( numberEl.val(), 10 ) - 1;
			if ( finalNumber < 1 ) {
				finalNumber = 1;
			}

			numberEl.val( finalNumber );
		})

		$j( '.inputqty-wrapper .input-plus' ).click( function() {
			var parentEl = $j( this ).parent( '.inputqty-wrapper' );
			var numberEl = parentEl.find( 'input[type=number]' );
			var finalNumber = parseInt( numberEl.val(), 10 ) + 1;

			numberEl.val( finalNumber );
		})

	// Shortcuts - Scroll to section
		$j( '.shortcuts .wp-block-button__link' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).parent().attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 64
			}, 2000);
		});

	// Shortcuts - Scroll to section
		$j( '.shortcuts-row p' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 112
			}, 1000);
		});

	// Accordion Shortcuts - Scroll to section and open the accordion
		$j( '.accordion-shortcuts .wp-block-button__link' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).parent().attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 128
			}, 2000, function() {
				if ( !$j( '.' + targetEl ).hasClass( 'active' ) ) {
					$j( '.' + targetEl ).addClass( 'active')
						.children( '.accordion-item-content' ).slideDown( 400 );
				}
			});
		});

	// Accordion
		// Open first accordion item on first accordion group
		$j( '.accordion-block-wrapper' ).first().children( '.accordion-item-wrapper' ).first().addClass( 'active' ).children( '.accordion-item-content' ).show();

		$j( '.accordion-item-title' ).click( function() {
			var thisEl = $j( this );
			var parentEl = $j( this ).parent( '.accordion-item-wrapper' );

			if ( parentEl.hasClass( 'active' ) ) 
			{
				thisEl.siblings( '.accordion-item-content' ).slideUp( 400 );
			}
			else 
			{
				thisEl.siblings( '.accordion-item-content' ).slideDown( 400 );
			}

			parentEl.toggleClass( 'active' );
		})

	// Template Stores
		// Shortcuts - Scroll to section
			$j( '.store-list' ).click(function(e) {
				e.preventDefault();

				var targetEl = $j( this ).attr( 'id' );	

				$j([document.documentElement, document.body]).animate({
					scrollTop: $j( '.' + targetEl ).offset().top - 128
				}, 2000);
			});

		// Accordion
			$j( '.store-location-title' ).click( function() {
				var thisEl = $j( this );
				var parentEl = $j( this ).parent( '.store-location' );

				if ( parentEl.hasClass( 'active' ) ) 
				{
					thisEl.siblings( '.store-location-list' ).slideUp( 400 );
				}
				else 
				{
					thisEl.siblings( '.store-location-list' ).slideDown( 400 );
				}

				parentEl.toggleClass( 'active' );
			})

	// Page - Professional - Stage 
		// HTML inside gutenberg button 
		$j( '.professional-stage-shortcuts .wp-block-button__link' ).each( function() {
			var thisEl = $j( this );
			var button_text = thisEl.text().replace( '(', '<span class="button-text-detail">(' );
			button_text = button_text.replace( ')', ')</span>' );
			
			thisEl.html( button_text );
		})

	// Page - FAQ
		// Shortcuts	
		$j( '.shortcut-columns p' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 128
			}, 2000);
		});
});

function getGridSize() {
	return ( window.innerWidth < 576 ) ? 1 :
		   ( window.innerWidth < 992 ) ? 2 : 3;
}

$j( window ).resize( function() {	
	// Areas of Work Carousel
		var gridSize = getGridSize();		
 
		flexslider.vars.minItems = gridSize;
		flexslider.vars.maxItems = gridSize;

	// Reset first submenu tab on entering mobile menu
	resetMobileMenu();
});

$j( window ).scroll( function() {
	// Scroll to top	
		if ( $j( window ).scrollTop() > vpHeight ) {
			$j( '#toTop' ).fadeIn(400);
		}
		else {
			$j( '#toTop' ).fadeOut(400);	
		}
	
	// Sticky Header
		if( $j( '.stickyheader' ).length ) 
		{
			if ( $j( window ).scrollTop() > headerHeight ) {
				headerEl.addClass( 'onScroll' );
			}
			else {
				headerEl.removeClass( 'onScroll' );
			}
		}	
});

function resetMobileMenu() {
	if ( window.innerWidth < 992 ) {
		$j( '.submenu-title-wrapper .submenu-title, .submenu-content-wrapper .submenu-accordion-wrapper' ).removeClass( 'active' );
	}
	else {
		$j( '.submenu-title-wrapper .submenu-title:first-child, .submenu-content-wrapper .submenu-accordion-wrapper:first-child' ).addClass( 'active' );
	}
}