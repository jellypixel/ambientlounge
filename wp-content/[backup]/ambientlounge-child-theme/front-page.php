<?php
	get_header();
?>
	<div class="entry-content">        
        <?php		
			if ( have_posts() ) 
			{
				while ( have_posts() ) 
				{
					the_post();
					global $post;

					the_content();
				}
			} 

			wp_reset_postdata();					
		?>
    </div>

<?php
	get_footer();
?>