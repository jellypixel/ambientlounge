<?php
/**
 * The template for displaying the header.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php generate_do_microdata( 'body' ); ?>>
	<?php
	/**
	 * wp_body_open hook.
	 *
	 * @since 2.3
	 */
	do_action( 'wp_body_open' ); // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedHooknameFound -- core WP hook.

	/**
	 * generate_before_header hook.
	 *
	 * @since 0.1
	 *
	 * @hooked generate_do_skip_to_content_link - 2
	 * @hooked generate_top_bar - 5
	 * @hooked generate_add_navigation_before_header - 5
	 */
	do_action( 'generate_before_header' );	
	
	function before_logo() {
		//return 'a';	
	}
	add_action( 'generate_before_logo_hook', 'before_logo' );

	/**
	 * generate_header hook.
	 *
	 * @since 1.3.42
	 *
	 * @hooked generate_construct_header - 10
	 */
	//do_action( 'generate_header' );
	$headerClass = '';
	/*$transparent_menu = get_post_meta( $post->ID, '_transparent_menu_meta_key', true );
	
	if ( $transparent_menu == 'yes' )
	{
		$headerClass .= 'transparentheader';
	}*/
	
	?>
    	<header <?php generate_do_attr( 'header', array( 'class' => $headerClass . ' ' ) ); ?>>
			<div <?php //generate_do_attr( 'inside-header' ); ?>>
            	<div class="header-top">
                	<div class="left-menu">
						<div class="burger-wrapper">
							<div class="burger-inner">
								<div class="burgerbar"></div>
								<div class="burgerbar"></div>
								<div class="burgerbar"></div>
							</div>
						</div>
						<nav id="site-navigation" <?php /*class="main-navigation"*/ ?>>
							<div class="inside-navigation">
								<div id="primary-menu" class="main-nav">

									<div class="mobile-primary-menu">										
										<div class="mobile-menu-title active" data-target="#pet">
											<?php _e( 'ペット', 'ambientlounge' ); ?>
										</div>
										<div class="mobile-menu-title" data-target="#sofa">
											<?php _e( 'ソファ', 'ambientlounge' ); ?>
										</div>
									</div>

									<ul class="desktop-primary-menu">
										<li id="pet" class="menu-title active">
											<a href="#"><?php _e( 'ソファ', 'ambientlounge' ); ?></a>	

											<nav class="megamenu-wrapper">
												<div class="submenu-title-wrapper">
													<div class="submenu-title active" data-target="#petlounge"><?php _e( 'ペットラウンジ', 'ambientlounge' ); ?></div>
													<div class="submenu-title" data-target="#reasonforchoosing"><?php _e( '選ぶ理由', 'ambientlounge' ); ?></div>
													<div class="submenu-title" data-target="#blog"><?php _e( 'Blog', 'ambientlounge' ); ?></div>
												</div>
												
												<div class="submenu-content-wrapper">
													<div class="container">

														<div id="petlounge" class="submenu-content submenu-accordion-wrapper active">
															<div class="submenu-accordion-title">
																<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
																</svg>

																<?php _e( 'ペットラウンジ', 'ambientlounge' ); ?>

																<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																</svg>
															</div>															
															<div class="submenu-accordion-content">		
																<div class="submenu-accordion-title">
																	<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
																	</svg>

																	<?php _e( 'ペットラウンジ', 'ambientlounge' ); ?>
																</div>																											
																<div class="columns gap-one-half">																	
																	<div id="petlounge-bedtypes" class="sub-submenu-accordion-wrapper active column-equal">
																		<?php	
																			// Pet Lounge Card - Desktop
																			menu_section_petloungecard();	

																			function menu_section_petloungecard() 
																			{
																				?>
																					<div class="sub-submenu-accordion-title">
																						<?php _e( '商品種類', 'ambientlounge' ); ?>

																						<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																							<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																						</svg>
																					</div>
																					<div class="sub-submenu-accordion-content">
																						<div id="petlounge-card" class="columns gap-half columns-down">
																							<a href="#" title="<?php _e( '迷ったらこちら DELUX', 'ambientlounge' ); ?>">
																								<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																									<div class="card-image">
																										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-01.svg" alt="<?php _e( '迷ったらこちら DELUX', 'ambientlounge' ); ?>">
																									</div>
																									<div class="card-title">
																										<div class="card-title-text">																										
																											<?php _e( '定番ベッド', 'ambientlounge' ); ?>
																										</div>
																									</div>																								
																								</div>
																							</a>

																							<a href="#" title="<?php _e( 'エコロジカルな再生商品 リファービッシュ', 'ambientlounge' ); ?>">
																								<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																									<div class="card-image">
																										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-02.svg" alt="<?php _e( 'エコロジカルな再生商品 リファービッシュ', 'ambientlounge' ); ?>">
																									</div>
																									<div class="card-title">																									
																										<div class="card-title-text">																										
																											<?php _e( '限定ベッド', 'ambientlounge' ); ?>
																										</div>
																									</div>																								
																								</div>
																							</a>

																							<a href="#" title="<?php _e( 'エコロジカルな再生商品 リファービッシュ', 'ambientlounge' ); ?>">
																								<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																									<div class="card-image">
																										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-03.svg" alt="<?php _e( 'エコロジカルな再生商品 リファービッシュ', 'ambientlounge' ); ?>">
																									</div>
																									<div class="card-title">	
																										<div class="card-title-subtext">	
																											<?php _e( 'セット商品', 'ambientlounge' ); ?>
																										</div>																								
																										<div class="card-title-text">																										
																											<?php _e( 'DELUX', 'ambientlounge' ); ?>
																										</div>
																									</div>																								
																								</div>
																							</a>

																							<a href="#" title="<?php _e( 'エコロジカルな再生商品 リファービッシュ', 'ambientlounge' ); ?>">
																								<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																									<div class="card-image">
																										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-04.svg" alt="<?php _e( 'エコロジカルな再生商品 リファービッシュ', 'ambientlounge' ); ?>">
																									</div>
																									<div class="card-title">
																										<div class="card-title-subtext">	
																											<?php _e( 'エコロジカルな再生商品', 'ambientlounge' ); ?>
																										</div>																								
																										<div class="card-title-text">																										
																											<?php _e( 'リファービッシュ', 'ambientlounge' ); ?>
																										</div>
																									</div>																								
																								</div>
																							</a>
																						</div>
																					</div>
																				<?php
																			}
																		?>
																	</div>
																	<div id="petlounge-sizechart" class="sub-submenu-accordion-wrapper column-equal">
																		<div class="sub-submenu-accordion-title">
																			<?php _e( 'サイズ', 'ambientlounge' ); ?>

																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content">
																			<div class="sizechart-columns">
																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'S', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-dog">
																						<a href="#" title="<?php _e( '小型犬', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_s.svg" alt="<?php _e( '小型犬', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '小型犬<br>10 k g', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="sizechart-cat">
																						<a href="#" title="<?php _e( '成猫1頭', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_s.svg" alt="<?php _e( '成猫1頭', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '成猫1頭', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>

																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'M', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-dog">
																						<a href="#" title="<?php _e( '中型犬', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_m.svg" alt="<?php _e( '中型犬', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '中型犬<br>25 k g', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="sizechart-cat">
																						<a href="#" title="<?php _e( '成猫2頭', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_m.svg" alt="<?php _e( '成猫2頭', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '成猫2頭', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>

																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'L', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-dog">
																						<a href="#" title="<?php _e( '大型犬', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_l.svg" alt="<?php _e( '大型犬', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '大型犬<br>40 k g', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="sizechart-cat">
																						<a href="#" title="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_l.svg" alt="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>

																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'XXL', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-dog">
																						<a href="#" title="<?php _e( '超大型犬', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_xxl.svg" alt="<?php _e( '超大型犬', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '超大型犬<br>40 k g以上', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="sizechart-cat">
																						<a href="#" title="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_xxl.svg" alt="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>
																			</div>

																			<?php
																				// Pet Lounge Card - Mobile
																				//menu_section_petloungecard();
																			?>
																		</div>																		
																	</div>
																	<div class="column-equal">

																		<div id="petlounge-colorpurposeaccessories" class="columns gap-full">
																			<?php
																			/*
																			<div class="sub-submenu-accordion-wrapper column">
																				<div class="sub-submenu-accordion-title column-attr">
																					<?php _e( 'カラー', 'ambientlounge' ); ?>

																					<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																						<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																					</svg>
																				</div>
																				<div class="sub-submenu-accordion-content column-value">
																					<a href="#" title="<?php _e( 'グレー', 'ambientlounge' ); ?>"><div class="tone color-gray"><span><?php _e( 'グレー', 'ambientlounge' ); ?></span></div></a>
																					<a href="#" title="<?php _e( 'ベージュ', 'ambientlounge' ); ?>"><div class="tone color-beige"><span><?php _e( 'ベージュ', 'ambientlounge' ); ?></span></div></a>
																					<a href="#" title="<?php _e( 'ブルードリーム', 'ambientlounge' ); ?>"><div class="tone color-bluedream"><span><?php _e( 'ブルードリーム', 'ambientlounge' ); ?></span></div></a>
																				</div>
																			</div>
																			*/
																			?>

																			<div class="sub-submenu-accordion-wrapper column">
																				<div class="sub-submenu-accordion-title column-attr">
																					<?php _e( 'アクセサリー', 'ambientlounge' ); ?>

																					<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																						<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																					</svg>
																				</div>
																				<div class="sub-submenu-accordion-content column-value">
																					<a href="#" title="<?php _e( '取替カバー', 'ambientlounge' ); ?>"><span><?php _e( '取替カバー', 'ambientlounge' ); ?></span></a>
																					<a href="#" title="<?php _e( 'The Shampoo', 'ambientlounge' ); ?>"><span><?php _e( 'The Shampoo', 'ambientlounge' ); ?></span></a>

																					<?php
																					/*
																					<a href="#" title="<?php _e( 'その他アクセサリー', 'ambientlounge' ); ?>"><span><?php _e( 'その他アクセサリー', 'ambientlounge' ); ?></span></a>
																					*/
																					?>
																				</div>
																			</div>

																			<div id="petlounge-purpose" class="sub-submenu-accordion-wrapper column">
																				<div class="sub-submenu-accordion-title column-attr">
																					<?php _e( 'ご利用用途', 'ambientlounge' ); ?>

																					<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																						<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																					</svg>
																				</div>
																				<div class="sub-submenu-accordion-content column-value">
																					<a href="#" title="<?php _e( '春夏商品', 'ambientlounge' ); ?>"><span><?php _e( '春夏商品', 'ambientlounge' ); ?></span></a>
																					<a href="#" title="<?php _e( '秋冬商品', 'ambientlounge' ); ?>"><span><?php _e( '秋冬商品', 'ambientlounge' ); ?></span></a>
																					<a href="#" title="<?php _e( 'シニア', 'ambientlounge' ); ?>"><span><?php _e( 'シニア', 'ambientlounge' ); ?></span></a>
																				</div>
																			</div>

																			
																		</div>

																		<div class="columns gap-full">
																			<div class="sub-submenu-accordion-wrapper column">
																				<div class="sub-submenu-accordion-title column-attr">
																					<?php _e( 'カラー', 'ambientlounge' ); ?>

																					<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																						<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																					</svg>
																				</div>
																				<div class="sub-submenu-accordion-content column-value">
																					<a href="#" title="<?php _e( 'グレー', 'ambientlounge' ); ?>"><div class="tone color-gray"><span><?php _e( 'グレー', 'ambientlounge' ); ?></span></div></a>
																					<a href="#" title="<?php _e( 'ベージュ', 'ambientlounge' ); ?>"><div class="tone color-beige"><span><?php _e( 'ベージュ', 'ambientlounge' ); ?></span></div></a>
																					<a href="#" title="<?php _e( 'ブルードリーム', 'ambientlounge' ); ?>"><div class="tone color-bluedream"><span><?php _e( 'ブルードリーム', 'ambientlounge' ); ?></span></div></a>
																				</div>
																			</div>
																		</div>

																		

																	</div>
																</div>

																<a href="#" class="soft-link"><?php _e( '全ての商品を見る', 'ambientlounge' ); ?></a>
															</div>
														</div>

														<div id="reasonforchoosing" class="submenu-content submenu-accordion-wrapper">
															<div class="submenu-accordion-title">
																<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
																</svg>

																<?php _e( '選ぶ理由', 'ambientlounge' ); ?>

																<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																</svg>
															</div>															
															<div class="submenu-accordion-content">
																<div class="submenu-accordion-title">
																	<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
																	</svg>

																	<?php _e( '選ぶ理由', 'ambientlounge' ); ?>
																</div>	
																<div class="columns">
																	<div class="sub-submenu-accordion-wrapper active">
																		<div class="sub-submenu-accordion-title">
																			<?php _e( '選ぶ理由', 'ambientlounge' ); ?>

																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content columns column-equal card card-columns radius-half has-lightgray-background-color">
																			<div class="column">
																				<div class="title">
																					<?php _e( '選ぶ理由', 'ambientlounge' ); ?>
																				</div>
																				<p><?php _e( 'デザイン性と安全性についてご紹介します', 'ambientlounge' ); ?></p>
																				<div class="columns gap-half">
																					<div class="column-equal">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/reasonforchoosing/LPDog.webp" alt="<?php _e( '犬向け', 'ambientlounge' ); ?>">
																						<a href="#" title="犬向け"><?php _e( '犬向け', 'ambientlounge' ); ?></a>
																					</div>
																					<div class="column-equal">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/reasonforchoosing/LPCat.webp" alt="<?php _e( '猫向け', 'ambientlounge' ); ?>">
																						<a href="#" title="猫向け"><?php _e( '猫向け', 'ambientlounge' ); ?></a>
																					</div>
																				</div>
																			</div>
																			<div class="column">
																				<div class="columns card card-columns radius-half gap-half padding-half has-white-background-color has-secondarydark-color">
																					<div class="column-equal">
																						<div class="title h6">
																							<?php _e( 'ベッドカバーについて', 'ambientlounge' ); ?>		
																						</div>
																						<p class="h7"><?php _e( 'お洗濯も簡単。ベッドカバーの魅力をご紹介', 'ambientlounge' ); ?></p>
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/reasonforchoosing/LPCover.webp" alt="<?php _e( 'ベッドカバーについて', 'ambientlounge' ); ?>">
																						<div class="columns gap-none">
																							<div class="column-equal">
																								<a href="#" class="link-arrow" title="犬向け"><?php _e( '犬向け', 'ambientlounge' ); ?></a>
																							</div>
																							<div class="column-equal">
																								<a href="#" class="link-arrow" title="猫向け"><?php _e( '猫向け', 'ambientlounge' ); ?></a>
																							</div>
																						</div>
																					</div>
																					<div class="column-equal">
																						<div class="title h6">
																							<?php _e( 'フィリングについて', 'ambientlounge' ); ?>		
																						</div>
																						<p class="h7"><?php _e( 'ペットの好みや性格、年齢に合わせて3種類をご用意', 'ambientlounge' ); ?></p>
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/reasonforchoosing/LPFilling.svg" alt="<?php _e( 'フィリングについて', 'ambientlounge' ); ?>">
																						<div class="columns gap-none">
																							<div class="column-equal">
																								<a href="#" class="link-arrow" title="犬向け"><?php _e( '犬向け', 'ambientlounge' ); ?></a>
																							</div>
																							<div class="column-equal">
																								<a href="#" class="link-arrow" title="猫向け"><?php _e( '猫向け', 'ambientlounge' ); ?></a>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="column-side">
																		<div class="sub-submenu-accordion-wrapper">
																			<div class="sub-submenu-accordion-title column-attr">
																				<div class="title">																			
																					<?php _e( 'サイズ選びに迷ったら', 'ambientlounge' ); ?>																				
																				</div>
																				<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																					<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																				</svg>
																			</div>
																			<div class="sub-submenu-accordion-content column-value">
																				<a href="#" title="サイズを比較する"><?php _e( 'サイズを比較する', 'ambientlounge' ); ?></a>
																				<a href="#" title="店舗一覧"><?php _e( '店舗一覧', 'ambientlounge' ); ?></a>
																			</div>
																		</div>

																		<div class="sub-submenu-accordion-wrapper">
																			<div class="sub-submenu-accordion-title column-attr">
																				<div class="title">
																					<?php _e( '購入について', 'ambientlounge' ); ?>																			
																				</div>

																				<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																					<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																				</svg>
																			</div>
																			<div class="sub-submenu-accordion-content column-value">
																				<a href="#" title="60日間トライアル"><?php _e( '60日間トライアル', 'ambientlounge' ); ?></a>
																				<a href="#" title="ご利用ガイド"><?php _e( 'ご利用ガイド', 'ambientlounge' ); ?></a>
																			</div>
																		</div>																		
																	</div>
																</div>
															</div>
														</div>

														<div id="blog" class="submenu-content submenu-accordion-wrapper">
															<div class="submenu-accordion-title">
																<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
																</svg>

																<?php _e( 'Blog', 'ambientlounge' ); ?>

																<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																</svg>
															</div>															
															<div class="submenu-accordion-content">
																<div class="submenu-accordion-title">
																	<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
																	</svg>

																	<?php _e( 'Blog', 'ambientlounge' ); ?>
																</div>	
															</div>
														</div>

													</div>													
												</div>	
											</nav>										
										</li>
										<li id="sofa" class="menu-title">
											<a href="#"><?php _e( 'ペット', 'ambientlounge' ); ?></a>										

											<nav class="megamenu-wrapper">
											</nav>
										</li>
									</ul>

									<div class="mobile-bottom-menu">
										<div class="myaccount-bottom-menu-wrapper">
											<a href="#">
												<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M9.67857 18.3571C14.4716 18.3571 18.3571 14.4716 18.3571 9.67857C18.3571 4.88553 14.4716 1 9.67857 1C4.88553 1 1 4.88553 1 9.67857C1 14.4716 4.88553 18.3571 9.67857 18.3571Z" stroke="#2F67B2" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
													<path d="M9.67984 12.9844C7.21127 12.9844 4.98377 14.0162 3.40234 15.6715C4.98377 17.3269 7.20806 18.3587 9.67984 18.3587C12.1516 18.3587 14.3759 17.3269 15.9573 15.6715C14.3759 14.0162 12.1516 12.9844 9.67984 12.9844Z" stroke="#2F67B2" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
													<path d="M9.68089 11.0102C11.3514 11.0102 12.7055 9.65604 12.7055 7.98558C12.7055 6.31512 11.3514 4.96094 9.68089 4.96094C8.01043 4.96094 6.65625 6.31512 6.65625 7.98558C6.65625 9.65604 8.01043 11.0102 9.68089 11.0102Z" stroke="#2F67B2" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
												</svg>
												<?php _e( 'マイアカウント', 'ambientlounge' ); ?>
											</a>
										</div>
										<div class="service-bottom-menu-wrapper">
											<a href="mailto:service@ambientlounge.co.jp">service@ambientlounge.co.jp</a>
											<a href="tel:0120429332">0120429332</a>
										</div>
									</div>
								</div>
							</div>
						</nav>
						<?php
                            /**
                             * generate_after_header_content hook.
                             *
                             * @since 0.1
                             *
                             * @hooked generate_add_navigation_float_right - 5
                             */
                             
                            // Remove Search
                            remove_action( 'generate_menu_bar_items', 'generate_do_navigation_search_button' );
                            remove_action( 'generate_inside_navigation', 'generate_navigation_search', 10 );
                            
                            // Remove Primary Menu
                            //remove_action( 'generate_after_header_content', 'generate_add_navigation_float_right', 5 );

							// Remove Mobile Primary Menu
							//remove_action( 'generate_before_navigation', 'generate_do_header_mobile_menu_toggle' );
                            
                            // Remove Secondary Menu
                            remove_action( 'generate_after_header_content', 'generate_do_secondary_navigation_float_right', 7 );
                            
                            /*
                                LIST ACTION HOOK
                                $hook_name = 'generate_after_header_content';
                                global $wp_filter;
                                var_dump( $wp_filter[$hook_name] );
                            */			
                            
                            // Primary Menu				
                            //do_action( 'generate_after_header_content' );
                        ?>
                        
                        
                    </div>
                    
                    <div class="logo-wrapper">
                     <?php
                        // Logo
                        /**
                         * generate_before_header_content hook.
                         *
                         * @since 0.1
                         */

						// Remove Logo from generate_before_header_content hook
						remove_action( 'generate_before_header_content', 'generate_do_site_logo', 5 );                        

						$alternate_logo = get_theme_mod( 'alternate_logo' );

						if ( $transparent_menu == 'yes' && !empty( $alternate_logo ) && substr( $alternate_logo, -14 ) != 'selectlogo.png' )
						{
							// Transparent Menu selected, get logo from alternate logo
							?>
								<div class="site-logo">
									<a href="<?php echo site_url(); ?>" rel="home">
										<img src="<?php echo get_theme_mod( 'alternate_logo' ) ?>" class="header-image is-logo-image" alt="<?php echo get_bloginfo( 'name' ); ?>" >
									</a>
								</div>
							<?php

							 do_action( 'generate_before_header_content' );
						}
						else
						{
							// Default logo, put logo back to generate_before_header_content hook
							add_action( 'generate_before_header_content', 'generate_do_site_logo', 5 );
							do_action( 'generate_before_header_content' );
						}  
        
                        if ( ! generate_is_using_flexbox() ) {
                            // Add our main header items.
                            generate_header_items();
                        }
                    ?>
                    </div>
                    
                    <div class="right-menu">
						<nav id="secondary-navigation">
							<div class="main-nav">
								<ul class="secondary-menu">
									<li id="menu-search-wrapper">
										<a aria-label="Open Search Bar" href="#"><?php _e( 'Search', 'ambientlounge' ); ?></a>		
									</li>
									<li id="menu-myaccount-wrapper">
										<a href="#"><?php _e( 'マイアカウント', 'ambientlounge' ); ?></a>
									</li>
									<li id="menu-cart-wrapper">
										<a href="#"><?php _e( 'お買い物かご', 'ambientlounge' ); ?></a>
									</li>
								</ul>
							</div>
						</nav>

						<?php
							/*
                            // Add Search
                            //add_action( 'new_search_location', 'generate_do_navigation_search_button', 5 );
                            //add_action( 'new_search_location', 'generate_navigation_search', 10 );
                            
                            // Add Secondary Menu
                            //add_action( 'new_secondary_menu_location', 'generate_do_secondary_navigation_float_right' );
                        ?>
                        <div class="header-top-search-wrapper">
                            <?php //do_action( 'new_search_location' ); ?>
                        </div>
                        <?php //do_action( 'new_secondary_menu_location' ); ?>
						<nav id="secondary-navigation" class="secondary-navigation">
							<div class="inside-navigation">
								<div class="main-nav">
									<ul class="secondary-menu">
										<li>
											<a href="#">マイアカウント</a>
										</li>
										<li>
											<a href="#">お買い物かご</a>
										</li>
									</ul>
								</div>
							</div>
						</nav>
							*/
						?>
                    </div> 
                </div>
			</div>
		</header>
    <?php

	/**
	 * generate_after_header hook.
	 *
	 * @since 0.1
	 *
	 * @hooked generate_featured_page_header - 10
	 */
	do_action( 'generate_after_header' );
	?>	

	<div <?php generate_do_attr( 'page' ); ?>>

		<?php
			/* Cart - Woocommerce/cart/ */	
			if ( is_cart() ) 
			{
				$image = get_field( 'image' );
				$title = get_field( 'title' );
				$description = get_field( 'description' );
				$link = get_field( 'link' );

				?>
					<a href="<?php echo $link; ?>" title="<?php echo $title; ?>" class="notice-link">
						<div class="notice-wrapper">
							<div class="notice-container">
								<div class="notice-inner">
									<div class="notice-image" style="background-image:url(<?php echo $image; ?>);">
									</div>
									<div class="notice-content">
										<div class="notice-title">
											<?php echo $title; ?>
										</div>
										<div class="notice-description">
											<?php echo $description; ?>
										</div>
									</div>
									<div class="notice-action">
										<svg width="44" height="45" viewBox="0 0 44 45" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M0 0.0749512H43.9998V44.24H0V0.0749512Z" fill="#F7F9FB"/>
											<path fill-rule="evenodd" clip-rule="evenodd" d="M17.2929 13.4178C17.6834 13.0258 18.3166 13.0258 18.7071 13.4178L27.2071 21.9497C27.5976 22.3416 27.5976 22.9772 27.2071 23.3692L18.7071 31.9011C18.3166 32.2931 17.6834 32.2931 17.2929 31.9011C16.9024 31.5091 16.9024 30.8735 17.2929 30.4815L25.0857 22.6594L17.2929 14.8373C16.9024 14.4453 16.9024 13.8098 17.2929 13.4178Z" fill="#B2BBC4"/>
										</svg>
									</div>
								</div>
							</div>
						</div>
					</a>
				<?php			
			}
		?>

		

		<?php
		/**
		 * generate_inside_site_container hook.
		 *
		 * @since 2.4
		 */
		do_action( 'generate_inside_site_container' );
		?>
		<div <?php generate_do_attr( 'site-content' ); ?>>
			<?php
			/**
			 * generate_inside_container hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_inside_container' );

			