<?php
	/**
	* Lost password reset form.
	*
	* This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-reset-password.php.
	*
	* HOWEVER, on occasion WooCommerce will need to update template files and you
	* (the theme developer) will need to copy the new files to your theme to
	* maintain compatibility. We try to do this as little as possible, but it does
	* happen. When this occurs the version of the template file will be bumped and
	* the readme will list any important changes.
	*
	* @see https://docs.woocommerce.com/document/template-structure/
	* @package WooCommerce\Templates
	* @version 7.0.1
	*/

	defined( 'ABSPATH' ) || exit;

	do_action( 'woocommerce_before_reset_password_form' );
?>

	<header class="entry-header" aria-label="Content">
		<div class="page-heading-wrapper">
			<div class="entry-container">			
				<h1 class="entry-title" itemprop="headline"><?php esc_html_e( 'マイアカウント', 'ambientlounge' ); ?></h1>
			</div>
		</div>
	</header>

	<div class="slim-container">
		<?php
			do_action( 'woocommerce_before_lost_password_form' );
		?>

		<div class="resetpassword-wrapper">
			
			<p><?php echo apply_filters( 'woocommerce_reset_password_message', esc_html__( '以下に新しいパスワードを入力します。', 'woocommerce' ) ); ?></p><?php // @codingStandardsIgnoreLine ?>

			<form method="post" class="woocommerce-ResetPassword lost_reset_password">

				<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
					<label for="password_1"><?php esc_html_e( '新規パスワード', 'ambientlounge' ); ?>&nbsp;<span class="required">*</span></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" placeholder="<?php esc_html_e( '新規パスワードを入力', 'ambientlounge' ); ?>" name="password_1" id="password_1" autocomplete="new-password" />
				</p>
				<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
					<label for="password_2"><?php esc_html_e( '新しいパスワードを再入力', 'ambientlounge' ); ?>&nbsp;<span class="required">*</span></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" placeholder="<?php esc_html_e( '新しいパスワードを再入力', 'ambientlounge' ); ?>" name="password_2" id="password_2" autocomplete="new-password" />
				</p>

				<input type="hidden" name="reset_key" value="<?php echo esc_attr( $args['key'] ); ?>" />
				<input type="hidden" name="reset_login" value="<?php echo esc_attr( $args['login'] ); ?>" />

				<div class="clear"></div>

				<?php do_action( 'woocommerce_resetpassword_form' ); ?>

				<p class="woocommerce-form-row form-row">
					<input type="hidden" name="wc_reset_password" value="true" />
					<button type="submit" class="woocommerce-Button button<?php echo esc_attr( wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '' ); ?>" value="<?php esc_attr_e( '保存', 'woocommerce' ); ?>"><?php esc_html_e( '保存', 'ambientlounge' ); ?></button>
				</p>

				<?php wp_nonce_field( 'reset_password', 'woocommerce-reset-password-nonce' ); ?>

			</form>
		</div>
	</div>

	<div aria-hidden="true" class="wp-block-spacer is-style-spacer188">
	</div>	


<?php
do_action( 'woocommerce_after_reset_password_form' );

