<?php
	/**
	* Checkout Form
	*
	* This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
	*
	* HOWEVER, on occasion WooCommerce will need to update template files and you
	* (the theme developer) will need to copy the new files to your theme to
	* maintain compatibility. We try to do this as little as possible, but it does
	* happen. When this occurs the version of the template file will be bumped and
	* the readme will list any important changes.
	*
	* @see https://docs.woocommerce.com/document/template-structure/
	* @package WooCommerce\Templates
	* @version 3.5.0
	*/

	if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}

	// Remove Coupon Code from header, to be shown in review-order.php
	remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
	// Contains Login/Notice
	do_action( 'woocommerce_before_checkout_form', $checkout );

	// Remove payment from below review order (right sidebar), to be shown below woocommerce_checkout_shipping (left sidebar, bottom)
	remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
	
?>
	<header class="entry-header" aria-label="Content">
		<div class="page-heading-wrapper">
			<div class="entry-container">			
				<h1 class="entry-title" itemprop="headline"><?php esc_html_e( '注文確認', 'ambientlounge' ); ?></h1>
			</div>
		</div>
	</header>

	<?php
		// If checkout registration is disabled and not logged in, the user cannot checkout.
		if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
			echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
			return;
		}
	?>

	<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
		<div class="checkout-wrapper">
			<div class="checkout-content">
				<div class="checkout-registereduser">
					<?php esc_html_e( '登録済みの方はこちら', 'ambientlounge' ); ?> <a href="#" title="<?php esc_html_e( 'ログインはこちらから', 'ambientlounge' ); ?>"><?php esc_html_e( 'ログインはこちらから', 'ambientlounge' ); ?></a>
					<div class="checkout-breadcrumb">
						<?php esc_html_e( '情報', 'ambientlounge' ); ?>
					</div>
				</div>

				<div class="checkout-expresscheckout-wrapper">
					<div class="checkout-expresscheckout-title">
						<?php esc_html_e( 'エクスプレスチェックアウト', 'ambientlounge' ); ?>				
					</div>
					<div class="checkout-expresscheckout">
					</div>
				</div>

				<div class="checkout-or-wrapper">
					<div class="checkout-or-inner">
						<?php esc_html_e( 'または', 'ambientlounge' ); ?>
					</div>
				</div>

				<?php 				
					if ( $checkout->get_checkout_fields() ) 
					{
						do_action( 'woocommerce_checkout_before_customer_details' );

						?>
							<div id="customer_details">
								<?php 
									do_action( 'woocommerce_checkout_billing' ); 
									
									do_action( 'woocommerce_checkout_shipping' );
								?>
							</div>
						<?php

						do_action( 'woocommerce_checkout_after_customer_details' );
					}
				?>		

				<div class="checkout-payment">
					<?php
						// Checkout payment, originall from woocommerce_checkout_order_review hook below
						add_action( 'show_checkout_payment', 'woocommerce_checkout_payment', 20 );
						do_action( 'show_checkout_payment' );
					?>
				</div>		
			</div>

			<div class="checkout-sidebar">
				<div class="checkout-sidebar-inner">
					<h3><?php esc_html_e( 'ご注文', 'woocommerce' ); ?></h3>

					<?php  
						do_action( 'woocommerce_checkout_before_order_review_heading' ); 
						do_action( 'woocommerce_checkout_before_order_review' ); 
					?>

					<div class="cart-return-link">
						<a href="<?php echo wc_get_cart_url(); ?>" class="link-arrow">		
							<?php esc_html_e( '編集', 'ambientlounge' ); ?>
						</a>
					</div>

					<div id="order_review" class="woocommerce-checkout-review-order">
						<?php 						
							// woocommerce_checkout_payment has been removed from this hook, to be used in below woocommerce_checkout_shipping above
							do_action( 'woocommerce_checkout_order_review' ); 					
						?>
					</div>

					<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
				</div>
			</div>
		</div>
	</form>

	<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

	<div aria-hidden="true" class="wp-block-spacer is-style-spacer188">
	</div>

<?php
	/*
	<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

		<?php if ( $checkout->get_checkout_fields() ) : ?>

			<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

			<div class="col2-set" id="customer_details">
				<div class="col-1">
					<?php do_action( 'woocommerce_checkout_billing' ); ?>
				</div>

				<div class="col-2">
					<?php do_action( 'woocommerce_checkout_shipping' ); ?>
				</div>
			</div>

			<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

		<?php endif; ?>
		
		<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
		
		<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>
		
		<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

		<div id="order_review" class="woocommerce-checkout-review-order">
			<?php do_action( 'woocommerce_checkout_order_review' ); ?>
		</div>

		<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

	</form>

	<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
	*/
?>
