<?php
/**
 * The template for displaying the footer.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

	</div>
</div>

<?php
/**
 * generate_before_footer hook.
 *
 * @since 0.1
 */
do_action( 'generate_before_footer' );
?>

<div <?php generate_do_attr( 'footer' ); ?>>
	<?php
	/**
	 * generate_before_footer_content hook.
	 *
	 * @since 0.1
	 */
	do_action( 'generate_before_footer_content' );

	?>
		<div class="footer-breadcrumb-wrapper">
			<div class="container">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<div class="yoast-breadcrumbs">','</div>' );
					}
				?>
			</div>
		</div>
		<div class="footer-contactinfo-wrapper">
			<div class="container">
				<div class="columns">
					<div class="column-equal">
						<div id="contactinfo-emailconsulting" class="footer-contactinfo">
							<div class="footer-contactinfo-icon">
								<svg width="49" height="49" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M24.35 47.7C37.2458 47.7 47.7 37.2458 47.7 24.35C47.7 11.4542 37.2458 1 24.35 1C11.4542 1 1 11.4542 1 24.35C1 37.2458 11.4542 47.7 24.35 47.7Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									<path d="M34.2468 16H13.7169C12.7742 16 12.0078 16.7664 12.0078 17.7091V31.0905C12.0078 32.0332 12.7742 32.7996 13.7169 32.7996H34.2535C35.1963 32.7996 35.9627 32.0332 35.9627 31.0905V17.7159C35.9627 16.7732 35.1963 16.0068 34.2535 16.0068L34.2468 16Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
									<path d="M12 17.4727L22.7973 24.5601C23.5162 25.0213 24.4386 25.0213 25.1575 24.5601L35.9548 17.4727" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
								</svg>
							</div>
							<div class="footer-contactinfo-content">
								<div class="footer-contactinfo-title">
									<?php _e( 'メールで相談', 'ambientlounge' ); ?>
								</div>
								<a href="#" title="<?php _e( 'お問合わせ', 'ambientlounge' ); ?>">
									<div class="std-button">
										<?php _e( 'お問合わせ', 'ambientlounge' ); ?>
									</div>
								</a>
								<div class="footer-contactinfo-detail">
									10:00AM – 05:00PM (<?php _e( '土日祝日を除く', 'ambientlounge' ); ?>)
								</div>
							</div>
						</div>
					</div>
					<div class="column-equal">
						<div id="contactinfo-customerservice" class="footer-contactinfo">
							<div class="footer-contactinfo-icon">
								<svg width="49" height="49" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
									<g clip-path="url(#clip0_3147_140402)">
										<path d="M24.35 47.71C37.25 47.71 47.7 37.25 47.7 24.36C47.7 11.47 37.25 1 24.35 1C11.45 1 1 11.46 1 24.35C1 37.24 11.46 47.7 24.35 47.7V47.71Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M29.5204 11.9004H19.1904C18.3399 11.9004 17.6504 12.5899 17.6504 13.4404V34.7104C17.6504 35.5609 18.3399 36.2504 19.1904 36.2504H29.5204C30.3709 36.2504 31.0604 35.5609 31.0604 34.7104V13.4404C31.0604 12.5899 30.3709 11.9004 29.5204 11.9004Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M31.0604 16.2109H17.6504V32.8309H31.0604V16.2109Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M22.6309 14.2012H26.0809" stroke="white" stroke-width="0.8" stroke-linecap="round" stroke-linejoin="round"/>
									</g>
									<g clip-path="url(#clip1_3147_140402)">
										<path d="M24.35 47.71C37.25 47.71 47.7 37.25 47.7 24.36C47.7 11.47 37.25 1 24.35 1C11.45 1 1 11.46 1 24.35C1 37.24 11.46 47.7 24.35 47.7V47.71Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M29.5204 11.9004H19.1904C18.3399 11.9004 17.6504 12.5899 17.6504 13.4404V34.7104C17.6504 35.5609 18.3399 36.2504 19.1904 36.2504H29.5204C30.3709 36.2504 31.0604 35.5609 31.0604 34.7104V13.4404C31.0604 12.5899 30.3709 11.9004 29.5204 11.9004Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M31.0604 16.2109H17.6504V32.8309H31.0604V16.2109Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M22.6309 14.2012H26.0809" stroke="white" stroke-width="0.8" stroke-linecap="round" stroke-linejoin="round"/>
									</g>
									<defs>
										<clipPath id="clip0_3147_140402">
											<rect width="48.71" height="48.71" fill="white"/>
										</clipPath>
										<clipPath id="clip1_3147_140402">
											<rect width="48.71" height="48.71" fill="white"/>
										</clipPath>
									</defs>
								</svg>
							</div>
							<div class="footer-contactinfo-content">
								<div class="footer-contactinfo-title">
									<?php _e( 'カスタマーサービス', 'ambientlounge' ); ?>
								</div>
								<div class="footer-contactinfo-phone">
									<svg width="39" height="12" viewBox="0 0 39 12" fill="none" xmlns="http://www.w3.org/2000/svg">
										<rect width="37.5" height="10" transform="translate(1 1)" fill="black"/>
										<path d="M38.4996 1L21.2402 1V11H38.4996V1Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M28.7773 7.13867V10.9993H30.9607V7.13867" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M30.9607 4.85493V1H28.7773V4.85493" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M38.4989 10.9993V7.13867H30.9609L38.4989 10.9993Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M21.2402 1V4.85493H28.7782L21.2402 1Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M21.2402 10.9993L28.7782 7.13867H21.2402V10.9993Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M30.9609 4.85493H38.4989V1L30.9609 4.85493Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M18.2593 1L1 1L1 11H18.2593V1Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M9.62896 8.97235C11.1996 8.97235 12.4728 7.6402 12.4728 5.99692C12.4728 4.35363 11.1996 3.02148 9.62896 3.02148C8.05837 3.02148 6.78516 4.35363 6.78516 5.99692C6.78516 7.6402 8.05837 8.97235 9.62896 8.97235Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
									</svg>
									0120-429-332
								</div>
								<div class="footer-contactinfo-detail">
									10:00AM – 05:00PM (<?php _e( '土日祝日を除く', 'ambientlounge' ); ?>)
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-links-wrapper">
			<div class="container">
				<div class="footer-links">
					<a href="#" title="<?php _e( 'ふるさと納税', 'ambientlounge' ); ?>"><?php _e( 'ふるさと納税', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( 'ブランドストーリー', 'ambientlounge' ); ?>"><?php _e( 'ブランドストーリー', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( 'スタイリングブログ', 'ambientlounge' ); ?>"><?php _e( 'スタイリングブログ', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( '取扱店舗', 'ambientlounge' ); ?>"><?php _e( '取扱店舗', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( 'お問合せ', 'ambientlounge' ); ?>"><?php _e( 'お問合せ', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( '法人のお客様 – Professional', 'ambientlounge' ); ?>"><?php _e( '法人のお客様 – Professional', 'ambientlounge' ); ?></a>
				</div>
				<div class="footer-links">
					<a href="#" title="<?php _e( 'ご利用ガイド', 'ambientlounge' ); ?>"><?php _e( 'ご利用ガイド', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( '配送方法･送料について', 'ambientlounge' ); ?>"><?php _e( '配送方法･送料について', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( '返品と交換について', 'ambientlounge' ); ?>"><?php _e( '返品と交換について', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( '安全性について', 'ambientlounge' ); ?>"><?php _e( '安全性について', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( '製品のお手入れ', 'ambientlounge' ); ?>"><?php _e( '製品のお手入れ', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( 'よくある質問', 'ambientlounge' ); ?>"><?php _e( 'よくある質問', 'ambientlounge' ); ?></a>
				</div>
				<div class="footer-links">
					<a href="#" title="<?php _e( '会社概要', 'ambientlounge' ); ?>"><?php _e( '会社概要', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( '利用規約', 'ambientlounge' ); ?>"><?php _e( '利用規約', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( '特定商取引法に基づく表記', 'ambientlounge' ); ?>"><?php _e( '特定商取引法に基づく表記', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( 'プライバシーポリシー', 'ambientlounge' ); ?>"><?php _e( 'プライバシーポリシー', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( 'NEWS', 'ambientlounge' ); ?>"><?php _e( 'NEWS', 'ambientlounge' ); ?></a>
					<a href="#" title="<?php _e( 'RECRUIT', 'ambientlounge' ); ?>"><?php _e( 'RECRUIT', 'ambientlounge' ); ?></a>
				</div>
			</div>			
		</div>
	<?php

	/**
	 * generate_footer hook.
	 *
	 * @since 1.3.42
	 *
	 * @hooked generate_construct_footer_widgets - 5
	 * @hooked generate_construct_footer - 10
	 */
	remove_action( 'generate_footer', 'generate_construct_footer_widgets', 5 );
	

	// Get how many widgets to show.
	$widgets = generate_get_footer_widgets();

	if ( ! empty( $widgets ) && 0 !== $widgets ) :

		// TWI If no footer widgets exist, we don't need to continue.
		if ( ! is_active_sidebar( 'footer-1' ) && ! is_active_sidebar( 'footer-2' ) && ! is_active_sidebar( 'footer-3' ) && ! is_active_sidebar( 'footer-4' ) && ! is_active_sidebar( 'footer-5' ) ) {
			//return;
		}

		// Set up the widget width.
		$widget_width = '';

		if ( 1 === (int) $widgets ) {
			$widget_width = '100';
		}

		if ( 2 === (int) $widgets ) {
			$widget_width = '50';
		}

		if ( 3 === (int) $widgets ) {
			$widget_width = '33';
		}

		if ( 4 === (int) $widgets ) {
			$widget_width = '25';
		}

		if ( 5 === (int) $widgets ) {
			$widget_width = '20';
		}
		
		?>
		<div id="footer-widgets" class="site footer-widgets">
			<div <?php generate_do_attr( 'footer-widgets-container' ); ?>>
				<?php
				/*
				<div class="inside-footer-widgets">
					<?php
						if ( $widgets >= 1 ) {
							generate_do_footer_widget( $widget_width, 1 );
						}
	
						if ( $widgets >= 2 ) {
							generate_do_footer_widget( $widget_width, 2 );
						}						
	
						if ( $widgets >= 3 ) {
							generate_do_footer_widget( $widget_width, 3 );
						}						
	
						if ( $widgets >= 4 ) {
							generate_do_footer_widget( $widget_width, 4 );							
						}
	
						if ( $widgets >= 5 ) {
							generate_do_footer_widget( $widget_width, 5 );
						}

						$widget_classes = sprintf(
							'footer-widget-%s',
							absint( $widget )
						);
					
						if ( ! generate_is_using_flexbox() ) {
							$widget_width = apply_filters( "generate_footer_widget_{$widget}_width", $widget_width );
							$tablet_widget_width = apply_filters( "generate_footer_widget_{$widget}_tablet_width", '50' );
					
							$widget_classes = sprintf(
								'footer-widget-%1$s grid-parent grid-%2$s tablet-grid-%3$s mobile-grid-100',
								absint( $widget ),
								absint( $widget_width ),
								absint( $tablet_widget_width )
							);
						}
						?>
						<div class="<?php echo $widget_classes; // phpcs:ignore ?>">
							<?php dynamic_sidebar( 'footer-' . absint( $widget ) ); ?>
						</div>
						<?php
					?>
				</div>
				*/
				?>

				<?php
					if ( is_active_sidebar( 'payment' ) ) 
					{
						?>
							<div class="payment-sidebar">
								<?php dynamic_sidebar( 'payment' );	?>
							</div>
						<?php
					}

					if ( is_active_sidebar( 'social' ) ) 
					{
						?>
							<div class="social-sidebar">
								<?php dynamic_sidebar( 'social' );	?>
							</div>
						<?php	
					}

					if ( is_active_sidebar( 'copyright' ) ) 
					{
						?>
							<div class="copyright-sidebar">
								<?php dynamic_sidebar( 'copyright' );	?>
							</div>
						<?php	
					}
				?>

			</div>
		</div>

		<?php
	endif;

	/**
	 * generate_after_footer_widgets hook.
	 *
	 * @since 0.1
	 */
	do_action( 'generate_after_footer_widgets' );
	
	do_action( 'generate_footer' );


	/**
	 * generate_after_footer_content hook.
	 *
	 * @since 0.1
	 */
	do_action( 'generate_after_footer_content' );
	?>
</div>

<?php
/**
 * generate_after_footer hook.
 *
 * @since 2.1
 */
do_action( 'generate_after_footer' );

wp_footer();
?>

</body>
</html>
