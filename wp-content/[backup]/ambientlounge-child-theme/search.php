<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

	<div <?php generate_do_attr( 'content' ); ?>>
		<main <?php generate_do_attr( 'main' ); ?>>
			<div class="content-cover no-sticky">
				<div class="entry-content ">  
					<?php
					/**
					 * generate_before_main_content hook.
					 *
					 * @since 0.1
					 */
					do_action( 'generate_before_main_content' ); ?>

					<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<div class="yoast-breadcrumbs">','</div>' );
						}
					?>
					
					<div class="content-title">
						<h1><?php _e( 'Search result for: ', 'generatepress' ); ?><?php the_search_query(); ?></h1>
					</div>

					<div class="content-wrapper rtl">
						<div class="content-left">     
							<div class="sidebar-list">
								<div class="filter-wrapper filter-search">
									<div class="filter-header">
										<h2>Search Filters</h2>
										<div class="filter-icon"></div>
									</div>
									<div class="filter-content">
										<form name='advanced-search' action="<?php echo esc_url( home_url( '/' ) ); ?>" method='get'>
											<fieldset>
												<legend><?php _e("Keyword", "generatepress"); ?></legend>
												<input type='search' name='s' class='search-field' placeholder='<?php _e("Search...", "generatepress"); ?>' autofocus value="<?php echo get_search_query(); ?>" />
											</fieldset>
											<fieldset>
												<legend><?php _e("Content Type", "generatepress"); ?></legend>
												<select name='content-type'>
													<option value='all' selected><?php _e("All Contents", "generatepress"); ?></option>
													<option value='post'><?php _e("Blog Posts", "generatepress"); ?></option>
													<option value='publications'><?php _e("Publications", "generatepress"); ?></option>
													<option value='areas_of_work'><?php _e("Areas of Work", "generatepress"); ?></option>
													<option value='events'><?php _e("Events", "generatepress"); ?></option>
													<option value='faculty'><?php _e("Faculties", "generatepress"); ?></option>
												</select>
												<select name='categories[]' multiple style='width: 100%;'>
													<option value=''></option>
												</select>
												<select name='tags[]' multiple style='width: 100%;'>
													<option value=''></option>
												</select>
											</fieldset>

											<input type='submit' value='Submit'>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="content-right">  
							<div class="post-list-wrapper">						
								<?php
									if ( generate_has_default_loop() ) {
										if ( have_posts() ) :
											/**
											* generate_before_loop hook.
											*
											* @since 3.1.0
											*/
											//do_action( 'generate_before_loop', 'search' );

											while ( have_posts() ) :

												the_post();

												generate_do_template_part( 'search' );

											endwhile;

											/**
											* generate_after_loop hook.
											*
											* @since 2.3
											*/
											do_action( 'generate_after_loop', 'search' );

										else :

											generate_do_template_part( 'none' );

										endif;
									}

									/**
									* generate_after_main_content hook.
									*
									* @since 0.1
									*/
									do_action( 'generate_after_main_content' );
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>

	<?php
	/**
	 * generate_after_primary_content_area hook.
	 *
	 * @since 2.0
	 */
	do_action( 'generate_after_primary_content_area' );

	generate_construct_sidebars();

	get_footer();
