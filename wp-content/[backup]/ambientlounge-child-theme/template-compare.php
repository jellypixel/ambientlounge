<?php
/*
	Template Name: Compare
*/	
?>
<?php
	get_header();

    
?>
    <div class="content-area">
        <header class="entry-header">            
            <div class="page-heading-wrapper">
                <div class="entry-container">
                    <h1 class="entry-title" itemprop="headline"><?php the_title(); ?></h1>
                </div>
            </div>
        </header>

        <div class="entry-content">     
            
            <?php
                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post();
                        the_content();
                    }
                }
            ?>   
            
            <div class="blog-category-wrapper lightgray-scheme <?php //bigger ?>">                
                <div class="blog-category-inner">
                    <?php         
                        global $post;
                        $args = array(
                            'post_type'      => 'page',
                            'posts_per_page' => -1,
                            'post_parent'    => $post->ID,
                            'order'          => 'ASC',
                            'orderby'        => 'menu_order'
                        );


                        $parent = new WP_Query( $args );

                        if ( $parent->have_posts() ) 
                        {
                            $i = 0;
                            while ( $parent->have_posts() ) 
                            {
                                $parent->the_post(); 
                                global $post;
                                ?>  
                                    <div class="blog-category <?php echo ( $i == 0 ) ? "active" : "" ?>" data-target="<?php echo $post->ID; ?>"><?php the_title(); ?></div>                                    
                                <?php
                                $i++;
                            }
                        }
                        wp_reset_postdata(); 
                    ?>                     
                </div>  
            </div>

            <div class="compare-content-wrapper">
                <?php         
                    global $post;
                    $args = array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_parent'    => $post->ID,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order'
                    );

                    $parent = new WP_Query( $args );

                    if ( $parent->have_posts() ) 
                    {
                        while ( $parent->have_posts() ) 
                        {
                            $parent->the_post(); 
                            global $post;
                            
                            ?>
                                <div id="<?php echo $post->ID; ?>" class="compare-content">
                                    <?php the_content(); ?>
                                </div>
                            <?php
                        }
                    }
                    wp_reset_postdata(); 
                ?>  
            </div>            
            
        </div>
    </div>

    <script type="text/javascript">
        var $j = jQuery.noConflict();

        $j(function(){	
            $j( '.blog-category' ).click( function() {
                var targetEl = $j( this ).attr( 'data-target' );

                $j( '#' + targetEl ).fadeIn( 400 ).siblings().hide();


            })
        });
        <?php
            /*
            var $j = jQuery.noConflict();
            var childContentEl = $j( '.compare-content-wrapper' );
            
            $j(function(){			
                // Load more button click
                $j( '.blog-category' ).click( function() {
                    loadChildPage( $j( this ).attr( 'data-target' ) );                
                });	
            });
                            
            function loadChildPage(postID) 
            {
                $j.ajax({	
                    url: "<?php bloginfo('wpurl') ?>/wp-admin/admin-ajax.php",
                    type:'POST',
                    data: "action=get_child_page&page_id="+ postID,
                    success: function(html){	
                        childContentEl.html( html );                    
                    }
                });
                return false;
            }
            */
        ?>
    </script>

<?php
	get_footer();
?>