<?php
	get_header();

    $cat = '';
    if ( isset( $_GET['cat'] ) ) 
    {
        $cat = $_GET['cat'];
    }

    $allCatClass = 'active';
    $categories = get_categories();
    foreach($categories as $category) 
    {
        if ( $cat == $category->term_id ) {
            $allCatClass = '';
        }
    }
?>
    <div class="content-area">
        <header class="entry-header">
            <h1><?php _e( 'Blog', 'ambientlounge' ); ?></h1>
        </header>

        <div class="entry-content">        
            <div class="blog-category-wrapper">
                <form action="" method="get">
                    <div class="blog-category-inner">
                        <div class="blog-category <?php echo $allCatClass; ?>" cat="-1"><?php _e( 'すべて', 'ambientlounge' ); ?></div>
                        <?php                            
                            foreach($categories as $category) 
                            {
                                ?>
                                    <div class="blog-category <?php echo ( $cat == $category->term_id) ? "active" : "" ?>" cat="<?php echo $category->term_id ?>"><?php echo $category->name ?></div>
                                <?php                                
                            }
                        ?>                     
                    </div>                    
                    <input type="hidden" name="cat" id="cat" value="">                        
                </form>
            </div>

            <div class="blog-wrapper">
                <?php		                
                    $args = array(
                        'post_type' => array( 'post' ),
                    );

                    if ( !empty( $cat ) ) {
                        $args['cat'] = $cat;
                    }
                    
                    $query = new WP_Query( $args );
                    
                    if ( $query->have_posts() ) 
                    {
                        while ( $query->have_posts() ) 
                        {
                            $query->the_post();

                            global $post;
							$postThumb = get_the_post_thumbnail_url( $post, 'mobile' );	

                            ?>
                                <div class="post-card">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <div class="post-card-thumb" style="background-image:url(<?php echo $postThumb; ?>);">
                                        </div>
                                    </a>
                                    <div class="post-card-description">
                                        <div class="post-card-title">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <h3><?php the_title(); ?></h3>
                                            </a>
                                        </div>
                                        <div class="post-card-content">
                                            <?php
                                                $excerpt = get_the_excerpt(); 

                                                $limitChar = 180;
                                                $excerpt = mb_substr( $excerpt, 0, $limitChar );

                                                echo $excerpt . '...';
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php                        
                        }
                    } 
                    wp_reset_postdata();			
                ?>
            </div>
        </div>
    </div>

<?php
	get_footer();
?>