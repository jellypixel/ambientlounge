<?php
	/*	 	  
	 * WORDPRESS: Functions
	 * Extend Wordpress built in functions and GeneratePress's functions.
	 */
	 
	/* 
	 * Table of contents:
	 * 1. Constants
	 * 2. Register Style/Script	
	 * 3. Prevent Render Blocking 
	 * 4. GeneratePress - Default Settings 
	 * 5. Theme Setup	  
	 *    a. Dequeue Leaks
	 *	  b. Theme Support
	 *    c. Image Size
	 *    d. Sidebar
	 *	  e. Excerpt
	 * 6. Gutenberg - Register Block Style
	 * 7. WPCF7
	 * 8. ACF Blocks
	 * 9. Custom Post Taxonomy
	 * 10. Template Compare - AJAX load child page
	 * 11. Woocommerce
	 */

	// Constants
		define( 'THEMEDIR', get_template_directory() . '/' );
		define( 'THEMEURI', get_stylesheet_directory_uri() . '/' );
		
	// Register Style/Script
		// Frontend
			add_action( 'wp_enqueue_scripts', 'enqueue_list', 20 );
		
			function enqueue_list() {
				// Style											
					wp_enqueue_style( 'flexslider-css',  THEMEURI . ( $css_path = 'src/vendor/flex/css/flexslider.css' ) );
					//wp_enqueue_style( 'select2-css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css' );
					wp_enqueue_style( 'font-notosansjp', 'https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;700&display=swap' );					
					wp_enqueue_style( 'default-css',  THEMEURI . ( $css_path = '/dist/style.min.css' ), array( 'generate-style' ), get_version( $css_path ) );					
					
				// Script			
					wp_enqueue_script( 'jquery' );					
					wp_enqueue_script( 'flexslider-js', THEMEURI . 'src/vendor/flex/js/jquery.flexslider-min.js',  array( 'jquery' ), null, true );				
					//wp_enqueue_script( 'select2-js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js',  array( 'jquery' ), '4.0.13', true );
					wp_enqueue_script( 'default-js', THEMEURI . 'src/js/script.js',  array( 'jquery' ), null, true );

					wp_add_inline_script('default-js', 'const HC =' . json_encode(array(
						'ajaxurl' => admin_url('admin-ajax.php'),
						'nonce' => wp_create_nonce('ajax-search-nonce'),
					)), 'before');
	
			}		
	
		// Backend
			add_action('admin_enqueue_scripts', 'admin_enqueue_list', 22);
			
			function admin_enqueue_list() {		
				wp_enqueue_style( 'admin-css', THEMEURI . ( $css_path = '/dist/style-admin.min.css' ) );
			}

			add_action( 'enqueue_block_editor_assets', 'block_editor_scripts' );

			function block_editor_scripts() {
				wp_enqueue_script( 'block-editor', THEMEURI . 'src/js/editor.js', array( 'wp-blocks', 'wp-dom' ) );
			}			

		// Replaces cache busting versioning
			function get_version($relative_file_path) {
				return date("ymd-Gis", filemtime( get_stylesheet_directory() . $relative_file_path ));
			}
			
	// Prevent Render Blocking
		add_filter( 'style_loader_tag', 'add_google_font_stylesheet_attributes', 10, 2 );
		
		function add_google_font_stylesheet_attributes( $html, $handle ) {
			if ( 'font-notosansjp' === $handle ) 
			{
				return str_replace( "rel='stylesheet'", "rel='stylesheet' media='print' onload=\"this.media='all'\"", $html );
			}
			
			return $html;
		}
		
	// GeneratePress - Default Settings
		if ( ! function_exists( 'generate_get_defaults' ) ) {
			/**
			 * Set default options
			 *
			 * @since 0.1
			 */
			 
			function generate_get_defaults() {
				
				return apply_filters(
					'generate_option_defaults',
					array(
						'hide_title' => '',
						'hide_tagline' => true,
						'logo' => '',
						'inline_logo_site_branding' => false,
						'retina_logo' => '',
						'logo_width' => '',
						'top_bar_width' => 'full',
						'top_bar_inner_width' => 'contained',
						'top_bar_alignment' => 'right',
						'container_width' => '944',
						'container_alignment' => 'text',
						'header_layout_setting' => 'fluid-header',
						'header_inner_width' => 'contained',
						'nav_alignment_setting' => is_rtl() ? 'right' : 'left',
						'header_alignment_setting' => is_rtl() ? 'right' : 'left',
						'nav_layout_setting' => 'fluid-nav',
						'nav_inner_width' => 'contained',
						'nav_position_setting' => 'nav-float-right',
						'nav_drop_point' => '',
						'nav_dropdown_type' => 'hover',
						'nav_dropdown_direction' => is_rtl() ? 'left' : 'right',
						'nav_search' => 'disable',
						'content_layout_setting' => 'separate-containers',
						'layout_setting' => 'right-sidebar',
						'blog_layout_setting' => 'right-sidebar',
						'single_layout_setting' => 'right-sidebar',
						'post_content' => 'excerpt',
						'footer_layout_setting' => 'fluid-footer',
						'footer_inner_width' => 'contained',
						'footer_widget_setting' => '3',
						'footer_bar_alignment' => 'right',
						'back_to_top' => '',
						'background_color' => 'var(--base-2)',
						'text_color' => 'var(--contrast)',
						'link_color' => 'var(--accent)',
						'link_color_hover' => 'var(--contrast)',
						'link_color_visited' => '',
						'font_awesome_essentials' => true,
						'icons' => 'svg',
						'combine_css' => true,
						'dynamic_css_cache' => true,
						'structure' => 'flexbox',
						'underline_links' => 'always',
						'font_manager' => array(),
						'typography' => array(),
						'google_font_display' => 'auto',
						'use_dynamic_typography' => true,
						'global_colors' => array(
							array(
								'name' => __( 'Black', 'generatepress' ),
								'slug' => 'black',
								'color' => '#000000',
							),	
							array(
								'name' => __( 'Dark', 'generatepress' ),
								'slug' => 'dark',
								'color' => '#181818',
							),
							array(
								'name' => __( 'Secondary Dark', 'generatepress' ),
								'slug' => 'secondarydark',
								'color' => '#888888',
							),
							array(
								'name' => __( 'Secondary', 'generatepress' ),
								'slug' => 'secondary',
								'color' => '#b2bbc4',
							),							
							array(
								'name' => __( 'Hairline', 'generatepress' ),
								'slug' => 'hairline',
								'color' => '#d2d2d7',
							),
							array(
								'name' => __( 'Light Gray', 'generatepress' ),
								'slug' => 'lightgray',
								'color' => '#f5f5f7',
							),
							array(
								'name' => __( 'White', 'generatepress' ),
								'slug' => 'white',
								'color' => '#ffffff',
							),
							array(
								'name' => __( 'Link', 'generatepress' ),
								'slug' => 'link',
								'color' => '#0066cc',
							),
							array(
								'name' => __( 'Pale Blue', 'generatepress' ),
								'slug' => 'paleblue',
								'color' => '#f7f9fb',
							),
							array(
								'name' => __( 'Green', 'generatepress' ),
								'slug' => 'green',
								'color' => '#7fb34d',
							),
							array(
								'name' => __( 'Red', 'generatepress' ),
								'slug' => 'red',
								'color' => '#f56300',
							)							
						)
					)
				);
			}
		}

	// Theme Setup
		add_action( 'after_setup_theme', 'initial_setup', 12 );
	
		function initial_setup()
		{	
			// Dequeue Leaks
				if ( !is_admin() ) {
					add_action( 'wp_enqueue_scripts', 'dequeue_leaks');
					
					function dequeue_leaks() {
						wp_deregister_style('common');
					}				
				}
		
			// Theme Support
				if ( function_exists( 'add_theme_support' ) ) {		
					add_theme_support( 'post-thumbnails', array( 'post' ) );
					add_theme_support( 'editor-styles' );
					add_theme_support( 'align-wide' );	
					
					// Non square custom logo
					add_theme_support( 'custom-logo', array(
						 'flex-height' => true,
						 'flex-width' => true
					) );						
			
					// Editor - Font Size
					add_theme_support(
						'editor-font-sizes',
						array(
							array(
								'name'      => __( 'H6', 'generatepress' ),
								'shortName' => __( 'H6', 'generatepress' ),
								'size'      => 12,
								'slug'      => 'H6',
							),
							array(
								'name'      => __( 'H5', 'generatepress' ),
								'shortName' => __( 'H5', 'generatepress' ),
								'size'      => 14,
								'slug'      => 'H5',
							),
							array(
								'name'      => __( 'Paragraph', 'generatepress' ),
								'shortName' => __( 'paragraph', 'generatepress' ),
								'size'      => 17,
								'slug'      => 'paragraph',
							),
							array(
								'name'      => __( 'H4 ', 'generatepress' ),
								'shortName' => __( 'Normal', 'generatepress' ),
								'size'      => 19,
								'slug'      => 'H4',
							),
							array(
								'name'      => __( 'H3 ', 'generatepress' ),
								'shortName' => __( 'H3', 'generatepress' ),
								'size'      => 24,
								'slug'      => 'H3',
							),
							array(
								'name'      => __( 'H2 ', 'generatepress' ),
								'shortName' => __( 'H2', 'generatepress' ),
								'size'      => 44,
								'slug'      => 'H2',
							),
							array(
								'name'      => __( 'H1 ', 'generatepress' ),
								'shortName' => __( 'H1', 'generatepress' ),
								'size'      => 68,
								'slug'      => 'H1',
							)
						)
					);
				}
		
			// Image Size
				if ( function_exists( 'add_image_size' ) ) {
					add_image_size( 'fullhd', 1920 );
					add_image_size( 'macbook', 1680 );
					add_image_size( 'laptop', 1366 );
					add_image_size( 'ipad', 1024 );
					add_image_size( 'tablet', 768 );
					add_image_size( 'mobile', 576 );	
				}
				
			// Sidebar
				register_sidebar( array(
					'name'          => 'Payment',
					'id'            => 'payment',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );

				register_sidebar( array(
					'name'          => 'Social',
					'id'            => 'social',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );

				register_sidebar( array(
					'name'          => 'Copyright',
					'id'            => 'copyright',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );
				
			// Excerpt
				function custom_excerpt_length( $length ) {
					if ( is_admin() ) {
						return $length;
					}
					return 5;
				}
				add_filter( 'excerpt_length', 'custom_excerpt_length', 99 );
		}	

	// Theme Customizer		
		//add_action( 'customize_register', 'themecustomizer_alternate_logo' );
		
		function themecustomizer_alternate_logo($wp_customize)
		{ 
			$wp_customize->add_setting( 'alternate_logo', array(
				'default' => get_theme_file_uri('img/selectlogo.png'), // Default
				'sanitize_callback' => 'esc_url_raw'
			));
		 
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'alternate_logo_control', array(
				'label' => 'Alternate Logo',
				'priority' => 9,
				'section' => 'title_tagline',
				'settings' => 'alternate_logo',
				'button_labels' => array(
									  'select' => 'Select logo',
									  'remove' => 'Remove',
									  'change' => 'Change logo',
								   )
			)));		 
		}

	// Metabox		
		// PAGE Metabox
		add_action( 'add_meta_boxes', 'add_page_metabox' );
		add_action( 'save_post', 'page_metabox_save' );	
		
		function add_page_metabox() 
		{		
			add_meta_box(
				'page_metabox', 
				'Page Options',
				'page_metabox_html', 
				'page'
			);		
		}	
		
		function page_metabox_html( $post ) 
		{
			$show_page_title = get_post_meta( $post->ID, '_show_page_title_meta_key', true );
			/*$transparent_menu = get_post_meta( $post->ID, '_transparent_menu_meta_key', true );
			
			if ( empty( $transparent_menu ) ) {
				$transparent_menu = 'no';	
			}*/
			?>
	            <div class="metabox-row">
                    <label for="show_page_title_field">Show Page Title</label>
                    <select name="show_page_title_field" id="show_page_title_field" class="postbox">
                        <option value="yes" <?php selected( $show_page_title, 'yes' ); ?>>Yes</option>
                        <option value="no" <?php selected( $show_page_title, 'no' ); ?>>No</option>
                    </select>
                </div>				
                
				<?php
				/*
                <div class="metabox-row">
                    <label for="transparent_menu_field">Transparent Header</label>
                    <select name="transparent_menu_field" id="transparent_menu_field" class="postbox">
                        <option value="yes" <?php selected( $transparent_menu, 'yes' ); ?>>Yes</option>
                        <option value="no" <?php selected( $transparent_menu, 'no' ); ?>>No</option>
                    </select>
                    <div class="metabox-description">
	                    Enabling this will transform header in this page into transparent header without background color and overlayed in front of content. Best paired with image/cover as the first block.<br>
                        Alternate logo will be used if you upload it in theme customizer (Appearance - Customize - Site identity).                        
                    </div>
                </div>
				*/
				?>
			<?php
		}		
		
		function page_metabox_save( $post_id ) 
		{
			if ( array_key_exists( 'show_page_title_field', $_POST ) ) {
				update_post_meta(
					$post_id,
					'_show_page_title_meta_key',
					$_POST['show_page_title_field']
				);
			}
			
			/*if ( array_key_exists( 'transparent_menu_field', $_POST ) ) {
				update_post_meta(
					$post_id,
					'_transparent_menu_meta_key',
					$_POST['transparent_menu_field']
				);
			}*/
		}

	// Gutenberg - Register Block Style
		// Block - Columns
		register_block_style(
			'core/columns',
			array(
				'name'         => 'slim',
				'label'        => 'Slim',
				'style_handle' => 'slim-style',
			)
		);

		// Block - Column
		register_block_style(
			'core/column',
			array(
				'name'         => 'card',
				'label'        => 'Card',
				'style_handle' => 'card-style',
			)
		);

		register_block_style(
			'core/column',
			array(
				'name'         => 'note',
				'label'        => 'Note',
				'style_handle' => 'note-style',
			)
		);

		// Block - Image
		register_block_style(
			'core/image',
			array(
				'name'         => 'square-border',
				'label'        => 'Square Border',
				'style_handle' => 'square-border-style',
			)
		);

		// Block - Spacer
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer188',
				'label'        => 'Spacer 188px',
				'style_handle' => 'spacer188-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer112',
				'label'        => 'Spacer 112px',
				'style_handle' => 'spacer112-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer80',
				'label'        => 'Spacer 80px',
				'style_handle' => 'spacer80-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer64',
				'label'        => 'Spacer 64px',
				'style_handle' => 'spacer64-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer40',
				'label'        => 'Spacer 40px',
				'style_handle' => 'spacer40-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer32',
				'label'        => 'Spacer 32px',
				'style_handle' => 'spacer32-style',
			)
		);

		// Block - Button
		register_block_style(
			'core/button',
			array(
				'name'         => 'blue',
				'label'        => 'Blue',
				'style_handle' => 'blue-style',
				'is_default'   => true
			)
		);
		
		register_block_style(
			'core/button',
			array(
				'name'         => 'black',
				'label'        => 'Black',
				'style_handle' => 'black-style',
			)
		);

		register_block_style(
			'core/button',
			array(
				'name'         => 'white',
				'label'        => 'White',
				'style_handle' => 'white-style',
			)
		);

		register_block_style(
			'core/button',
			array(
				'name'         => 'gray',
				'label'        => 'Gray',
				'style_handle' => 'gray-style',
			)
		);

		// Block - Gallery
		register_block_style(
			'core/gallery',
			array(
				'name'         => 'default',
				'label'        => 'Default',
				'style_handle' => 'default-style',
				'is_default'   => true
			)
		);

		register_block_style(
			'core/gallery',
			array(
				'name'         => 'vertical',
				'label'        => 'Vertical',
				'style_handle' => 'vertical-style'
			)
		);

	// WPCF7
		// Validate Email
		add_filter( 'wpcf7_validate_email*', 'custom_email_confirmation_validation_filter', 20, 2 );

		function custom_email_confirmation_validation_filter( $result, $tag ) 
		{
			if ( 'email-address-confirmation' == $tag->name ) {
				$your_email = isset( $_POST['email-address'] ) ? trim( $_POST['email-address'] ) : '';
				$your_email_confirm = isset( $_POST['email-address-confirmation'] ) ? trim( $_POST['email-address-confirmation'] ) : '';
		
				if ( $your_email != $your_email_confirm ) {
					$result->invalidate( $tag, "Email address doesn't match." );
				}
			}

			return $result;
		}	

	// ACF BLOCKS
		// Ambient Lounge Category Block
			add_filter( 'block_categories', 'ambientlounge_block_categories' );

			function ambientlounge_block_categories( $categories ) {
				$category_slugs = wp_list_pluck( $categories, 'slug' );
				return in_array( 'ambientlounge', $category_slugs, true ) ? $categories : array_merge(
					$categories,
					array(
						array(
							'slug'  => 'ambientlounge',
							'title' => __( 'Ambient Lounge', 'ambientlounge' ),
							'icon'  => null,
						),
					)
				);
			}

		// Blocks
			add_action('acf/init', 'acf_init_block_types');

			function acf_init_block_types() 
			{	
				if( function_exists('acf_register_block_type') ) 
				{
					// Accordion Group
					acf_register_block_type(array(
						'name'              => 'accordion-group',
						'title'             => 'Accordion',
						'description'       => 'Create accordion.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'list-view',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template' => 'inc/blocks/accordion-group.php',
					));

					// Accordion Item
					acf_register_block_type(array(
						'name'              => 'accordion-item',
						'title'             => 'Accordion Item',
						'description'       => 'Create accordion item.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'align-center',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template' => 'inc/blocks/accordion-item.php',
					));

					// Highlight Product - SCRAP
					acf_register_block_type(array(
						'name'              => 'highlight-product',
						'title'             => 'Highlight Product',
						'description'       => 'Show and highlight a product.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'megaphone',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template' => 'inc/blocks/highlight-product.php',
					));

					// Product Card Group
					acf_register_block_type(array(
						'name'              => 'product-card-group',
						'title'             => 'Product Card Group',
						'description'       => 'Show single product.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'columns',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/product-card-group.php'
					));

					// Product Card
					acf_register_block_type(array(
						'name'              => 'product-card',
						'title'             => 'Product Card',
						'description'       => 'Show single product.',
						'category'          => 'widgets',
						'mode'              => 'preview',
						'icon'				=> 'star-filled',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/product-card.php'
					));

					// Product Card - Upsell
					acf_register_block_type(array(
						'name'              => 'product-card-upsell',
						'title'             => 'Product Card Upsell',
						'description'       => 'Show single product - upsell version.',
						'category'          => 'widgets',
						'mode'              => 'preview',
						'icon'				=> 'star-empty',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/product-card-upsell.php'
					));
				}
			}
	
	// Custom Post Taxonomy
		// CPT - Retail		
			add_action('init', 'register_cpt_retail');
			
			function register_cpt_retail() {
				register_post_type(
					'retail',
					array(
						'labels' => array(
							'name' 			=> _x( 'Retails', 'pepperit' ),
							'singular_name' => _x( 'Retail', 'pepperit' ),
							'menu_name' 	=> __( 'Retails', 'pepperit' ),					
							'add_new'		=> __( 'Add New Retail', 'pepperit' ),
							'add_new_item'	=> __( 'Add New Retail', 'pepperit' ),
							'edit_item'		=> __( 'Edit Retail', 'pepperit' ),					
							'search_items'	=> __( 'Search Retails', 'pepperit' ),
							'not_found'		=> __( 'No Retail found', 'pepperit' ),
							'not_found_in_trash' => __( 'No Retail found in Trash', 'pepperit' ),					
						),
						'public'			=> true,
						'has_archive' 		=> false,
						'rewrite' 			=> array( 
							'slug' 			=> 'retail',
							'with_front' 	=> false
						),
						'show_in_rest'		=> true,
						'supports' 			=> array( 
												'title', 
												'editor', 										
												'thumbnail',
												'revisions'
											),
						'menu_position' 	=> 5,
						'menu_icon' 		=> 'dashicons-location',
						'can_export' 		=> true,
					)
				);
				
				// Add day archive (and pagination)
				add_rewrite_rule("retail/([0-9]{4})/([0-9]{2})/([0-9]{2})/page/?([0-9]{1,})/?",'index.php?post_type=retail&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]','top');
				add_rewrite_rule("retail/([0-9]{4})/([0-9]{2})/([0-9]{2})/?",'index.php?post_type=retail&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]','top');
				
				// Add month archive (and pagination)
				add_rewrite_rule("retail/([0-9]{4})/([0-9]{2})/page/?([0-9]{1,})/?",'index.php?post_type=retail&year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]','top');
				add_rewrite_rule("retail/([0-9]{4})/([0-9]{2})/?",'index.php?post_type=retail&year=$matches[1]&monthnum=$matches[2]','top');
				
				// Add year archive (and pagination)
				add_rewrite_rule("retail/([0-9]{4})/page/?([0-9]{1,})/?",'index.php?post_type=retail&year=$matches[1]&paged=$matches[2]','top');
				add_rewrite_rule("retail/([0-9]{4})/?",'index.php?post_type=retail&year=$matches[1]','top');
				
			}	
			
			// CPT Retail - Columns List
			add_filter( 'manage_retail_posts_columns', 'cpt_retail_columns' );
			
			function cpt_retail_columns( $retailColumns )
			{
				$retailColumns = array(
					'cb' => '<input type="checkbox">',
					//'article_featured_image' => __( 'Featured Image', 'pepperit' ),
					'title' => __( 'Title', 'pepperit' ),			
					'author' => __( 'Author', 'pepperit' ),
					'comments' => '<span class="vers"><div title="Comments" class="comment-grey-bubble"></div></span>',
					'date' => __( 'Date', 'pepperit' ) 
				);
				
				return $retailColumns;
			}	
			
			// CPT Retail - Custom Columns
			add_action( 'manage_posts_custom_column', 'cpt_retail_custom_columns' );
			
			function cpt_retail_custom_columns( $retailColumns )
			{
				global $post;
			
				switch ( $retailColumns )
				{
					case 'retail_featured_image':			
						if ( has_post_thumbnail() )
						{
							the_post_thumbnail( 'medium' );
						}				
						break;
				}		
			} 
			
			// CPT Retail - Category
			add_action( 'init', 'cpt_retail_category', 0 );
		
			function cpt_retail_category() {	 
				$labels = array(
					'name' => _x( 'Retail Categories', 'pepperit' ),
					'singular_name' => _x( 'Category', 'pepperit' ),
					'search_items' =>  __( 'Search Categories' ),
					'all_items' => __( 'All Categories' ),
					'parent_item' => __( 'Parent Category' ),
					'parent_item_colon' => __( 'Parent Category:' ),
					'edit_item' => __( 'Edit Category' ), 
					'update_item' => __( 'Update Category' ),
					'add_new_item' => __( 'Add New Category' ),
					'new_item_name' => __( 'New Category Name' ),
					'menu_name' => __( 'Categories' ),
				); 	
				
				register_taxonomy(
					'retail_category',
					array('retail'), 
					array(
						'hierarchical' => true,
						'labels' => $labels,
						'show_ui' => true,
						'show_admin_column' => true,
						'show_in_rest'      => true,
						'query_var' => true,
						'rewrite' => array( 'slug' => 'retail-category' ),
					)
				);
			}
			
			// CPT Retail - Location
			add_action( 'init', 'cpt_retail_location', 0 );
		
			function cpt_retail_location() {	 
				$labels = array(
					'name' => _x( 'Retail Locations', 'pepperit' ),
					'singular_name' => _x( 'Location', 'pepperit' ),
					'search_items' =>  __( 'Search Locations' ),
					'all_items' => __( 'All Locations' ),
					'parent_item' => __( 'Parent Location' ),
					'parent_item_colon' => __( 'Parent Location:' ),
					'edit_item' => __( 'Edit Location' ), 
					'update_item' => __( 'Update Location' ),
					'add_new_item' => __( 'Add New Location' ),
					'new_item_name' => __( 'New Location Name' ),
					'menu_name' => __( 'Locations' ),
				); 	
				
				register_taxonomy(
					'retail_location',
					array('retail'), 
					array(
						'hierarchical' => true,
						'labels' => $labels,
						'show_ui' => true,
						'show_admin_column' => true,
						'show_in_rest'      => true,
						'query_var' => true,
						'rewrite' => array( 'slug' => 'retail-location' ),
					)
				);
			}

	// Template Compare - AJAX load child page
		add_action('wp_ajax_get_child_page', 'load_child_page');           // for logged in user
		add_action('wp_ajax_nopriv_get_child_page', 'load_child_page');    // if user not logged in

		function load_child_page()
		{	
			$page_id = $_POST[ 'page_id' ];

			if ( !empty( $page_id ) )
			{
				$args = array(
					'page_id' => $page_id
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						$query->the_post();
						
						the_content();
					}
				} 
				wp_reset_postdata();	
			}		

			exit;
		}

	// Woocommerce
		// Custom Cart is empty message
			remove_action( 'woocommerce_cart_is_empty', 'wc_empty_cart_message', 10 );
			add_action( 'woocommerce_cart_is_empty', 'custom_empty_cart_message', 10 );

			function custom_empty_cart_message() {
				$html  = '<div class="cart-empty">';
				$html .= wp_kses_post( apply_filters( 'wc_empty_cart_message', __( 'Your cart is currently empty.', 'ambientlounge' ) ) );
				echo $html . '</div>';
			}

		// Change Labels in dashboard
			add_filter ( 'woocommerce_account_menu_items', 'dashboard_custom_label' );

			function dashboard_custom_label() {
				$myorder = array(									
					'dashboard'          => __( 'ダッシュボード', 'woocommerce' ),
					'orders'             => __( '注文', 'woocommerce' ),
					'edit-address'       => __( '住所', 'woocommerce' ),
					'payment-methods'    => __( '決済方法', 'woocommerce' ),
					'edit-account'       => __( 'アカウント詳細', 'woocommerce' ),
					'customer-logout'    => __( 'ログアウト', 'woocommerce' ),
				);

				return $myorder;
			}

		// Change Address labels
			add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );

			function wc_billing_field_strings( $translated_text, $text, $domain ) {
				switch ( $translated_text ) 
				{
					case 'Billing address' :
						$translated_text = __( '請求先住所', 'ambientlounge' );
						break;

					case 'Shipping address' :
						$translated_text = __( 'お届け先住所', 'ambientlounge' );
						break;
				}
				return $translated_text;
			}















add_filter('acf/load_field/name=variation_select', 'acf_load_variation');

function acf_load_variation( $field ) {
    
    // reset choices
    $field['choices'] = array();    
    
    // get the textarea value from options page without any formatting
    $product_postobject = get_field('product', 1290, false);

	//$id = $product_postobject->ID;
	
	//$product = wc_get_product( $id );

	$choices = array( 'a', 'b', 'c' );
	foreach( $choices as $choice ) {
			
		$field['choices'][ $choice ] = 'a';
		
	}

	if ( is_a( $product, 'WC_Product' ) ) {

		
		if ( $product->is_type( 'variable' ) ) {
			$variations = $product->get_available_variations();    
			
			

			/*foreach( $variations as $variation ) 
			{
				//$field['choices'][ $variation['variation_id'] ] = $variation['variation_id'];
				$field['choices'][ 'a' ] = 'b';
			}*/
		}
	}

	//$variations = $product->get_available_variations();  

	/*if ( $product->is_type( 'variable' ) ) 
	{
		// Product has variation
		$variations = $product->get_available_variations();    

		foreach( $variations as $variation ) 
		{
			//$field['choices'][ $variation['variation_id'] ] = $variation['variation_id'];
			$field['choices'][ 'a' ] = 'b';
		}

	}*/

	/*$choices = array( 'a', 'b', 'c' );
	foreach( $choices as $choice ) {
            
		$field['choices'][ $choice ] = $choice;
		
	}*/

    
    // explode the value so that each line is a new array piece
    /*$choices = explode("\n", $choices);

    
    // remove any unwanted white space
    $choices = array_map('trim', $choices);*/

    
    // loop through array and add to field 'choices'
    /*if( is_array($choices) ) {
        
        foreach( $choices as $choice ) {
            
            $field['choices'][ $choice ] = $choice;
            
        }
        
    }*/
    

    // return the field
    return $field;
    
}



			
?>
