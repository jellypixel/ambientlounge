<?php
	get_header();
	
	//$select_category = isset( $_GET['cat'] ) ? $_GET['cat'] : ''; 
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;	

	$term = get_queried_object(); 
	
	$args = array(
		'post_type' => 'post',
		'category__in' => $term->term_id
	);
	
	$query = new WP_Query( $args );		
	$postCount = $query->found_posts;
?>

	<div class="content-cover no-sticky">
        <div class="entry-content">  
            <div class="content-wrapper">
            
                <div class="content-left">         
					<div class="entry-title archive-title	">
						<h1>News</h1>
						<h4>Category: <?php echo get_the_archive_title(); ?></h4>
					</div>

                    <div class="post-list-wrapper">
                        <div class="post-list">
                            <?php
                                $args = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => 10,
                                    'paged' => $paged,
									'category__in' => $term->term_id
                                    //'post__not_in' => get_option( 'sticky_posts' ),
                                    //'offset' => 4
                                );
                                
								$query = new WP_Query( $args );								 

								if ( $query->have_posts() ) 
                                {
                                    while ( $query->have_posts() ) 
                                    {
                                        $query->the_post();
                                        global $post;
            
                                        $postThumb = get_the_post_thumbnail_url( $post, 'post-thumb' );
                                        $postThumbClass = '';
                                        
                                        if ( empty( $postThumb ) ) {
                                            $postThumbClass = 'no-post-thumb';	
                                        }
            
                                        ?>
                                            <div class="post-box">
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                    <div class="post-thumb <?php echo $postThumbClass; ?>" style="background-image:url(<?php echo $postThumb; ?>);">
                                                    </div>
                                                </a>
                                                <div class="post-content">
                                                    <div class="post-title">
                                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                            <h4><?php the_title(); ?></h4>
                                                        </a>
                                                    </div>
                                                    <div class="post-date">
                                                        <?php
                                                            $dateLink = get_month_link( get_the_time('Y'), get_the_time('n') );																															
                                                        ?>
                                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark">
                                                            <time class="entry-date" datetime="<?php echo esc_attr(get_the_date('Y-m-j')); ?>">
                                                                <?php echo esc_html( get_the_date( 'M j, Y' ) ); ?>
                                                            </time>
                                                        </a>
                                                    </div>                                        
                                                    <div class="post-excerpt">
                                                        <?php the_excerpt(); ?>
                                                    </div>										
                                                </div>								
                                            </div>
                                        <?php
                                    }
                                } 
                                
                                ?>
                                    <div class="paging-wrapper">
                                        <?php	
                                            

                                            echo paginate_links( array(
                                                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                                'total'        => ceil( $postCount / 10 ),//$query->max_num_pages,
                                                'current'      => max( 1, get_query_var( 'paged' ) ),
                                                'format'       => '?paged=%#%',
                                                'show_all'     => false,
                                                'type'         => 'plain',
                                                'end_size'     => 2,
                                                'mid_size'     => 1,
                                                'prev_next'    => true,
                                                'prev_text'    => sprintf( '<i></i> %1$s', __( 'Prev', 'harvard' ) ),
                                                'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'harvard' ) ),
                                                'add_args'     => false,
                                                'add_fragment' => '',
                                            ) );
                                        ?>
                                    </div>
                                <?php
                                            
                                wp_reset_postdata();					
                            ?>
                        </div>        	
                    </div>
            
                </div>
                
                <div class="content-right">
                </div>
                
            </div>
    
        </div>
    </div>

<?php
	get_footer();
?>