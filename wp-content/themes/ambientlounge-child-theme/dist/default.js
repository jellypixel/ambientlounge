var $j = jQuery.noConflict();

var vpHeight = $j( window ).height();
var headerHeight = 0;
var headerEl;
var footerTop = $j( '#footer-widgets' ).position().top;
var flexslider = { vars:{} };

$j(function(){	
	// Search
		$j( '.search-field' ).attr( 'placeholder', 'Search...' );

	// Megamenu 
		$j( '.submenu-title' ).click( function(){
			$j( this ).toggleClass( 'active' ).siblings().removeClass( 'active' );
		})		

	// Mobile Menu
		// Burger bar on click
		alert('x');
		$j( '.burger-wrapper' ).click( function() {
			$j( this ).toggleClass( 'active' );
		});

		var nav = $j( '.menu-wrapper' );
		var headerWrapper = $j( '.header-wrapper' );
		var slidingOverlay = $j( '.slidingmenu-overlay' );
	
		// Burger bar on click
		$j( '.nav-toggler' ).click( function() {
			// Sliding Menu
			if ( nav.hasClass( 'slidingmenu' ) )
			{
				// Close Sliding Menu
				if ( headerWrapper.hasClass( 'open' ) )
				{
					nav.animate({ 'left' : '-220px' }, function() {
						nav.hide();	
						slidingOverlay.fadeOut(400);
					});
				}
				// Open Sliding Menu
				else
				{
					nav.show();
					nav.animate({ 'left' : '0' }, 400);
					slidingOverlay.fadeIn(400);
				}
			}
			// Dropdown Menu
			else
			{	
				nav.slideToggle( 400 );
			}
			
			headerWrapper.toggleClass( 'open' );
		});
		
		// Close Sliding Menu on Overlay Click
		slidingOverlay.click( function() {
			nav.animate({ 'left' : '-220px' }, function() {
				nav.hide();	
				slidingOverlay.fadeOut(400);
				
				headerWrapper.toggleClass( 'open' );
			});
		});
		
	// Sticky
		headerEl = $j( 'header' );
		if( headerEl.length && headerEl.hasClass( 'stickyheader' ) ) 
		{
			var topBar = $j( '.topbar-wrapper' ).outerHeight();			
			var headerBar = $j( '.header-wrapper' ).outerHeight();
			topBar = topBar ? topBar : 0;
			headerBar = headerBar ? headerBar : 0;
			headerHeight = topBar + headerBar;
			
			if( $j( '.stickyheader:not(.transparentheader)' ).length ) {			
				$j( 'body' ).css({ 'padding-top' : headerHeight });
			}
		}
		
	// Search
		$j( 'body' ).on( 'click', '.search-wrapper svg', function() {
			headerEl.toggleClass( 'searchopen' );
			
			$j( '.search-input' ).focus();
		});
		
	// Gutenberg Block Image - Fit Container
		$j( '.is-style-fit-container' ).parent( '.wp-block-column' ).css({ 'position' : 'relative' });
	
	// To Top	
		$j( '#toTop' ).click( function() {
			$j( 'html, body' ).animate({
				scrollTop: 0
			}, 400);
		});
		
	// Category and Year Filter
		$j( '#cat' ).change( function(e) {			
			e.preventDefault();
			//$j( this ).parent( 'form' ).submit();			
			window.location.href = $j( this ).val();	
		});
		
	// Areas of Work Carousel
		$j('.carousel-wrapper').flexslider({
			animation: "slide",
			animationLoop: false,
			itemWidth: 210,
			itemMargin: 50,
			minItems: getGridSize(), 
			maxItems: getGridSize(),
			prevText: '',
			nextText: '',
			controlNav: false,
			directionNav: true
		});
		
	// Filter List - Convert from Gutenberg
		if ( $j( '.faculty-sidebar' ).length ) {
			var thisEl = $j( '.faculty-sidebar' );	
			thisEl.addClass( 'filter-wrapper' );
			
			thisEl.children().not( 'h4' ).wrapAll( '<div class="filter-content"></div>' );
			//thisEl.children( 'h4' ).wrap( '<div class="filter-header"></div>' );
			//thisEl.children( '.filter-header' ).append( '<div class="filter-icon"></div>' );
			thisEl.prepend( '<div class="filter-header"><h2>' + thisEl.children( 'h4' ).html() +'</h2><div class="filter-icon"></div></div>' );
			thisEl.children( 'h4' ).remove();
		}
		
	// Filter List - General
		$j( 'body' ).on( 'click', '.filter-icon', function() {
			var filterWrapper = $j( this ).parents( '.filter-wrapper' );
			var filterContent = $j( '.filter-content' );
			
			if ( filterWrapper.hasClass( 'open' ) ) {
				filterContent.slideUp(200, function() {
					filterWrapper.removeClass( 'open' );	
				});				
			}
			else {
				filterContent.slideDown(400, function() {
					filterWrapper.addClass( 'open' );	
				});	
			}
		});
		
	//
	/*var searchItem = $j( '.search-item > a ' );
	var searchContent = searchItem.html();
	var searchAria = searchItem.attr( 'aria-label' );
	searchItem.replaceWith( '<div aria-label="' + searchAria + '">' + searchContent + '</div>' );*/
	
	// WAVE Fix
	$j( '.wp-block-cover' ).each( function() {
		var hasBackgroundColorEl = $j( this ).find( '.has-background-color' );
		var hasForegroundColorEl = $j( this ).find( '.has-foreground-color' );
		
		if ( hasBackgroundColorEl.length ) {
			$j( this ).css({ 'background-color' : '#000' });
		}
		else if ( hasForegroundColorEl.length ) {
			$j( this ).css({ 'background-color' : '#fff' });
		}
	});
	
	$j( '.flex-prev' ).attr({ 'aria-label' : 'Previous Slides' });
	$j( '.flex-next' ).attr({ 'aria-label' : 'Next Slides' });
	
	$j( '.header-image' ).attr({ 'alt' : 'Harvard Law School Logo' });
	
	/*$j( '.search-field' ).attr({ 'id' : 's' });
	$j( '.navigation-search' ).prepend( '<label for="s" style="display:none;">Search</label>' );*/
	
	$j( '.screen-reader-text' ).removeAttr( 'title' );
	
	$j( '.gutentor-post-module .posted-on a' ).each( function() {
		var postedOn = $j( this ).parent( '.posted-on' );
		var dateEl = $j( this ).html();
		
		$j( this ).remove();
		postedOn.html( dateEl );
		
	});
	
	$j( '.post-list-wrapper .post-list .post-box .post-content .post-excerpt a, .blog-stage-wrapper .post-sticky .post-sticky-row .post-sticky-rest .post-sticky-rest-box .post-sticky-rest-content .post-sticky-rest-excerpt a' ).remove();
});

function getGridSize() {
	return ( window.innerWidth < 576 ) ? 1 :
		   ( window.innerWidth < 992 ) ? 2 : 3;
}

$j( window ).resize( function() {	
	// Areas of Work Carousel
		var gridSize = getGridSize();		
 
		flexslider.vars.minItems = gridSize;
		flexslider.vars.maxItems = gridSize;
});

$j( window ).scroll( function() {
	// Scroll to top	
		if ( $j( window ).scrollTop() > vpHeight ) {
			$j( '#toTop' ).fadeIn(400);
		}
		else {
			$j( '#toTop' ).fadeOut(400);	
		}
	
	// Sticky Header
		if( $j( '.stickyheader' ).length ) 
		{
			if ( $j( window ).scrollTop() > headerHeight ) {
				headerEl.addClass( 'onScroll' );
			}
			else {
				headerEl.removeClass( 'onScroll' );
			}
		}	
});