<?php
/**
 * The template for displaying the header.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php generate_do_microdata( 'body' ); ?>>
	<?php
	/**
	 * wp_body_open hook.
	 *
	 * @since 2.3
	 */
	do_action( 'wp_body_open' ); // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedHooknameFound -- core WP hook.

	/**
	 * generate_before_header hook.
	 *
	 * @since 0.1
	 *
	 * @hooked generate_do_skip_to_content_link - 2
	 * @hooked generate_top_bar - 5
	 * @hooked generate_add_navigation_before_header - 5
	 */
	do_action( 'generate_before_header' );	
	
	function before_logo() {
		//return 'a';	
	}
	add_action( 'generate_before_logo_hook', 'before_logo' );

	/**
	 * generate_header hook.
	 *
	 * @since 1.3.42
	 *
	 * @hooked generate_construct_header - 10
	 */
	//do_action( 'generate_header' );
	$headerClass = '';
	$transparent_menu = get_post_meta( $post->ID, '_transparent_menu_meta_key', true );
	
	if ( $transparent_menu == 'yes' )
	{
		$headerClass .= 'transparentheader';
	}
	
	?>
    	<header <?php generate_do_attr( 'header', array( 'class' => $headerClass . ' ' ) ); ?>>
			<div <?php generate_do_attr( 'inside-header' ); ?>>
            	<div class="header-top">
                	<div class="left-menu">
						<?php
                            /**
                             * generate_after_header_content hook.
                             *
                             * @since 0.1
                             *
                             * @hooked generate_add_navigation_float_right - 5
                             */
                             
                            // Remove Search
                            remove_action( 'generate_menu_bar_items', 'generate_do_navigation_search_button' );
                            remove_action( 'generate_inside_navigation', 'generate_navigation_search', 10 );
                            
                            // Remove Primary Menu
                            //remove_action( 'generate_after_header_content', 'generate_add_navigation_float_right', 5 );

							// Remove Mobile Primary Menu
							//remove_action( 'generate_before_navigation', 'generate_do_header_mobile_menu_toggle' );
                            
                            // Remove Secondary Menu
                            remove_action( 'generate_after_header_content', 'generate_do_secondary_navigation_float_right', 7 );
                            
                            /*
                                LIST ACTION HOOK
                                $hook_name = 'generate_after_header_content';
                                global $wp_filter;
                                var_dump( $wp_filter[$hook_name] );
                            */			
                            
                            // Primary Menu				
                            do_action( 'generate_after_header_content' );
                        ?>
                        
                        
                    </div>
                    
                    <div class="logo-wrapper">
                     <?php
                        // Logo
                        /**
                         * generate_before_header_content hook.
                         *
                         * @since 0.1
                         */

						// Remove Logo from generate_before_header_content hook
						remove_action( 'generate_before_header_content', 'generate_do_site_logo', 5 );                        

						$alternate_logo = get_theme_mod( 'alternate_logo' );

						if ( $transparent_menu == 'yes' && !empty( $alternate_logo ) && substr( $alternate_logo, -14 ) != 'selectlogo.png' )
						{
							// Transparent Menu selected, get logo from alternate logo
							?>
								<div class="site-logo">
									<a href="<?php echo site_url(); ?>" rel="home">
										<img src="<?php echo get_theme_mod( 'alternate_logo' ) ?>" class="header-image is-logo-image" alt="<?php echo get_bloginfo( 'name' ); ?>" >
									</a>
								</div>
							<?php

							 do_action( 'generate_before_header_content' );
						}
						else
						{
							// Default logo, put logo back to generate_before_header_content hook
							add_action( 'generate_before_header_content', 'generate_do_site_logo', 5 );
							do_action( 'generate_before_header_content' );
						}  
        
                        if ( ! generate_is_using_flexbox() ) {
                            // Add our main header items.
                            generate_header_items();
                        }
                    ?>
                    </div>
                    
                    <div class="right-menu">
						<?php
                            // Add Search
                            add_action( 'new_search_location', 'generate_do_navigation_search_button', 5 );
                            add_action( 'new_search_location', 'generate_navigation_search', 10 );
                            
                            // Add Secondary Menu
                            add_action( 'new_secondary_menu_location', 'generate_do_secondary_navigation_float_right' );
                        ?>
                        <div class="header-top-search-wrapper">
                            <?php do_action( 'new_search_location' ); ?>
                        </div>
                        <?php do_action( 'new_secondary_menu_location' ); ?>
                    </div> 
                </div>
			</div>
		</header>
    <?php

	/**
	 * generate_after_header hook.
	 *
	 * @since 0.1
	 *
	 * @hooked generate_featured_page_header - 10
	 */
	do_action( 'generate_after_header' );
	?>

	<div <?php generate_do_attr( 'page' ); ?>>
		<?php
		/**
		 * generate_inside_site_container hook.
		 *
		 * @since 2.4
		 */
		do_action( 'generate_inside_site_container' );
		?>
		<div <?php generate_do_attr( 'site-content' ); ?>>
			<?php
			/**
			 * generate_inside_container hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_inside_container' );

			