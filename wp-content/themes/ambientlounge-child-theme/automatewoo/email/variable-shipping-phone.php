<?php

if ( ! defined( 'ABSPATH' ) ) exit;
 
/**
 * @class AW_Variable_Shipping_Phone
 */
class My_AutomateWoo_Variable_Shipping_Phone extends AutomateWoo\Variable {

	/** @var bool - whether to allow setting a fallback value for this variable  */
	public $use_fallback = false;
	
	public function load_admin_details() {
		$this->description = __( "Display the shipping phone of a order", 'automatewoo');
	}

	/**
	 * @param $order WC_Order
	 * @param $parameters array
	 * @return string
	 */
	public function get_value( $order, $parameters ) {
		return $order->get_shipping_phone();
	}

}

return new My_AutomateWoo_Variable_Shipping_Phone();