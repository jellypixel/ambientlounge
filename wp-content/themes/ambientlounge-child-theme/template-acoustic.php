<?php
/*
	Template Name: Acoustic
*/
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/adjustment.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/main-dark.all.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/sofa.acoustic.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/sofa.acoustic-new.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/top-bar.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/flickity.min.css">
    <div class="header-inner"></div>
    <div class="top-bar-new d-flex justify-content-between" id="top-bar-new">
        <div class="top-bar-new-title" id="top-bar-new-title">ACOUSTIC</div>
        <a class="btn btn-primary top-bar-new-btn" id="top-bar-new-btn" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/">商品ページを見る</a>
    </div>
    <section id="banner-top">
        <div class="container-fluid bg-white">
            <div class="container-dark">
                <div class="row justify-content-center text-center">
                    <div class="top-banner">
                        <div class="top-banner-title-lg">ACOUSTIC</div>
                        <div class="top-banner-heading-lg">くつろぎのローソファ</div>
                    </div>
                </div>
                <div class="row justify-content-center text-center">
                    <div class="top-banner-2-desc desc-text-lg px-0">
                        ロータイプだからこそ得られる
                        <br>開放感を洋室でも和室でも。
                        <br>ゆりかごの中でうとうと眠ってしまうような感覚は、
                        <br>お昼寝にも最高のお供。
                    </div>
                    <div class="top-banner-2-desc desc-text-sm px-0">
                        ロータイプだからこそ得られる
                        <br>開放感を洋室でも和室でも。
                        <br>ゆりかごの中で
                        <br>うとうと眠ってしまうような感覚は、
                        <br>お昼寝にも最高のお供。
                    </div>
                </div>
                <div class="row justify-content-center text-center">
                    <img class="satisfaction-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/butterfly_new/satisfaction.webp"/>
                </div>
            </div>
        </div>
    </section>
    <section id="sofa-acoustic">
        <!--container bg-black start-->
        <div class="container-fluid bg-grey pt-5">
            <div class="container-dark">
                <div class="row banner-row-light clearfix">
                    <div class="col-xs-12 col-md-4 p-0 order-2 order-md-1">
                        <div class="banner-img-float-left">
                            <div class="sofa-banner-1"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-8 order-1 p-2 order-md-2">
                        <div class="banner-text-float-right">
                            <p class="banner-title-lg text-gradient-blue">約束の座り心地で、
                                <br>究極のリラックス時間</p>
                            <p class="banner-subtitle text-black desc-text-lg text-desc-float-right">大切な人と、誰にも邪魔されない一人時間、
                                <br>子供やペットと。ソファを中心に過ごす、
                                <br>かけがえのないリラックス時間に。
                                <br>ちょうど良い柔らかさ、
                                <br>ほっとする座り心地。
                            </p>
                            <p class="banner-subtitle text-black desc-text-sm text-desc-float-right">大切な人と、誰にも邪魔されない一人時間、子供やペットと。ソファを中心に過ごす、かけがえのないリラックス時間にお供します。ちょうど良い柔らかさ、ほっとする座り心地。</p>
                        </div>
                    </div>
                </div>
                <div class="row banner-row-dark">
                    <div class="col-xs-12 col-md-6 m-auto banner-text-left">
                        <p class="banner-title-lg text-gradient-pink banner-title-m-right">こだわるのは、
                            <br>おしゃれな空間づくり</p>
                        <p class="banner-subtitle desc-text-lg">永く愛用していただきたいから、
                            <br>差し色からベーシックまで、和室、
                            <br>洋室どんなお部屋にもマッチする
                            <br>重厚な生地は時を経ても
                            <br>色あせるません。</p>
                        <p class="banner-subtitle desc-text-sm">永く愛用していただきたいから、従来のビーズクッションでは叶わなかった洗練されたデザインにこだわりました。差し色からベーシックまで和室、洋室どんなお部屋にもマッチする重厚な生地は時を経ても色あせることなくたたずみます。</p>
                    </div>
                    <div class="col-xs-12 col-md-6 px-3 p-md-0">
                        <img class="banner-img-3" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/banner_img_3.webp"/>
                    </div>
                </div>
                <div class="row banner-row-light">
                    <div class="col-xs-12 col-md-6 p-2 m-auto order-md-2">
                        <div class="text-end banner-text-right">
                            <p class="banner-title-lg text-gradient-blue banner-title-m-left">角のない安全
                                <br>でやさしい作り</p>
                            <p class="banner-subtitle text-black desc-text-lg float-end">フレームレスであることの最大のメリットは
                                <br>角がないこと。
                                <br>小さなお子様やペット、
                                <br>シニアの方にも安全なソファです。
                            </p>
                            <p class="banner-subtitle text-black desc-text-sm float-end">フレームレスであることの最大のメリットは角がないこと。小さなお子様やペット、シニアの方にも安全なソファです。</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 order-md-1 p-0">
                        <div class="sofa-banner-3"></div>
                    </div>
                </div>
                <div class="row banner-row-dark">
                    <div class="col-xs-12 col-md-6 m-auto banner-text-left">
                        <p class="banner-title-lg text-gradient-pink banner-title-m-right-lg">軽量で楽々
                            <br>移動可能</p>
                        <p class="banner-subtitle desc-text-lg">フローリングはもちろん、
                            <br>畳のお部屋でも接地面を傷つけることなく、
                            <br>全ての場面でくつろぎ空間を可能にしました。女性やお子様でも簡単に移動しやすく、
                            <br>コロンと転がして動かして
                            <br>お掃除も楽ちんです。</p>
                        <p class="banner-subtitle desc-text-sm">フローリングはもちろん、畳のお部屋でも接地面を傷つけることなく、全ての場面でくつろぎ空間を可能にしました。女性やお子様でも簡単に移動しやすく、コロンと転がして動かしてお掃除も楽ちんです。</p>
                        <p class="banner-subtitle">重さ 約 <span class="text-weight-desc text-gradient-pink">8kg</span></p>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0">
                        <div class="sofa-banner-4"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--container bg-black end-->
        <!--container filling material start -->
        <div class="container-fluid bg-light">
            <div class="container-dark">
                <div class="row justify-content-center text-center bg-white">
                    <p class="light-text-title">生地の色バリエーション</p>
                    <p class="text-subtitle-dark px-4">豊富な20種類のカラーから<br>ずっとかけていたくなる秘密の極上ソファを</p>
                    <div class="container carousel-controls">
                        <div class="row">
                            <div class="col-xs-6 p-0 text-start text-md-center">
                                <span class="carousel-btn" id="carousel-back"><i class="bi bi-chevron-left" aria-hidden="true"></i> 前の商品</span>
                            </div>
                            <div class="col-xs-6 p-0 text-end text-md-center">
                                <span class="carousel-btn" id="carousel-forward">次の商品 <i class="bi bi-chevron-right" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row p-0" id="sofa-carousel">
                        <div class="carousel sofa-gallery data-flickity p-0">
                            <div class="carousel-cell cell1" data-color="keystone-grey">
                                <p class="sofa-img-title">ソフトグレー</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-soft-grey.webp" alt="ソフトグレー">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=soft-grey" data-color="soft-grey">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell2" data-color="dark-grey">
                                <p class="sofa-img-title">ダークグレー</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-dark-grey.webp" alt="ダークグレー">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=dark-grey" data-color="dark-grey">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell3" data-color="purple">
                                <p class="sofa-img-title">パープル</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-purple.webp" alt="パープル">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=purple" data-color="purple">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell4" data-color="pink">
                                <p class="sofa-img-title">ピンク</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-pink.webp" alt="ピンク">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=pink" data-color="pink">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell5" data-color="brown">
                                <p class="sofa-img-title">ブラウン</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-brown.webp" alt="ブラウン">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=brown" data-color="brown">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell6" data-color="black">
                                <p class="sofa-img-title">ブラック</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-black.webp" alt="ブラック">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=black" data-color="black">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell7" data-color="blue">
                                <p class="sofa-img-title">ブルー</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-blue.webp" alt="ブルー">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=blue" data-color="blue">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell8" data-color="beige">
                                <p class="sofa-img-title">ベージュ</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-beige.webp" alt="ベージュ">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=beige" data-color="beige">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell9" data-color="light-grey">
                                <p class="sofa-img-title">ライトグレー</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-light-grey.webp" alt="ライトグレー">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=light-grey" data-color="light-grey">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell10" data-color="lime">
                                <p class="sofa-img-title">ライム</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-green.webp" alt="ライム">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=lime" data-color="lime">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell11" data-color="red">
                                <p class="sofa-img-title">レッド</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/bg-white/colours-red.webp" alt="レッド">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=red" data-color="red">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell12" data-color="soft-black-waterresistant">
                                <p class="sofa-img-title">ソフトブラック</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sofa_acoustic/bg-white/color-soft-black.webp" alt="ソフトブラック・インテリアプラス">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=soft-black-waterresistant" data-color="soft-black-waterresistant">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                            <div class="carousel-cell cell13" data-color="blue-dream-waterresistant">
                                <p class="sofa-img-title">ブルードリーム</p>
                                <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sofa_acoustic/bg-white/color-blue-dream.webp" alt="ブルードリーム・インテリアプラス">
                                <p><a class="product-details-link" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=blue-dream-waterresistant" data-color="blue-dream-waterresistant">購入<i class="bi bi-chevron-right"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center color-panel">
                        <div class="col-xs-12 col-md-8 p-3 text-start">
                            <p class="interior-color-title">インテリア</p>
                            <div class="p-0">
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell1" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_1.webp" alt="ソフトグレー">
                                    <figcaption class="figure-caption">ソフトグレー</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell2" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_2.webp" alt="ダークグレー">
                                    <figcaption class="figure-caption">ダークグレー</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell3" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_3.webp" alt="パープル">
                                    <figcaption class="figure-caption">パープル</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell4" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_4.webp" alt="ピンク">
                                    <figcaption class="figure-caption">ピンク</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell5" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_5.webp" alt="ブラウン">
                                    <figcaption class="figure-caption">ブラウン</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell6" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_6.webp" alt="ブラック">
                                    <figcaption class="figure-caption">ブラック</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell7" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_7.webp" alt="ブルー">
                                    <figcaption class="figure-caption">ブルー</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell8" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_8.webp" alt="ベージュ">
                                    <figcaption class="figure-caption">ベージュ</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell9" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_9.webp" alt="ライトグレー">
                                    <figcaption class="figure-caption">ライトグレー</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell10" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_10.webp" alt="ライム">
                                    <figcaption class="figure-caption">ライム</figcaption>
                                </figure>
                                <figure class="figure">
                                    <img class="sofa-color-img" data-selector=".cell11" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa/color-btn/sofa_color_11.webp" alt="レッド">
                                    <figcaption class="figure-caption">レッド</figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-8 p-3 text-start d-flex m-auto">
                            <img class="d-flex p-2 interior-plus-black-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/interior-plus-black.png"/>
                            <span class="my-auto interior-plus-desc">
                            <p class="fw-bold">インテリア＋プラス</p>
                            <p>撥水性/褪色防止/引っ掻きに強い/ペットフレンドリー</p>
                        </span>
                        </div>
<!--                        <div class="col-xs-12 col-md-8 p-3 text-start">-->
<!--                            <p>現在インテリア＋プラスの取り扱いはありません。</p>-->
<!--                        </div>-->
                        <div class="col-xs-12 col-md-8 p-3 text-start">
                            <figure class="figure">
                                <img class="sofa-color-img" data-selector=".cell12" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sofa/color-btn/sofa_color_19.webp" alt="ソフトブラック・インテリアプラス">
                                <figcaption class="figure-caption">ソフトブラック・<br>インテリアプラス</figcaption>
                            </figure>
                            <figure class="figure">
                                <img class="sofa-color-img" data-selector=".cell13" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sofa/color-btn/sofa_color_20.webp" alt="ブルードリーム・インテリアプラス">
                                <figcaption class="figure-caption">ブルードリーム・<br>インテリアプラス</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center pb-5">
                    <div class="col-xs-12 p-0 text-center">
                        <p class="light-text-title">生地の種類</p>
                    </div>
                    <div class="col-xs-12 col-md-8 p-2 text-center">
                        <div class="card details-card p-0">
                            <div class="card-body">
                                <p class="details-card-title">インテリア</p>
                                <p class="details-card-subtitle">重厚感と弾力感のある丈夫な織りのファブリックコレクション。
                                    <br>カラーによって様々な色や素材が折り重なった存在感あふれる風合いが楽しめます。</p>
                            </div>
                            <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/material_detail_1.webp" class="card-img-bottom" alt="...">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-8 p-2 text-center">
                        <div class="card details-card p-0">
                            <div class="card-body">
                                <img class="interior-plus-blue-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/interior-plus-blue.webp" alt="interior-plus-blue"/>
                                <p class="details-card-title">インテリアプラス</p>
                                <p class="details-card-subtitle">ペットやお子様との暮らしに最適。
                                    <br>引っ掻きや汚れに強い撥水高機能ファブリック</p>
                            </div>
                            <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/material_detail_2.webp" class="card-img-bottom" alt="...">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--container filling material end-->
    </section>

    <!--spin view section start-->
    <section id="spin">
        <div class="container-fluid bg-white">
            <div class="container-dark">
                <div class="row text-center">
                    <div class="col">
                        <p class="light-text-title">ソフトファニチャーを<br>３D体験!</p>
                        <p class="light-text-subtitle">クリック＆ドラッグで360度ご覧いただけます</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="cloudimage-360"
                             data-folder="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/green/"
                             data-filename="{index}.webp"
                             data-amount="19"
                             data-bottom-circle="true"
                             data-lazyload>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="cloudimage-360"
                             data-folder="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_acoustic/black/"
                             data-filename="{index}.webp"
                             data-amount="18"
                             data-bottom-circle="true"
                             data-lazyload>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--spin view section end-->

    <!--container bg-white start-->
    <div class="container-fluid bg-white">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-8 p-2 text-center">
                    <div class="ar-panel" id="ar-panel">
                        <div class="row position-relative">
                            <div class="col-xs-3 col-md-3 p-2 mb-0">
                                <img class="ar-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/ar-icon.webp" alt="AR">
                            </div>
                            <div class="col-xs-7 col-md-8 p-2 m-auto">
                                <div class="card-body">
                                    <p class="ar-panel-title text-start">拡張現実(AR)で見る</p>
                                    <p class="ar-panel-subtitle text-start">ソファを自分のお部屋に置いてチェック</p>
                                </div>
                            </div>
                            <div class="col-xs-2 col-md-1 p-2 position-static m-auto">
                                <a class="vr-icon-android mobile stretched-link" href="intent://arvr.google.com/scene-viewer/1.0?file=<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/ar_files/sofa/acoustic/gltf/acoustic_luscious_grey.gltf&utm_source=devtools#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=https://developers.google.com/ar;end;" rel="ar">
                                    <img class="w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/chevron-right-icon.webp" alt="chevron-right"/>
                                </a>
                                <a class="vr-icon-ios mobile stretched-link" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/ar_files/sofa/acoustic/acoustic_luscious_grey.usdz" rel="ar">
                                    <img class="w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/chevron-right-icon.webp" alt="chevron-right"/>
                                </a>
                                <a class="vr-icon-others mobile stretched-link" href="https://ambientloungestaging.kinsta.cloud/scene-viewer.php?url=<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/ar_files/sofa/acoustic/gltf/acoustic_luscious_grey.gltf" rel="ar">
                                    <img class="w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/chevron-right-icon.webp" alt="chevron-right"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 ar-panel-caption">※iPhoneはSafari、AndroidはChromeで開いてください。</div>
            </div>
            <div class="row justify-content-center">
                <div class="support-panel">
                    <div class="support-subtitle">お試しいただける</div>
                    <div class="support-title">サポートサービス</div>
                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-md-3 text-center support-list">
                            <p class="support-list-title">１年間の<br>製品保証</p>
                            <p class="support-list-text">アンビエントラウンジでは全ての商品に対し、商品到着より１年間、製造過程における製品不良があった場合には
                                返送料弊社負担の上、修理等の対応を承ります。</p>
                            <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/acoustic_new/support-img-1.webp"/>
                        </div>
                        <div class="col-xs-12 col-md-3 text-center support-list">
                            <p class="support-list-title">送料無料</p>
                            <p class="support-list-text">大型ソファも対象！サイト内全ての商品が一部地域を除き、送料無料です。沖縄及び北海道・九州地方の一部離島については送料が発生いたします。ご注文の翌々営業日までに送料についてのご案内メールをお送りいたします。</p>
                            <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/acoustic_new/support-img-2.webp"/>
                        </div>
                        <div class="col-xs-12 col-md-3 text-center support-list">
                            <p class="support-list-title">30日間<br>返金保証</p>
                            <p class="support-list-text">ソファのトライアルは30日間。部屋に合わなかった、雰囲気が違ったなど気に入らない場合は、期間内なら全額返金いたします！</p>
                            <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/acoustic_new/support-img-3.webp"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 text-center">
                    <p class="light-text-title">インテリアに合わないか<br>心配ですか？</p>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-4 text-center">
                    <div class="panel-blue p-2 post-details-text">気に入らなければ<b>最大30日以内</b>の返品OK!</div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/trial_1.webp" alt=""/>
                    <p class="post-desc-text">カラーやフィリングをカスタマイズしてご注文</p>
                    <p class="post-details-text">最短翌日出荷</p>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/trial_2.webp" alt=""/>
                    <p class="post-desc-text">ご注文を受けてから日本ラボで作成しお届け</p>
                    <p class="post-details-text">日本人スタッフによる<br>最高品質の製造</p>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/trial_3.webp" alt=""/>
                    <p class="post-desc-text">お試しトライアル期間</p>
                    <!--                    <p><span class="text-grey" style="font-size:12px;line-height: 18px;">※ ペットラウンジ 60日間・ソファ 30日間</span></p>-->
                    <p class="post-details-text">気に入らない場合は、期間内なら全額返金。<br>※適用条件は「詳しく見る」をクリックしてご確認ください</p>
                </div>
            </div>
            <div class="row justify-content-center py-5">
                <div class="col-md-3 text-center"><a class="btn btn-primary btn-details-lg" href="https://ambientlounge.co.jp/30days-trial/">詳しく見る</a></div>
            </div>
        </div>
    </div>

    <!--container bg-white end-->

    <section id="sofa-review">
        <!--container review start-->
        <div class="bg-light p-3 p-xl-5">
            <div class="container-dark">
                <div class="row justify-content-center text-center">
                    <div class="col-md-12 p-0">
                        <p class="light-text-title">カスタマーレビュー</p>
                    </div>
                    <div class="col-xs-12 col-md-10 p-0">
                        <div class="row review-panel">
                            <div class="col-xs-12 col-md-3 p-0 m-auto"><div class="review-img review-img-1"></div></div>
                            <div class="col-xs-12 col-md-9 p-0 text-start m-auto">
                                <p class="review-panel-text"><span class="review-panel-text-bold">オシャレです！ </span> 見た目は立体的で近未来感がありとてもオシャレです。大きさはソファなのでそこそこ大きいですが、骨組みが無いので動かしやすく、座った感じは沈み過ぎず絶妙なフィット感で包み込まれます。生地はしっかりとした厚手で座面と背面それぞれのビーズが分かれている為、大きな型崩れを感じません。なんだか座っていると良い時間を過ごしている気分になります。</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-10 p-0">
                        <div class="row review-panel">
                            <div class="col-xs-12 col-md-3 p-0 m-auto"><div class="review-img review-img-2"></div></div>
                            <div class="col-xs-12 col-md-9 p-0 text-start m-auto">
                                <p class="review-panel-text"><span class="review-panel-text-bold">子供と妊婦の妻に </span> 3歳の子どもと妊婦の妻に買いました。3歳の子どももよく座っていて、長時間座って本を読んだり、テレビを観たりしています。少し大きいかなと思いましたが、買ってよかったです。</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-10 p-0">
                        <div class="row review-panel">
                            <div class="col-xs-12 col-md-3 p-0 m-auto"><div class="review-img review-img-3"></div></div>
                            <div class="col-xs-12 col-md-9 p-0 text-start m-auto">
                                <p class="review-panel-text"><span class="review-panel-text-bold">人をだめにするソファから乗り換え </span> 持ち運べる気軽なソファが欲しくて探していましたがなかなかなく。座り心地もとてもよく、色味も使い勝手も大満足です。人をだめにするソファから乗り換えましたが、人をだめにする感がグレードアップしてしまいました！</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--container review end-->
    </section>
    <!--container lookbook start-->
    <div class="container-fluid container-5 p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-8 col-md-12 p-2 order-2 order-md-1">
                    <p class="au_design_title">オリジナルの生活スタイルを</p>
                </div>
                <div class="col-xs-4 col-md-2 p-2 my-auto text-end order-1 order-md-2">
                    <img class="au_design_logo" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/au_design_logo.webp" alt="...">
                </div>
                <div class="col-xs-12 col-sm-8 col-md-6 p-0 p-md-3 order-3 order-md-3">
                    <p class="text-desc-sm text-desc-sm-black text-start">ゆったりと時間が流れるオーストラリアの生活の中でも、模様替えは頻繁。
                        <br>気分に合わせていつでも生活空間を替えたいけど、普通の家具は重くて面倒。
                        <br>ビーズクッションもあるけど、オシャレじゃない。
                        <br>そんな願いを叶えたのが軽量で美しいプレミアムソフトソファ。</p>
                </div>
                <div class="col-xs-12 p-0 mt-5 order-4">
                    <div class="row justify-content-center">
                        <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_1.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_2.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_3.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_4.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_5.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_6.webp" style="width:100%" alt=""/></div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center py-5">
                <div class="col-md-3 text-center"><a href="https://ambientlounge.co.jp/lookbook/?size=sofa" class="btn btn-primary btn-details-lg">LOOKBOOKを見る</a></div>
            </div>
        </div>
    </div>
    <!--container lookbook end-->
    <section id="banner-bottom">
        <!--container bg-black start-->
        <div class="container-fluid container-2">
            <div class="container-dark">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="bottom-banner-header">人気のソファ</p>
                    </div>
                </div>
                <div class="row banner-bottom-row-dark">
                    <div class="col-xs-12 col-md-6 banner-text-box">
                        <p class="banner-bottom-subtitle">人気No1のソファ</p>
                        <p class="banner-bottom-title text-gradient-blue">バタフライ</p>
                        <p><a class="btn btn-primary details-btn" href="https://ambientlounge.co.jp/butterfly/">詳細を見る</a></p>
                        <p><a class="buy-link" href="https://ambientlounge.co.jp/products/sofa/1-person/butterfly/">購入<i class="bi bi-chevron-right"></i></a></p>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0">
                        <div class="banner-bottom-img-1"></div>
                    </div>
                </div>
                <div class="row banner-bottom-row-dark mb-0">
                    <div class="col-xs-12 banner-text-box banner-text-box-mt">
                        <p class="banner-bottom-subtitle">ファスナーでつなぎ合わせて</p>
                        <p class="banner-bottom-title text-gradient-blue">モジュラー</p>
                        <p><a class="btn btn-primary details-btn" href="https://ambientlounge.co.jp/modular/">詳細を見る</a></p>
                        <p><a class="buy-link" href="https://ambientlounge.co.jp/collection/modular/">購入<i class="bi bi-chevron-right"></i></a></p>
                    </div>
                    <div class="col-xs-12 p-0 my-auto">
                        <div class="banner-bottom-img-3"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--container bg-black end-->
    </section>
    <script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/jquery-3.6.1.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/float-top-bar.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/lazysizes-5.3.2.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/flickity.pkgd.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/js-cloudimage-360-view-2.1.0.min.js"></script>

    <script>
        // window.CI360.init();
        var backBtn = document.getElementById("carousel-back");
        var nextBtn = document.getElementById("carousel-forward");
        var flky = new Flickity( '.sofa-gallery', {
            // options, defaults listed
            accessibility: true,
            // enable keyboard navigation, pressing left & right keys
            adaptiveHeight: false,
            // set carousel height to the selected slide
            autoPlay: false,
            // advances to the next cell
            // if true, default is 3 seconds
            // or set time between advances in milliseconds
            // i.e. `autoPlay: 1000` will advance every 1 second
            cellAlign: 'center',
            // alignment of cells, 'center', 'left', or 'right'
            // or a decimal 0-1, 0 is beginning (left) of container, 1 is end (right)
            cellSelector: undefined,
            // specify selector for cell elements
            contain: false,
            // will contain cells to container
            // so no excess scroll at beginning or end
            // has no effect if wrapAround is enabled
            draggable: '>1',
            // enables dragging & flicking
            // if at least 2 cells
            dragThreshold: 3,
            // number of pixels a user must scroll horizontally to start dragging
            // increase to allow more room for vertical scroll for touch devices
            freeScroll: false,
            // enables content to be freely scrolled and flicked
            // without aligning cells
            selectedAttraction: 0.1,
            friction: 0.8,
            // smaller number = easier to flick farther
            groupCells: false,
            // group cells together in slides
            initialIndex: 0,
            // zero-based index of the initial selected cell
            lazyLoad: 2,
            // enable lazy-loading images
            // set img data-flickity-lazyload="src.jpg"
            // set to number to load images adjacent cells
            percentPosition: true,
            // sets positioning in percent values, rather than pixels
            // Enable if items have percent widths
            // Disable if items have pixel widths, like images
            prevNextButtons: false,
            // creates and enables buttons to click to previous & next cells
            pageDots: false,
            // create and enable page dots
            resize: true,
            // listens to window resize events to adjust size & positions
            rightToLeft: false,
            // enables right-to-left layout
            setGallerySize: true,
            // sets the height of gallery
            // disable if gallery already has height set with CSS
            watchCSS: false,
            // watches the content of :after of the element
            // activates if #element:after { content: 'flickity' }
            wrapAround: true,
            // at end of cells, wraps-around to first for infinite scrolling

        });
        backBtn.addEventListener("click",function(){
            flky.previous();
        });
        nextBtn.addEventListener("click",function(){
            flky.next();
        });
        // let buyBtn = document.getElementsByClassName("product-details-link");
        // for (let i = 0; i < buyBtn.length; i++) {
        //     buyBtn[i].addEventListener("click",function(){
        //         let color = buyBtn[i].getAttribute("data-color")
        //         let url = "https://ambientlounge.co.jp/products/sofa/1-person/acoustic/?attribute_pa_color=";
        //         console.log(color);
        //         // window.open(url+color,'_self');
        //     })
        // }

        let sofaColorBtn = document.getElementsByClassName("sofa-color-img");
        for (let i = 0; i < sofaColorBtn.length; i++) {
            sofaColorBtn[i].addEventListener("click",function(){
                let selector = sofaColorBtn[i].getAttribute('data-selector')
                flky.selectCell(selector)
            })
        }
        let ua = navigator.userAgent
        let androidVrBtn = document.querySelectorAll(".vr-icon-android.mobile");
        let iosVrBtn = document.querySelectorAll(".vr-icon-ios.mobile");
        let otherVrBtn = document.querySelectorAll(".vr-icon-others.mobile");
        if (/android/i.test(ua)) {
            for (let i = 0; i < androidVrBtn.length; i++) {
                androidVrBtn[i].classList.add('show');
            }
        }
        else if (/iPad|iPhone|iPod/.test(ua)) {
            for (let i = 0; i < iosVrBtn.length; i++) {
                iosVrBtn[i].classList.add('show');
            }
        }
        else {
            for (let i = 0; i < otherVrBtn.length; i++) {
                otherVrBtn[i].classList.add('show');
            }
        }
    </script>
<?php
get_footer();
?>