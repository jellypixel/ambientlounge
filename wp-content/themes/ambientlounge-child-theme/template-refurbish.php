<?php
/*
	Template Name: Refurbish
*/
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/adjustment.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/font/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/main-dark.all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/lp-refurbish.css?v=2"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto Sans JP:100,200,300,400,500,600,700|Noto Sans JP:600|Open Sans:600|Open Sans|Open Sans:700&amp;subset=latin-ext&amp;display=swap" media="all" onload="this.media='all'">

<div id="main-content">
    <div class="container-fluid bg-light">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <p class="top-banner-title">Eco Friendly</p>
                <p class="top-banner-text top-banner-heading-lg">エコロジカルな活動を
                    <br>エコフレンドリー</p>
                <p class="top-banner-text top-banner-heading-sm desc-text-lg">エコフレンドリーとは返品された商品を整備し、新品に準じる状態に仕上げたものです。
                    <br>アンビエントラウンジジャパンには2つの大きな願いがあります。
                    <br>1つ目は、全ての商品がお客様に愛され、長く使って頂けること。
                    <br>2つ目に、環境に配慮した、皆様から支持されるブランドに成長して行きたい。ということ。</p>
                <p class="top-banner-text top-banner-heading-sm desc-text-sm">エコフレンドリーとは返品された商品を整備し、新品に準じる状態に仕上げたものです。アンビエントラウンジジャパンには2つの大きな願いがあります。1つ目は、全ての商品がお客様に愛され、長く使って頂けること。2つ目に、環境に配慮した、皆様から支持されるブランドに成長して行きたい。ということ。</p>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-11 col-md-9 p-0 text-center">
                    <img class="w-100 pt-5" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/refurbish/banner-top-2-eco.webp" alt="re-banner" id="topBanner" usemap="#ecobanner"/>
                    <map name="ecobanner" id="ecobanner">
                    </map>
                </div>
            </div>
            <div class="row justify-content-center pt-3">
                <div class="col-xs-6 col-md-5 text-center p-2">
                    <p class="top-img-caption">ECOペットラウンジ</p>
<!--                    <p class="top-img-caption-sm">※ベース本体のみ、カバーなし</p>-->
                </div>
                <div class="col-xs-6 col-md-4 text-center p-2">
                    <p class="top-img-caption">ECOペットラウンジカバー</p>
                </div>
            </div>
            <div class="row justify-content-center pt-3">
                <div class="col-xs-6 col-md-5 text-center">
                    <a class="btn btn-primary btn-details-lg" href="https://ambientlounge.co.jp/products/eco-campaign/petlounge-eco/">購入</a>
                </div>
                <div class="col-xs-6 col-md-4 text-center">
                    <a class="btn btn-primary btn-details-lg" href="https://ambientlounge.co.jp/products/eco-campaign/covers-eco/">購入</a>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 p-0 text-center">
                    <div class="top-banner-container text-center">
                        <p class="ref-top-banner-desc">※エコフレンドリー商品の返品・交換はできません。
                            <br>※在庫数には限りがございます。不定期にリストックされます。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--container bg-black start-->
    <section id="lp-refurbish">
        <div class="container-fluid container-2">
            <div class="container-dark">
                <div class="row banner-row-dark">
                    <div class="col-xs-12 col-md-6 m-0 m-md-auto order-2 order-md-1">
                        <div class="banner-img-1"></div>
                    </div>
                    <div class="col-xs-12 col-md-6 m-auto text-end banner-title-right order-1 order-md-2">
                        <p class="banner-title-lg text-gradient-blue banner-title-m-left">シェアすることは
                            <br>思いやること</p>
                        <p class="banner-subtitle pt-md-4 desc-text-lg">実際のイメージと異なった場合や、ペットが使ってくれない。などの理由で60日トライアル期間中に返品された商品があります。それらをプロフェッショナルクリーニングで丁寧な洗い、乾燥、特殊抗菌処理、検品仕上げにかけて生まれ変わらせました。このエコフレンドリー商品をご購入いただくことは、私たちが提案する循環型Project継続への力になります。</p>
                        <p class="banner-subtitle pt-3 desc-text-sm">実際のイメージと異なった場合や、ペットが使ってくれない。などの理由で60日トライアル期間中に返品された商品があります。それらをプロフェッショナルクリーニングで丁寧な洗い、乾燥、特殊抗菌処理、検品仕上げにかけて生まれ変わらせました。このエコフレンドリー商品をご購入いただくことは、私たちが提案する循環型Project継続への力になります。</p>
                    </div>
                </div>
                <div class="row banner-row-dark mb-0">
                    <div class="col-xs-12 col-md-6 m-auto text-start banner-title-left">
                        <p class="banner-title-lg text-gradient-pink banner-title-m-right">抗菌・防臭・
                            <br>抗ウイルス仕上げ</p>
                        <p class="banner-subtitle pt-3 pt-md-4">目指したのは新品に準じる仕上がり
                            <br>プロフェッショナルクリーニングで丁寧な洗いにかけられた商品は、オーリスWV-32で抗ウィルス加工が施されます。これによる生地上のウィルス低減率は99.9%以上！同時に抗菌、防臭効果もあるから安心してご使用いただけます。
                            <br>また、生地を痛めない様にタンブル乾燥では無く自然乾燥仕上げにこだわりました。乾燥後、丁寧なブラッシング工程を経て、検品をパスした商品だけがお客様に届けられています。
                        </p>
                    </div>
                    <div class="col-xs-12 col-md-6 m-auto">
                        <div class="ref-banner-img-1"></div>
                        <div class="ref-banner-img-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--container bg-black end-->

    <!--container bg-white start-->
    <div class="container-fluid bg-white">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <div class="col-xs-12">
                    <p class="ref-spec-title">仕様</p>
                </div>
                <div class="col-xs-12">
                    <p class="ref-spec-subtitle">ベース</p>
                </div>
                <div class="col-xs-12 p-0">
                    <table class="table table-borderless ref-spec-table-1">
                        <tr class="bg-light text-start">
                            <th class="table-1-col1">カラー</th>
                            <th>素材</th>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="table-1-col1">グレー</th>
                            <th class="table-font-light">ナイロン100％（撥水・UVカット）</th>
                        </tr>
                        <tr class="bg-light text-start">
                            <th class="table-1-col1">ベージュ</th>
                            <th class="table-font-light">ナイロン100％（撥水・UVカット）</th>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="table-1-col1">ブルードリーム</th>
                            <th class="table-font-light">ポリエステル100%（一部リサイクルポリエステル使用・撥水）</th>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-12">
                    <p class="ref-spec-subtitle">カバー生地</p>
                </div>
                <div class="col-xs-12 p-0">
                    <table class="table table-borderless ref-spec-table-2">
                        <tr class="bg-light text-start">
                            <th>カバー生地</th>
                            <th>カラー</th>
                            <th>素材</th>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="table-font-light">エコファー</th>
                            <th class="table-font-light">ウルフグレー</th>
                            <th class="table-font-light">ポリエステル100%</th>
                        </tr>
                        <tr class="bg-light text-start">
                            <th class="table-font-light">エコファー</th>
                            <th class="table-font-light">ラビットベージュ</th>
                            <th class="table-font-light">ポリエステル100%</th>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="table-font-light">エコファー</th>
                            <th class="table-font-light">バンビグレー</th>
                            <th class="table-font-light">ポリエステル100%</th>
                        </tr>
                        <tr class="bg-light text-start">
                            <th class="table-font-light">キルト</th>
                            <th class="table-font-light">グレー</th>
                            <th class="table-font-light">原着アクリル100%</th>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="table-font-light">キルト</th>
                            <th class="table-font-light">ベージュ</th>
                            <th class="table-font-light">原着アクリル100%</th>
                        </tr>
                        <tr class="bg-light text-start">
                            <th class="table-font-light">オーガニックコットンブレンド</th>
                            <th class="table-font-light">シープオーガニック</th>
                            <th class="table-font-light">オーガニックコットン80%<br>ポリエステル20％</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--container bg-white end-->

    <!--container product white panel start-->
    <section id="lp-refurbish-product-w">
        <div class="container-fluid bg-light container-5 p-3 p-xl-5">
            <div class="container-dark">
                <div class="row justify-content-center">
                    <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                        <div class="product-panel-white">
                            <p class="light-panel-title">ベッド</p>
                            <p class="light-panel-subtitle">耐久性に優れ、いつも清潔、そして極上の寝心地。</p>
                            <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/pet-lounge/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                            <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-1.webp"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                        <div class="product-panel-white">
                            <p class="light-panel-title">アクセサリー</p>
                            <p class="light-panel-subtitle desc-text-lg">ブランケットやジェルマット、メンテンス用品など<br>いろいろなアイテムが勢ぞろい</p>
                            <p class="light-panel-subtitle desc-text-sm">ブランケットやジェルマット、メンテンス用品などいろいろなアイテムが勢ぞろい</p>
                            <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/pet-accessories/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                            <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-2.webp"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                        <div class="product-panel-white">
                            <p class="light-panel-title">DELUX</p>
                            <p class="light-panel-subtitle">犬ペットラウンジ<br>取替カバー3種、メンテナンスや快適をさらに向上させるアクセサリー、リピーター続出のthe Shampooがセットになりました。</p>
                            <a class="light-panel-link" href="https://ambientlounge.co.jp/delux/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                            <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa-top/product-1.webp"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                        <div class="product-panel-white">
                            <p class="light-panel-title">The Shampoo</p>
                            <p class="light-panel-subtitle">愛犬のグルーミング商品</p>
                            <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/the-shampoo-intro/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                            <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-4.webp"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--container product white panel end-->
</div>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/bootstrap.bundle.min.js"></script>
<script>
    let topBanner= document.getElementById('topBanner');
    let bannerMap = document.getElementById('ecobanner');
    let bannerWidth = topBanner.width;
    let bannerHeight = topBanner.height;
    let img1_coords_start = '0,' + (bannerHeight*0.4).toFixed(2);
    let img1_coords_end = ',' + (bannerWidth*0.47).toFixed(2) + ',' + (bannerHeight*1.2).toFixed(2);

    let img2_coords_start = (bannerWidth*0.6).toFixed(2) + ',' + (bannerHeight*0.4).toFixed(2);
    let img2_coords_end = ',' + bannerWidth + ',' + (bannerHeight*1.2).toFixed(2);
    const areaOne = document.createElement('area');
    areaOne.setAttribute('shape','rect');
    areaOne.setAttribute('coords',img1_coords_start + img1_coords_end);
    areaOne.setAttribute('href','https://ambientlounge.co.jp/products/eco-campaign/petlounge-eco/');
    bannerMap.appendChild(areaOne);

    const areaTwo = document.createElement('area');
    areaTwo.setAttribute('shape','rect');
    areaTwo.setAttribute('coords',img2_coords_start + img2_coords_end);
    areaTwo.setAttribute('href','https://ambientlounge.co.jp/products/eco-campaign/covers-eco/');
    bannerMap.appendChild(areaTwo);
</script>
<?php
get_footer();
?>