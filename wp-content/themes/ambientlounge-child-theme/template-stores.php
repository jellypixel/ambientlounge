<?php
/*
	Template Name: Stores
*/	
?>
<?php
	get_header();

    $cat = '';
    if ( isset( $_GET['cat'] ) ) 
    {
        $cat = $_GET['cat'];
    }
    else {
        $cat = 'retail-sofa'; // First retail category
    }

    /*$allCatClass = 'active';
    $categories = get_terms([
        'taxonomy' => 'retail_category',
        'hide_empty' => true,
    ]);

    foreach($categories as $category) 
    {
        if ( $cat == $category->term_id ) {
            $allCatClass = '';
        }
    }*/

    $thisURL = strtok($_SERVER["REQUEST_URI"], '?');
?>
    <div class="content-area">
        <header class="entry-header">            
            <div class="page-heading-wrapper">
                <div class="entry-container">
                    <h1 class="entry-title" itemprop="headline"><?php the_title(); ?></h1>
                </div>
            </div>
        </header>

        <div class="entry-content">        
            <div class="blog-category-wrapper">
                <form action="" method="get">
                    <div class="blog-category-inner">                        
                        <?php                        
                            $categories = get_terms([                                
                                'taxonomy' => 'retail_category',
                                'hide_empty' => true,
                            ]);

                            foreach( $categories as $category ) 
                            {                            
                                ?>
                                    <a href="<?php echo $thisURL . '?cat=' . $category->slug; ?>" title="<?php echo $category->name ?>">
                                        <div class="blog-category <?php echo ( $cat == $category->slug) ? "active" : "" ?>" cat="<?php echo $category->term_id ?>"><?php echo $category->name ?></div>
                                    </a>
                                <?php                                
                            }
                        ?>                     
                    </div>                    
                    <input type="hidden" name="cat" id="cat" value="">                        
                </form>
            </div>
            
            <?php
                $locations = get_terms([
                    'taxonomy' => 'retail_location',
                    'hide_empty' => true,
                ]);

                if ( !empty( $locations ) ) {
                    ?>
                        <div class="store-location-wrapper">
                            <div class="slim-container">
                                <?php
                                    foreach( $locations as $location ) 
                                    {  
                                        $args = array(
                                            'post_type' => array( 'retail' ), 
                                            'order' => 'ASC'                   
                                        );                                        
                                        
                                        $taxQueryArr =  array(
                                                            array(
                                                                'taxonomy'  => 'retail_location',
                                                                'field'     => 'term_id',
                                                                'terms'     => $location->term_id
                                                            )
                                                        );

                                        if ( !empty( $cat ) ) 
                                        {                                                                        
                                            $catArr =   array(
                                                            'taxonomy'  => 'retail_category',
                                                            'field'     => 'slug',
                                                            'terms'     => $cat
                                                        );

                                            array_push( $taxQueryArr, $catArr );
                                            $taxQueryArr = array_merge(  $taxQueryArr, array( 'relation' => 'AND' ) );                                            
                                        }

                                        $args['tax_query'] = $taxQueryArr;                                        

                                        $query = new WP_Query( $args );
                
                                        if ( $query->have_posts() )  
                                        {                                        
                                            ?>
                                                <div class="store-location">
                                                    <div class="store-location-title">
                                                        <?php echo $location->name; ?>
                                                    </div>
                                                    <div class="store-location-list">
                                                        <?php
                                                            while ( $query->have_posts() ) 
                                                            {
                                                                $query->the_post();
                                                                global $post;

                                                                ?>
                                                                    <div id="store-<?php echo $post->ID; ?>" class="store-list"><?php the_title(); ?></div>
                                                                <?php
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php                                        
                                        }
                                        wp_reset_postdata();
                                    }                                        
                                ?>
                            </div>
                        </div>

                        <div class="store-location-detail-wrapper">                            
                            <?php
                                foreach( $locations as $location ) 
                                {  
                                    $args = array(
                                        'post_type' => array( 'retail' ), 
                                        'order' => 'ASC',
                                        'posts_per_page' => -1
                                    );                                        
                                    
                                    $taxQueryArr =  array(
                                                        array(
                                                            'taxonomy'  => 'retail_location',
                                                            'field'     => 'term_id',
                                                            'terms'     => $location->term_id
                                                        )
                                                    );
         
                                    if ( !empty( $cat ) ) {
                                                                    
                                        $catArr =   array(
                                                        'taxonomy'  => 'retail_category',
                                                        'field'     => 'slug',
                                                        'terms'     => $cat
                                                    );

                                        array_push( $taxQueryArr, $catArr );
                                        $taxQueryArr = array_merge(  $taxQueryArr, array( 'relation' => 'AND' ) );                                        
                                    }

                                    $args['tax_query'] = $taxQueryArr;                                        

                                    $query = new WP_Query( $args );
            
                                    if ( $query->have_posts() )  
                                    {
                                        ?>
                                            <div class="store-location-detail">
                                                <div class="store-location-detail-title">
                                                    <h2><?php echo $location->name; ?></h2>
                                                </div>
                                                <div class="store-location-detail-list">
                                                    <?php
                                                        while ( $query->have_posts() ) 
                                                        {
                                                            $query->the_post();
                                                            global $post;

                                                            $postThumb = get_the_post_thumbnail_url( $post, 'full' );	

                                                            $product_handled = get_field( 'product_handled' );
                                                            $address = get_field( 'address' );
                                                            $phone = get_field( 'phone' );
                                                            $free_dial_phone = get_field( 'free_dial_phone' );
                                                            $email = get_field( 'email' );
                                                            $open = get_field( 'open' );
                                                            $website = get_field( 'website' );
                                                            $map = get_field( 'location' );
                                                            ?>
                                                                <div class="store-detail <?php echo 'store-' . $post->ID; ?>">
                                                                    <div class="store-detail-photo" style="background-image:url(<?php echo $postThumb; ?>);">
                                                                    </div>
                                                                    <div class="store-detail-content">
                                                                        <div class="store-detail-content-top">
                                                                            <div class="store-detail-title">
                                                                                <h5><?php the_title(); ?></h5>
                                                                            </div>
                                                                            <div class="store-detail-product-handled store-detail-row">
                                                                                <div class="store-detail-attr bold">
                                                                                    <?php _e( '取扱商品', 'ambientlounge' ); ?>
                                                                                </div>
                                                                                <div class="store-detail-value">
                                                                                    <?php echo $product_handled; ?>
                                                                                </div>
                                                                            </div>
                                                                            <div class="store-detail-address">
                                                                                <?php echo $address; ?>
                                                                            </div>
                                                                            <div class="store-detail-contact">
                                                                                <div class="store-detail-phone store-detail-row">
                                                                                    <div class="store-detail-attr">
                                                                                        <?php _e( 'TEL', 'ambientlounge' ); ?>:
                                                                                    </div>
                                                                                    <div class="store-detail-value">
                                                                                        <a href="tel:<?php echo str_replace( array( '-', ' '), '', $phone ); ?>" title="今すぐ電話">
                                                                                            <?php echo $phone; ?>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="store-detail-open store-detail-row">
                                                                                    <div class="store-detail-attr">
                                                                                        <?php _e( 'OPEN', 'ambientlounge' ); ?>:
                                                                                    </div>
                                                                                    <div class="store-detail-value">
                                                                                        <?php echo $open; ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="store-detail-others">
                                                                                <?php
                                                                                    if ( !empty ( $free_dial_phone ) ) 
                                                                                    {
                                                                                        ?>
                                                                                            <div class="store-detail-freedial store-detail-row">
                                                                                                <div class="store-detail-attr">
                                                                                                    <?php _e( 'フリーダイヤル', 'ambientlounge' ); ?>:
                                                                                                </div>
                                                                                                <div class="store-detail-value">
                                                                                                    <a href="tel:<?php echo str_replace( '-', '', $phone ); ?>" title="フリーダイヤル">
                                                                                                        <?php echo $free_dial_phone; ?>
                                                                                                    </a>
                                                                                                </div>                                                                                                
                                                                                            </div>
                                                                                        <?php
                                                                                    } 

                                                                                    if ( !empty ( $email ) ) 
                                                                                    {
                                                                                        ?>
                                                                                            <div class="store-detail-email store-detail-row">
                                                                                                <div class="store-detail-attr">
                                                                                                    <?php _e( 'メール', 'ambientlounge' ); ?>:
                                                                                                </div>
                                                                                                <div class="store-detail-value">
                                                                                                    <a href="mailto:<?php echo $email; ?>" title="メール">
                                                                                                        <?php echo $email; ?>
                                                                                                    </a>
                                                                                                </div>                                                                                                
                                                                                            </div>
                                                                                        <?php
                                                                                    }                                                                                     
                                                                                ?>
                                                                                
                                                                                <div class="store-detail-website">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="store-detail-content-bottom">
                                                                            <div class="store-detail-action">
                                                                                <?php
                                                                                    if ( !empty( $phone ) )
                                                                                    {
                                                                                        ?>
                                                                                            <a href="tel:<?php echo str_replace( array( '-', ' '), '', $phone ); ?>" title="今すぐ電話">
                                                                                                <?php _e( '今すぐ電話', 'ambientlounge' ); ?>
                                                                                            </a>
                                                                                        <?php
                                                                                    }



                                                                                    if ( !empty( $website ) ) 
                                                                                    {
                                                                                        ?>
                                                                                            <a href="<?php echo $website; ?>" title="情報を見る" target="_blank">
                                                                                                <?php _e( '情報を見る', 'ambientlounge' ); ?>
                                                                                            </a>
                                                                                        <?php
                                                                                    }

                                                                                    if ( !empty( $location ) ) 
                                                                                    {
                                                                                        ?>
                                                                                            <a href="<?php echo $map; ?>" title="地図を見る" target="_blank">
                                                                                                <?php _e( '地図を見る', 'ambientlounge' ); ?>
                                                                                            </a>
                                                                                        <?php
                                                                                    }                                                                                    
                                                                                ?>

                                                                                <a href="#" title="TOP" class="toTop">
                                                                                    <?php _e( 'TOP', 'ambientlounge' ); ?>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        <?php                                        
                                    }
                                    wp_reset_postdata();
                                }                                        
                            ?>
                        </div>
                    <?php
                }
            ?>
        </div>
    </div>

<?php
	get_footer();
?>