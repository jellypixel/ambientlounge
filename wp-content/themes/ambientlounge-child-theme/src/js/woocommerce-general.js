var $j = jQuery.noConflict();

$j(function(){	

    // Separate woocommerce notice/info/error with its cta button        
        var woocommerceMessageEl = $j( '.woocommerce-message' );
        var woocommerceMessageContent = woocommerceMessageEl.wrapInner( '<div class="woocommerce-message-content"></div>' );            
        
        woocommerceMessageContent.find( '.button' ).appendTo( woocommerceMessageEl );

    


});