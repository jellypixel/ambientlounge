var $j = jQuery.noConflict();

$j(function(){	

	/**
	 * Deluxe Info
	 * 
	 * This render the deluxe information on /products/dog-bed/pet-lounge/
	 */
  
		var parentWrapper = $j( '.variations_form' );
		var deluxeInfo = parentWrapper.find( '.variation-wrap.pa_deluxe_info' );
		var bedCoverSelector = parentWrapper.find( '.variation-wrap.pa_cover-selection' );
		var bedFillingSelector = parentWrapper.find( '.variation-wrap.pa_filling' );
		var bedPackageSelector = parentWrapper.find( '.tawcvs-swatches[data-attribute_name=attribute_pa_product-type]' );

		checkIfNotStandardPackage();

		$j( 'body' ).on( 'click', bedPackageSelector.find( 'swatch' ), function() {
			checkIfNotStandardPackage();
		});

		function checkIfNotStandardPackage() 		
		{      
			// Before everything, if christmas bundle pressed, this 3 will be hidden (code below), so re-show to make sure it's visible
			parentWrapper.find(".pa_deluxe_info.grey-cover .swatch-deluxe-three, .pa_deluxe_info.beige-cover .swatch-deluxe-three, .pa_deluxe_info.blue-cover .swatch-deluxe-three").show();
			// Also this
			parentWrapper.find(".pa_deluxe_info.blue-cover .swatch-deluxe-two .sub-swatch:not(.christmas-only), .pa_deluxe_info.grey-cover .swatch-deluxe-two .sub-swatch:not(.christmas-only), .pa_deluxe_info.beige-cover .swatch-deluxe-two .sub-swatch:not(.christmas-only)").removeClass( 'hide' );
			parentWrapper.find(".pa_deluxe_info.blue-cover .swatch-deluxe-two .christmas-only, .pa_deluxe_info.grey-cover .swatch-deluxe-two .christmas-only, .pa_deluxe_info.beige-cover .swatch-deluxe-two .christmas-only").addClass( 'hide' );

			// Delux Bundle  
			if ( bedPackageSelector.find( '.swatch-delux' ).hasClass( 'selected' ) ) {
				
				if(parentWrapper.find(".swatch-grey-base").hasClass("selected")) {
					parentWrapper.find(".pa_deluxe_info.blue-cover").slideUp(400);
					parentWrapper.find(".pa_deluxe_info.beige-cover").slideUp(400);
					parentWrapper.find(".pa_deluxe_info.grey-cover").slideDown(400);
				} 
				
				if(parentWrapper.find(".swatch-beige-base").hasClass("selected")) {
					parentWrapper.find(".pa_deluxe_info.blue-cover").slideUp(400);
					parentWrapper.find(".pa_deluxe_info.beige-cover").slideDown(400);
					parentWrapper.find(".pa_deluxe_info.grey-cover").slideUp(400);
				} 
				
				if(parentWrapper.find(".swatch-blue-base").hasClass("selected")) {
					parentWrapper.find(".pa_deluxe_info.blue-cover").slideDown(400);
					parentWrapper.find(".pa_deluxe_info.beige-cover").slideUp(400);
					parentWrapper.find(".pa_deluxe_info.grey-cover").slideUp(400);
				}     				
				
				bedCoverSelector.hide();
				bedFillingSelector.hide();
			}
			// Christmas Bundle
			else if ( bedPackageSelector.find( '.swatch-christmas-bundle' ).hasClass( 'selected' ) ) {
				
				if(parentWrapper.find(".swatch-grey-base").hasClass("selected")) {
					// Same behavior as delux bundle:
					parentWrapper.find(".pa_deluxe_info.blue-cover").slideUp(400);
					parentWrapper.find(".pa_deluxe_info.beige-cover").slideUp(400);
					parentWrapper.find(".pa_deluxe_info.grey-cover").slideDown(400);

					// Special behavior for christmas bundle:
					parentWrapper.find(".pa_deluxe_info.grey-cover .swatch-deluxe-three").hide();
					parentWrapper.find(".pa_deluxe_info.grey-cover .swatch-deluxe-two .sub-swatch:not(.christmas-only)").addClass( 'hide' );
					parentWrapper.find(".pa_deluxe_info.grey-cover .swatch-deluxe-two .christmas-only").removeClass( 'hide' );
				} 
				
				if(parentWrapper.find(".swatch-beige-base").hasClass("selected")) {
					// Same behavior as delux bundle:
					parentWrapper.find(".pa_deluxe_info.blue-cover").slideUp(400);
					parentWrapper.find(".pa_deluxe_info.beige-cover").slideDown(400);
					parentWrapper.find(".pa_deluxe_info.grey-cover").slideUp(400);

					// Special behavior for christmas bundle:
					parentWrapper.find(".pa_deluxe_info.beige-cover .swatch-deluxe-three").hide();
					parentWrapper.find(".pa_deluxe_info.beige-cover .swatch-deluxe-two .sub-swatch:not(.christmas-only)").addClass( 'hide' );
					parentWrapper.find(".pa_deluxe_info.beige-cover .swatch-deluxe-two .christmas-only").removeClass( 'hide' );
				} 
				
				if(parentWrapper.find(".swatch-blue-base").hasClass("selected")) {
					// Same behavior as delux bundle:
					parentWrapper.find(".pa_deluxe_info.blue-cover").slideDown(400);
					parentWrapper.find(".pa_deluxe_info.beige-cover").slideUp(400);
					parentWrapper.find(".pa_deluxe_info.grey-cover").slideUp(400);

					// Special behavior for christmas bundle:
					parentWrapper.find(".pa_deluxe_info.blue-cover .swatch-deluxe-three").hide();
					parentWrapper.find(".pa_deluxe_info.blue-cover .swatch-deluxe-two .sub-swatch:not(.christmas-only)").addClass( 'hide' );
					parentWrapper.find(".pa_deluxe_info.blue-cover .swatch-deluxe-two .christmas-only").removeClass( 'hide' );
				}     

				// Special behavior for christmas bundle:
				/*parentWrapper.find(".pa_deluxe_info.blue-cover .swatch-deluxe-two .deluxe-only").addClass( 'hide' );
				parentWrapper.find(".pa_deluxe_info.blue-cover .swatch-deluxe-two .christmas-only").removeClass( 'hide' );*/
				
				// Same behavior as delux bundle:
				bedCoverSelector.hide();
				bedFillingSelector.hide();
			}
			// Default / Standard Package
			else {
				deluxeInfo.slideUp( 400 );

				bedCoverSelector.show();
				bedFillingSelector.show();
			}
		}

		deluxeInfo.find( '.info-link a').click( function(e) {
			e.stopPropagation();            
		})

	/**
	 * Sticky Pricing
	 * 
	 * Makes the thumbnail, description and text syncs with the selection
	 */
	if(parentWrapper.length) {
		jQuery(".sticky-price-name").html(jQuery(".product_title").html());

		// Check for changes in the variation ID
		var initialVariationID = jQuery('.woocommerce-variation-add-to-cart .variation_id').val();
		jQuery('.single-product-wrapper .single-product-right .woocommerce-variation-add-to-cart .variation_id').change(function() {
			// Get the new variation ID
			var newVariationID = jQuery(this).val();

			// Check if the variation ID has changed
			if (newVariationID !== initialVariationID) {
				// Update the top bar price with the variation price
				var variationPrice = jQuery('.single-product-right .single_variation_wrap .woocommerce-variation-price .price').html();
				jQuery('.single-product-topbar-price').html(variationPrice);

				// Copy the image
				var productImage = jQuery('.woocommerce-product-gallery img').first().attr('src');
				jQuery('.single-product-topbar-left img').attr('src', productImage);

				// See the price
				var priceNumNearTitle = jQuery(".single-product-wrapper .single-product-right .woocommerce-Price-amount bdi").first().clone().children().remove().end().text();
				var priceNumNearQuantity = jQuery(".single-product-wrapper .single_variation_wrap .woocommerce-Price-amount bdi").first().clone().children().remove().end().text();

				if(priceNumNearQuantity === 0) {
					jQuery('.single-product-right .single_variation_wrap .single_variation .woocommerce-variation-price').remove();
					jQuery('.single-product-right .single_variation_wrap .single_variation').append('<div class="woocommerce-variation-price"><span class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">¥</span>0</bdi></span> <small class="woocommerce-price-suffix">(税込/送料無料)</small></span></div>');
				} else if (priceNumNearQuantity === "") {

					if(jQuery(".single-product-wrapper .single-product-right del .woocommerce-Price-amount bdi").length) {

						var discountedPrice = jQuery(".single-product-wrapper .single-product-right ins .woocommerce-Price-amount bdi").first().clone().children().remove().end().text();

						jQuery('.single-product-right .single_variation_wrap .single_variation .woocommerce-variation-price').remove();
						jQuery('.single-product-right .single_variation_wrap .single_variation').append('<div class="woocommerce-variation-price"><span class="price"><del aria-hidden="true"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">¥</span>'+priceNumNearTitle+'</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">¥</span>'+discountedPrice+'</bdi></span></ins> <small class="woocommerce-price-suffix">(税込/送料無料)</small></span></div>');

					} else {

						jQuery('.single-product-right .single_variation_wrap .single_variation .woocommerce-variation-price').remove();
						jQuery('.single-product-right .single_variation_wrap .single_variation').append('<div class="woocommerce-variation-price"><span class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">¥</span>'+priceNumNearTitle+'</bdi></span> <small class="woocommerce-price-suffix">(税込/送料無料)</small></span></div>');

					}

				}

				// Update the initial variation ID
				initialVariationID = newVariationID;
			}
		});
	} 

	/**
	 * Monitor changes to add to cart
	 */
	var addToCartButton = jQuery('.single_add_to_cart_button');
	var observer = new MutationObserver(function(mutationsList) {
        for (var mutation of mutationsList) {
          if (mutation.attributeName === 'class') {
			var isDisabled = mutation.target.classList.contains('disabled');

            var topbarAddToCart = jQuery('.single-product-topbar-addtocart');
			if (isDisabled) {
				topbarAddToCart.addClass('disabled');
			} else {
				topbarAddToCart.removeClass('disabled');
			}
          }
        }
    });

    observer.observe(addToCartButton[0], { attributes: true });

	/**
	 * If button is disabled - try to salvage
	 */
	jQuery(window).load(function(){

		let counter = 0;

		/*
		let interval = setInterval(() => {

			if (20 !== counter) {

				if(jQuery(".single-product-right .single_variation_wrap .single_add_to_cart_button").hasClass("disabled")) {

					jQuery('.single-product-right .variation-wrap .tawcvs-swatches').each(function(index) {
						
						setTimeout(() => {
							// Find the first swatch that does not have the class 'selected' or 'disabled' using jQuery
							var firstUnselectedSwatch = jQuery(this).find('.swatch:not(.selected):not(.disabled)').first();

							// If we found an unselected swatch, trigger a click event on it using jQuery
							if (firstUnselectedSwatch.length > 0) {
								firstUnselectedSwatch.trigger('click');
							}
						}, index * 200); // Delay each iteration by 0.1 seconds (100 milliseconds) 

					});
					
				} else { 
					clearInterval(interval)
				}

				counter++;
				
			} else {
				clearInterval(interval);
			}

		}, 500);
		*/

		counter = 0;
		intervalalt = setInterval(() => {

			if (20 !== counter) {

				if(jQuery(".single-product-right .single_variation_wrap .single_add_to_cart_button").hasClass("disabled")) {
							
					jQuery(".reset_variations").trigger('click');

					// Look for items that is "totally out of stock"
					setTimeout(function() {
						jQuery('.tawcvs-swatches .swatch.disabled').each(function() {
							if (jQuery(this).closest('.tawcvs-swatches').data('attribute_name') !== 'attribute_pa_color') {
								jQuery(this).addClass('totally-out-of-stock');
							} 
						});
					}, 200);

					setTimeout(function() {
						jQuery('.single-product-right .variation-wrap .tawcvs-swatches').each(function(index) {
								
							setTimeout(() => {
								// Find the first swatch that does not have the class 'selected' or 'disabled' using jQuery
								var firstUnselectedSwatch = jQuery(this).find('.swatch:not(.selected):not(.disabled)').first();
			
								// If we found an unselected swatch, trigger a click event on it using jQuery
								if (firstUnselectedSwatch.length > 0) {
									firstUnselectedSwatch.trigger('click');
								}
							}, index * 200); // Delay each iteration by 0.1 seconds (100 milliseconds) 

						});
					}, 200);
								
				} else {
					clearInterval(intervalalt)
				}

				counter++;

			} else {
				clearInterval(intervalalt)
			}
		}, 500);

	});

	/**
	 * Paidy
	 */
	jQuery(".single-product-wrapper .single_variation_wrap .woocommerce-variation-add-to-cart .variation_id").each(function(i, obj) {
			
		var target = jQuery(this);
		var config = { characterData: true, childList: true, attributes: true};

		var observer = new MutationObserver(function(mutations) {

			mutations.forEach ( function (mutation) {
		
				let priceNormal = jQuery(".single-product-wrapper .single_variation_wrap .woocommerce-Price-amount bdi").first().clone().children().remove().end().text();
				let priceDiscount = jQuery(".single-product-wrapper .single_variation_wrap ins .woocommerce-Price-amount bdi").first().clone().children().remove().end().text();

				if(priceDiscount == "")
					price = priceNormal;
				else
					price = priceDiscount;
					
				price = parseFloat(price.replace(/,/g, ''));

				if(!isNaN(price)) {	

					jQuery("._paidy-promotional-messaging").attr("data-amount", price);
					_paidy && _paidy("pm:refresh");
					jQuery("._paidy-promotional-messaging").css("width", "100%");
						
					if(price > 3000) {
						jQuery("._paidy-promotional-messaging div").show();
					} else {
						jQuery("._paidy-promotional-messaging div").hide();
					}
				}

			} );

		});
	
		observer.observe(target[0], config);
	});

	/**
	 * Related Products
	 */
	$j( 'body' ).on( 'click', '.related.products .pa_color .swatch', function() {
		var formEl = $j( this ).parents( '.variations_form' );
		var formAction = formEl.attr( 'action' );
		var selectedColor = $j( this ).attr( 'data-value' );

		var formActionWithoutParameter = formAction.split('?')[0];

		formEl.attr( 'action', formActionWithoutParameter + '?attribute_pa_color=' + selectedColor );

		$j( this ).closest( '.product-type-variable ' ).find('a').each(function(index){
			$j( this ).attr("href", formActionWithoutParameter + '?attribute_pa_color=' + selectedColor);
		});
	})

	/**
	 * VR
	 */

	if(window.innerWidth <= 767) {
		if (isIOS()) {
			jQuery(".vr-ios").css('display', 'block');
		} else {
			jQuery(".vr-android").css('display', 'block');
		} 
	} else {
		jQuery(".vr-others").css('display', 'block');
	}

	/**
	 * Recreate dogfinder select function from /dog-lp-resources/main_dev.js
	 */		 
	// Open/Close Select Box
		jQuery( 'body' ).on( 'click', '.choices', function() {
			var thisEl = jQuery( this );		
			thisEl.toggleClass( 'is-open' );
			thisEl.find( '.choices__list--dropdown' ).toggleClass( 'is-active' );
		});

	// Hover Effect on Option
		$j( 'body' ).on( 'mouseover', '.choices__list .choices__item', function() {
			$j( this ).addClass( 'is-highlighted' ).siblings().removeClass( 'is-highlighted' );
		});

	// Click Option
		$j( 'body' ).on( 'click', '.choices__list .choices__item', function() {		
			var parentEl = jQuery( this ).parents( '.choices' ); // Parent wrapper of (Fake) Select Box
			var previewEl = parentEl.siblings( '#dogfinder' ); // Div to display data
			var dataValue = jQuery( this ).attr( 'data-value' ); // Selected option value
			var optionText= jQuery( this ).html(); // Selected option name

			// Put current selected value on fake select
				var currentOptionDisplay = parentEl.find( '.choices__list--single .choices__item' );
				currentOptionDisplay.attr( 'value', dataValue );
				currentOptionDisplay.html( optionText );
			
			// Put value into actual select
				var dogfinderOption = parentEl.find( '#df-select option' );
				dogfinderOption.attr( 'value', dataValue );
				dogfinderOption.html( optionText );

			// Show Data (Original Code)
				previewEl.find( '#df-breed' ).html( dogData[dataValue].breed );
				previewEl.find( '#df-height' ).html( dogData[dataValue].height + 'CM' );
				previewEl.find( '#df-weight' ).html( dogData[dataValue].weight + 'KG' );
				previewEl.find( '#df-info' ).html( dogData[dataValue].text );	

				if(!dogData[dataValue].height || dogData[dataValue].height === "") {
					previewEl.find('.dogfinder-left').addClass('hidden');
					previewEl.find('.dogfinder-right').removeClass('vc_col-sm-6');
				} else {
					previewEl.find('.dogfinder-left').removeClass('hidden');
					previewEl.find('.dogfinder-right').addClass('vc_col-sm-6');
				}

				var bedUrl = bedData.genericUrl;
				var buttonText = 'Order now';				

				switch (dogData[dataValue].size) {
					case 'xxlarge':
						bedUrl = bedData.xxlargeUrl;
						break;

					case 'large':
						bedUrl = bedData.largeUrl;
						break;

					case 'medium':
						bedUrl = bedData.mediumUrl;
						break;

					case 'small':
						bedUrl = bedData.smallUrl;
						break;

					default:
						bedUrl = bedData.genericUrl;
				}				

				previewEl.find( '#df-order-button a ').attr( 'href', bedUrl );
				previewEl.find( '#df-dog-size' ).html( dogData[dataValue].size );
				previewEl.find( '#df-bed-size' ).html( dogData[dataValue].size );
				previewEl.removeClass( 'dog-xs dog-small dog-medium dog-large dog-generic dog-xxlarge' )
				previewEl.addClass('dog-' + dogData[dataValue].size); 
		});

		// Try to get the thumbnail if the clicked element is out of stock
		// .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_color"] .swatch.disabled
		// .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_color"] .swatch.disabled, .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_size"] .swatch.disabled, .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_bed-selection"] .swatch.disabled, .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_cover-selection"] .swatch.disabled
		$j('body').on('click', '.single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_color"] .swatch.disabled', function() {

			$j(this).addClass('selected').siblings().removeClass('selected');
			$j(this).parent().siblings().find('.swatch').removeClass('selected');

			// Extract data from the clicked element
			var attributeValue = $j(this).data('value');
			var attributeName = $j(this).closest('.tawcvs-swatches').data('attribute_name');
			var productId = $j('.product.type-product').attr('id');
			productId = productId.replace('post-', '');
	 
			// Get other selected attributes excluding the current attribute_name
			var selectedAttributes = {};
			selectedAttributes[attributeName] = attributeValue;
			$j('.single-product-wrapper .variation-selector select').each(function() {
				var attribute = $j(this).data('attribute_name');
				var value = $j(this).val();
				console.log(value);
				if (attribute != attributeName) {
					selectedAttributes[attribute] = value;
				}
			});
	
			// Prepare data to be sent via Ajax
			var requestData = {
				action: 'get_thumbnail_for_variation', // WordPress Ajax action
				product_id: productId,
				selected_attributes: selectedAttributes
			};

			jQuery('select[name="' + attributeName + '"]').val('').trigger('change');

			var loaderimage = $j('<img src="https://ambientlounge.co.jp/wp-content/uploads/2024/04/loader.gif" class="galleryloader"/>');
			$j('.woocommerce-product-gallery__wrapper > div:first-child > a > img:first-child').css('opacity', '0').after(loaderimage);
	
			// Send Ajax request to WordPress
			// 'https://ambientlounge.co.jp/wp-admin/admin-ajax.php'
			$j.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php', // WordPress Ajax URL
				data: requestData,
				success: function(response) {
					if (!response.error) {
						$j(this).addClass("selected");
						$j(".galleryloader").remove();
						$j('.woocommerce-product-gallery__wrapper > div:first-child > a > img:first-child')
							.attr('src', response.url)
							.removeAttr("srcset")
							.css("opacity", "100");
						$j('.woocommerce-product-gallery__wrapper > div:first-child > img')
							.attr('src', response.url)
							.removeAttr("srcset");
						$j('.single-product-topbar-left > img')
							.attr('src', response.url)
							.removeAttr("srcset");
						$j('.single-product-topbar-price').html(response.price);
						$j('.woocommerce-variation-price .price').html(response.price);
						$j('.single-product-topbar-addtocart').addClass('disabled');
						$j('.single_add_to_cart_button').attr('disabled', 'disabled');			
					} else {
						$j('.single-product-topbar-price').html("");
						$j('.woocommerce-variation-price .price').html("");
						$j('.single-product-topbar-addtocart').addClass('disabled');
						$j('.single_add_to_cart_button').attr('disabled', 'disabled');	
						console.error(response.error);
					}
				}, 
				error: function(error) {
					console.error(error);
				}
			}); 
		});

		$j('body').on('click', '.single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_color"] .swatch:not(.disabled)', function() {
			
			var realImage = $j('.woocommerce-product-gallery__wrapper > div:first-child > a').attr("href");
			$j('.woocommerce-product-gallery__wrapper > div:first-child > a > img:first-child')
							.attr('src', realImage)
							.removeAttr("srcset")
							.css("opacity", "100");
							
		});

		// .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_color"] .swatch:not(.disabled)
		// .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_color"] .swatch:not(.disabled), .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_size"] .swatch:not(.disabled), .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_bed-selection"] .swatch:not(.disabled), .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_cover-selection"] .swatch:not(.disabled)
		$j('body').on('click', ' .single-product-wrapper .tawcvs-swatches[data-attribute_name="attribute_pa_color"] .swatch:not(.disabled)', function() {
			$j('.single-product-topbar-addtocart').removeClass('disabled');
			$j('.single_add_to_cart_button').removeAttr('disabled');
		});

		// Check if 'nonwaterresistant-color' is not found
		if (jQuery('.single_product_right [data-attribute_name="attribute_pa_color"] .nonwaterresistant-color').length === 0) {
			jQuery('label[for="pa_color"]').hide();
		}

		// Flex Slider
		jQuery('.product-images-carousel').flexslider({
			animation: "slide",
			animationLoop: false,			
			controlNav: "thumbnails",
			prevText:'',
			nextText:'',
			//asNavFor: '.product-images-carousel-thumb',
			init: function (slider) {
				// lazy load
				jQuery("img.lazy").slice(0,2).each(function () {
					var src = jQuery(this).attr("data-src");
					jQuery(this).attr("src", src).removeAttr("data-src").removeClass("lazy");
				});
			},
			before: function (slider) {
				// lazy load
				jQuery("img.lazy").slice(0,2).each(function () {
					var src = jQuery(this).attr("data-src");
					jQuery(this).attr("src", src).removeAttr("data-src").removeClass("lazy");
				});
			}
		});

});  

function isIOS() {
	return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
}