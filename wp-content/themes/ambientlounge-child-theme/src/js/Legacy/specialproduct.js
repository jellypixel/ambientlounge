var $j = jQuery.noConflict();
 
$j(function(){
    /* Deluxe Info */
        var parentWrapper = $j( '.variations_form' );
        var deluxeInfo = parentWrapper.find( '.variation-wrap.pa_deluxe_info' );
        var bedCoverSelector = parentWrapper.find( '.variation-wrap.pa_cover-selection' );
        var bedFillingSelector = parentWrapper.find( '.variation-wrap.pa_filling' );
        var bedPackageSelector = parentWrapper.find( '.tawcvs-swatches[data-attribute_name=attribute_pa_product-type]' );

        isDeluxe();

        $j( 'body' ).on( 'click', bedPackageSelector.find( 'swatch' ), function() {
            isDeluxe();
        });

        function isDeluxe() {        
            if ( bedPackageSelector.find( '.swatch-delux' ).hasClass( 'selected' ) ) {
                
                if(parentWrapper.find(".swatch-grey-base").hasClass("selected")) {
                    parentWrapper.find(".pa_deluxe_info.blue-cover").slideUp(400);
                    parentWrapper.find(".pa_deluxe_info.beige-cover").slideUp(400);
                    parentWrapper.find(".pa_deluxe_info.grey-cover").slideDown(400);
                } 
                
                if(parentWrapper.find(".swatch-beige-base").hasClass("selected")) {
                    parentWrapper.find(".pa_deluxe_info.blue-cover").slideUp(400);
                    parentWrapper.find(".pa_deluxe_info.beige-cover").slideDown(400);
                    parentWrapper.find(".pa_deluxe_info.grey-cover").slideUp(400);
                } 
                
                if(parentWrapper.find(".swatch-blue-base").hasClass("selected")) {
                    parentWrapper.find(".pa_deluxe_info.blue-cover").slideDown(400);
                    parentWrapper.find(".pa_deluxe_info.beige-cover").slideUp(400);
                    parentWrapper.find(".pa_deluxe_info.grey-cover").slideUp(400);
                }     
                
                bedCoverSelector.hide();
                bedFillingSelector.hide();
            }
            else {
                deluxeInfo.slideUp( 400 );

                bedCoverSelector.show();
                bedFillingSelector.show();
            }
        }

        deluxeInfo.find( '.info-link a').click( function(e) {
            e.stopPropagation();            
        })

    /* Modal */
        var modalOverlay = $j( '.modal-overlay' );
        var modalWrapper = $j( '.modal-wrapper' );
        var modalContentList = $j( '.modal-content-list' );
        var modalContentInner = $j( '.modal-content-inner' );

        // Append Info Link - Modal Trigger
            $j( '.variation-wrap.pa_filling .variation-label label' ).append( '<div class="info-link"><a id="openmodal-filling" href="#content-filling">フィリングの種類<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.04688 8.95312C7.11794 8.95312 8.79688 7.27419 8.79688 5.20312C8.79688 3.13206 7.11794 1.45312 5.04688 1.45312C2.97581 1.45312 1.29688 3.13206 1.29688 5.20312C1.29688 7.27419 2.97581 8.95312 5.04688 8.95312ZM5.04688 9.65625C7.50627 9.65625 9.5 7.66252 9.5 5.20312C9.5 2.74373 7.50627 0.75 5.04688 0.75C2.58748 0.75 0.59375 2.74373 0.59375 5.20312C0.59375 7.66252 2.58748 9.65625 5.04688 9.65625Z" fill="#888888"/><path d="M4.62063 5.72391H5.35188C5.26188 4.94766 6.49375 4.84641 6.49375 4.02516C6.49375 3.27703 5.8975 2.89453 5.08187 2.89453C4.48 2.89453 3.985 3.17016 3.625 3.58641L4.08625 4.00828C4.36187 3.72141 4.64313 3.56391 4.98625 3.56391C5.43063 3.56391 5.70063 3.75516 5.70063 4.09828C5.70063 4.64391 4.5025 4.84078 4.62063 5.72391ZM4.99188 7.22578C5.27313 7.22578 5.48688 7.01766 5.48688 6.72516C5.48688 6.43266 5.27313 6.23016 4.99188 6.23016C4.705 6.23016 4.49125 6.43266 4.49125 6.72516C4.49125 7.01766 4.69937 7.22578 4.99188 7.22578Z" fill="#888888"/></svg></a></div>' );

        // Click Trigger
            $j( 'body' ).on( 'click', '#openmodal-filling, #openmodal-filling-deluxe, #openmodal-trial, #openmodal-deliverydays', function(e) {
                e.preventDefault();

                openModal( $j( this ).attr( 'href' ) );                
            });

        // Close
            $j( 'body' ).on( 'click', '.modal-close', function(e) {
                e.stopPropagation();

                closeModal();
            });

            $j( 'body' ).on( 'click', '.modal-overlay', function(e) {
                if ( $j( e.target ).is( '.modal-overlay' ) ) {
                    closeModal();
                } 
            });

        function openModal( contentClass ) {
            modalContentInner.html( modalContentList.find( contentClass ).prop("outerHTML") );
            modalWrapper.attr({ 'name' : contentClass.substring(1) })            

            modalOverlay.fadeIn(400, function() {
                modalWrapper.fadeIn(200, function() {
                    modalWrapper.css({ 'display' : 'flex' })
                });
            })
        }

        function closeModal() {
            modalWrapper.fadeOut(400, function() {
                modalOverlay.fadeOut(200, function() {
                    modalContentInner.empty();

                    modalWrapper.attr({ 'name' : '' })
                });
            })
        }

    /* Info Link */
        // Append on Swatch Bed Selection
            $j( '.variation-wrap.pa_bed-selection .variation-label label' ).append( '<div class="info-link"><a href="https://ambientlounge.co.jp/products/swatch/pet-swatch-order/" target="_blank" title="生地サンプル請求">生地サンプル請求<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"/><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"/></svg></a></div>' );

        // Append on Swatch Cover Selection
            $j( '.variation-wrap.pa_cover-selection .variation-label label' ).append( '<div class="info-link"><a href="https://ambientlounge.co.jp/products/swatch/pet-swatch-order/" target="_blank" title="生地サンプル請求">生地サンプル請求<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"/><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"/></svg></a></div>' );

    /* Pricing */
        function capitalize(s)
        { 
            try {
                if (typeof(s[0]) != "undefined") {
                    return s[0].toUpperCase() + s.slice(1);
                } else {
                    return "";
                }
            } catch (exceptionVar) {
                return "";
            } 
        }

        // Sticky Price
        if(parentWrapper.length) {
            jQuery(".sticky-price-name").html(jQuery(".product_title").html());

            jQuery(document).on("click", ".sticky-price-action .button", function() {
                jQuery(".single_add_to_cart_button").trigger("click");
            });

            jQuery.ajaxPrefilter( function( options, originalOptions, jqXHR ) {

                if ( originalOptions.url.includes('get_variation')) {
                
                    var orignalSuccess = originalOptions.success;
                    
                    options.success = function( response ) {
            
                        if ( orignalSuccess && 'function' === typeof orignalSuccess ) {
                            orignalSuccess( response );
                        }

                        if(typeof(response.display_price) != "undefined") {
                            jQuery("._paidy-promotional-messaging").attr("data-amount", response.display_price);
                            _paidy && _paidy("pm:refresh");
                            jQuery("._paidy-promotional-messaging").css("width", "100%");

                            var attrTxt = "";
                            for(var key in response.attributes) {

                                if(jQuery('.tawcvs-swatches[data-attribute_name="'+key+'"] .swatch-image.selected img').length) {
                                    attrTxt = attrTxt + jQuery('.tawcvs-swatches[data-attribute_name="'+key+'"] .swatch-image.selected img').attr("alt") + " / ";
                                }

                                if(jQuery('.tawcvs-swatches[data-attribute_name="'+key+'"] .swatch-label.selected').length) {
                                    attrTxt = attrTxt + jQuery('.tawcvs-swatches[data-attribute_name="'+key+'"] .swatch-label.selected')[0].childNodes[0].nodeValue + " / ";
                                }

                                if(jQuery('.tawcvs-swatches[data-attribute_name="'+key+'"] .swatch.selected .swatch-label').length) {
                                    attrTxt = attrTxt + jQuery('.tawcvs-swatches[data-attribute_name="'+key+'"] .swatch.selected .swatch-label')[0].childNodes[0].nodeValue + " / ";
                                }
                            }
                            attrTxt = capitalize(attrTxt.slice(0, -3));

                            jQuery(".sticky-price-options").html(attrTxt);
                            jQuery(".sticky-price-cost").html(response.price_html);
                            jQuery(".sticky-price-attribute").html("選択中の商品");
                            jQuery(".sticky-price-action").show();

                            if(response.image.url != "")
                                jQuery(".sticky-price-photo img").attr("src", response.image.url);
                            else 
                                jQuery(".sticky-price-photo img").attr("src", jQuery("#product-images .flickity-slider div:first-child").attr("data-thumb"));
                        } else {
                            jQuery(".sticky-price-options").html("");
                            jQuery(".sticky-price-cost").html("");
                            jQuery(".sticky-price-photo img").attr("src", "");
                            jQuery(".sticky-price-action").hide();
                            jQuery(".sticky-price-attribute").html("商品の選択に一致するものがありません。別の組み合わせを選択してください。");
                        }
                        
                    };
                }
            } );
        } else {
            // Paidy
            jQuery.ajaxPrefilter( function( options, originalOptions, jqXHR ) {

                if ( originalOptions.url.includes('get_variation')) {
                
                    var orignalSuccess = originalOptions.success;
                    
                    options.success = function( response ) {
            
                        if ( orignalSuccess && 'function' === typeof orignalSuccess ) {
                            orignalSuccess( response );
                        }
    
                        if(typeof(response.display_price) != "undefined") {
                            jQuery("._paidy-promotional-messaging").attr("data-amount", response.display_price);
                            _paidy && _paidy("pm:refresh");
                            jQuery("._paidy-promotional-messaging").css("width", "100%");
                        }
                    };
                }
            } );
        }
  
        // Paidy
        jQuery(".single-product-wrapper .single_variation_wrap .woocommerce-variation-add-to-cart .variation_id").each(function(i, obj) {
           
            var target = jQuery(this);
            var config = { characterData: true, childList: true, attributes: true};

            var observer = new MutationObserver(function(mutations) {

                mutations.forEach ( function (mutation) {
            
                    let price = jQuery(".single-product-wrapper .single_variation_wrap ins .woocommerce-Price-amount bdi").clone().children().remove().end().text();

                    if(price == '') {
                        price = jQuery(".single-product-wrapper .single_variation_wrap .woocommerce-Price-amount bdi").clone().children().remove().end().text();
                    }
                    price = parseFloat(price.replace(/,/g, ''));
                    
                    jQuery("._paidy-promotional-messaging").attr("data-amount", price);
                    _paidy && _paidy("pm:refresh");
                    jQuery("._paidy-promotional-messaging").css("width", "100%");

                } );

            });
           
            observer.observe(target[0], config);

        });

    /* Cart */
        jQuery('.woocommerce-cart .item-variation-name:contains("カバーを選択:")').parent().hide();
        jQuery('.woocommerce-cart .item-variation-name:contains("フィリングを選択:")').parent().hide();

});