var $j = jQuery.noConflict();

var vpHeight = $j( window ).height();
var headerHeight = 0;
var headerEl;
var footerTop = $j( '#footer-widgets' ).position().top;
var flexslider = { vars:{} };

$j(function(){	
	// Search
		$j( '.search-field' ).attr( 'placeholder', 'Search...' );

		var siteHeader = $j( '.site-header' );
		var headerSearchWrapper = $j( '.header-search-wrapper' );
		var headerSearchInner = $j( '.header-search-inner' );
		var headerSearchFormWrapper = $j( '.header-search-form-wrapper' );
		var headerSearchInput = headerSearchWrapper.find( '#header-search-input' );

		$j( '#menu-search-wrapper a' ).click( function() {		
			if ( siteHeader.hasClass( 'search-open' ) )
			{
				closeSearch();
			}
			else 
			{			
				openSearch();
			}
		})

		/*$j( 'body' ).on( 'click', '.modal-close', function(e) {
			e.stopPropagation();

			closeModal();
		});*/

		$j( 'body' ).on( 'click', '.header-search-wrapper', function(e) {
			if ( $j( e.target ).is( '.header-search-wrapper' ) ) {
				closeSearch();
			} 
		});

		function openSearch() {									
			headerSearchWrapper.fadeIn( 200, function() {
				headerSearchInner.slideDown( 200, function() {
					headerSearchFormWrapper.fadeIn( 600, function() {
						headerSearchInput.focus();
					});
				});	
			});			

			siteHeader.addClass( 'search-open' );
		}

		function closeSearch() {			
			headerSearchInner.slideUp( 200, function() {
				headerSearchWrapper.fadeOut( 200 );
				headerSearchFormWrapper.hide();
			});		

			siteHeader.removeClass( 'search-open' );
		}

	// Megamenu 		
		var submenuContentWrapper = $j( '.submenu-content-wrapper' );

		$j( '.submenu-title' ).click( function(){
			var submenuContentTargetID = $j( this ).attr( 'data-target' );

			$j( this ).toggleClass( 'active' )
				.siblings()
					.removeClass( 'active' );
			
			submenuContentWrapper.find( $j( submenuContentTargetID ) )
				.addClass( 'active')
					.siblings()
						.removeClass( 'active' );
		})		

	// Mobile Menu
		// Root Menu - Tabbing
		var primaryMenu = $j( '.desktop-primary-menu' );

		$j( '.mobile-menu-title' ).click( function() {
			var menuContentTargetID = $j( this ).attr( 'data-target' );

			if ( $j( this ).hasClass( 'active' ) ) {
				$j( this ).removeClass( 'active' );
			}
			else {
				$j( this ).addClass( 'active' );
			}

			$j( this )
				.siblings()
					.removeClass( 'active' );
			
			primaryMenu.find( $j( menuContentTargetID ) )
				.addClass( 'active')
					.siblings()
						.removeClass( 'active' );
		});

		// Sub Menu (Open)
		$j( '.submenu-accordion-title' ).click( function() {
			var thisEl = $j( this ).parent( '.submenu-accordion-wrapper' );			
			thisEl.siblings( '.active' ).removeClass( 'active' );
			thisEl.toggleClass( 'active' );
		});

		// Sub Menu (Close)
		$j( '.submenu-accordion-content > .submenu-accordion-title' ).click( function() {
			var thisEl = $j( this ).parents( '.submenu-accordion-wrapper' );

			thisEl.siblings( '.active' ).removeClass( 'active' );
			thisEl.toggleClass( 'active' );
		});

		// Sub Menu Level 2 - Accordion
		$j( '.sub-submenu-accordion-title' ).click( function() {
			var thisEl = $j( this ).parent( '.sub-submenu-accordion-wrapper' );
			
			if ( thisEl.hasClass( 'active' ) ) 
			{
				thisEl.find( '.sub-submenu-accordion-content' ).slideUp( 400, function() {
					thisEl.removeClass( 'active' );
				});
			}
			else 
			{
				thisEl.find( '.sub-submenu-accordion-content' ).slideDown( 400 );
				thisEl.addClass( 'active' );
			}
		});

		// Burger bar on click		
		$j( '.burger-wrapper' ).click( function() {
			$j( this )
				.toggleClass( 'active' )
				.siblings( '#site-navigation' ).toggleClass( 'active' );
		});

		// Reset first submenu tab on entering mobile menu
		resetMobileMenu();
		
	// Sticky
		headerEl = $j( 'header' );
		if( headerEl.length && headerEl.hasClass( 'stickyheader' ) ) 
		{
			var topBar = $j( '.topbar-wrapper' ).outerHeight();			
			var headerBar = $j( '.header-wrapper' ).outerHeight();
			topBar = topBar ? topBar : 0;
			headerBar = headerBar ? headerBar : 0;
			headerHeight = topBar + headerBar;
			
			if( $j( '.stickyheader:not(.transparentheader)' ).length ) {			
				$j( 'body' ).css({ 'padding-top' : headerHeight });
			}
		}
		
	// Search
		$j( 'body' ).on( 'click', '.search-wrapper svg', function() {
			headerEl.toggleClass( 'searchopen' );
			
			$j( '.search-input' ).focus();
		});
		
	// Gutenberg Block Image - Fit Container
		$j( '.is-style-fit-container' ).parent( '.wp-block-column' ).css({ 'position' : 'relative' });
	
	// To Top	
		$j( '#toTop, .toTop' ).click( function(e) {
			e.preventDefault();
			$j( 'html, body' ).animate({
				scrollTop: 0
			}, 2000);
		});
		
	// Category and Year Filter
		$j( '#cat' ).change( function(e) {			
			e.preventDefault();
			//$j( this ).parent( 'form' ).submit();			
			window.location.href = $j( this ).val();	
		});
		
	// Areas of Work Carousel
		$j('.carousel-wrapper').flexslider({
			animation: "slide",
			animationLoop: false,
			itemWidth: 210,
			itemMargin: 50,
			minItems: getGridSize(), 
			maxItems: getGridSize(),
			prevText: '',
			nextText: '',
			controlNav: false,
			directionNav: true
		});
		
	// Filter List - Convert from Gutenberg
		if ( $j( '.faculty-sidebar' ).length ) {
			var thisEl = $j( '.faculty-sidebar' );	
			thisEl.addClass( 'filter-wrapper' );
			
			thisEl.children().not( 'h4' ).wrapAll( '<div class="filter-content"></div>' );
			//thisEl.children( 'h4' ).wrap( '<div class="filter-header"></div>' );
			//thisEl.children( '.filter-header' ).append( '<div class="filter-icon"></div>' );
			thisEl.prepend( '<div class="filter-header"><h2>' + thisEl.children( 'h4' ).html() +'</h2><div class="filter-icon"></div></div>' );
			thisEl.children( 'h4' ).remove();
		}
		
	// Filter List - General
		$j( 'body' ).on( 'click', '.filter-icon', function() {
			var filterWrapper = $j( this ).parents( '.filter-wrapper' );
			var filterContent = $j( '.filter-content' );
			
			if ( filterWrapper.hasClass( 'open' ) ) {
				filterContent.slideUp(200, function() {
					filterWrapper.removeClass( 'open' );	
				});				
			}
			else {
				filterContent.slideDown(400, function() {
					filterWrapper.addClass( 'open' );	
				});	
			}
		});

	// Styling Input File
		$j( '#photo-attachment-trigger' ).each( function()
		{
			var attachmentFileInput = $j( this ).find( '#photo-attachment' );
			var attachmentLabel	= $j( this ).siblings( '#photo-attachment-label' );
			var attachmentLabelValue = attachmentLabel.children( 'span' ).html();

			attachmentFileInput.on( 'change', function( e ) {
				var fileName = '';

				if( this.files && this.files.length > 1 ) 
				{
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				}
				else if( e.target.value )
				{
					fileName = e.target.value.split( '\\' ).pop();
				}

				if( fileName ) 
				{
					attachmentLabel.html( '<span>' + fileName + '</span><span id="photo-attachment-remove"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" aria-hidden="true" focusable="false"><path d="M12 13.06l3.712 3.713 1.061-1.06L13.061 12l3.712-3.712-1.06-1.06L12 10.938 8.288 7.227l-1.061 1.06L10.939 12l-3.712 3.712 1.06 1.061L12 13.061z"></path></svg></span>' );

					var reader = new FileReader();				
					reader.onload = function (e) {
						$j( '#photo-attachment-preview' ).html( '<img src="' + e.target.result + '">').addClass( 'file-added' );
					}

					reader.readAsDataURL( this.files[0] );
				}
				else 
				{
					attachmentLabel.html( attachmentLabelValue );
				}
			});

			// FF fix
			attachmentFileInput
				.on( 'focus', function(){ attachmentFileInput.addClass( 'has-focus' ); })
				.on( 'blur', function(){ attachmentFileInput.removeClass( 'has-focus' ); });
		});

		$j( 'body' ).on( 'click', '#photo-attachment-remove', function() {
			var triggerEl = $j( this ).parent( '#photo-attachment-label' ).siblings( '#photo-attachment-trigger' );

			triggerEl.find( '#photo-attachment-preview' )
				.removeClass( 'file-added' )
				.empty();

			triggerEl.find( '#photo-attachment' ).val( '' );

			$j( this ).siblings( 'span' ).html( 'No file chosen' );
			$j( this ).remove();
		})

	// Styling Input Number 
		$j( document ).on( 'click', '.inputqty-wrapper .input-minus', function() {
			var parentEl = $j( this ).parent( '.inputqty-wrapper' );
			var numberEl = parentEl.find( 'input[type=number]' );
			var finalNumber = parseInt( numberEl.val(), 10 ) - 1;
			if ( finalNumber < 1 ) {
				finalNumber = 1;
			}
			numberEl.val( finalNumber ).trigger("change");
		});
		
		$j( document ).on( 'click', '.inputqty-wrapper .input-plus', function() {
			var parentEl = $j( this ).parent( '.inputqty-wrapper' );
			var numberEl = parentEl.find( 'input[type=number]' );
			var finalNumber = parseInt( numberEl.val(), 10 ) + 1;
			numberEl.val( finalNumber ).trigger("change");
		});

	// Shortcuts - Scroll to section
		$j( '.shortcuts .wp-block-button__link' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).parent().attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 64
			}, 2000);
		});

	// Shortcuts - Scroll to section
		$j( '.shortcuts-row p' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 112
			}, 1000);
		});

	// Block - Accordion
		// Open first accordion item on first accordion group
		$j( '.accordion-block-wrapper' ).first().children( '.accordion-item-wrapper' ).first().addClass( 'active' ).children( '.accordion-item-content' ).show();

		$j( '.accordion-item-title' ).click( function() {
			var thisEl = $j( this );
			var parentEl = $j( this ).parent( '.accordion-item-wrapper' );

			if ( parentEl.hasClass( 'active' ) ) 
			{
				thisEl.siblings( '.accordion-item-content' ).slideUp( 400 );
			}
			else 
			{
				thisEl.siblings( '.accordion-item-content' ).slideDown( 400 );
			}

			parentEl.toggleClass( 'active' );
		});

	// Accordion Shortcuts - Scroll to section and open the accordion
		$j( '.accordion-shortcuts .wp-block-button__link' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).parent().attr( 'id' );				

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 128
			}, 2000, function() {
				if ( !$j( '.' + targetEl ).hasClass( 'active' ) ) {
					$j( '.' + targetEl ).addClass( 'active')
						.children( '.accordion-item-content' ).slideDown( 400 );
				}
			});
		});

		// Accordion Shortcut from URL
		if ( $j( '.accordion-shortcuts' ).length )
		{
			var targetEl = getURLParameter( 's' );	

			if ( targetEl !== '' )
			{
				$j( '.accordion-block-wrapper' ).find( '.accordion-item-wrapper' ).removeClass( 'active' ).children( '.accordion-item-content' ).hide();
				$j( '.' + targetEl ).addClass( 'active' ).children( '.accordion-item-content' ).show();
				
				$j([document.documentElement, document.body]).animate({
					scrollTop: $j( '.' + targetEl ).offset().top - 128
				}, 1000 );			
			}
		}	

	// Template Stores
		// Shortcuts - Scroll to section
			$j( '.store-list' ).click(function(e) {
				e.preventDefault();

				var targetEl = $j( this ).attr( 'id' );	

				$j([document.documentElement, document.body]).animate({
					scrollTop: $j( '.' + targetEl ).offset().top - 128
				}, 2000);
			});

		// Accordion
			$j( '.store-location-title' ).click( function() {
				var thisEl = $j( this );
				var parentEl = $j( this ).parent( '.store-location' );

				if ( parentEl.hasClass( 'active' ) ) 
				{
					thisEl.siblings( '.store-location-list' ).slideUp( 400 );
				}
				else 
				{
					thisEl.siblings( '.store-location-list' ).slideDown( 400 );
				}

				parentEl.toggleClass( 'active' );
			})

	// Template Collection
		// Fix gap when using column with specific width (%)
		$j( '.page-template-template-collection .compare-content > .wp-block-columns').each( function() {
			var columnCount = $j( this ).children( '.wp-block-column' ).length;
			var slimGap = $j( this ).hasClass( 'slim-gap' );
			var gap = 32;
			if ( slimGap ) { gap = 16; }

			$j( this ).children( '.wp-block-column' ).each( function() {
				var flexBasis = $j( this ).css( 'flex-basis' );				
			
				if ( flexBasis != null && flexBasis.slice(-1) == '%' ) {
					flexBasis = flexBasis.slice(0,-1);

					

					if ( flexBasis == 50)
					{	
						$j( this ).css({ 'flex-basis' : 'calc(' + flexBasis + '% - ' + ( gap / 2 ) + 'px)' });
					}
				}
			})
		})

	// Template Offers
		// Put class in swatch circle type parent
		$j( '.page-template-template-offers .swatch-shape-circle' ).closest( '.tawcvs-swatches' ).addClass( 'swatches-circle' );


	// Page - Professional - Stage 
		// HTML inside gutenberg button 
		$j( '.professional-stage-shortcuts .wp-block-button__link' ).each( function() {
			var thisEl = $j( this );
			var button_text = thisEl.text().replace( '(', '<span class="button-text-detail">(' );
			button_text = button_text.replace( ')', ')</span>' );
			
			thisEl.html( button_text );
		})

	// Page - FAQ
		// Shortcuts	
		$j( '.shortcut-columns p' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 128
			}, 2000);
		});

	// Woocommerce 
		// Checkout - Toggle form login link below title
			$j( '.checkout-registereduser').click( function() {
				$j( '.woocommerce-form-login' ).slideToggle( 400 );
			})

		// Checkout - move amazon pay button from woocommerce notification to below
			setTimeout( function() {
				$j( '#pay_with_amazon' ).prependTo( '.checkout-expresscheckout');
				$j( '.wc-amazon-payments-advanced-info' ).hide();
			}, 2000 );

		// Checkout - collapsible mobile sidebar 
			$j( '.checkout-sidebar-inner' ).click( function() {
				var sidebarEl = $j( this ).parents( '.checkout-sidebar' );
				var sidebarContentEl = sidebarEl.find( '.checkout-sidebar-innermost' );

				if ( sidebarEl.hasClass( 'collapsed' ) )
				{
					sidebarContentEl.slideDown( 400 );
				}
				else
				{
					sidebarContentEl.slideUp( 400 );
				}

				sidebarEl.toggleClass( 'collapsed' );
			})

		// Qty
			$j( '.minus-btn' ).click( function() {				
				var qtyEl = $j( this ).siblings( '.qty' );
				var qtyVal = parseInt( qtyEl.val(), 10 );
				
				if ( qtyVal > 1 ) {
					qtyEl.val( qtyVal - 1 );
				}				
			})

			$j( '.plus-btn' ).click( function() {				
				var qtyEl = $j( this ).siblings( '.qty' );
				var qtyVal = parseInt( qtyEl.val(), 10 );
				
				qtyEl.val( qtyVal + 1 );				
			})

			$j( '.qty' ).keypress( function(e) {
				if (e.which < 48 || e.which > 57)
				{
					e.preventDefault();
				}
			})

		// Related Products - Change link on choosing variation
			$j( 'body' ).on( 'click', '.related.products .variations_form .variation-wrap.pa_color .swatch', function() {
				var formEl = $j( this ).parents( '.variations_form' );
				var formAction = formEl.attr( 'action' );
				var selectedColor = $j( this ).attr( 'data-value' );

				var formActionWithoutParameter = formAction.split('?')[0];

				formEl.attr( 'action', formActionWithoutParameter + '?attribute_pa_color=' + selectedColor );
			})

		// Archive/Collection Page - Pop Up Filter
			var lightboxOverlay = $j( '.lightbox-overlay' );
			var lightboxEl = $j( '.lightbox-wrapper' );

			// Create a function to observe the DOM changes
			var attrAdded = false;

			function observeFilterDOM() {
				// Select the target node to observe
				var targetNode = document.body;

				// Create a new MutationObserver
				var observer = new MutationObserver(function(mutations) {
					// Check each mutation that occurred
					mutations.forEach(function(mutation) {
					// Check if any nodes were added
					if (mutation.addedNodes && mutation.addedNodes.length > 0) {
						// Check if the desired element now exists
						var element = jQuery('.wc-block-attribute-filter-list');
						if (element.length) {
						// Disconnect the observer
						observer.disconnect();

						// Execute your code
						if(!attrAdded) {
							$j('.wc-blocks-filter-wrapper .collection-filter-color .wc-block-components-checkbox .wc-block-components-checkbox__input').each(function() {
							var id = $j(this).attr('id');
							var swatch = $j('.swatch[data-value="' + id + '"]');
							if (swatch.length > 0) {
								var backgroundImage = window.productAttributeImageUrls[id];
								backgroundImage = backgroundImage.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
								if(jQuery(this).siblings('.wc-block-components-checkbox__label').find('img').length == 0) {
								jQuery(this).siblings('.wc-block-components-checkbox__label').prepend('<img src="' + backgroundImage + '">');
								}
							}
							});
				
							jQuery('.wp-block-woocommerce-filter-wrapper .collection-filter-color .style-list ul').each(function() {
							var waterResistantDiv = jQuery('<div class="water-resistant"></div>');
							var normalDiv = jQuery('<div class="normal"></div>');  
							
							jQuery(this).find('li').each(function() {
							// Remove li elements with class "show-more"
							if (jQuery(this).hasClass('show-more')) {
								jQuery(this).remove();
								return;
							}
							
							var checkbox = jQuery(this).find('.wc-block-checkbox-list__checkbox');
							var label = checkbox.find('label');
							
							if (label.attr('for') && label.attr('for').indexOf('waterresistant') !== -1) {
								waterResistantDiv.append(jQuery(this));
							} else {
								normalDiv.append(jQuery(this));
							}
							
							// Unhide li elements with hidden attribute
							if (jQuery(this).attr('hidden')) {
								jQuery(this).removeAttr('hidden');
							}
							});
							
							jQuery(this).append(normalDiv);
							jQuery(this).append(waterResistantDiv);
							});				  
				
							// Filter Title
							/*$j( '.wc-blocks-filter-wrapper .collection-filter-color' ).prepend( '<div class="collection-filter-title">カラー</div>');
							$j( '.wc-blocks-filter-wrapper .collection-filter-category' ).prepend( '<div class="collection-filter-title">タイプ</div>');
							$j( '.wc-blocks-filter-wrapper .collection-filter-price' ).prepend( '<div class="collection-filter-title">価格</div>');*/
				
							// Filter Color Type
							$j( '.wc-blocks-filter-wrapper .collection-filter-color .normal' ).prepend( '<div class="collection-filter-color-normal">インテリア</div>' );
							$j( '.wc-blocks-filter-wrapper .collection-filter-color .water-resistant' ).prepend( '<div class="collection-filter-color-water-resistant"><div class="collection-filter-color-water-resistant-image"></div><div class="collection-filter-color-water-resistant-heading"><div class="collection-filter-color-water-resistant-title">インテリア＋プラス</div><div class="collection-filter-color-water-resistant-subtitle">撥水性/褪色防止/引っ掻きに強い/ペットフレンドリー</div></div></div>' );
				
							attrAdded = true;
						}
						
						}
					}
					});
				});

				// Start observing the target node for DOM changes
				observer.observe(targetNode, { childList: true, subtree: true });
			}

			observeFilterDOM();

			$j( '.popup-filter ').click( function() {
				//openLightbox();
				slideLightbox();
			})

			$j( '.lightbox-overlay:not(.slide-lightbox) .lightbox-close').click( function(e) {
				e.preventDefault();
				closeLightbox();
			})

			lightboxOverlay.click( function(e) {
				 if ($j(e.target).has('.lightbox-wrapper').length) {
					closeLightbox();
				}
			})

			function openLightbox() {
				lightboxOverlay.fadeIn( 200, function() {
					lightboxEl.fadeIn(400);
				});
			}

			function closeLightbox() {
				lightboxEl.fadeOut( 200, function() {
					lightboxOverlay.fadeOut(400);
				});
			}

			function slideLightbox() {
				if ( lightboxOverlay.hasClass( 'active' ) )
				{
					lightboxEl.slideUp(400, function() {
						lightboxOverlay.hide().removeClass( 'active' );											
					});
					
				}
				else {
					lightboxOverlay.show().addClass( 'active' );
					lightboxEl.slideDown(400);
				}
			}


		// Attribute
			$j( 'body' ).on( 'click', '.attribute-selection', function() {
						
				let url = $j(this).parent().parent().find("a").attr("attr");

				$j( this )
					.addClass( 'selected' )
					.siblings()
						.removeClass( 'selected' );

				let priceString = addCommas( $j( this ).attr( 'price' ) ); priceString = priceString.replace("-", " - ¥");
				let saleString = addCommas( $j( this ).attr( 'sale' ) ); saleString = saleString.replace("-", " - ¥");

				if($j( this ).attr("sale")) {
					$j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-price' ).html( '<del>¥' + priceString + "</del>");
					$j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-sale' ).html( '¥' + saleString );
				} else {
					$j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-price' ).html( '¥' + priceString );
					$j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-sale' ).html( '' );
				}
					
				var linkEl = $j( this ).parents( '.productattribute-wrapper' ).children( 'a' );						
				var href = linkEl.attr( 'href' );			
				var buttonEl = $j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-action > a' );
				var targetSize = $j( this ).attr( 'value' );
				if ( href.indexOf( url ) > -1 )
				{							
					var regex = new RegExp('('+url+'=).*?(&|$)', "g");
					var new_href = href.replace(regex,'$1' + targetSize + '$2');

					linkEl.attr({ 'href' : new_href });
					buttonEl.attr({ 'href' : new_href });
				}
				else
				{
					if ( href.indexOf( '?' ) > -1 ) {
						var new_href = href + '&' + url + '=' + targetSize;
					
						linkEl.attr({ 'href' : new_href });
						buttonEl.attr({ 'href' : new_href });
					} else {
						var new_href = href + '?' + url + '=' + targetSize;
					
						linkEl.attr({ 'href' : new_href });
						buttonEl.attr({ 'href' : new_href });
					}
				}
			});

	// JP Plugin
	    /* Deluxe Info */
        var parentWrapper = $j( '.variations_form' );
        var deluxeInfo = parentWrapper.find( '.variation-wrap.pa_deluxe_info' );
        var bedCoverSelector = parentWrapper.find( '.variation-wrap.pa_cover-selection' );
        var bedFillingSelector = parentWrapper.find( '.variation-wrap.pa_filling' );
        var bedPackageSelector = parentWrapper.find( '.tawcvs-swatches[data-attribute_name=attribute_pa_product-type]' );

        isDeluxe();

        $j( 'body' ).on( 'click', bedPackageSelector.find( 'swatch' ), function() {
            isDeluxe();
        });

        function isDeluxe() {        
            if ( bedPackageSelector.find( '.swatch-delux' ).hasClass( 'selected' ) ) {
                
                if(parentWrapper.find(".swatch-grey-base").hasClass("selected")) {
                    parentWrapper.find(".pa_deluxe_info.blue-cover").slideUp(400);
                    parentWrapper.find(".pa_deluxe_info.beige-cover").slideUp(400);
                    parentWrapper.find(".pa_deluxe_info.grey-cover").slideDown(400);
                } 
                
                if(parentWrapper.find(".swatch-beige-base").hasClass("selected")) {
                    parentWrapper.find(".pa_deluxe_info.blue-cover").slideUp(400);
                    parentWrapper.find(".pa_deluxe_info.beige-cover").slideDown(400);
                    parentWrapper.find(".pa_deluxe_info.grey-cover").slideUp(400);
                } 
                
                if(parentWrapper.find(".swatch-blue-base").hasClass("selected")) {
                    parentWrapper.find(".pa_deluxe_info.blue-cover").slideDown(400);
                    parentWrapper.find(".pa_deluxe_info.beige-cover").slideUp(400);
                    parentWrapper.find(".pa_deluxe_info.grey-cover").slideUp(400);
                }     
                
                bedCoverSelector.hide();
                bedFillingSelector.hide();
            }
            else {
                deluxeInfo.slideUp( 400 );

                bedCoverSelector.show();
                bedFillingSelector.show();
            }
        }

        deluxeInfo.find( '.info-link a').click( function(e) {
            e.stopPropagation();            
        })

    	/* Modal */
			var modalOverlay = $j( '.modal-overlay' );
			var modalWrapper = $j( '.modal-wrapper' );
			var modalContentList = $j( '.modal-content-list' );
			var modalContentInner = $j( '.modal-content-inner' );

			// Append Info Link - Modal Trigger
				//$j( '.specialproduct-configurator .variation-wrap.pa_filling .variation-label label' ).append( '<div class="info-link"><a id="openmodal-filling" href="#content-filling">フィリングの種類<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.04688 8.95312C7.11794 8.95312 8.79688 7.27419 8.79688 5.20312C8.79688 3.13206 7.11794 1.45312 5.04688 1.45312C2.97581 1.45312 1.29688 3.13206 1.29688 5.20312C1.29688 7.27419 2.97581 8.95312 5.04688 8.95312ZM5.04688 9.65625C7.50627 9.65625 9.5 7.66252 9.5 5.20312C9.5 2.74373 7.50627 0.75 5.04688 0.75C2.58748 0.75 0.59375 2.74373 0.59375 5.20312C0.59375 7.66252 2.58748 9.65625 5.04688 9.65625Z" fill="#888888"/><path d="M4.62063 5.72391H5.35188C5.26188 4.94766 6.49375 4.84641 6.49375 4.02516C6.49375 3.27703 5.8975 2.89453 5.08187 2.89453C4.48 2.89453 3.985 3.17016 3.625 3.58641L4.08625 4.00828C4.36187 3.72141 4.64313 3.56391 4.98625 3.56391C5.43063 3.56391 5.70063 3.75516 5.70063 4.09828C5.70063 4.64391 4.5025 4.84078 4.62063 5.72391ZM4.99188 7.22578C5.27313 7.22578 5.48688 7.01766 5.48688 6.72516C5.48688 6.43266 5.27313 6.23016 4.99188 6.23016C4.705 6.23016 4.49125 6.43266 4.49125 6.72516C4.49125 7.01766 4.69937 7.22578 4.99188 7.22578Z" fill="#888888"/></svg></a></div>' );
				var productTypeFillingEl = $j( '.variation-wrap.pa_filling .variation-label label' );
				
				if ( productTypeFillingEl.hasClass( 'sofa-filling' ) )
				{
					productTypeFillingEl.append( '<div class="info-link"><a id="openmodal-filling" href="#content-filling-sofa">フィリングの種類<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.04688 8.95312C7.11794 8.95312 8.79688 7.27419 8.79688 5.20312C8.79688 3.13206 7.11794 1.45312 5.04688 1.45312C2.97581 1.45312 1.29688 3.13206 1.29688 5.20312C1.29688 7.27419 2.97581 8.95312 5.04688 8.95312ZM5.04688 9.65625C7.50627 9.65625 9.5 7.66252 9.5 5.20312C9.5 2.74373 7.50627 0.75 5.04688 0.75C2.58748 0.75 0.59375 2.74373 0.59375 5.20312C0.59375 7.66252 2.58748 9.65625 5.04688 9.65625Z" fill="#888888"/><path d="M4.62063 5.72391H5.35188C5.26188 4.94766 6.49375 4.84641 6.49375 4.02516C6.49375 3.27703 5.8975 2.89453 5.08187 2.89453C4.48 2.89453 3.985 3.17016 3.625 3.58641L4.08625 4.00828C4.36187 3.72141 4.64313 3.56391 4.98625 3.56391C5.43063 3.56391 5.70063 3.75516 5.70063 4.09828C5.70063 4.64391 4.5025 4.84078 4.62063 5.72391ZM4.99188 7.22578C5.27313 7.22578 5.48688 7.01766 5.48688 6.72516C5.48688 6.43266 5.27313 6.23016 4.99188 6.23016C4.705 6.23016 4.49125 6.43266 4.49125 6.72516C4.49125 7.01766 4.69937 7.22578 4.99188 7.22578Z" fill="#888888"/></svg></a></div>' );
				}
				else if ( productTypeFillingEl.hasClass( 'cat-filling' ) )
				{
					productTypeFillingEl.append( '<div class="info-link"><a id="openmodal-filling" href="#content-filling-cat">フィリングの種類<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.04688 8.95312C7.11794 8.95312 8.79688 7.27419 8.79688 5.20312C8.79688 3.13206 7.11794 1.45312 5.04688 1.45312C2.97581 1.45312 1.29688 3.13206 1.29688 5.20312C1.29688 7.27419 2.97581 8.95312 5.04688 8.95312ZM5.04688 9.65625C7.50627 9.65625 9.5 7.66252 9.5 5.20312C9.5 2.74373 7.50627 0.75 5.04688 0.75C2.58748 0.75 0.59375 2.74373 0.59375 5.20312C0.59375 7.66252 2.58748 9.65625 5.04688 9.65625Z" fill="#888888"/><path d="M4.62063 5.72391H5.35188C5.26188 4.94766 6.49375 4.84641 6.49375 4.02516C6.49375 3.27703 5.8975 2.89453 5.08187 2.89453C4.48 2.89453 3.985 3.17016 3.625 3.58641L4.08625 4.00828C4.36187 3.72141 4.64313 3.56391 4.98625 3.56391C5.43063 3.56391 5.70063 3.75516 5.70063 4.09828C5.70063 4.64391 4.5025 4.84078 4.62063 5.72391ZM4.99188 7.22578C5.27313 7.22578 5.48688 7.01766 5.48688 6.72516C5.48688 6.43266 5.27313 6.23016 4.99188 6.23016C4.705 6.23016 4.49125 6.43266 4.49125 6.72516C4.49125 7.01766 4.69937 7.22578 4.99188 7.22578Z" fill="#888888"/></svg></a></div>' );
				}
				else
				{
					productTypeFillingEl.append( '<div class="info-link"><a id="openmodal-filling" href="#content-filling-dog">フィリングの種類<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.04688 8.95312C7.11794 8.95312 8.79688 7.27419 8.79688 5.20312C8.79688 3.13206 7.11794 1.45312 5.04688 1.45312C2.97581 1.45312 1.29688 3.13206 1.29688 5.20312C1.29688 7.27419 2.97581 8.95312 5.04688 8.95312ZM5.04688 9.65625C7.50627 9.65625 9.5 7.66252 9.5 5.20312C9.5 2.74373 7.50627 0.75 5.04688 0.75C2.58748 0.75 0.59375 2.74373 0.59375 5.20312C0.59375 7.66252 2.58748 9.65625 5.04688 9.65625Z" fill="#888888"/><path d="M4.62063 5.72391H5.35188C5.26188 4.94766 6.49375 4.84641 6.49375 4.02516C6.49375 3.27703 5.8975 2.89453 5.08187 2.89453C4.48 2.89453 3.985 3.17016 3.625 3.58641L4.08625 4.00828C4.36187 3.72141 4.64313 3.56391 4.98625 3.56391C5.43063 3.56391 5.70063 3.75516 5.70063 4.09828C5.70063 4.64391 4.5025 4.84078 4.62063 5.72391ZM4.99188 7.22578C5.27313 7.22578 5.48688 7.01766 5.48688 6.72516C5.48688 6.43266 5.27313 6.23016 4.99188 6.23016C4.705 6.23016 4.49125 6.43266 4.49125 6.72516C4.49125 7.01766 4.69937 7.22578 4.99188 7.22578Z" fill="#888888"/></svg></a></div>' );
				}

			// Click Trigger
				$j( 'body' ).on( 'click', '#openmodal-filling, #openmodal-filling-deluxe, #openmodal-trial, #openmodal-deliverydays', function(e) {
					e.preventDefault();

					openModal( $j( this ).attr( 'href' ) );                
				});

			// Close
				$j( 'body' ).on( 'click', '.modal-close', function(e) {
					e.stopPropagation();

					closeModal();
				});

				$j( 'body' ).on( 'click', '.modal-overlay', function(e) {
					if ( $j( e.target ).is( '.modal-overlay' ) ) {
						closeModal();
					} 
				});

			function openModal( contentClass ) {					
				modalContentInner.html( modalContentList.find( contentClass ).prop("outerHTML") );
				modalWrapper.attr({ 'name' : contentClass.substring(1) })            

				modalOverlay.fadeIn(400, function() {
					modalWrapper.fadeIn(200, function() {
						modalWrapper.css({ 'display' : 'flex' })
					});
				})
			}			

			function closeModal() {
				modalWrapper.fadeOut(400, function() {
					modalOverlay.fadeOut(200, function() {
						modalContentInner.empty();

						modalWrapper.attr({ 'name' : '' })
					});
				})
			}

		/* Info Link */
			// Append on Swatch Bed Selection
				//$j( '.specialproduct-configurator .variation-wrap.pa_bed-selection .variation-label label' ).append( '<div class="info-link"><a href="https://ambientlounge.co.jp/products/swatch/pet-swatch-order/" target="_blank" title="生地サンプル請求">生地サンプル請求<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"/><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"/></svg></a></div>' );
				$j( '.variation-wrap.pa_bed-selection .variation-label label' ).append( '<div class="info-link"><a href="' + currDomain + '/pet-sample/" target="_blank" title="生地サンプル請求">生地サンプル請求<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"/><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"/></svg></a></div>' );

			// Append on Swatch Cover Selection
				//$j( '.specialproduct-configurator .variation-wrap.pa_cover-selection .variation-label label' ).append( '<div class="info-link"><a href="https://ambientlounge.co.jp/products/swatch/pet-swatch-order/" target="_blank" title="生地サンプル請求">生地サンプル請求<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"/><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"/></svg></a></div>' );
				$j( '.variation-wrap.pa_cover-selection .variation-label label' ).append( '<div class="info-link"><a href="' + currDomain + '/pet-sample/" target="_blank" title="生地サンプル請求">生地サンプル請求<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"/><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"/></svg></a></div>' );

		/* Pricing */
			function capitalize(s)
			{ 
				try {
					if (typeof(s[0]) != "undefined") {
						return s[0].toUpperCase() + s.slice(1);
					} else {
						return "";
					}
				} catch (exceptionVar) {
					return "";
				} 
			}

			// Sticky Price
			if(parentWrapper.length) {
				jQuery(".sticky-price-name").html(jQuery(".product_title").html());

				// Check for changes in the variation ID
				var initialVariationID = jQuery('.woocommerce-variation-add-to-cart .variation_id').val();
				jQuery('.single-product-wrapper .single-product-right .woocommerce-variation-add-to-cart .variation_id').change(function() {
					// Get the new variation ID
					var newVariationID = jQuery(this).val();
	
					// Check if the variation ID has changed
					if (newVariationID !== initialVariationID) {
						// Update the top bar price with the variation price
						var variationPrice = jQuery('.woocommerce-variation-price .price').html();
						jQuery('.single-product-topbar-price').html(variationPrice);

						// Copy the image
						var productImage = jQuery('.woocommerce-product-gallery img').first().attr('src');
						jQuery('.single-product-topbar-left img').attr('src', productImage);

						// See the price
						var priceNum = jQuery(".single-product-wrapper .single_variation_wrap .woocommerce-Price-amount bdi").first().clone().children().remove().end().text();

						if(priceNum == 0)
							jQuery('.single-product-right .single_variation_wrap .single_variation').append('<div class="woocommerce-variation-price"><span class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">¥</span>0</bdi></span> <small class="woocommerce-price-suffix">(税込/送料無料)</small></span></div>');
	
						// Update the initial variation ID
						initialVariationID = newVariationID;
					}
				});

				// Show the sticky price
				jQuery(".single-product-topbar").show();
			} else {
				// Paidy
				jQuery.ajaxPrefilter( function( options, originalOptions, jqXHR ) {

					if ( originalOptions.url.includes('get_variation')) {
					
						var orignalSuccess = originalOptions.success;
						
						options.success = function( response ) {
				
							if ( orignalSuccess && 'function' === typeof orignalSuccess ) {
								orignalSuccess( response );
							}
		
							if(typeof(response.display_price) != "undefined") {
								jQuery("._paidy-promotional-messaging").attr("data-amount", response.display_price);
								_paidy && _paidy("pm:refresh");
								jQuery("._paidy-promotional-messaging").css("width", "100%");
							}
						};
					}
				} );
			}

			// 0 Price
			if(jQuery('.single-product-wrapper .single-product-right .single_variation_wrap .single_variation').children().length === 0) {
				jQuery('.single-product-topbar-price').append('<span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">¥</span>0</bdi></span><small class="woocommerce-price-suffix">(税込/送料無料)</small>');
				jQuery('.single-product-right .single_variation_wrap .single_variation').append('<div class="woocommerce-variation-price"><span class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">¥</span>0</bdi></span> <small class="woocommerce-price-suffix">(税込/送料無料)</small></span></div>');
			}
	
			// Paidy
			jQuery(".single-product-wrapper .single_variation_wrap .woocommerce-variation-add-to-cart .variation_id").each(function(i, obj) {
			
				var target = jQuery(this);
				var config = { characterData: true, childList: true, attributes: true};

				var observer = new MutationObserver(function(mutations) {

					mutations.forEach ( function (mutation) {
				
						let priceNormal = jQuery(".single-product-wrapper .single_variation_wrap .woocommerce-Price-amount bdi").first().clone().children().remove().end().text();
						let priceDiscount = jQuery(".single-product-wrapper .single_variation_wrap ins .woocommerce-Price-amount bdi").first().clone().children().remove().end().text();

						if(priceDiscount == "")
							price = priceNormal;
						else
							price = priceDiscount;
							
						price = parseFloat(price.replace(/,/g, ''));

						if(!isNaN(price)) {	

							jQuery("._paidy-promotional-messaging").attr("data-amount", price);
							_paidy && _paidy("pm:refresh");
							jQuery("._paidy-promotional-messaging").css("width", "100%");
								
							if(price > 3000) {
								jQuery("._paidy-promotional-messaging div").show();
							} else {
								jQuery("._paidy-promotional-messaging div").hide();
							}
						}

					} );

				});
			
				observer.observe(target[0], config);

			});

			// Collection / Archive - Swatch Click - Change URL depending on variation
			jQuery('body').on('click', '.woocommerce.archive .swatch-item-wrapper', function (e) {	
				var linkEl = jQuery( this ).parents( 'li.type-product' ).find( '.woocommerce-LoopProduct-link' );
				var href = linkEl.attr( 'href' );			
				var variation = jQuery( this ).parents( '.tawcvs-swatches' ).first().attr( 'data-attribute_name' );
				var attribute = jQuery( this ).find('.swatch').first().attr( 'data-value' );
				if ( href.indexOf( 'attribute_pa_color' ) > -1 )
				{							
					var new_href = href.replace(/(attribute_pa_color=).*?(&|$)/,'$1' + attribute + '$2');
					
					linkEl.attr({ 'href' : new_href });
				}
				else 
				{
					var new_href = href + '?attribute_pa_color=' + attribute;
					
					linkEl.attr({ 'href' : new_href });
				}
			});

			// Collection / Archive - On load, if parameter exist, pick all the relevant image
			// Extract the filter_color parameter value from the URL
			
			//jQuery(".woocommerce-archive-wrapper .entry-content .products ").css("visibility", "visible");
			
			jQuery(window).load(function() {
				var urlParams = new URLSearchParams(window.location.search);
				var filterColor = urlParams.get('filter_color');
	
				// Check if filter_color exists and trigger click event on matching elements
				if (filterColor) {
					jQuery('.swatch-' + filterColor).not('.selected').trigger('click');
	
					// Select li elements that do not have .swatch-[filterColor] inside
					jQuery('li.product-type-variable').filter(':has(div.disabled.swatch-' + filterColor + ')').hide();
				}

				//jQuery(".woocommerce-archive-wrapper .entry-content .products ").css("visibility", "visible");
			});

			/* Cart */
			jQuery('.woocommerce-cart .item-variation-name:contains("カバーを選択:")').parent().hide();
			jQuery('.woocommerce-cart .item-variation-name:contains("フィリングを選択:")').parent().hide();

	// Showmore
		var lineHeight = 24.5;
		var showLines = 3;

		$j( '.showmore-content' ).each( function() {
			var thisEl = $j( this );
			thisEl.attr({ 'data-height' : thisEl.height() })
			thisEl.css({ 'height' : ( lineHeight * showLines ) + 'px' });
		});		

		$j( '.showmore-button' ).click( function() {
			var thisEl = $j( this );
			var contentEl = thisEl.parents( '.showmore-wrapper' ).siblings( '.showmore-content' );			

			if ( contentEl.hasClass( 'show' ) ) 
			{
				contentEl.removeClass( 'show' );
                contentEl.css({ 'height' : ( lineHeight * showLines ) + 'px' });
				thisEl.html( 'もっと詳しく' );
			}
			else
			{
				contentEl.addClass( 'show' );
				contentEl.css({ 'height' : contentEl.attr( 'data-height' ) + 'px'  });
				thisEl.html( '閉じる' );
			}
		});

	// Template Offer
		$j( '.offer-confirm-addtocart' ).click( function() {
			var productEl = $j( this ).closest( '.product' );
			var productThumb = productEl.find( '.attachment-woocommerce_thumbnail' ).attr( 'src' );
			var productTitle = productEl.find( '.woocommerce-loop-product__title' ).html();

			modalContentList.find( '#modal-offers .modal-thumb' ).empty().append( '<img src="' + productThumb + '" alt="">')

			openModal( '#modal-offers' );			
		});

		// Monitor the value of class single_variation_wrap
		$j('.page-template-template-offers .single_variation_wrap .variation_id').on('change', function() {
			var newValue = $j(this).val();

			if((typeof(newValue) == "undefined") || (newValue == "")) {
				$j(this).closest('.type-product').find('.offer-confirm-addtocart').attr('disabled', true);
			} else {
				// Copy the value to data-product_id attribute of class offer-confirm-addtocart
				$j(this).closest('.type-product').find('.offer-confirm-addtocart').attr('disabled', false);
				$j(this).closest('.type-product').find('.offer-confirm-addtocart').attr('data-product_id', newValue);
				$j(this).closest('.type-product').find('.price').html($j(this).closest('.type-product').find('.single_variation_wrap .price').html());
			}
		});

		// Add "variation-label" div inside "variation-selector"
			jQuery('.page-template-template-offers .variation-selector').append('<div class="variation-label-sync"></div>');

		// Update "variation-label" on select value change
			jQuery('.page-template-template-offers .variation-selector select').on('change', function() {
				const selectedValue = jQuery(this).find("option:selected").text();
				jQuery(this).siblings('.variation-label-sync').text(selectedValue);
			});

			jQuery('.page-template-template-offers .variation-selector select').trigger("change");
});  

function getURLParameter( paramTerm ) 
{
	var url = window.location.search.substring(1);
	var urlParameters = url.split('&');
	var parameter;
	var i;

	for ( i = 0; i < urlParameters.length; i++ ) 
	{
		parameter = urlParameters[i].split( '=' );

		if (parameter[0] === paramTerm ) 
		{
			return parameter[1] === undefined ? true : decodeURIComponent( parameter[1] );
		}
	}

	return false;
};

function getGridSize() {
	return ( window.innerWidth < 576 ) ? 1 :
		   ( window.innerWidth < 992 ) ? 2 : 3;
}

function addCommas(nStr)
{
	nStr = nStr.trim();
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

$j( window ).resize( function() {	
	// Carousel
		var gridSize = getGridSize();		
 
		flexslider.vars.minItems = gridSize;
		flexslider.vars.maxItems = gridSize;

	// Reset first submenu tab on entering mobile menu
	resetMobileMenu();
});

$j( window ).scroll( function() {
	// Scroll to top	
		if ( $j( window ).scrollTop() > vpHeight ) {
			$j( '#toTop' ).fadeIn(400);
		}
		else {
			$j( '#toTop' ).fadeOut(400);	
		}
	
	// Sticky Header
		if( $j( '.stickyheader' ).length ) 
		{
			if ( $j( window ).scrollTop() > headerHeight ) {
				headerEl.addClass( 'onScroll' );
			}
			else {
				headerEl.removeClass( 'onScroll' );
			}
		}	
});

function resetMobileMenu() {
	if ( window.innerWidth < 992 ) {
		$j( '.submenu-title-wrapper .submenu-title, .submenu-content-wrapper .submenu-accordion-wrapper' ).removeClass( 'active' );
	}
	else {
		$j( '.submenu-title-wrapper .submenu-title:first-child, .submenu-content-wrapper .submenu-accordion-wrapper:first-child' ).addClass( 'active' );
	}
}