var $j = jQuery.noConflict();
var flexslider = { vars:{} };

$j(function(){	

	/**
	 * Menu
	 */

	// Search
		$j( '.search-field' ).attr( 'placeholder', 'Search...' );

		var siteHeader = $j( '.site-header' );
		var headerSearchWrapper = $j( '.header-search-wrapper' );
		var headerSearchInner = $j( '.header-search-inner' );
		var headerSearchFormWrapper = $j( '.header-search-form-wrapper' );
		var headerSearchInput = headerSearchWrapper.find( '#header-search-input' );

		$j( '#menu-search-wrapper a' ).click( function() {		
			if ( siteHeader.hasClass( 'search-open' ) )
			{
				closeSearch();
			}
			else 
			{			
				openSearch();
			}
		})

		$j( 'body' ).on( 'click', '.header-search-wrapper', function(e) {
			if ( $j( e.target ).is( '.header-search-wrapper' ) ) {
				closeSearch();
			} 
		});

		function openSearch() {									
			headerSearchWrapper.fadeIn( 200, function() {
				headerSearchInner.slideDown( 200, function() {
					headerSearchFormWrapper.fadeIn( 600, function() {
						headerSearchInput.focus();
					});
				});	
			});			

			siteHeader.addClass( 'search-open' );
		}

		function closeSearch() {			
			headerSearchInner.slideUp( 200, function() {
				headerSearchWrapper.fadeOut( 200 );
				headerSearchFormWrapper.hide();
			});		

			siteHeader.removeClass( 'search-open' );
		}

	// Megamenu 		
		var submenuContentWrapper = $j( '.submenu-content-wrapper' );

		$j( '.submenu-title' ).click( function(){
			var submenuContentTargetID = $j( this ).attr( 'data-target' );

			$j( this ).toggleClass( 'active' )
				.siblings()
					.removeClass( 'active' );

			if (submenuContentTargetID === '#sofa-blog') {
				submenuContentWrapper.find("#sofa-blog.desktop-only")
					.addClass( 'active')
						.siblings() 
							.removeClass( 'active' );
			} else {
				submenuContentWrapper.find( $j( submenuContentTargetID ) )
					.addClass( 'active')
						.siblings()
							.removeClass( 'active' );
			}
		})		

	// Mobile Menu
		// Root Menu - Tabbing
		var primaryMenu = $j( '.desktop-primary-menu' );

		$j( '.mobile-menu-title' ).click( function() {
			var menuContentTargetID = $j( this ).attr( 'data-target' );

			/*if ( $j( this ).hasClass( 'active' ) ) {
				$j( this ).removeClass( 'active' );
			}
			else {
				$j( this ).addClass( 'active' );
			}

			$j( this )
				.siblings()
					.removeClass( 'active' );
			
			primaryMenu.find( $j( menuContentTargetID ) )
				.addClass( 'active')
					.siblings()
						.removeClass( 'active' );*/

			if ( $j( this ).hasClass( 'active' ) ) {
				//$j( this ).removeClass( 'active' );
			}
			else {
				$j( this ).addClass( 'active' ).siblings().removeClass( 'active' );

				primaryMenu.find( $j( menuContentTargetID ) )
					.addClass( 'active')
						.siblings()
							.removeClass( 'active' );
			}		
			
		});

		// Sub Menu (Open)
		$j( '.submenu-accordion-title' ).click( function() {
			var thisEl = $j( this ).parent( '.submenu-accordion-wrapper' );			
			thisEl.siblings( '.active' ).removeClass( 'active' );
			thisEl.toggleClass( 'active' );
		});

		// Sub Menu (Close)
		$j( '.submenu-accordion-content > .submenu-accordion-title' ).click( function() {
			var thisEl = $j( this ).parents( '.submenu-accordion-wrapper' );

			thisEl.siblings( '.active' ).removeClass( 'active' );
			thisEl.toggleClass( 'active' );
		});

		// Sub Menu Level 2 - Accordion
		$j( '.sub-submenu-accordion-title' ).click( function() {
			var thisEl = $j( this ).parent( '.sub-submenu-accordion-wrapper' );
			
			if ( thisEl.hasClass( 'active' ) ) 
			{
				thisEl.find( '.sub-submenu-accordion-content' ).slideUp( 400, function() {
					thisEl.removeClass( 'active' );
				});
			}
			else 
			{
				thisEl.find( '.sub-submenu-accordion-content' ).slideDown( 400 );
				thisEl.addClass( 'active' );
			}
		});

		// Burger bar on click		
		$j( '.burger-wrapper' ).click( function() {
			$j( this )
				.toggleClass( 'active' )
				.siblings( '#site-navigation' ).toggleClass( 'active' );

			$j( 'body' ).toggleClass( 'menu-active' );
		});

		// Reset first submenu tab on entering mobile menu
		resetMobileMenu();

	/**
	 * Auto scroll
	 */
	// Shortcuts - Scroll to section
		$j( '.shortcuts .wp-block-button__link' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).parent().attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 64
			}, 2000);
		});

	// Shortcuts - Scroll to section
		$j( '.shortcuts-row p' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 112
			}, 1000);
		});

	/**
	 * Blocks
	 */
	// Block - Accordion
		// Open first accordion item on first accordion group
		$j( '.accordion-block-wrapper' ).first().children( '.accordion-item-wrapper' ).first().addClass( 'active' ).children( '.accordion-item-content' ).show();

		$j( '.accordion-item-title' ).click( function() {
			var thisEl = $j( this );
			var parentEl = $j( this ).parent( '.accordion-item-wrapper' );

			if ( parentEl.hasClass( 'active' ) ) 
			{
				thisEl.siblings( '.accordion-item-content' ).slideUp( 400 );
			}
			else 
			{
				thisEl.siblings( '.accordion-item-content' ).slideDown( 400 );
			}

			parentEl.toggleClass( 'active' );
		});

	// Accordion Shortcuts - Scroll to section and open the accordion
		$j( '.accordion-shortcuts .wp-block-button__link' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).parent().attr( 'id' );				

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 128
			}, 2000, function() {
				if ( !$j( '.' + targetEl ).hasClass( 'active' ) ) {
					$j( '.' + targetEl ).addClass( 'active')
						.children( '.accordion-item-content' ).slideDown( 400 );
				}
			});
		});

		// Accordion Shortcut from URL
		if ( $j( '.accordion-shortcuts' ).length )
		{
			var targetEl = getURLParameter( 's' );	

			if ( targetEl !== '' )
			{
				$j( '.accordion-block-wrapper' ).find( '.accordion-item-wrapper' ).removeClass( 'active' ).children( '.accordion-item-content' ).hide();
				$j( '.' + targetEl ).addClass( 'active' ).children( '.accordion-item-content' ).show();
				
				$j([document.documentElement, document.body]).animate({
					scrollTop: $j( '.' + targetEl ).offset().top - 128
				}, 1000 );			
			}
		}	

	/**
	 * Template and Pages
	 */
	// Contact
		// Styling Input File
		$j( '#photo-attachment-trigger' ).each( function()
		{
			var attachmentFileInput = $j( this ).find( '#photo-attachment' );
			var attachmentLabel	= $j( this ).siblings( '#photo-attachment-label' );
			var attachmentLabelValue = attachmentLabel.children( 'span' ).html();

			attachmentFileInput.on( 'change', function( e ) {
				var fileName = '';

				if( this.files && this.files.length > 1 ) 
				{
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				}
				else if( e.target.value )
				{
					fileName = e.target.value.split( '\\' ).pop();
				}

				if( fileName ) 
				{
					attachmentLabel.html( '<span>' + fileName + '</span><span id="photo-attachment-remove"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" aria-hidden="true" focusable="false"><path d="M12 13.06l3.712 3.713 1.061-1.06L13.061 12l3.712-3.712-1.06-1.06L12 10.938 8.288 7.227l-1.061 1.06L10.939 12l-3.712 3.712 1.06 1.061L12 13.061z"></path></svg></span>' );

					var reader = new FileReader();				
					reader.onload = function (e) {
						$j( '#photo-attachment-preview' ).html( '<img src="' + e.target.result + '">').addClass( 'file-added' );
					}

					reader.readAsDataURL( this.files[0] );
				}
				else 
				{
					attachmentLabel.html( attachmentLabelValue );
				}
			});

			// FF fix
			attachmentFileInput
				.on( 'focus', function(){ attachmentFileInput.addClass( 'has-focus' ); })
				.on( 'blur', function(){ attachmentFileInput.removeClass( 'has-focus' ); });
		});

		$j( 'body' ).on( 'click', '#photo-attachment-remove', function() {
			var triggerEl = $j( this ).parent( '#photo-attachment-label' ).siblings( '#photo-attachment-trigger' );

			triggerEl.find( '#photo-attachment-preview' )
				.removeClass( 'file-added' )
				.empty();

			triggerEl.find( '#photo-attachment' ).val( '' );

			$j( this ).siblings( 'span' ).html( 'No file chosen' );
			$j( this ).remove();
		})

	// Template Stores
		// Shortcuts - Scroll to section
			$j( '.store-list' ).click(function(e) {
				e.preventDefault();

				var targetEl = $j( this ).attr( 'id' );	

				$j([document.documentElement, document.body]).animate({
					scrollTop: $j( '.' + targetEl ).offset().top - 128
				}, 2000);
			});

		// Accordion
			$j( '.store-location-title' ).click( function() {
				var thisEl = $j( this );
				var parentEl = $j( this ).parent( '.store-location' );

				if ( parentEl.hasClass( 'active' ) ) 
				{
					thisEl.siblings( '.store-location-list' ).slideUp( 400 );
				}
				else 
				{
					thisEl.siblings( '.store-location-list' ).slideDown( 400 );
				}

				parentEl.toggleClass( 'active' );
			})

	// Template Collection
		// Fix gap when using column with specific width (%)
		$j( '.page-template-template-collection .compare-content > .wp-block-columns').each( function() {
			var columnCount = $j( this ).children( '.wp-block-column' ).length;
			var slimGap = $j( this ).hasClass( 'slim-gap' );
			var gap = 32;
			if ( slimGap ) { gap = 16; }

			$j( this ).children( '.wp-block-column' ).each( function() {
				var flexBasis = $j( this ).css( 'flex-basis' );				
			
				if ( flexBasis != null && flexBasis.slice(-1) == '%' ) {
					flexBasis = flexBasis.slice(0,-1);

					

					if ( flexBasis == 50)
					{	
						$j( this ).css({ 'flex-basis' : 'calc(' + flexBasis + '% - ' + ( gap / 2 ) + 'px)' });
					}
				}
			})
		})

	// Template B2B
		setTimeout( function() {
			// Remove add to cart button, change into headline        
			/*$j( '.page-template-template-b2b .wc-product-table td.col-add-to-cart a.button:contains("カートに追加")' ).each( function() {
				$j( this ).after( '<span class="highlight-outofstock">在庫切れ</span>');
				$j( this ).remove();
			});*/

			// Change price suffix
        	$j( '.page-template-template-b2b .wc-product-table td.col-price .woocommerce-price-suffix:contains("(税込/送料無料)")' ).html( '税込' );
		}, 100 );

	// Page - Professional - Stage 
		// HTML inside gutenberg button 
		$j( '.professional-stage-shortcuts .wp-block-button__link' ).each( function() {
			var thisEl = $j( this );
			var button_text = thisEl.text().replace( '(', '<span class="button-text-detail">(' );
			button_text = button_text.replace( ')', ')</span>' );
			
			thisEl.html( button_text );
		})

	// Page - FAQ
		// Shortcuts	
		$j( '.shortcut-columns p' ).click(function(e) {
			e.preventDefault();

			var targetEl = $j( this ).attr( 'id' );	

			$j([document.documentElement, document.body]).animate({
				scrollTop: $j( '.' + targetEl ).offset().top - 128
			}, 2000);
		});

	// Woocommerce 
		/* Quantity Selector */
		$j( '.minus-btn' ).click( function() {				
			var qtyEl = $j( this ).siblings( '.qty' );
			var qtyVal = parseInt( qtyEl.val(), 10 );
			
			if ( qtyVal > 1 ) {
				qtyEl.val( qtyVal - 1 );
			}	
			
			qtyEl.trigger('change');
		})

		$j( '.plus-btn' ).click( function() {				
			var qtyEl = $j( this ).siblings( '.qty' );
			var qtyVal = parseInt( qtyEl.val(), 10 );
			
			qtyEl.val( qtyVal + 1 );	
			qtyEl.trigger('change');			
		})

		$j( '.qty' ).keypress( function(e) {
			if (e.which < 48 || e.which > 57)
			{
				e.preventDefault();
			}
		})

		// This is probably old script for the old attribute shortcode
		/*
			$j( 'body' ).on( 'click', '.attribute-selection', function() {
						
				let url = $j(this).parent().parent().find("a").attr("attr");

				$j( this )
					.addClass( 'selected' )
					.siblings()
						.removeClass( 'selected' );

				let priceString = addCommas( $j( this ).attr( 'price' ) ); priceString = priceString.replace("-", " - ¥");
				let saleString = addCommas( $j( this ).attr( 'sale' ) ); saleString = saleString.replace("-", " - ¥");

				if($j( this ).attr("sale")) {
					$j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-price' ).html( '<del>¥' + priceString + "</del>");
					$j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-sale' ).html( '¥' + saleString );
				} else {
					$j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-price' ).html( '¥' + priceString );
					$j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-sale' ).html( '' );
				}
					
				var linkEl = $j( this ).parents( '.productattribute-wrapper' ).children( 'a' );						
				var href = linkEl.attr( 'href' );			
				var buttonEl = $j( this ).parents( '.productattribute-wrapper' ).find( '.productattribute-action > a' );
				var targetSize = $j( this ).attr( 'value' );
				if ( href.indexOf( url ) > -1 )
				{							
					var regex = new RegExp('('+url+'=).*?(&|$)', "g");
					var new_href = href.replace(regex,'$1' + targetSize + '$2');

					linkEl.attr({ 'href' : new_href });
					buttonEl.attr({ 'href' : new_href });
				}
				else
				{
					if ( href.indexOf( '?' ) > -1 ) {
						var new_href = href + '&' + url + '=' + targetSize;
					
						linkEl.attr({ 'href' : new_href });
						buttonEl.attr({ 'href' : new_href });
					} else {
						var new_href = href + '?' + url + '=' + targetSize;
					
						linkEl.attr({ 'href' : new_href });
						buttonEl.attr({ 'href' : new_href });
					}
				}
			});
		*/

	// Modal
		var modalOverlay = $j( '.modal-overlay' );
		var modalWrapper = $j( '.modal-wrapper' );
		var modalContentList = $j( '.modal-content-list' );
		var modalContentInner = $j( '.modal-content-inner' );

		// Click Trigger
			$j( 'body' ).on( 'click', '#openmodal-filling, #openmodal-filling-deluxe, #openmodal-trial, #openmodal-deliverydays', function(e) {
				e.preventDefault();

				openModal( $j( this ).attr( 'href' ) );                
			});

		// Close
			$j( 'body' ).on( 'click', '.modal-close', function(e) {
				e.stopPropagation();

				closeModal();
			});

			$j( 'body' ).on( 'click', '.modal-overlay', function(e) {
				if ( $j( e.target ).is( '.modal-overlay' ) ) {
					closeModal();
				} 
			});

		function openModal( contentClass ) {					
			modalContentInner.html( modalContentList.find( contentClass ).prop("outerHTML") );
			modalWrapper.attr({ 'name' : contentClass.substring(1) })            

			modalOverlay.fadeIn(400, function() {
				modalWrapper.fadeIn(200, function() {
					modalWrapper.css({ 'display' : 'flex' })
				});
			})
		}			

		function closeModal() {
			modalWrapper.fadeOut(400, function() {
				modalOverlay.fadeOut(200, function() {
					modalContentInner.empty();

					modalWrapper.attr({ 'name' : '' })
				});
			})
		}

	// Showmore
		var lineHeight = 24.5;
		var showLines = 3;

		$j( '.showmore-content' ).each( function() {
			var thisEl = $j( this );
			thisEl.attr({ 'data-height' : thisEl.height() })
			thisEl.css({ 'height' : ( lineHeight * showLines ) + 'px' });
		});		

		$j( '.showmore-button' ).click( function() {
			var thisEl = $j( this );
			var contentEl = thisEl.parents( '.showmore-wrapper' ).siblings( '.showmore-content' );			

			if ( contentEl.hasClass( 'show' ) ) 
			{
				contentEl.removeClass( 'show' );
                contentEl.css({ 'height' : ( lineHeight * showLines ) + 'px' });
				thisEl.html( 'もっと詳しく' );
			}
			else
			{
				contentEl.addClass( 'show' );
				contentEl.css({ 'height' : contentEl.attr( 'data-height' ) + 'px'  });
				thisEl.html( '閉じる' );
			}
		});

	// Magic filter for wholesale
	// Create the input element
	$j(window).load(function() {
		var skuFilterInput = $j('<input>').attr({
			type: 'text',
			id: 'sku-filter',
			placeholder: 'Filter by SKU'
		  }).css({
			display: 'block',
			margin: '10px 0',
			padding: '5px'
		  });
	  
		  // Inject the input element after .form-header.wwof-section
		$j('.form-header.wwof-section').after(skuFilterInput);
	
		$j('#sku-filter').on('input', function() {
			var filterValue = $j(this).val().toLowerCase();
			$j('tbody tr').each(function() {
			var sku = $j(this).find('td[data-label="SKU"] span').text().toLowerCase();
			if (sku.includes(filterValue)) {
				$j(this).removeClass('hidden');
			} else {
				$j(this).addClass('hidden');
			}
			});
		});
	});

});  

function getURLParameter( paramTerm ) 
{
	var url = window.location.search.substring(1);
	var urlParameters = url.split('&');
	var parameter;
	var i;

	for ( i = 0; i < urlParameters.length; i++ ) 
	{
		parameter = urlParameters[i].split( '=' );

		if (parameter[0] === paramTerm ) 
		{
			return parameter[1] === undefined ? true : decodeURIComponent( parameter[1] );
		}
	}

	return false;
};

function getGridSize() {
	return ( window.innerWidth < 576 ) ? 1 :
		   ( window.innerWidth < 992 ) ? 2 : 3;
}

function addCommas(nStr)
{
	nStr = nStr.trim();
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

$j( window ).resize( function() {	
	// Carousel
	var gridSize = getGridSize();		

	flexslider.vars.minItems = gridSize;
	flexslider.vars.maxItems = gridSize;

	// Reset first submenu tab on entering mobile menu
	resetMobileMenu();
});

function resetMobileMenu() {
	if ( window.innerWidth < 992 ) {
		$j( '.submenu-title-wrapper .submenu-title, .submenu-content-wrapper .submenu-accordion-wrapper' ).removeClass( 'active' );
	}
	else {
		$j( '.submenu-title-wrapper .submenu-title:first-child, .submenu-content-wrapper .submenu-accordion-wrapper:first-child' ).addClass( 'active' );
	}
}