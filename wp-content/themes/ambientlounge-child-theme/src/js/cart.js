var $j = jQuery.noConflict();

$j(function(){	

	// Styling Input Number 
	$j( document ).on( 'click', '.inputqty-wrapper .input-minus', function() {
		var parentEl = $j( this ).parent( '.inputqty-wrapper' );
		var numberEl = parentEl.find( 'input[type=number]' );
		var finalNumber = parseInt( numberEl.val(), 10 ) - 1;
		if ( finalNumber < 1 ) {
			finalNumber = 1;
		}
		numberEl.val( finalNumber ).trigger("change");
	});
	
	$j( document ).on( 'click', '.inputqty-wrapper .input-plus', function() {
		var parentEl = $j( this ).parent( '.inputqty-wrapper' );
		var numberEl = parentEl.find( 'input[type=number]' );
		var finalNumber = parseInt( numberEl.val(), 10 ) + 1;
		numberEl.val( finalNumber ).trigger("change");
	});

	// Hide labels
	$j('.woocommerce-cart .item-variation-name:contains("カバーを選択:")').parent().hide();
	$j('.woocommerce-cart .item-variation-name:contains("フィリングを選択:")').parent().hide();

});  