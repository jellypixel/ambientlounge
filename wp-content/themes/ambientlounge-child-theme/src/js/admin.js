jQuery(function($){
    $( '#woocommerce-product-data' ).on('woocommerce_variations_loaded',function(){
        $( '.rwmb-select_advanced' ).each( update );
    });

    $('body').on('click','.chained-warehouse-button.add',function(){
        var row = $(this).closest('.row').clone();
        $(this).closest('.warehouse-chained-product-wrapper').append(row).find('.rwmb-select_advanced').trigger('clone')


    });
    $('body').on('click','.chained-warehouse-button.remove',function(){
        if($(this).closest('.row').siblings('.row').length > 0){
            $( this )
                .closest( '.woocommerce_variation' )
                .addClass( 'variation-needs-update' );
            $(this).closest('.row').remove();
            $( 'button.cancel-variation-changes, button.save-variation-changes' ).removeAttr( 'disabled' );

        }
    });

    // Handle clicks on the verify/unverify buttons
        $(document).on('click', '.verify-order, .unverify-order', function (e) {
            e.preventDefault();

            var button = $(this);
            var orderId = button.data('order-id');

            // Send AJAX request to toggle verified order status
            $.ajax({
                url: verifiedOrderAjax.ajax_url,
                type: 'POST',
                data: {
                    action: 'toggle_verified_order_status',
                    order_id: orderId,
                    nonce: verifiedOrderAjax.nonce,
                },
                success: function (response) {
                    if (response.success) {
                        // Update button text and class
                        if (response.data.is_verified) {
                            button.text('Unverify');
                            button.attr('class', 'button unverify-order');
                            button.closest('tr').addClass('verified-order');
                        } else {
                            button.text('Verify');
                            button.attr('class', 'button verify-order');
                            button.closest('tr').removeClass('verified-order');
                        }
                    } else {
                        console.error(response.data.error);
                    }
                },
                error: function (xhr, status, error) {
                    console.error('AJAX error:', error);
                },
            });
        });
 
    // Color the verified row
        $('.post-type-shop_order .wp-list-table tr').each(function() {
            // Find the button with the class 'unverify-order' within the row
            var button = $(this).find('button.unverify-order');
            
            // Check if the button's text content is "Unverify"
            if (button.text().trim() === 'Unverify') {
                // Add a class to highlight the row
                $(this).addClass('verified-order');
            }
        });

    // Elite highlighter
        const observer = new MutationObserver(mutations => {
            mutations.forEach(mutation => {
                $( mutation.addedNodes ).each(function() {
                    $( this ).find( '#order_line_items .display_meta p, .wc-order-preview-table .wc-order-item-meta p' ).filter(function() {                
                        return $.trim($(this).text()) === "エリート";
                    }).addClass( 'elite-highlighter' );
                });
            });
        });

        // Start observing the body for dynamically added elements
        observer.observe(document.body, { childList: true, subtree: true });

        // Initial
        $( '#order_line_items .display_meta p, .wc-order-preview-table .wc-order-item-meta p' ).filter(function() {
            return $.trim($(this).text()) === "エリート";
        }).addClass( 'elite-highlighter' );

    function reorderSelected( $select2 ) {
        var selected = $select2.data( 'selected' );
        if ( ! selected ) {
            return;
        }
        selected.forEach( function ( value ) {
            var option = $select2.children( '[value="' + value + '"]' );
            option.detach();
            $select2.append( option );
        } );
        $select2.trigger( 'change' );
    }

    function update() {
        var $this = $( this ),
            options = $this.data( 'options' );
        $this.removeClass( 'select2-hidden-accessible' );
        $this.siblings( '.select2-container' ).remove();
        $this.show().select2( options );

        if ( ! $this.attr( 'multiple' ) ) {
            return;
        }

        reorderSelected( $this );

        /**
         * Preserve the order that options are selected.
         * @see https://github.com/select2/select2/issues/3106#issuecomment-255492815
         */
        $this.on( 'select2:select', function ( event ) {
            var option = $this.children( '[value="' + event.params.data.id + '"]' );
            option.detach();
            $this.append( option ).trigger( 'change' );
        } );
    }
})