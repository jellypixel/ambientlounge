var $j = jQuery.noConflict();

$j(function(){	

	/**
	 * If button is disabled - try to salvage
	 */
	jQuery(window).load(function(){

		let counter = 0;
		let interval = setInterval(() => {

			if (10 !== counter) {

				jQuery('body.woocommerce.archive .entry-content .product').each(function() {
					var product = jQuery(this);
					var addToCartButton = product.find('.single_add_to_cart_button');
					
					// Check if the add to cart button is disabled
					if (addToCartButton.hasClass('disabled')) {

						product.find('.pa_filling .tawcvs-swatches .swatch.selected').first().trigger('click');
						product.find('.pa_filling .tawcvs-swatches .swatch').not('.selected').first().trigger('click');
						product.find('.pa_color .tawcvs-swatches .swatch').not('.selected').not('.disabled').first().trigger('click');

					}
				});

				counter++;
			} else {
				clearInterval(interval)
			}

		}, 500);

	});

	/**
	 * Change URL depending on the variation
	 */

	jQuery('body.woocommerce.archive').on('click', '.swatch-item-wrapper', function (e) {	
		var linkEl = jQuery( this ).parents( 'li.type-product' ).find( '.woocommerce-LoopProduct-link' );
		var href = linkEl.attr( 'href' );			
		var variation = jQuery( this ).parents( '.tawcvs-swatches' ).first().attr( 'data-attribute_name' );
		var attribute = jQuery( this ).find('.swatch').first().attr( 'data-value' );
		if ( href.indexOf( 'attribute_pa_color' ) > -1 )
		{							
			var new_href = href.replace(/(attribute_pa_color=).*?(&|$)/,'$1' + attribute + '$2');
			
			linkEl.attr({ 'href' : new_href });
		}
		else 
		{
			var new_href = href + '?attribute_pa_color=' + attribute;
			
			linkEl.attr({ 'href' : new_href });
		}
	});

	/**
	 * Change selected color depending on the attribute
	 */
	jQuery(window).load(function() {
		var urlParams = new URLSearchParams(window.location.search);
		var filterColor = urlParams.get('filter_color');

		// Check if filter_color exists and trigger click event on matching elements
		if (filterColor) {
			jQuery('.swatch-' + filterColor).not('.selected').trigger('click');

			// Select li elements that do not have .swatch-[filterColor] inside
			jQuery('li.product-type-variable').filter(':has(div.disabled.swatch-' + filterColor + ')').hide();
		}

		//jQuery(".woocommerce-archive-wrapper .entry-content .products ").css("visibility", "visible");
	});

	/**
	 * If using filter: remove all item that is out of stock but have that variation.
	 
	var urlParams = new URLSearchParams(window.location.search);
	var filterColorValue = urlParams.get("filter_color");
	var targetSwatch = jQuery(".swatch[data-value='" + filterColorValue + "']");

	if (targetSwatch.hasClass("disabled")) {
		var parentProduct = targetSwatch.closest(".product");
		parentProduct.hide();
	}
	*/

	/**
	 * Popup Filter
	 */
	var lightboxOverlay = $j( '.lightbox-overlay' );
	var lightboxEl = $j( '.lightbox-wrapper' );

	// Create a function to observe the DOM changes
	var attrAdded = false;

	function observeFilterDOM() {
		// Select the target node to observe
		var targetNode = document.body;

		// Create a new MutationObserver
		var observer = new MutationObserver(function(mutations) {
			// Check each mutation that occurred
			mutations.forEach(function(mutation) {
			// Check if any nodes were added
			if (mutation.addedNodes && mutation.addedNodes.length > 0) {
				// Check if the desired element now exists
				var element = jQuery('.wc-block-attribute-filter-list');
				if (element.length) {
				// Disconnect the observer
				observer.disconnect();

				// Execute your code
				if(!attrAdded) {

					var checkExist = setInterval(function() {
						if (jQuery('.collection-filter-color .wc-block-components-checkbox__label img').length) {
						   clearInterval(checkExist);
						} else {
							$j('.wc-blocks-filter-wrapper .collection-filter-color .wc-block-components-checkbox .wc-block-components-checkbox__input').each(function() {
							var id = $j(this).attr('id');
							var swatch = $j('.swatch[data-value="' + id + '"]');
							if (swatch.length > 0) {
								var backgroundImage = window.productAttributeImageUrls[id];
								backgroundImage = backgroundImage.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
								if(jQuery(this).siblings('.wc-block-components-checkbox__label').find('img').length == 0) {
								jQuery(this).siblings('.wc-block-components-checkbox__label').prepend('<img src="' + backgroundImage + '">');
								}
							}
							});
				
							jQuery('.wp-block-woocommerce-filter-wrapper .collection-filter-color .style-list ul').each(function() {
							var waterResistantDiv = jQuery('<div class="water-resistant"></div>');
							var normalDiv = jQuery('<div class="normal"></div>');  
							
							jQuery(this).find('li').each(function() {
							// Remove li elements with class "show-more"
							if (jQuery(this).hasClass('show-more')) {
								jQuery(this).remove();
								return;
							}
							
							var checkbox = jQuery(this).find('.wc-block-checkbox-list__checkbox');
							var label = checkbox.find('label');
							
							if (label.attr('for') && label.attr('for').indexOf('waterresistant') !== -1) {
								waterResistantDiv.append(jQuery(this));
							} else {
								normalDiv.append(jQuery(this));
							}
							
							// Unhide li elements with hidden attribute
							if (jQuery(this).attr('hidden')) {
								jQuery(this).removeAttr('hidden');
							}
							});
							
							jQuery(this).append(normalDiv);
							jQuery(this).append(waterResistantDiv);
							});				  
				
							// Filter Color Type
							$j( '.wc-blocks-filter-wrapper .collection-filter-color .normal' ).prepend( '<div class="collection-filter-color-normal">インテリア</div>' );
							$j( '.wc-blocks-filter-wrapper .collection-filter-color .water-resistant' ).prepend( '<div class="collection-filter-color-water-resistant"><div class="collection-filter-color-water-resistant-image"></div><div class="collection-filter-color-water-resistant-heading"><div class="collection-filter-color-water-resistant-title">インテリア＋プラス</div><div class="collection-filter-color-water-resistant-subtitle">撥水性/褪色防止/引っ掻きに強い/ペットフレンドリー</div></div></div>' );
				
							attrAdded = true;
						}
					 }, 100);
				}
				
				}
			}
			});
		});

		// Start observing the target node for DOM changes
		observer.observe(targetNode, { childList: true, subtree: true });
	}

	observeFilterDOM();

	$j( '.popup-filter ').click( function() {
		//openLightbox();
		slideLightbox();
	})

	$j( '.lightbox-overlay:not(.slide-lightbox) .lightbox-close').click( function(e) {
		e.preventDefault();
		closeLightbox();
	})

	lightboxOverlay.click( function(e) {
		 if ($j(e.target).has('.lightbox-wrapper').length) {
			closeLightbox();
		}
	})

	function openLightbox() {
		lightboxOverlay.fadeIn( 200, function() {
			lightboxEl.fadeIn(400);
		});
	}

	function closeLightbox() {
		lightboxEl.fadeOut( 200, function() {
			lightboxOverlay.fadeOut(400);
		});
	}

	function slideLightbox() {
		if ( lightboxOverlay.hasClass( 'active' ) )
		{
			lightboxEl.slideUp(400, function() {
				lightboxOverlay.hide().removeClass( 'active' );											
			});
			
		}
		else {
			lightboxOverlay.show().addClass( 'active' );
			lightboxEl.slideDown(400);
		}
	}

});  