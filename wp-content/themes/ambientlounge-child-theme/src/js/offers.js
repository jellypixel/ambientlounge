var $j = jQuery.noConflict();

$j(function(){	

	// Modal
	var modalOverlay = $j( '.modal-overlay' );
	var modalWrapper = $j( '.modal-wrapper' );
	var modalContentList = $j( '.modal-content-list' );
	var modalContentInner = $j( '.modal-content-inner' );

	// Close
	$j( 'body' ).on( 'click', '.modal-close', function(e) {
		e.stopPropagation();

		closeModal();
	});

	$j( 'body' ).on( 'click', '.modal-overlay', function(e) {
		if ( $j( e.target ).is( '.modal-overlay' ) ) {
			closeModal();
		} 
	});

	function openModal( contentClass ) {					
		modalContentInner.html( modalContentList.find( contentClass ).prop("outerHTML") );
		modalWrapper.attr({ 'name' : contentClass.substring(1) })            

		modalOverlay.fadeIn(400, function() {
			modalWrapper.fadeIn(200, function() {
				modalWrapper.css({ 'display' : 'flex' })
			});
		})
	}			

	function closeModal() {
		modalWrapper.fadeOut(400, function() {
			modalOverlay.fadeOut(200, function() {
				modalContentInner.empty();

				modalWrapper.attr({ 'name' : '' })
			});
		})
	}

	/**
	 * Offer Modal
	 */
	$j( '.offer-confirm-addtocart' ).click( function() {
		var productEl = $j( this ).closest( '.product' );
		var productThumb = productEl.find( '.attachment-woocommerce_thumbnail' ).attr( 'src' );
		var productTitle = productEl.find( '.woocommerce-loop-product__title' ).html();

		modalContentList.find( '#modal-offers .modal-thumb' ).empty().append( '<img src="' + productThumb + '" alt="">')

		openModal( '#modal-offers' );			
	});

	/**
	 * Monitor value
	 */
	$j('.page-template-template-offers .single_variation_wrap .variation_id').on('change', function() {
		var newValue = $j(this).val();

		if((typeof(newValue) == "undefined") || (newValue == "")) {
			$j(this).closest('.type-product').find('.offer-confirm-addtocart').attr('disabled', true);
		} else {
			// Copy the value to data-product_id attribute of class offer-confirm-addtocart
			$j(this).closest('.type-product').find('.offer-confirm-addtocart').attr('disabled', false);
			$j(this).closest('.type-product').find('.offer-confirm-addtocart').attr('data-product_id', newValue);
			$j(this).closest('.type-product').find('.price').html($j(this).closest('.type-product').find('.single_variation_wrap .price').html());
		}
	});

	/**
	 * Add "variation-label" div inside "variation-selector"
	 */
	jQuery('.page-template-template-offers .variation-selector').append('<div class="variation-label-sync"></div>');

	/**
	 * Update "variation-label" on select value change
	 */
	jQuery('.page-template-template-offers .variation-selector select').on('change', function() {
		const selectedValue = jQuery(this).find("option:selected").text();
		jQuery(this).siblings('.variation-label-sync').text(selectedValue);
	});

	jQuery('.page-template-template-offers .variation-selector select').trigger("change");

	/**
	 * Put class in swatch circle type parent
	 */
	$j( '.page-template-template-offers .swatch-shape-circle' ).closest( '.tawcvs-swatches' ).addClass( 'swatches-circle' );
});  