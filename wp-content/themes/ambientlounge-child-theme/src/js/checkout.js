var $j = jQuery.noConflict();

$j(function(){	

	// Checkout - Toggle form login link below title
	$j( '.checkout-registereduser').click( function() {
		$j( '.woocommerce-form-login' ).slideToggle( 400 );
	})

	// Checkout - move amazon pay button from woocommerce notification to below
	setTimeout( function() {
		$j( '#pay_with_amazon' ).prependTo( '.checkout-expresscheckout');
		$j( '.wc-amazon-payments-advanced-info' ).hide();
	}, 2000 );

	// Checkout - collapsible mobile sidebar  
	$j( '.checkout-sidebar-topbar' ).click( function() {
		var sidebarEl = $j( this ).parents( '.checkout-sidebar' );
		var sidebarContentEl = sidebarEl.find( '.checkout-sidebar-innermost' );

		if ( sidebarEl.hasClass( 'collapsed' ) )
		{
			sidebarContentEl.slideDown( 400 );
		}
		else
		{
			sidebarContentEl.slideUp( 400 );
		}

		sidebarEl.toggleClass( 'collapsed' );
	})

	// Remove populated fields
	if($j("body.woocommerce-checkout").length) {
	
		if ((window.location.href.indexOf('order-pay') == -1) && (window.location.href.indexOf('order-received') == -1) && (window.location.href.indexOf('payment-method') == -1))
		{
			if($j(".woocommerce-checkout #amazon_customer_details").length) {
				$j(".woocommerce-checkout #billing_yomigana_last_name_field").hide();
				$j(".woocommerce-checkout #billing_yomigana_first_name_field").hide();
				$j(".woocommerce-checkout #shipping_yomigana_last_name_field").hide();
				$j(".woocommerce-checkout #shipping_yomigana_first_name_field").hide();
				$j(".woocommerce-checkout #billing_state_field").hide();
			} 
		}
	}

	// Automatically enter zipcode
	var gettingPostcode = false;

    $j("#shipping_postcode").on('input',function() {
        postcodeLookup('shipping',$j(this).val())
    });

    $j("#billing_postcode").on('input',function() {
        postcodeLookup('billing',$j(this).val())
    });

    function postcodeLookup(section, postcode){
        var postcodeAscii = postcode.replace(
            /[\uff01-\uff5e]/g,
            function(ch) { return String.fromCharCode(ch.charCodeAt(0) - 0xfee0); }
        ); 

        postcodeAscii = postcodeAscii.replace(/ー|－|‐|-|―/gi, '');

        var postCodePatt = new RegExp(/^\d{7}/);
        if(postCodePatt.test(postcodeAscii)){
            if(gettingPostcode === false){
                gettingPostcode = true;
 
                // https://postcodes.peakdigital.cloud/
                var jqxhr = $j.ajax({
                    url: 'https://ambientlounge.co.jp/wp-content/themes/ambientlounge-child-theme/postcodelookup.php',
                    data: {postcode: postcodeAscii}
                })
                    .done(function(data) {
                        if(section === 'shipping'){
                            setShippingAddress(data.data);
                        } else if (section ==='billing'){
                            setBillingAddress(data.data);
                        } 
                    })
                    .always(function() {
                        gettingPostcode = false;
                    });

            }
        }
    };

	function setShippingAddress(data){
        $j('#shipping_city').val(data.kanji_city);
        $j('#shipping_address_1').val(data.kanji_address);

        // $j('#shipping_state').val(data.state_code).change();
        jQuery("#shipping_state").find('option').each(function() {   
            if (jQuery(this).text() === data.state_code) {
                jQuery(this).prop('selected', true);
                jQuery("#billing_state").trigger('change');
                return false; // Break the loop once the option is found and selected
            }
        });
    }

    function setBillingAddress(data){
        $j('#billing_city').val(data.kanji_city);
        $j('#billing_address_1').val(data.kanji_address);

        // $j('#billing_state').val(data.state_code).change();
        jQuery("#billing_state").find('option').each(function() {   
            if (jQuery(this).text() === data.state_code) {
                jQuery(this).prop('selected', true);
                jQuery("#billing_state").trigger('change');
                return false; // Break the loop once the option is found and selected
            }
        });
    }

    // Remove some info for wholesale customer
    if ($("body").hasClass("b2b_customer") && $("body").hasClass("woocommerce-order-received")) {
        let start = $(".woocommerce-order-overview");
        let end = $(".wc-furikomi-bank-details-heading");

        if (start.length && end.length) {
            start.nextUntil(end, "p, ul, ol").remove();
        }

        $(".wc-furikomi-bank-details-heading, .wc-furikomi-bank-details").remove();
    }
	
});  