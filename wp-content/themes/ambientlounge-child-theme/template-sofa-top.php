<?php
/*
	Template Name: Sofa Top
*/
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/adjustment.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/font/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/main-dark.all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/sofa-top.css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto Sans JP:100,200,300,400,500,600,700|Noto Sans JP:600|Open Sans:600|Open Sans|Open Sans:700&amp;subset=latin-ext&amp;display=swap" media="all" onload="this.media='all'">

<section id="banner-bottom">
    <!--container bg-black start-->
    <div class="container-fluid container-2">
        <div class="container-dark">
            <div class="row">
                <div class="col-xs-12">
                    <p class="bottom-banner-header">ベストセラー</p>
                </div>
            </div>
            <div class="row banner-bottom-row-dark">
                <div class="col-xs-12 col-md-6 banner-text-box">
                    <p class="banner-bottom-subtitle">人気No1のソファ</p>
                    <p class="banner-bottom-title text-gradient-blue">バタフライ</p>
                    <p><a class="btn btn-primary details-btn" href="https://ambientlounge.co.jp/butterfly/">詳細を見る</a></p>
                    <p><a class="buy-link text-decoration-none" href="https://ambientlounge.co.jp/products/sofa/1-person/butterfly/">購入<i class="bi bi-chevron-right"></i></a></p>
                </div>
                <div class="col-xs-12 col-md-6 p-0">
                    <div class="banner-bottom-img-1"></div>
                </div>
            </div>
            <div class="row banner-bottom-row-dark">
                <div class="col-xs-12 col-md-6 banner-text-box order-1 order-md-2">
                    <p class="banner-bottom-subtitle">つつみこまれる心地よさ</p>
                    <p class="banner-bottom-title text-gradient-pink">アクスティック</p>
                    <p><a class="btn btn-primary details-btn" href="https://ambientlounge.co.jp/acoustic/">詳細を見る</a></p>
                    <p><a class="buy-link text-decoration-none" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/">購入<i class="bi bi-chevron-right"></i></a></p>
                </div>
                <div class="col-xs-12 col-md-6 p-0 order-2 order-md-1">
                    <div class="banner-bottom-img-2"></div>
                </div>
            </div>
            <div class="row banner-bottom-row-dark">
                <div class="col-xs-12 banner-text-box banner-text-box-mt">
                    <p class="banner-bottom-subtitle">ファスナーでつなぎ合わせて</p>
                    <p class="banner-bottom-title text-gradient-blue">モジュラー</p>
                    <p><a class="btn btn-primary details-btn" href="https://ambientlounge.co.jp/modular/">詳細を見る</a></p>
                    <p><a class="buy-link text-decoration-none" href="https://ambientlounge.co.jp/collection/modular/">購入<i class="bi bi-chevron-right"></i></a></p>
                </div>
                <div class="col-xs-12 p-0 my-auto">
                    <div class="banner-bottom-img-3"></div>
                </div>
            </div>
        </div>
    </div>
    <!--container bg-black end-->
</section>

<section id="product-details">
    <div class="container-fluid bg-light p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                    <div class="product-panel-white">
                        <p class="light-panel-title">アクセサリー</p>
                        <p class="light-panel-subtitle">クッションやブランケット、メンテンス用品など
                            <br>いろいろなアイテムが勢ぞろい</p>
                        <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/sofa-accessories/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                        <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa-top/sofa-top-1.webp"/>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                    <div class="product-panel-white">
                        <p class="light-panel-title">オットマン・<br>テーブル</p>
                        <p class="light-panel-subtitle">テーマに合わせて4種ご用意</p>
                        <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/collection/sofa/ottoman/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                        <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa-top/sofa-top-2.webp"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="sofa-compare">       
    <?php
        $sofa_compare_ID = 78784;
    ?>

    <div class="content-area">
        <header class="entry-header">            
            <div class="page-heading-wrapper">
                <div class="entry-container">
                    <?php
                        $args = array(
                            'post_type'      => 'page',
                            'posts_per_page' => -1,
                            'post_parent'    => $sofa_compare_ID,
                            'order'          => 'ASC',
                            'orderby'        => 'menu_order'
                        );

                        $parent = new WP_Query( $args );

                        if ( $parent->have_posts() ) 
                        {
                            $i = 0;
                            while ( $parent->have_posts() ) 
                            {
                                $parent->the_post(); 
                                global $post;
                                
                                $custom_page_title = get_field( 'custom_page_title');

                                $page_title = get_the_title();
                                if ( !empty( $custom_page_title ) )
                                {
                                    $page_title = get_field( 'custom_page_title');
                                }
                                
                                ?>  
                                    <h1 class="entry-title <?php echo ( $i == 0 ) ? "active" : "" ?>" itemprop="headline" postid="<?php echo $post->ID; ?>" data-slug="<?php echo $post->post_name; ?>"><?php echo nl2br( $page_title ); ?></h1>
                                <?php
                                $i++;
                            }
                        }
                        wp_reset_postdata(); 
                    ?>  
                </div>
            </div>
        </header>

        <div class="entry-content">                     
            <?php                    
                $args = array(
                    'page_id' => $sofa_compare_ID,
                );      

                $query = new WP_Query( $args );
                if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) {
                        $query->the_post();
                        the_content();
                    }
                }
                wp_reset_postdata();
            ?>   
            
            <div class="blog-category-wrapper <?php //bigger ?>">                
                <div class="blog-category-inner">
                    <?php   
                        $args = array(
                            'post_type'      => 'page',
                            'posts_per_page' => -1,
                            'post_parent'    => $sofa_compare_ID,
                            'order'          => 'ASC',
                            'orderby'        => 'menu_order'
                        );

                        $parent = new WP_Query( $args );

                        if ( $parent->have_posts() ) 
                        {
                            $i = 0;
                            while ( $parent->have_posts() ) 
                            {
                                $parent->the_post(); 
                                global $post;
                                
                                $custom_page_title = get_field( 'custom_page_title');
                                $custom_tab_title = get_field( 'custom_tab_title');

                                $tab_title = get_the_title();
                                if ( !empty( $custom_tab_title ) )
                                {
                                    $tab_title = $custom_tab_title;
                                }
                                
                                ?>  
                                    <div class="blog-category <?php echo ( $i == 0 ) ? "active" : "" ?>" data-target="<?php echo $post->ID; ?>" data-slug="<?php echo $post->post_name; ?>"><?php echo $tab_title; ?></div>                                    
                                <?php
                                $i++;
                            }
                        }
                        wp_reset_postdata(); 
                    ?>                     
                </div>  
            </div>

            <div class="compare-content-wrapper">
                <?php         
                    global $post;
                    $args = array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_parent'    => $sofa_compare_ID,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order'
                    );

                    $parent = new WP_Query( $args );

                    if ( $parent->have_posts() ) 
                    {
                        while ( $parent->have_posts() ) 
                        {
                            $parent->the_post(); 
                            global $post;
                            
                            ?>
                                <div id="<?php echo $post->ID; ?>" data-slug="<?php echo $post->post_name; ?>" class="compare-content">
                                    <?php the_content(); ?>
                                </div>
                            <?php
                        }
                    }
                    wp_reset_postdata(); 
                ?>  
            </div>            
            
        </div>
    </div>

    <script type="text/javascript">
        var $j = jQuery.noConflict();

        $j(function(){	
            $j( '.blog-category' ).click( function() {
                var targetEl = $j( this ).attr( 'data-target' );

                $j( this ).addClass( 'active' ).siblings().removeClass( 'active' );

                $j( '.page-heading-wrapper .entry-container[postid="' + $j( this ).attr( 'data-target' ) + '"]' ).addClass( 'active' ).siblings().removeClass( 'active' );

                $j( '#' + targetEl ).fadeIn( 400 ).siblings().hide();
            })
        });
    </script>    
</section>

<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/bootstrap.bundle.min.js"></script>
<?php
get_footer();
?>