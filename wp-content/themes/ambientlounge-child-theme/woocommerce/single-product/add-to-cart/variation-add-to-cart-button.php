<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">	
	<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );		
	?>

	<?php
		woocommerce_quantity_input(
			array(
				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
				'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
			)
		);
	?>

	<?php
		do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>

	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>	

	
	<button type="submit" class="single_add_to_cart_button button alt<?php echo esc_attr( wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '' ); ?>"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
	
	<?php
	/*
	<a href="<?php the_permalink(); ?>">
		<div class="std-button">
			<?php the_title(); ?>
		</div>
	</a>
	*/
	?>
	

	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>

<div class="variation-info-wrapper">
	<div class="variation-info">
		<div class="variation-info-icon">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/woocommerce-general/confirmdeliverydate.svg" alt="Confirm Delivery Date">
		</div>
		<div class="variation-info-link">
			<a id="openmodal-filling" href="#content-deliverydays"><?php _e( 'お届け予定日を確認', 'ambientlounge' ); ?></a>
		</div>
		<div class="variation-info-description">
			<?php 
				$product_type = get_field( 'product_type' );				

				if ( $product_type == 'butterfly' || $product_type == 'acoustic' ) { // Sofa
					_e( '最短3営業日〜10営業日以内に発送', 'ambientlounge' );
				}
				else { // Dog / Cat
					_e( '最短2営業日〜5営業日以内に発送', 'ambientlounge' );
				}
			?>
		</div>
	</div>
	<div class="variation-info">
		<div class="variation-info-icon">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/woocommerce-general/findastore.svg" alt="Find a Store">
		</div>
		<div class="variation-info-link">
			<a href="https://ambientlounge.co.jp/retail-locations/"><?php _e( '店舗を探す', 'ambientlounge' ); ?></a>
		</div>
		<div class="variation-info-description">			
			<?php _e( '取り扱い店舗をご案内します', 'ambientlounge' ); ?>
		</div>
	</div>
</div>
