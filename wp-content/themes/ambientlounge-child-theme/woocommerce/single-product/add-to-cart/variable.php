<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 6.1.0
 */

defined( 'ABSPATH' ) || exit; 

global $product;
 
$attribute_keys  = array_keys( $attributes );
$variations_json = wp_json_encode( $available_variations );
$variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

	<?php 
		if ( empty( $available_variations ) && false !== $available_variations ) 
		{
			?>
				<p class="stock out-of-stock"><?php echo esc_html( apply_filters( 'woocommerce_out_of_stock_message', __( 'This product is currently out of stock and unavailable.', 'woocommerce' ) ) ); ?></p>
			<?php  
		}
		else 
		{
			?>
				<?php
				/*
				<table class="variations" cellspacing="0" role="presentation">
					<tbody>
						<?php foreach ( $attributes as $attribute_name => $options ) : ?>
							<tr>
								<th class="label"><label for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>"><?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok. ?></label></th>
								<td class="value">
									<?php
										wc_dropdown_variation_attribute_options(
											array(
												'options'   => $options,
												'attribute' => $attribute_name,
												'product'   => $product,
											)
										);
										echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a>' ) ) : '';
									?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				*/
				?>

				<div class="variations">
					<?php 
						foreach ( $attributes as $attribute_name => $options ) 
						{							
							?>
								<div class="variation-wrap <?php echo $attribute_name ?>">
									<div class="variation-label">
										<?php
											if ( $attribute_name == 'pa_color' )	
											{
												global $product;
												$fabricSampleRequestID = $fabricSampleRequestLink = '';
												
												// Only for sofa product
												if( has_term( array( 'sofa' ), 'product_cat', $product->id ) ) {
													$fabricSampleRequestID = '';
													$fabricSampleRequestLink = $GLOBALS['currDomain'] . '/sofa-sample/';
												}
												// Otherwise get lightbox
												else {
													$fabricSampleRequestID = 'openmodal-filling';
													$fabricSampleRequestLink = '#content-filling';
												}

												// PA Color
												?>
													<div class="pa_color_variation_label">														
														<?php echo __( 'カラー', 'ambientlounge' ); ?>
														<div class="info-link">
															<a id="<?php echo $fabricSampleRequestID; ?>" href="<?php echo $fabricSampleRequestLink; ?>"><?php echo __( '生地サンプル請求', 'ambientlounge' ); ?><svg width="8" height="9" viewBox="0 0 8 9" fill="none" xmlns="http://www.w3.org/2000/svg">
	<path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 1.50781C4.37615 1.50781 4.21875 1.35041 4.21875 1.15625C4.21875 0.962087 4.37615 0.804688 4.57031 0.804688H7.03125C7.22541 0.804688 7.38281 0.962087 7.38281 1.15625V3.61719C7.38281 3.81135 7.22541 3.96875 7.03125 3.96875C6.83709 3.96875 6.67969 3.81135 6.67969 3.61719V2.005L3.17828 5.5064C3.04099 5.6437 2.81839 5.6437 2.6811 5.5064C2.5438 5.36911 2.5438 5.14651 2.6811 5.00922L6.1825 1.50781H4.57031Z" fill="#888888"/>
	<path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 1.15625C0.117188 0.962087 0.274587 0.804688 0.46875 0.804688H2.51953C2.71369 0.804688 2.87109 0.962087 2.87109 1.15625C2.87109 1.35041 2.71369 1.50781 2.51953 1.50781H0.820312V7.36719H6.67969V5.66797C6.67969 5.47381 6.83709 5.31641 7.03125 5.31641C7.22541 5.31641 7.38281 5.47381 7.38281 5.66797V7.71875C7.38281 7.91291 7.22541 8.07031 7.03125 8.07031H0.46875C0.274587 8.07031 0.117188 7.91291 0.117188 7.71875V1.15625Z" fill="#888888"/></svg>
															</a>
														</div>
													</div>
													
													<label for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
														<?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok.  ?>
													</label>
												<?php
											}
											else if ( $attribute_name == 'pa_bed-selection' )	{
												?>
													<div class="variation-label">
														<label class="dog-filling" for="pa_bed-selection">
															<?php echo __( 'ベッドを選択', 'ambientlounge' ); ?>																												
															<div class="info-link">
																<a href="/pet-sample/" title="<?php echo __( '生地サンプル請求', 'ambientlounge' ); ?>">																	
																	<?php echo __( '生地サンプル請求', 'ambientlounge' ); ?>
																	<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg>
																</a>
															</div>
														</label>								
													</div>
												<?php
											}
											else if ( $attribute_name == 'pa_cover-selection' )	{
												?>
													<div class="variation-label">
														<label class="dog-filling" for="pa_cover-selection">															
															<?php echo __( 'カバーを選択', 'ambientlounge' ); ?>								
															<div class="info-link">
																<a href="/pet-sample/" title="<?php echo __( '生地サンプル請求', 'ambientlounge' ); ?>">																	
																	<?php echo __( '生地サンプル請求', 'ambientlounge' ); ?>
																	<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg>
																</a>
															</div>
														</label>			
														
														<?php
															if( $product->id == 79755 ) {
																echo __( '<div class="sub-label">セットされるカバーもエコペットカバーとなります</div>', 'ambientlounge' ); 
															}
														?>	
													</div>
												<?php
											}
											else if ( $attribute_name == 'pa_filling' )	{

												$fillingLink = '#content-filling-sofa';
												if ( $product_type == 'dog' ) {
													$fillingLink = '#content-filling-dog';
												}
												else if ( $product_type == 'cat' ) {
													$fillingLink = '#content-filling-cat';
												}

												?>
													<div class="variation-label">
														<label class="dog-filling" for="pa_filling">																	
															<?php echo __( 'フィリングを選択', 'ambientlounge' ); ?>	
															<!--										
															<div class="info-link">
																<a id="openmodal-filling" href="<?php echo $fillingLink; ?>">																	
																	<?php echo __( 'フィリングの種類', 'ambientlounge' ); ?>
																	<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.04688 8.95312C7.11794 8.95312 8.79688 7.27419 8.79688 5.20312C8.79688 3.13206 7.11794 1.45312 5.04688 1.45312C2.97581 1.45312 1.29688 3.13206 1.29688 5.20312C1.29688 7.27419 2.97581 8.95312 5.04688 8.95312ZM5.04688 9.65625C7.50627 9.65625 9.5 7.66252 9.5 5.20312C9.5 2.74373 7.50627 0.75 5.04688 0.75C2.58748 0.75 0.59375 2.74373 0.59375 5.20312C0.59375 7.66252 2.58748 9.65625 5.04688 9.65625Z" fill="#888888"></path><path d="M4.62063 5.72391H5.35188C5.26188 4.94766 6.49375 4.84641 6.49375 4.02516C6.49375 3.27703 5.8975 2.89453 5.08187 2.89453C4.48 2.89453 3.985 3.17016 3.625 3.58641L4.08625 4.00828C4.36187 3.72141 4.64313 3.56391 4.98625 3.56391C5.43063 3.56391 5.70063 3.75516 5.70063 4.09828C5.70063 4.64391 4.5025 4.84078 4.62063 5.72391ZM4.99188 7.22578C5.27313 7.22578 5.48688 7.01766 5.48688 6.72516C5.48688 6.43266 5.27313 6.23016 4.99188 6.23016C4.705 6.23016 4.49125 6.43266 4.49125 6.72516C4.49125 7.01766 4.69937 7.22578 4.99188 7.22578Z" fill="#888888"></path></svg>
																</a>
															</div>
															-->
														</label>								
													</div> 
												<?php
												
											}
											else if ( $attribute_name == 'pa_size' )
											{
												$product_type = get_field( 'product_type' );
												$classFillingModal = '';												

												if ( $product_type == 'dog' ) {
													$classFillingModal = 'dog-filling';
												}
												else if ( $product_type == 'cat' ) {
													$classFillingModal = 'cat-filling';
												}
												else if ( $product_type == 'butterfly' || $product_type == 'acoustic' ) {
													$classFillingModal = 'sofa-filling';
												}
												if( $product->id == 64552 )  {
												?>
													<a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge-plus/" class="slim-link" style="text-decoration: underline; display: block; margin-bottom: 20px; margin-top:-20px; color: #e93121; border-radius: 10px;"><img style="border-radius:10px;" src="https://ambientlounge.co.jp/wp-content/uploads/2024/04/AL-Banner-240412-Plus.jpg"/></a>

													<label class="<?php echo $classFillingModal; ?>" for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
														<?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok.  ?>
														<div class="info-link">
															<a id="openmodal-filling" href="#content-petsize">																	
																<?php echo __( 'サイズ比較表', 'ambientlounge' ); ?>
																<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg>
															</a>
														</div>
													</label>
												<?php
												} 

												if( $product->id == 79755 )  {
													?>
														<a href="https://ambientlounge.co.jp/products/eco-campaign/covers-eco/" class="slim-link" style="text-decoration: underline; display: block; margin-bottom: 20px; margin-top:-20px; color: #e93121; border-radius: 10px;"><img style="border-radius:10px;" src="https://ambientlounge.co.jp/wp-content/uploads/2024/06/WhatsApp-Image-2024-06-21-at-2.57.59-PM.jpeg"/></a>
	
														<label class="<?php echo $classFillingModal; ?>" for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
															<?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok.  ?>
															<div class="info-link">
																<a id="openmodal-filling" href="#content-petsize">																	
																	<?php echo __( 'サイズ比較表', 'ambientlounge' ); ?>
																	<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg>
																</a>
															</div>
														</label>
													<?php
												} 

												if( $product->id == 81304) {
													?>
														<a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge-plus/" class="slim-link" style="text-decoration: underline; display: block; margin-bottom: 20px; color: #e93121; border-radius: 10px;"><img style="border-radius:10px;" src="https://ambientlounge.co.jp/wp-content/uploads/2024/04/AL-Banner-240412-Plus.jpg"/></a>
	
														<label class="<?php echo $classFillingModal; ?>" for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
															<?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok.  ?>
															<div class="info-link">
																<a id="openmodal-filling" href="#content-petsize">																	
																	<?php echo __( 'サイズ比較表', 'ambientlounge' ); ?>
																	<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg>
																</a>
															</div>
														</label>
													<?php
													} 

												if ($product->id == 85745 ) {
													?>
														<a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/" class="slim-link" style="text-decoration: underline; display: block; margin-bottom: 20px; color: #e93121; border-radius: 10px; margin-top: 10px;"><img style="border-radius:10px;" src="https://ambientlounge.co.jp/wp-content/uploads/2024/04/AL-Banner-240412-Classic.jpg"/></a>

														<label class="<?php echo $classFillingModal; ?>" for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
															<?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok.  ?>
															<div class="info-link">
																<a id="openmodal-filling" href="#content-petsize">																	
																	<?php echo __( 'サイズ比較表', 'ambientlounge' ); ?>
																	<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg>
																</a>
															</div>
														</label>
													<?php
												}

												if (($product->id == 91303 ) || ($product->id == 91894 )) {
													?>
														<label class="<?php echo $classFillingModal; ?>" for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
															<?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok.  ?>
															<div class="info-link">
																<a id="openmodal-filling" href="#content-petsize">																	
																	<?php echo __( 'サイズ比較表', 'ambientlounge' ); ?>
																	<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg>
																</a>
															</div>
														</label>
													<?php
												}
											}
											else if ( $attribute_name == 'pa_product-type' ) {
												/*
												if ($product->id == 91303 ) {
													?>
														<a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/" class="slim-link" style="text-decoration: underline; display: block; margin-bottom: 20px; color: #e93121; border-radius: 10px; margin-top: 10px;"><img style="border-radius:10px;" src="https://ambientlounge.co.jp/wp-content/uploads/2024/04/AL-Banner-240412-Classic.jpg"/></a>

														<label class="<?php echo $classFillingModal; ?>" for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
															<?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok.  ?>
															<div class="info-link">
																<a id="openmodal-filling" href="#content-petsize">																	
																	<?php echo __( 'サイズ比較表', 'ambientlounge' ); ?>
																	<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg>
																</a>
															</div>
														</label>
													<?php
												}
												*/
											}
											else if ( $attribute_name == 'pa_size-cat' )
											{
												$product_type = get_field( 'product_type' );
												$classFillingModal = '';												

												if ( $product_type == 'dog' ) {
													$classFillingModal = 'dog-filling';
												}
												else if ( $product_type == 'cat' ) {
													$classFillingModal = 'cat-filling';
												}
												else if ( $product_type == 'butterfly' || $product_type == 'acoustic' ) {
													$classFillingModal = 'sofa-filling';
												}
 
												?>
													<a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge-plus/" class="slim-link" style="text-decoration: underline; display: block; margin-bottom: 20px; color: #e93121; border-radius: 10px;"><img style="border-radius:10px;" src="https://ambientlounge.co.jp/wp-content/uploads/2024/04/AL-Banner-240412-Plus.jpg"/></a>

													<label class="<?php echo $classFillingModal; ?>" for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
														<?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok.  ?>
														<div class="info-link">
															<a id="openmodal-filling" href="#content-petsize">																	
																<?php echo __( 'サイズ比較表', 'ambientlounge' ); ?>
																<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg>
															</a>
														</div>
													</label>
												<?php
											}
											else
											{	
												$product_type = get_field( 'product_type' );
												$classFillingModal = '';												

												if ( $product_type == 'dog' ) {
													$classFillingModal = 'dog-filling';
												}
												else if ( $product_type == 'cat' ) {
													$classFillingModal = 'cat-filling';
												}
												else if ( $product_type == 'butterfly' || $product_type == 'acoustic' ) {
													$classFillingModal = 'sofa-filling';
												}

												?>
													<label class="<?php echo $classFillingModal; ?>" for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
														<?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok.  ?>
													</label>
												<?php
											}		
										?>										
									</div>
									<div class="variation-value value">
										<?php
											wc_dropdown_variation_attribute_options(
												array(
													'options'   => $options,
													'attribute' => $attribute_name,
													'product'   => $product,
												)
											);
											echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a>' ) ) : '';

											if ( $attribute_name == 'pa_size' ) {
												if (($product->id == 91303 ) || ($product->id == 91894 )) {
													?>
														<div class="size-note">
															*XSはLow (ロー)のみ <br> *XXLはHigh (ハイ)のみ
														</div>
													<?php
												}
											}
										?>
									</div>
								</div>
							<?php
						}

						/*
							Single Product - "Pet Lounge" Deluxe Variation 
							From JP Plugin
						*/
						$el = 
							'<div class="variation-wrap pa_deluxe_info grey-cover">
								<div class="variation-label">
									<label for="pa_deluxe_info">
										セット内容       
										<div class="sizeguide-link">
											※セット内容は変更できません
										</div>                 
									</label>
								</div>
								<div class="variation-value value">                                
									<div class="tawcvs-swatches" data-attribute_name="attribute_pa_deluxe_info">
										<div class="swatchclone swatch-deluxe-one selected" title="deluxe-one" data-value="deluxe-one">
											<div class="swatch-top">
												<div class="swatch-label">
													ペットラウンジ 1点
												</div>
												<div class="swatch-image">
													<img src="https://ambientlounge.co.jp/wp-content/uploads/2022/11/delux-grey-base1000x1000.jpg">
													<div class="swatch-label">
														※ ベース本体
													</div>
												</div>
											</div>
											<div class="swatch-bottom">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/filling-1.svg">                                
												</div>                            
												<div class="swatch-text">
													<div class="swatch-label">
														フィリング
														<div class="info-link"><span id="openmodal-filling-deluxe" href="#content-filling">フィリングの種類<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.04688 8.95312C7.11794 8.95312 8.79688 7.27419 8.79688 5.20312C8.79688 3.13206 7.11794 1.45312 5.04688 1.45312C2.97581 1.45312 1.29688 3.13206 1.29688 5.20312C1.29688 7.27419 2.97581 8.95312 5.04688 8.95312ZM5.04688 9.65625C7.50627 9.65625 9.5 7.66252 9.5 5.20312C9.5 2.74373 7.50627 0.75 5.04688 0.75C2.58748 0.75 0.59375 2.74373 0.59375 5.20312C0.59375 7.66252 2.58748 9.65625 5.04688 9.65625Z" fill="#888888"/><path d="M4.62063 5.72391H5.35188C5.26188 4.94766 6.49375 4.84641 6.49375 4.02516C6.49375 3.27703 5.8975 2.89453 5.08187 2.89453C4.48 2.89453 3.985 3.17016 3.625 3.58641L4.08625 4.00828C4.36187 3.72141 4.64313 3.56391 4.98625 3.56391C5.43063 3.56391 5.70063 3.75516 5.70063 4.09828C5.70063 4.64391 4.5025 4.84078 4.62063 5.72391ZM4.99188 7.22578C5.27313 7.22578 5.48688 7.01766 5.48688 6.72516C5.48688 6.43266 5.27313 6.23016 4.99188 6.23016C4.705 6.23016 4.49125 6.43266 4.49125 6.72516C4.49125 7.01766 4.69937 7.22578 4.99188 7.22578Z" fill="#888888"/></svg></span></div>
													</div>
													<div class="swatch-label">
														プレミアム（柔らかフィット)
													</div>
													<div class="swatch-description">
														とても柔らか / 素材：低反発ウレタン  +  ビーズ + ウレタンフォーム
													</div>
												</div>
											</div>
										</div>

										<div class="swatchclone swatch-deluxe-two selected" title="deluxe-two" data-value="deluxe-two">
											<div class="swatch-label">
												ベッド取替カバー
												<div class="info-link"><a href="https://ambientlounge.co.jp/products/swatch/pet-swatch-order/" target="_blank" title="生地サンプル請求">生地サンプル請求<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg></a></div>
											</div>
											
											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-quiltgrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														キルトグレー
													</div>
													<div class="swatch-description">
														汚れに強く、撥水性のある高機能ファブリック
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-wolfgrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ウルフグレー
													</div>
													<div class="swatch-description">
														本物の動物の毛の風合いをリアルに再現
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-bambigrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														バンビグレー
													</div>
													<div class="swatch-description">
														ランクを超えた、極上ラグジュアリー感
													</div>
												</div>
											</div>										

											
										
											<div class="sub-swatch christmas-only hide">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-wolfgrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ウルフグレー
													</div>
													<div class="swatch-description">
														本物の動物の毛の風合いをリアルに再現
													</div>
												</div>
											</div>

											<div class="sub-swatch christmas-only hide">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/christmas/ambientlounge-Petlounge-cover-rabbitbeige-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ラビットベージュ
													</div>
													<div class="swatch-description">
														うっとりととろける様な柔らかさが魅力
													</div>
												</div>
											</div>

											<div class="sub-swatch christmas-only hide">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-bambigrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														バンビグレー
													</div>
													<div class="swatch-description">
														ランクを超えた、極上ラグジュアリー感
													</div>
												</div>
											</div>
											
										</div>

										<div class="swatchclone swatch-deluxe-three selected" title="deluxe-three" data-value="deluxe-three">
											<div class="swatch-label">
												アクセサリー 5点
											</div>
											<div class="swatch-sublabel">
												※アクセサリー類はトライアル対象外ですのでご注意ください。
											</div>
											
											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/GelPac-Box-Grey-S-DSC07545-1500x1500-1-3.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														クールジェルマット
													</div>
													<div class="swatch-description">
														ベッドにしいてひんやり過ごす夏に
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/pocketlounge2-1-1.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ポケラウンジ
													</div>
													<div class="swatch-description">
														ポケットサイズになったお出かけポーチ
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="https://ambientlounge.co.jp/wp-content/uploads/2022/11/Greige-blanket.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ブランケット グレージュ
													</div>
													<div class="swatch-description">
														耐久性の高いマイクロフリース
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/LaundryNet-25liter-Product-Studio-2-DSC07224-1.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ウォッシュネット
													</div>
													<div class="swatch-description">
														カバーやペット服のお洗濯に
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/theShampoo-300ml-3.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														the Shampoo
													</div>
													<div class="swatch-description">
														家族みんなで使える無添加シャンプー
													</div>
												</div>
											</div>
											
										</div>
									</div>       
								</div>
							</div>';

						$el .= 
							'<div class="variation-wrap pa_deluxe_info blue-cover">
								<div class="variation-label">
									<label for="pa_deluxe_info">
										セット内容       
										<div class="sizeguide-link">
											※セット内容は変更できません
										</div>                 
									</label>
								</div>
								<div class="variation-value value">                                
									<div class="tawcvs-swatches" data-attribute_name="attribute_pa_deluxe_info">
										<div class="swatchclone swatch-deluxe-one selected" title="deluxe-one" data-value="deluxe-one">
											<div class="swatch-top">
												<div class="swatch-label">
													ペットラウンジ 1点
												</div>
												<div class="swatch-image">
													<img src="https://ambientlounge.co.jp/wp-content/uploads/2022/11/delux-blue-base1000x1000.jpg">
													<div class="swatch-label">
														※ ベース本体
													</div>
												</div>
											</div>
											<div class="swatch-bottom">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/filling-1.svg">                                
												</div>                            
												<div class="swatch-text">
													<div class="swatch-label">
														フィリング
														<div class="info-link"><span id="openmodal-filling-deluxe" href="#content-filling">フィリングの種類<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.04688 8.95312C7.11794 8.95312 8.79688 7.27419 8.79688 5.20312C8.79688 3.13206 7.11794 1.45312 5.04688 1.45312C2.97581 1.45312 1.29688 3.13206 1.29688 5.20312C1.29688 7.27419 2.97581 8.95312 5.04688 8.95312ZM5.04688 9.65625C7.50627 9.65625 9.5 7.66252 9.5 5.20312C9.5 2.74373 7.50627 0.75 5.04688 0.75C2.58748 0.75 0.59375 2.74373 0.59375 5.20312C0.59375 7.66252 2.58748 9.65625 5.04688 9.65625Z" fill="#888888"/><path d="M4.62063 5.72391H5.35188C5.26188 4.94766 6.49375 4.84641 6.49375 4.02516C6.49375 3.27703 5.8975 2.89453 5.08187 2.89453C4.48 2.89453 3.985 3.17016 3.625 3.58641L4.08625 4.00828C4.36187 3.72141 4.64313 3.56391 4.98625 3.56391C5.43063 3.56391 5.70063 3.75516 5.70063 4.09828C5.70063 4.64391 4.5025 4.84078 4.62063 5.72391ZM4.99188 7.22578C5.27313 7.22578 5.48688 7.01766 5.48688 6.72516C5.48688 6.43266 5.27313 6.23016 4.99188 6.23016C4.705 6.23016 4.49125 6.43266 4.49125 6.72516C4.49125 7.01766 4.69937 7.22578 4.99188 7.22578Z" fill="#888888"/></svg></span></div>
													</div>
													<div class="swatch-label">
														プレミアム（柔らかフィット)
													</div>
													<div class="swatch-description">
														とても柔らか / 素材：低反発ウレタン  +  ビーズ + ウレタンフォーム
													</div>
												</div>
											</div>
										</div>

										<div class="swatchclone swatch-deluxe-two selected" title="deluxe-two" data-value="deluxe-two">
											<div class="swatch-label">
												ベッド取替カバー
												<div class="info-link"><a href="https://ambientlounge.co.jp/products/swatch/pet-swatch-order/" target="_blank" title="生地サンプル請求">生地サンプル請求<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg></a></div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="https://ambientlounge.co.jp/wp-content/uploads/2022/10/ambientloungejapan-PetLounge-SheepOrganic-AttributeImage-min-280x280.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														シープオーガニック
													</div>
													<div class="swatch-description">
														環境と皮膚にやさしいオーガニックコットンブレンド【Japan Limited Edition】
													</div>
												</div>
											</div>
											
											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-quiltgrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														キルトグレー
													</div>
													<div class="swatch-description">
														汚れに強く、撥水性のある高機能ファブリック
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-bambigrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														バンビグレー
													</div>
													<div class="swatch-description">
														ランクを超えた、極上ラグジュアリー感
													</div>
												</div>
											</div>


																					
											<div class="sub-swatch christmas-only hide">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-wolfgrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ウルフグレー
													</div>
													<div class="swatch-description">
														本物の動物の毛の風合いをリアルに再現
													</div>
												</div>
											</div>

											<div class="sub-swatch christmas-only hide">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/christmas/ambientlounge-Petlounge-cover-rabbitbeige-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ラビットベージュ
													</div>
													<div class="swatch-description">
														うっとりととろける様な柔らかさが魅力
													</div>
												</div>
											</div>

											<div class="sub-swatch christmas-only hide">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-bambigrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														バンビグレー
													</div>
													<div class="swatch-description">
														ランクを超えた、極上ラグジュアリー感
													</div>
												</div>
											</div>
											
										</div>

										<div class="swatchclone swatch-deluxe-three selected" title="deluxe-three" data-value="deluxe-three">
											<div class="swatch-label">
												アクセサリー 5点
											</div>
											<div class="swatch-sublabel">
												※アクセサリー類はトライアル対象外ですのでご注意ください。
											</div>
											
											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/GelPac-Box-Grey-S-DSC07545-1500x1500-1-3.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														クールジェルマット
													</div>
													<div class="swatch-description">
														ベッドにしいてひんやり過ごす夏に
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/pocketlounge2-1-1.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ポケラウンジ
													</div>
													<div class="swatch-description">
														ポケットサイズになったお出かけポーチ
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="https://ambientlounge.co.jp/wp-content/uploads/2022/11/Greige-blanket.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ブランケット グレージュ
													</div>
													<div class="swatch-description">
														耐久性の高いマイクロフリース
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/LaundryNet-25liter-Product-Studio-2-DSC07224-1.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ウォッシュネット
													</div>
													<div class="swatch-description">
														カバーやペット服のお洗濯に
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/theShampoo-300ml-3.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														the Shampoo
													</div>
													<div class="swatch-description">
														家族みんなで使える無添加シャンプー
													</div>
												</div>
											</div>
											
										</div>
									</div>       
								</div>
							</div>';

						$el .= 
							'<div class="variation-wrap pa_deluxe_info beige-cover">
								<div class="variation-label">
									<label for="pa_deluxe_info">
										セット内容       
										<div class="sizeguide-link">
											※セット内容は変更できません
										</div>                 
									</label>
								</div>
								<div class="variation-value value">                                
									<div class="tawcvs-swatches" data-attribute_name="attribute_pa_deluxe_info">
										<div class="swatchclone swatch-deluxe-one selected" title="deluxe-one" data-value="deluxe-one">
											<div class="swatch-top">
												<div class="swatch-label">
													ペットラウンジ 1点
												</div>
												<div class="swatch-image">
													<img src="https://ambientlounge.co.jp/wp-content/uploads/2022/11/delux-beige-base1000x1000.jpg">
													<div class="swatch-label">
														※ ベース本体
													</div>
												</div>
											</div>
											<div class="swatch-bottom">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/filling-1.svg">                                
												</div>                            
												<div class="swatch-text">
													<div class="swatch-label">
														フィリング
														<div class="info-link"><span id="openmodal-filling-deluxe" href="#content-filling">フィリングの種類<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.04688 8.95312C7.11794 8.95312 8.79688 7.27419 8.79688 5.20312C8.79688 3.13206 7.11794 1.45312 5.04688 1.45312C2.97581 1.45312 1.29688 3.13206 1.29688 5.20312C1.29688 7.27419 2.97581 8.95312 5.04688 8.95312ZM5.04688 9.65625C7.50627 9.65625 9.5 7.66252 9.5 5.20312C9.5 2.74373 7.50627 0.75 5.04688 0.75C2.58748 0.75 0.59375 2.74373 0.59375 5.20312C0.59375 7.66252 2.58748 9.65625 5.04688 9.65625Z" fill="#888888"/><path d="M4.62063 5.72391H5.35188C5.26188 4.94766 6.49375 4.84641 6.49375 4.02516C6.49375 3.27703 5.8975 2.89453 5.08187 2.89453C4.48 2.89453 3.985 3.17016 3.625 3.58641L4.08625 4.00828C4.36187 3.72141 4.64313 3.56391 4.98625 3.56391C5.43063 3.56391 5.70063 3.75516 5.70063 4.09828C5.70063 4.64391 4.5025 4.84078 4.62063 5.72391ZM4.99188 7.22578C5.27313 7.22578 5.48688 7.01766 5.48688 6.72516C5.48688 6.43266 5.27313 6.23016 4.99188 6.23016C4.705 6.23016 4.49125 6.43266 4.49125 6.72516C4.49125 7.01766 4.69937 7.22578 4.99188 7.22578Z" fill="#888888"/></svg></span></div>
													</div>
													<div class="swatch-label">
														プレミアム（柔らかフィット)
													</div>
													<div class="swatch-description">
														とても柔らか / 素材：低反発ウレタン  +  ビーズ + ウレタンフォーム
													</div>
												</div>
											</div>
										</div>

										<div class="swatchclone swatch-deluxe-two selected" title="deluxe-two" data-value="deluxe-two">
											<div class="swatch-label">
												ベッド取替カバー
												<div class="info-link"><a href="https://ambientlounge.co.jp/products/swatch/pet-swatch-order/" target="_blank" title="生地サンプル請求">生地サンプル請求<svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.57031 5.50781C4.37615 5.50781 4.21875 5.35041 4.21875 5.15625C4.21875 4.96209 4.37615 4.80469 4.57031 4.80469H7.03125C7.22541 4.80469 7.38281 4.96209 7.38281 5.15625V7.61719C7.38281 7.81135 7.22541 7.96875 7.03125 7.96875C6.83709 7.96875 6.67969 7.81135 6.67969 7.61719V6.005L3.17828 9.5064C3.04099 9.6437 2.81839 9.6437 2.6811 9.5064C2.5438 9.36911 2.5438 9.14651 2.6811 9.00922L6.1825 5.50781H4.57031Z" fill="#888888"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.117188 5.15625C0.117188 4.96209 0.274587 4.80469 0.46875 4.80469H2.51953C2.71369 4.80469 2.87109 4.96209 2.87109 5.15625C2.87109 5.35041 2.71369 5.50781 2.51953 5.50781H0.820312V11.3672H6.67969V9.66797C6.67969 9.47381 6.83709 9.31641 7.03125 9.31641C7.22541 9.31641 7.38281 9.47381 7.38281 9.66797V11.7188C7.38281 11.9129 7.22541 12.0703 7.03125 12.0703H0.46875C0.274587 12.0703 0.117188 11.9129 0.117188 11.7188V5.15625Z" fill="#888888"></path></svg></a></div>
											</div>											
											
											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="https://ambientlounge.co.jp/wp-content/uploads/2022/11/delux-quilt-beige-cover.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														キルトベージュ
													</div>
													<div class="swatch-description">
														汚れに強く、撥水性のある高機能ファブリック
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="https://ambientlounge.co.jp/wp-content/uploads/2022/11/delux-rabbit-beige-cover.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ラビットベージュ
													</div>
													<div class="swatch-description">
														本物の動物の毛の風合いをリアルに再現
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-bambigrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														バンビグレー
													</div>
													<div class="swatch-description">
														ランクを超えた、極上ラグジュアリー感
													</div>
												</div>
											</div>

											
										
											<div class="sub-swatch christmas-only hide">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-wolfgrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ウルフグレー
													</div>
													<div class="swatch-description">
														本物の動物の毛の風合いをリアルに再現
													</div>
												</div>
											</div>

											<div class="sub-swatch christmas-only hide">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/christmas/ambientlounge-Petlounge-cover-rabbitbeige-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ラビットベージュ
													</div>
													<div class="swatch-description">
														うっとりととろける様な柔らかさが魅力
													</div>
												</div>
											</div>

											<div class="sub-swatch christmas-only hide">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/ambientlounge-Petlounge-cover-bambigrey-selecter.jpg">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														バンビグレー
													</div>
													<div class="swatch-description">
														ランクを超えた、極上ラグジュアリー感
													</div>
												</div>
											</div>
											
										</div>

										<div class="swatchclone swatch-deluxe-three selected" title="deluxe-three" data-value="deluxe-three">
											<div class="swatch-label">
												アクセサリー 5点
											</div>
											<div class="swatch-sublabel">
												※アクセサリー類はトライアル対象外ですのでご注意ください。
											</div>
											
											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/GelPac-Box-Grey-S-DSC07545-1500x1500-1-3.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														クールジェルマット
													</div>
													<div class="swatch-description">
														ベッドにしいてひんやり過ごす夏に
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/pocketlounge2-1-1.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ポケラウンジ
													</div>
													<div class="swatch-description">
														ポケットサイズになったお出かけポーチ
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="https://ambientlounge.co.jp/wp-content/uploads/2022/11/Greige-blanket.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ブランケット グレージュ
													</div>
													<div class="swatch-description">
														耐久性の高いマイクロフリース
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/LaundryNet-25liter-Product-Studio-2-DSC07224-1.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														ウォッシュネット
													</div>
													<div class="swatch-description">
														カバーやペット服のお洗濯に
													</div>
												</div>
											</div>

											<div class="sub-swatch">
												<div class="swatch-image">
													<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/product/theShampoo-300ml-3.png">
												</div>
												<div class="swatch-text">
													<div class="swatch-label">
														the Shampoo
													</div>
													<div class="swatch-description">
														家族みんなで使える無添加シャンプー
													</div>
												</div>
											</div>
											
										</div>
									</div>       
								</div>
							</div>';

						// Show this only when  'pa_product-type' variation exist
						foreach ( $attributes as $attribute_name => $options ) 
						{							
							if ( $attribute_name == 'pa_product-type' )
							{
								echo $el;
							}
						}						
					?>
				</div>

				<?php do_action( 'woocommerce_after_variations_table' ); ?>
				<?php //do_action( 'ambient_custom_before_single_variation' ); ?>

				<div class="single_variation_wrap">
					<?php
						/**
						* Hook: woocommerce_before_single_variation.
						*/
						do_action( 'woocommerce_before_single_variation' );

						/**
						* Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
						*
						* @since 2.4.0
						* @hooked woocommerce_single_variation - 10 Empty div for variation data.
						* @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
						*/										
						do_action( 'woocommerce_single_variation' );

						/**
						* Hook: woocommerce_after_single_variation.
						*/
						do_action( 'woocommerce_after_single_variation' );
					?>
				</div>
			<?php
		}
	?>

	<?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>

<?php
do_action( 'woocommerce_after_add_to_cart_form' );
