<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/*
if ($product->is_type('variable')) {
 
	// All out of stock
    $variations = $product->get_available_variations();

    $out_of_stock_variations = array();

    foreach ($variations as $variation) {
        $variation_id = $variation['variation_id'];
        $variation_obj = wc_get_product($variation_id);

        if (!$variation_obj->is_in_stock()) {
            $out_of_stock_variations[] = $variation;
        }
    }

    // Create a JavaScript variable with the list of attributes for out-of-stock variations
    echo '<script>';
    echo 'var outOfStockVariationsAttributes = ' . json_encode(array_column($out_of_stock_variations, 'attributes')) . ';';
    echo '</script>';
}
*/

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

// Product Warranty Info Module
	function get_product_warranty_info() 
	{
		?>
			<div class="product-warranty-info">
				<?php
					// Warranty Info
					if ( get_field( 'warranty_type' ) ) 
					{
						global $product;

						// Special Product Configurator IDs
						//$specialproductconfigurator_IDs = array( 64552, 8250, 71776 );

						if ( !empty( $product ) ) //&& in_array( $product->get_id(), $specialproductconfigurator_IDs ) ) 
						{
							//if ( has_term( array( 'sofa' ), 'product_cat', $product->id ) )
							if ( get_field( 'warranty_type' ) == 'thirty' )
							{
								?>
									<a id="openmodal-trial" href="#content-trial30" title="<?php _e( '30日間トライアル', 'ambientlounge' ); ?>">
										<div class="pill-wrapper">
											<div class="pill-icon">											
												<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/specialproduct/return30.svg" alt="<?php _e( '30日間トライアル', 'ambientlounge' ); ?>">
											</div>
											<div class="pill-content">
												<?php
													if ( $flagSofa == true )
													{
														?>	
															<?php _e( 'ベッドを使ってくれるか心配ですか？', 'ambientlounge' ); ?><br>
															<?php _e( '30日間トライアル', 'ambientlounge' ); ?>
														<?php
													}
													else 
													{
														?>
															<?php _e( 'インテリアに合わないか心配ですか？', 'ambientlounge' ); ?><br>
															<?php _e( '30日間トライアル', 'ambientlounge' ); ?>	
														<?php	
													}
												?>
											</div>
											<div class="pill-arrow">
												<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/specialproduct/arrow.svg" alt="<?php _e( '60日間トライアル', 'ambientlounge' ); ?>">
											</div>
										</div>
									</a>
								<?php
							}
							else if ( get_field( 'warranty_type' ) == 'sixty' )
							{
								?>
									<a id="openmodal-trial" href="#content-trial" title="<?php _e( '60日間トライアル', 'ambientlounge' ); ?>">
										<div class="pill-wrapper">
											<div class="pill-icon">											
												<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/specialproduct/return.svg" alt="<?php _e( '60日間トライアル', 'ambientlounge' ); ?>">
											</div>
											<div class="pill-content">
												<?php _e( 'ベッドを使ってくれるか心配ですか？', 'ambientlounge' ); ?><br>
												<?php _e( '60日間トライアル', 'ambientlounge' ); ?>
											</div>
											<div class="pill-arrow">
												<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/specialproduct/arrow.svg" alt="<?php _e( '60日間トライアル', 'ambientlounge' ); ?>">
											</div>
										</div>
									</a>
								<?php
							}
						}
					}
				?>

				<?php
					// AR
					if( get_field( 'ios' ) ) 
					{
						echo '<a class="vr-ios mobile" href="'.get_field("ios").'" rel="ar">
								<img src="'.get_field("banner").'">
							</a>';
					}

					if( get_field( 'android' ) ) 
					{
						echo '<a class="vr-android mobile" href="intent://arvr.google.com/scene-viewer/1.0?file='.get_field("android").'#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=https://developers.google.com/ar;end;">
								<img src="'.get_field("banner").'">
							</a>
							<a class="vr-others" href="http://ambientlounge.co.jp/scene-viewer.php?url='.get_field("android").'" target="_blank">
								<img src="'.get_field("banner").'">
							</a>';
					}
				?>

				<div class="why-wrapper">
					<div class="why-title">
						<?php _e( '自信があるからこそ安心を提供します', 'ambientlounge' ); ?>										
					</div>
					<div class="why-icons">
						<?php if( !has_term( array( 'eco-campaign' ), 'product_cat', $product->id ) ) { 
								$warrantyyear = get_field('warrantyyear_type');
								if (empty($warrantyyear) || $warrantyyear == 'year') {
						?>
							<div class="why-icon">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/specialproduct/warranty.svg" alt="<?php _e( '１年保証', 'ambientlounge' ); ?>	">
								<div class="why-icon-title">												
									<?php _e( '１年保証', 'ambientlounge' ); ?>	
								</div>
							</div>
						<?php }} ?>
						<div class="why-icon">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/specialproduct/shipping.svg" alt="<?php _e( '送料無料※', 'ambientlounge' ); ?>">
							<div class="why-icon-title">												
								<?php _e( '送料無料※', 'ambientlounge' ); ?>	
							</div>
						</div>
						<div class="why-icon">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/specialproduct/finish.svg" alt="<?php _e( '日本仕上げ', 'ambientlounge' ); ?>">
							<div class="why-icon-title">												
								<?php _e( '日本仕上げ', 'ambientlounge' ); ?>
							</div>
						</div>
					</div>
					<div class="why-description">
						<?php _e( '※ 沖縄及び北海道・九州地方の一部離島を除く', 'ambientlounge' ); ?><br>
						購入に関するお悩みは<a href="https://ambientlounge.co.jp/shopping-guide/" target="_blank" title="ご利用ガイド">ご利用ガイド</a>をご参考ください										
					</div>
				</div>

			</div>
		<?php				
	}
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="single-product-wrapper">	
		<div class="single-product-left">
			<div class="sticky">
				<?php
					/**
					* Hook: woocommerce_before_single_product_summary.
					*
					* @hooked woocommerce_show_product_sale_flash - 10
					* @hooked woocommerce_show_product_images - 20
					*/

					// Get Product Warranty Info Here
					add_action( 'woocommerce_before_single_product_summary', 'get_product_warranty_info', 30 );

					do_action( 'woocommerce_before_single_product_summary' );				
				?>			
			</div>			
		</div>

		<div class="single-product-right">
			<div class="summary entry-summary">
				<?php
					/**
					* Hook: woocommerce_single_product_summary.
					*
					* @hooked woocommerce_template_single_title - 5
					* @hooked woocommerce_template_single_rating - 10
					* @hooked woocommerce_template_single_price - 10
					* @hooked woocommerce_template_single_excerpt - 20
					* @hooked woocommerce_template_single_add_to_cart - 30
					* @hooked woocommerce_template_single_meta - 40
					* @hooked woocommerce_template_single_sharing - 50
					* @hooked WC_Structured_Data::generate_product_data() - 60
					*/
					remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

					// Get Product Warranty Info Here
					add_action( 'woocommerce_single_product_summary', 'get_product_warranty_info', 70 );

					do_action( 'woocommerce_single_product_summary' );
				?>
			</div>
		</div>
		
	</div>		
</div>

<?php
	// Content
	if ( get_the_content() ) 
	{
		?>
			<div class="single-product-content entry-content">
				<?php the_content(); ?>
			</div>
		<?php
	}

	// JudgeMe - icon
	$classProductAfter = '';
	$product_type = get_field( 'product_type' );	
	if ( $product_type )
	{
		$classProductAfter = 'jdgm-' . $product_type;
	}
?>

<div class="single-product-after <?php echo $classProductAfter; ?>">
	<?php
		/**
		* Hook: woocommerce_after_single_product_summary.
		*
		* @hooked woocommerce_output_product_data_tabs - 10
		* @hooked woocommerce_upsell_display - 15
		* @hooked woocommerce_output_related_products - 20
		*/
		//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 ); 
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 ); 
		//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 ); 
		do_action( 'woocommerce_after_single_product_summary' );
	?>

	<?php 
		do_action( 'woocommerce_after_single_product' ); 
	?>
</div>
