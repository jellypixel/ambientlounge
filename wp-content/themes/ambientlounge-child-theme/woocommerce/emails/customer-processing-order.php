<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?><p><?php echo $order->get_billing_last_name().' '.$order->get_billing_first_name(); ?>様</p>
<p>お客様のご注文を受け付けました。現在、処理中でございます。ご注文の詳細は以下の通りです。</p>
<?php $items = $order->get_items();
	$back_order_products = [];
	foreach ( $items as $item ) {
		$product =  $order->get_product_from_item( $item );
		if( $product->get_stock_quantity() <= 0){
			$back_order_products[] = '「'.$product->get_title().'」';
		}
	}

    if(!empty($back_order_products) && $product->backorders_require_notification()){
        echo "<p><hr><br>ご注文いただきました商品、".implode('、',$back_order_products)." は現在在庫がありません。<br>具体的な入荷時期についてはお問い合わせいただきますようお願いいたします。<br><br><hr></p>";
    }

/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Emails::order_schema_markup() Adds Schema.org markup.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

?>
    <p>
        通常、注文確定（支払い完了）からペットラウンジ、ペットブランケットは５営業日以内、ソファー、テーブル、オットマンは10営業日以内に配送センターより発送いたします。<br>
        発送手続き完了時に、自動配信メールにて伝票番号をお知らせいたします。ヤマト運輸、または佐川急便のウェブサイトで配送状況をご確認いただけます。
    </p>
    <p>
        【商品の発送から配達までにかかる日数の目安】<br>
        　関東・甲信越地方：３〜４日<br>
        　中部地方：４〜５日<br>
        　関西・中国四国・九州・東北・北海道地方：６〜７日<br>
        　佐渡・沖縄：７日〜１４日
    </p>
<?php

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
