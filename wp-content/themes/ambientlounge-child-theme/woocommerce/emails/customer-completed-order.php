<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

    <p><?php echo $order->get_billing_last_name(); ?>様,</p>
<p>
    この度は、アンビエントラウンジをご購入いただき、誠にありがとうございます。
</p>
<p>
    ご注文商品の発送手続きが完了いたしました。<br>
    商品が無事にお客様のお手元に到着するように、注文時にご登録いただいたお電話が通じるようお願い申し上げます。
</p>
<p>[配送業者]</p>
<ul>
    <li>ペットラウンジ Sサイズ・Mサイズ及びブランケット全サイズ：佐川急便</li>
    <li>ペットラウンジ Lサイズ及びソファ、テーブル等：ヤマトホームコンビニエンス</li>
</ul>
<p>＊Lサイズ及びソファ、テーブル等をご注文のお客様につきましては、配達予定日当日の8時〜9時頃に配送業者のドライバーまたは営業所より、直接お電話にて配達時間帯に関するご連絡が入ります。</p>

<p>お荷物の配送状況を以下のリンク先にてご確認いただけます。 お問い合わせ伝票番号を入力し、“お問い合わせ開始“をクリックしてください。<br>
    ＊リンク先は配送業者のウェブサイトですので、反映までにお時間がかかる場合がございます。
</p>
<p>
    ・配送状況確認URL：ヤマト運輸<br>
    （<a href="https://toi.kuronekoyamato.co.jp/cgi-bin/tneko">https://toi.kuronekoyamato.co.jp/cgi-bin/tneko</a>）
</p>
<p>
    ・配送状況確認URL：佐川急便<br>
    （<a href="http://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp">http://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp</a>）
</p>
<p>
    商品到着まで今しばらくお待ちください。
</p>
<p>
    ご注文いただきました内容につきましては以下の通りです。
</p>
<?php

/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

?>

<p>またのご利用をお待ちいたしております。
    ありがとうございました。<br>
</p>

<?php
/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
