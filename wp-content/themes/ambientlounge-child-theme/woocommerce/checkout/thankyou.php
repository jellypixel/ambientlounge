<?php
	/**
	* Thankyou page
	*
	* This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
	*
	* HOWEVER, on occasion WooCommerce will need to update template files and you
	* (the theme developer) will need to copy the new files to your theme to
	* maintain compatibility. We try to do this as little as possible, but it does
	* happen. When this occurs the version of the template file will be bumped and
	* the readme will list any important changes.
	*
	* @see https://docs.woocommerce.com/document/template-structure/
	* @package WooCommerce\Templates
	* @version 3.7.0
	*/

	defined( 'ABSPATH' ) || exit;
?>

<div class="slim-container">

	<div class="woocommerce-order">
		<?php
			if ( $order ) 
			{	
				do_action( 'woocommerce_before_thankyou', $order->get_id() );				
				
				if ( $order->has_status( 'failed' ) )
				{
					?>
						<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

						<?php get_template_part( 'woocommerce/checkout/section', 'social' ); ?>

						<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
							<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
							<?php 
								if ( is_user_logged_in() )
								{
									?>
										<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
									<?php
								} 
							?>
						</p>
					<?php
				}
				else
				{
					?>
						<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

						<?php get_template_part( 'woocommerce/checkout/section', 'social' ); ?>

						<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

							<li class="woocommerce-order-overview__order order">
								<?php esc_html_e( 'Order number:', 'woocommerce' ); ?>
								<strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
							</li>

							<li class="woocommerce-order-overview__date date">
								<?php esc_html_e( 'Date:', 'woocommerce' ); ?>
								<strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
							</li>

							<?php 
								if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) 
								{
									?>
										<li class="woocommerce-order-overview__email email">
											<?php esc_html_e( 'Email:', 'woocommerce' ); ?>
											<strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
										</li>
									<?php
								}
							?>

							<li class="woocommerce-order-overview__total total">
								<?php esc_html_e( 'Total:', 'woocommerce' ); ?>
								<strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
							</li>

							<?php 
								if ( $order->get_payment_method_title() )
								{
									?>
										<li class="woocommerce-order-overview__payment-method method">
											<?php esc_html_e( 'Payment method:', 'woocommerce' ); ?>
											<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
										</li>
									<?php
								}
							?>

						</ul>
					<?php					
				}

				do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() );
				do_action( 'woocommerce_thankyou', $order->get_id() );

				// On success, check billing phone
				$billing_phone = $order->get_billing_phone();

				if($billing_phone == '0120-429-332')
					$billing_phone = "";

				if ( empty( $billing_phone ) )
				{	
					?>
						<div class="modalaction-overlay">
							<div class="modalaction-wrapper">
								<div class="modalaction-close">
									<svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M32.2344 32.2324L11.7688 11.7668" stroke="#888888"/>
										<path d="M11.7656 32.2324L32.2312 11.7668" stroke="#888888"/>
									</svg>
								</div>
								
								<div class="modalaction-content-wrapper">
									<div class="modalaction-content-inner">
										<div class="modalaction-content">
											<div class="modalaction-title">
												携帯番号が必要
											</div>		
											<div class="modalaction-description">									
												この注文には電話番号が入力されていません。配達を確実にするために、ここに電話番号を入力してください

												<form id="billing_phone_form" name="billing_phone_form" data-order-id="<?php echo $order->get_id(); ?>">
													<input type="text" id="billing_phone" name="billing_phone" minlength="8" maxlength="15" required>
													<button type="submit">申し込む</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<script>
							var $j = jQuery.noConflict();
							var modalActionOverlay = $j( '.modalaction-overlay' );
							var modalActionWrapper = $j( '.modalaction-wrapper' );	
							var billingPhoneForm = $j( '#billing_phone_form' );

							$j(function(){	
								// Show modal
								openModalAction( 'billingphone');

								// Close
									$j( 'body' ).on( 'click', '.modalaction-close', function(e) {
										e.stopPropagation();

										closeModalAction();
									});

									/*$j( 'body' ).on( 'click', '.modalaction-overlay', function(e) {
										if ( $j( e.target ).is( '.modalaction-overlay' ) ) {
											closeModalAction();
										} 
									});*/

								// Modal action functions
									function openModalAction( formType ) {					
										modalActionOverlay.fadeIn(400, function() {
											modalActionWrapper.fadeIn(200, function() {
												modalActionWrapper.css({ 'display' : 'flex' })

												if ( formType == 'billingform' ) {
													$j( 'form[name="billing_phone_form"]' ).removeClass( 'novalid loading' );
												}
											});
										})
									}			

									function closeModalAction() {
										modalActionWrapper.fadeOut(400, function() {
											modalActionOverlay.fadeOut(200, function() {
												
											});
										})
									}		

								// Billing phone form action
									$j('form[name="billing_phone_form"]').on('submit', function(e) {
										e.preventDefault();

										var orderId = $j(this).data('order-id');
										var billingPhone = $j('#billing_phone').val();

										if ( billingPhone != '' )
										{
											billingPhoneForm.addClass( 'loading' );
											billingPhoneForm.removeClass( 'notvalid' );

											$j.ajax({
												url: woocommerce_params.ajax_url, 
												type: 'POST',
												data: {
													action: 'update_order_billing_phone', 
													billing_phone: billingPhone,
													order_id: orderId,
													security: woocommerce_params.ajax_nonce 
												},
												success: function(response) {
													if ( response.success ) {
														billingPhoneForm.removeClass( 'loading' );
														closeModalAction();
													} 
													/*else {
														alert('Error: ' + response.data);
													}*/
												}
											});
										}
										else 
										{
											billingPhoneForm.addClass( 'notvalid' );
										}
									});					
							});
						</script>	
					<?php						
				}
			}
			else
			{
				?>
					<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
						<?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					</p>
				<?php

				get_template_part( 'woocommerce/checkout/section', 'social' );
			}
		?>
	</div>

	<div style="height:100px" aria-hidden="true" class="wp-block-spacer is-style-spacer188"></div>
</div>