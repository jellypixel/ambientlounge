<?php
	/**
	* Review order table
	*
	* This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
	*
	* HOWEVER, on occasion WooCommerce will need to update template files and you
	* (the theme developer) will need to copy the new files to your theme to
	* maintain compatibility. We try to do this as little as possible, but it does
	* happen. When this occurs the version of the template file will be bumped and
	* the readme will list any important changes.
	*
	* @see https://docs.woocommerce.com/document/template-structure/
	* @package WooCommerce\Templates
	* @version 5.2.0
	*/

	defined( 'ABSPATH' ) || exit;
?>

<div class="order-review shop_table woocommerce-checkout-review-order-table">
	<?php
		do_action( 'woocommerce_review_order_before_cart_contents' );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) 
		{
			$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) 
			{
				?>
					<div class="order-row <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
						<div class="order-column product-thumb">
							<?php
								//echo apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

								$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
								$productThumb = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'mobile' );
								//$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );

								if ( !empty( $productThumb ) )
								{
									?>
										<img src="<?php echo $productThumb[0]; ?>" class="attachment-woocommerce_thumbnail" alt="<?php echo $_product->get_name(); ?>">
									<?php	
								}
							?>
						</div>
						<div class="order-column product-name">
							<?php echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) ) . '&nbsp;'; ?>
							<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times;&nbsp;%s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
							<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						</div>
						<div class="order-column product-total">
							<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						</div>
					</div>
				<?php
			}
		}

		do_action( 'woocommerce_review_order_after_cart_contents' );
	?>

	<div class="carttotal-row carttotal-subtotal">
		<div class="carttotal-attr">
			<?php esc_html_e( '小計', 'ambientlounge' ); ?>		
		</div>
		<div class="carttotal-value">
			<?php wc_cart_totals_subtotal_html(); ?>
		</div>
	</div>

	
	<?php 
		// Coupon List

		foreach ( WC()->cart->get_coupons() as $code => $coupon ) 
		{
			?>
				<div class="carttotal-row cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
					<div class="carttotal-attr">
						<?php wc_cart_totals_coupon_label( $coupon ); ?>
					</div>
					<div class="carttotal-value" data-title="<?php echo esc_attr( wc_cart_totals_coupon_label( $coupon, false ) ); ?>">
						<?php wc_cart_totals_coupon_html( $coupon ); ?>
					</div>
				</div>
			<?php
		}
	?>

	<?php 
		// Shipping

		if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) 
		{
			do_action( 'woocommerce_review_order_before_shipping' );
			wc_cart_totals_shipping_html();
			do_action( 'woocommerce_review_order_after_shipping' );
		}
	?>

	<?php
		// Fee

		foreach ( WC()->cart->get_fees() as $fee )
		{
			?>
				<div class="carttotal-row">
					<div class="carttotal-attr">
						<?php echo esc_html( $fee->name ); ?>
					</div>
					<div class="carttotal-value" data-title="<?php echo esc_attr( $fee->name ); ?>">
						<?php wc_cart_totals_fee_html( $fee ); ?>
					</div>
				</div>
			<?php
		}
	?>

	<?php
		// Tax

		if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) 
		{
			$taxable_address = WC()->customer->get_taxable_address();
			$estimated_text  = '';

			if ( WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping() ) {
				// translators: %s location.
				$estimated_text = sprintf( ' <small>' . esc_html__( '(estimated for %s)', 'woocommerce' ) . '</small>', WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] );
			}

			if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) 
			{
				foreach ( WC()->cart->get_tax_totals() as $code => $tax ) // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
				{ 
					?>
						<div class="carttotal-row tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
							<div class="carttotal-attr">
								<?php echo esc_html( $tax->label ) . $estimated_text; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
							</div>
							<div class="carttotal-value" data-title="<?php echo esc_attr( $tax->label ); ?>">
								<?php echo wp_kses_post( $tax->formatted_amount ); ?>
							</div>
						</div>
					<?php
				}
			} 
			else 
			{
				?>
					<div class="carttotal-row tax-total">
						<div class="carttotal-attr">
							<?php echo esc_html( WC()->countries->tax_or_vat() ) . $estimated_text; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						</div>
						<div class="carttotal-value" data-title="<?php echo esc_attr( WC()->countries->tax_or_vat() ); ?>">
							<?php wc_cart_totals_taxes_total_html(); ?>
						</div>
					</div>
				<?php
			}
		}
	?>

	<?php
		// Coupon Code
	?>
	<div class="checkout-couponcode-wrapper">
		<?php
			add_action( 'show_coupon_code', 'woocommerce_checkout_coupon_form', 10 );
			do_action( 'show_coupon_code');
		?>
	</div>

	<?php
		// Totals
	?>
	<div class="cartotal-total">
		<?php	
			do_action( 'woocommerce_review_order_before_order_total' );	
		?>
		<div class="carttotal-row">
			<div class="carttotal-attr">
				<?php esc_html_e( '合計', 'ambientlounge' ); ?>
			</div>
			<div class="carttotal-value">
				<?php wc_cart_totals_order_total_html(); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>
	</div>

</div>

<?php
/*
<table class="shop_table woocommerce-checkout-review-order-table">
	<thead>
		<tr>
			<th class="product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
			<th class="product-total"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		do_action( 'woocommerce_review_order_before_cart_contents' );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) 
			{
				?>
					<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
						<td class="product-name">
							<?php echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) ) . '&nbsp;'; ?>
							<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times;&nbsp;%s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
							<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						</td>
						<td class="product-total">
							<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						</td>
					</tr>
				<?php
			}
		}

		do_action( 'woocommerce_review_order_after_cart_contents' );
		?>
	</tbody>
	<tfoot>

		<tr class="cart-subtotal">
			<th><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></th>
			<td><?php wc_cart_totals_subtotal_html(); ?></td>
		</tr>

		<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
			<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
				<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
				<td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

			<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

			<?php wc_cart_totals_shipping_html(); ?>

			<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

		<?php endif; ?>

		<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
			<tr class="fee">
				<th><?php echo esc_html( $fee->name ); ?></th>
				<td><?php wc_cart_totals_fee_html( $fee ); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
			<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
				<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
					<tr class="tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
						<th><?php echo esc_html( $tax->label ); ?></th>
						<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php else : ?>
				<tr class="tax-total">
					<th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
					<td><?php wc_cart_totals_taxes_total_html(); ?></td>
				</tr>
			<?php endif; ?>
		<?php endif; ?>

		<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

		<tr class="order-total">
			<th><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
			<td><?php wc_cart_totals_order_total_html(); ?></td>
		</tr>

		<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

	</tfoot>
</table>
*/
?>