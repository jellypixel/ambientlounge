<?php
	/**
	* My Account page
	*
	* This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
	*
	* HOWEVER, on occasion WooCommerce will need to update template files and you
	* (the theme developer) will need to copy the new files to your theme to
	* maintain compatibility. We try to do this as little as possible, but it does
	* happen. When this occurs the version of the template file will be bumped and
	* the readme will list any important changes.
	*
	* @see     https://docs.woocommerce.com/document/template-structure/
	* @package WooCommerce\Templates
	* @version 3.5.0
	*/

	defined( 'ABSPATH' ) || exit;

	$allowed_html = array(
		'a' => array(
			'href' => array(),
		),
	);
?>

	<header class="entry-header" aria-label="Content">
		<div class="myaccount-header">
			<div class="entry-container slim-container">
				<div class="myaccount-avatar">
					<?php
						$user_avatar = get_avatar_url( get_current_user_id(), array( 'size' => 56 ) );

						if ( empty( $user_avatar ) ) 
						{
							$user_avatar = get_stylesheet_directory_uri() . '/img/avatar-default.svg';							
						}
					?>
					<img src="<?php echo $user_avatar; ?>">					
				</div>
				<h1 class="entry-title" itemprop="headline">			
					<?php
						printf(
							/* translators: 1: user display name 2: logout url */
							/*wp_kses( __( 'Hi %1$s (not %1$s? <a href="%2$s">Log out</a>)', 'ambientlounge' ), $allowed_html ),
							'<strong>' . esc_html( $current_user->display_name ) . '</strong>',
							esc_url( wc_logout_url() )*/
							wp_kses( __( 'Hi %1$s', 'ambientlounge' ), $allowed_html ),	'<strong>' . esc_html( $current_user->display_name ) . '</strong>'
						);
					?>
				</h1>
				<?php
					/*
						<div class="myaccount-description">
							<?php								
								// translators: 1: Orders URL 2: Address URL 3: Account URL. 
								//$dashboard_desc = __( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">billing address</a>, and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' );
								$dashboard_desc = __( 'アカウントダッシュボードでは、最近のご注文の確認、<a href="%1$s">請求先住所の管理</a>、<a href="%2$s">パスワードとアカウント詳細</a>の編集などが行えます。', 'ambientlounge' );
								if ( wc_shipping_enabled() ) {
									// translators: 1: Orders URL 2: Addresses URL 3: Account URL. 
									//$dashboard_desc = __( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a>, and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' );
									$dashboard_desc = __( 'アカウントダッシュボードでは、最近のご注文の確認、<a href="%1$s">お届け先住所と請求先住所の管理</a>、<a href="%2$s">パスワードとアカウント詳細</a>の編集などが行えます。', 'ambientlounge' );
								}
								
								printf(
									wp_kses( $dashboard_desc, $allowed_html ),
									//esc_url( wc_get_endpoint_url( 'orders' ) ),
									esc_url( wc_get_endpoint_url( 'edit-address' ) ),
									esc_url( wc_get_endpoint_url( 'edit-account' ) )
								);								
							?>
						</div>
					*/
				?>
			</div>
		</div>
	</header>

	<div class="myaccount-wrapper">
		<div class="slim-container">
			<div class="myaccount-navigation">
				<?php
					/**
					* My Account navigation.
					*
					* @since 2.6.0
					*/
					do_action( 'woocommerce_account_navigation' ); 
				?>
			</div>
			<div class="myaccount-content">
				<div class="woocommerce-MyAccount-content">
					<?php
						/**
						* My Account content.
						*
						* @since 2.6.0
						*/
						do_action( 'woocommerce_account_content' );
					?>	
				</div>
			</div>
		</div>
	</div>

	<div aria-hidden="true" class="wp-block-spacer is-style-spacer188">
	</div>