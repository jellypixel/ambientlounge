<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );


?>
<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title">
			<?php 
				$archive_product_title = woocommerce_page_title( false ); 

				if ( $archive_product_title == 'ソファ' ) {
					echo 'ソファ一覧';
				}	
				else {
					woocommerce_page_title();
				}
			?>
			
		</h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */

	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
	add_action( 'woocommerce_before_shop_loop', 'addFilterPopUp', 20);

	function addFilterPopUp( $url_parameters ) 
	{
		$url_parameters = $_SERVER['QUERY_STRING'];
		$classLightboxActive = '';
		if ( !empty( $url_parameters ) ) {
			$classLightboxActive = 'active';
		}

		?>
			<div class="woocommerce-collection-meta">
				<div class="popup-filter">フィルター</div>
				<?php
					add_action( 'meta-catalog', 'woocommerce_catalog_ordering', '30' );
					do_action( 'meta-catalog' );
				?>
			</div>

			<div class="lightbox-overlay slide-lightbox <?php echo $classLightboxActive; ?>">
				<div class="lightbox-wrapper">
					<form action="">
						<div class="lightbox-inner">
							<?php dynamic_sidebar( 'collection_filter' ); ?>
							<?php
							/*
							<div class="row">
								<div class="col-xl-4">
									<div class="popup-filter-section-title">
										カラー
									</div>
									<div class="popup-filter-section-content">
										...
									</div>
								</div>
								<div class="col-xl-4">
									<div class="popup-filter-section-title">
										タイプ
									</div>
									<div class="popup-filter-section-content">
										...
									</div>
								</div>
								<div class="col-xl-4">
									<div class="popup-filter-section-title">
										価格
									</div>
									<div class="popup-filter-section-content">
										...
									</div>
								</div>
							</div>
							*/
							?>
						</div>
						<div class="lightbox-footer">
							<div class="lightbox-footer-left">
								<a href="#" class="lightbox-close">取消</a>
							</div>
							<div class="lightbox-footer-right">
								<a href="#" class="lightbox-close">リセット</a>								
								<input type="submit" value="7商品を見る">
							</div>
						</div>
					</form>
				</div>
			</div>
		<?php
	}
	?>
		<div class="woocommerce-collection-meta-wrapper">
			<div class="container">
				<?php do_action( 'woocommerce_before_shop_loop' ); ?>
			</div>
		</div>
	<?php

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

// Get all color
$terms = get_terms("pa_color");
foreach ( $terms as $term ) {
	$value = get_term_meta( $term->term_id, 'image', true );
	$image = wp_get_attachment_image_src( $value );
	echo '<div class="swatch swatch-'.$term->slug.'" title="'.$term->name.'" data-value="'.$term->slug.'">';
		echo '<div class="swatch-image" style="background-image:url( '.$image[0].' );"></div>';
	echo '</div>';
}

get_footer( 'shop' );
