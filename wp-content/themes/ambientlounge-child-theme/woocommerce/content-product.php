<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>
	<?php
		// Template Offers - Tagline
		/*if ( is_page_template( 'template-offers.php' ) )
		{
			?>
				<div class="offer-tagline">
					<?php
						if ( !empty( get_field( 'offer_tagline' ) ) )
						{
							the_field( 'offer_tagline' );
						}						
					?>
				</div>
			<?php				
		}*/

		/**
		* Hook: woocommerce_before_shop_loop_item.
		*
		* @hooked woocommerce_template_loop_product_link_open - 10
		*/
		do_action( 'woocommerce_before_shop_loop_item' );

		/**
		* Hook: woocommerce_before_shop_loop_item_title.
		*
		* @hooked woocommerce_show_product_loop_sale_flash - 10
		* @hooked woocommerce_template_loop_product_thumbnail - 10
		*/
		do_action( 'woocommerce_before_shop_loop_item_title' );

		/**
		* Hook: woocommerce_shop_loop_item_title.
		*
		* @hooked woocommerce_template_loop_product_title - 10
		*/
		do_action( 'woocommerce_shop_loop_item_title' );		

		// Template Offers - Short Description
		/*if ( is_page_template( 'template-offers.php' ) )
		{
			?>
				<div class="offer-shortdescription-wrapper">
					<div class="showmore-content">
						<?php
							global $product;
							echo $product->post->post_excerpt;					
						?>
					</div>
					<div class="showmore-wrapper">
						<div class="showmore-overlay">
						</div>
						<div class="showmore-button">
							もっと詳しく
						</div>
					</div>
				</div>
			<?php				
		}*/

		if ( is_archive() ) 
		{
			?>
				<div class="woocommerce-cat-title">
					<?php echo single_cat_title(); ?>
				</div>
			<?php
		}

		/**
		* Hook: woocommerce_after_shop_loop_item_title.
		*
		* @hooked woocommerce_template_loop_rating - 5
		* @hooked woocommerce_template_loop_price - 10
		*/
		do_action( 'woocommerce_after_shop_loop_item_title' );

		/**
		* Hook: woocommerce_after_shop_loop_item.
		*
		* @hooked woocommerce_template_loop_product_link_close - 5
		* @hooked woocommerce_template_loop_add_to_cart - 10
		*/
		do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li>
