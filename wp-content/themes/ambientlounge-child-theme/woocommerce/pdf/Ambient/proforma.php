<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php /* do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); */ ?>
 
<?php 
global $wp_roles;

//To see if it's gift shipping or not
$isGift = get_post_meta($this->order->get_id(),'is_a_gift',true) == '1';
$printTotals = $this->get_woocommerce_totals();

$oldPrice = 0;
$newPrice = 0;

?>

<?php //To see whether this is a B2B order or not

     // Get the customer ID
     $customer_id = $order->get_user_id();

     if ($customer_id) {

         // 2. Get the user's roles
        $user = get_user_by('id', $customer_id);
        $roles = $user->roles;

        // 3. Loop through the roles to find one starting with "B2B"
        $b2b_role = '';
        foreach ($roles as $role) {
            
            $role = translate_user_role( $wp_roles->roles[ $role ]['name'] );

            if (stripos($role, 'B2B') !== FALSE) { // Role starts with "B2B"
                $b2b_role = $role;
                break;
            }
        }
        
        // If we found a B2B role, proceed with extracting information
        if ($b2b_role) {
            // Remove the 'wcwp_b2b' part
            $b2b_role = str_replace('B2B-', '', $b2b_role);

            // Split the role into components
            $components = explode('-', $b2b_role);

            // Extract the discount amount
            $discount = isset($components[0]) ? intval($components[0]) : 0;

            // Extract the shipping type (either 'shipping' or 'freeshipping')
            $shipping_type = isset($components[1]) ? $components[1] : '';

            // Extract the invoice deadline (last part of the string)
            $invoice_deadline = isset($components[2]) ? $components[2] : '';

            // Default deadline date (today)
            $deadline_date = $order->get_date_created();
            $deadline_date = new DateTime($deadline_date);

            // Handle different deadline cases
            if (stripos($invoice_deadline, 'EOM') !== false) {
                // End of month (EOM)
                $deadline_date->modify('last day of this month');
                
                // Check if there's an additional modifier (e.g., +65d, +2m, +1y)
                if (preg_match('/EOM\+(\d+)([dmy])/', $invoice_deadline, $matches)) {
                    // Matches[1] will contain the number, Matches[2] will contain the unit (d, m, y)
                    $number = (int)$matches[1];
                    $unit = $matches[2]; 

                    // Modify the date based on the unit (d = days, m = months, y = years)
                    if ($unit === 'd') {
                        $deadline_date->modify('+' . $number . ' days');
                    } elseif ($unit === 'm') {
                        $deadline_date->modify('+' . $number . ' months');
                    } elseif ($unit === 'y') {
                        $deadline_date->modify('+' . $number . ' years');
                    }
                } 
            } 
            elseif (stripos($invoice_deadline, 'EONM') !== false) {
                // End of next month (EONM)
                $deadline_date->modify('last day of next month');
                
                // Check if there's an additional modifier (e.g., +2m, +1y)
                if (preg_match('/EONM\+(\d+)([dmy])/', $invoice_deadline, $matches)) {
                    // Matches[1] will contain the number, Matches[2] will contain the unit (d, m, y)
                    $number = (int)$matches[1];
                    $unit = $matches[2];

                    // Modify the date based on the unit (d = days, m = months, y = years)
                    if ($unit === 'd') {
                        $deadline_date->modify('+' . $number . ' days');
                    } elseif ($unit === 'm') {
                        $deadline_date->modify('+' . $number . ' months');
                    } elseif ($unit === 'y') {
                        $deadline_date->modify('+' . $number . ' years');
                    }
                }
            }            

            // Format the deadline date in "YYYY年MM月DD日"
            $formatted_deadline_date = $deadline_date->format('Y年m月d日');
        }
     }

?>

<p class="header" style="text-align: center"><?php $this->header_logo();?></p>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

<style>
    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }
</style>


<table class="order-data-addresses" style="line-height:1.1; margin-bottom: 0px;">
    <tr>
        <td style="position:relative;" colspan="2">
            <?php
                $titleInvoice = _x( '御見積書', 'woocommerce-pdf-invoices-packing-slips' );
            ?>
            <h1 style="font-size:3em; text-align: center; padding:5px 0; margin:0;"><?php echo $titleInvoice; ?></h1>            
        </td>
    </tr>

    <tr>
        <td class="table-separator" style="padding:6px 0;" colspan="2">
        </td>
    </tr>

	<tr>
        <td class="address billing-address" style="line-height:1.2">        
            <h3 style="font-size:1.2em"><?php _e( '請求先住所', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>    
                <div>
                    <?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
                    <?php $this->billing_address(); ?>
                    <?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>
                </div>
                <?php if ( isset($this->settings['display_email']) ) { ?>
                    <div class="billing-email"><?php $this->billing_email(); ?></div>
                <?php } ?>
                <?php if ( isset($this->settings['display_phone']) ) { ?>
                    <div class="billing-phone"><?php $this->billing_phone(); ?></div>
                <?php } ?>
        </td>
        
        <td style="text-align:right;" class="ambient-logo">
            <div>
                <img style="display:inline; max-width:200px; padding:0 0 5px 0;" src="https://www.ambientlounge.co.jp/wp-content/uploads/2016/12/ambient-logo.png">
                <?php
                    if ( $b2b_role )
                    {
                        ?>                    
                            <img style="display:inline; right:0; top:0; max-height:50px;" src="https://ambientlounge.co.jp/wp-content/uploads/2024/12/ambietlounge-seal.png">
                        <?php
                    }
                ?>
            </div>
            <p style="text-align:right; line-height:1.2;">
                <?php _e( 'Ambient Lounge Japan株式会社', 'woocommerce-pdf-invoices-packing-slips' ); ?><br>
                <?php _e( '群馬県利根郡みなかみ町川上32-20', 'woocommerce-pdf-invoices-packing-slips' ); ?><br>
                <?php _e( 'TEL: 0120-429-332', 'woocommerce-pdf-invoices-packing-slips' ); ?>
            </p>
        </td>   
	</tr>

    <tr>
        <td class="table-separator" style="padding:12px 0;" colspan="2">
        </td>
    </tr>

    <tr>        
        <td class="address">
            <div class="shipping-address">
            <?php if ( isset($this->settings['display_shipping_address']) ) { ?>
                <h3 style="font-size:1.2em; font-weight:bold; padding-bottom:2px;"><?php _e( '配送先住所', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
                <?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
                <div>
                    <?php $this->shipping_address(); ?>
                </div>
                <?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?>
            <?php } ?>            
            </div>
            
            <div class="table-separator" style="padding:12px 0;">
            </div>
                
            
        </td> 

        <td class="order-data clearfix" style="text-align:right;">
            <table style="float:right;">
                <?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
                <tr class="registration-number">                    
                    <th style="text-align:right;"><?php _e( '登録番号', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                    <td style="text-align:right; border-bottom:1px solid #000;"><?php _e( 'T3011001104769', 'woocommerce-pdf-invoices-packing-slips' ); ?></td>
                </tr>
                <?php 
                    /*if ( isset($this->settings['display_number']) ) { 
                        ?>
                            <tr class="invoice-number">
                                <th style="text-align:right;"><?php _e( '請求書No.', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                                <td style="text-align:right; border-bottom:1px solid #000;"><?php $this->invoice_number(); ?></td>
                            </tr>
                        <?php 
                    } */ 
                ?>       
                <tr class="order-number">
                    <th style="text-align:right;"><?php _e( '請求書No.', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                    <td style="text-align:right; border-bottom:1px solid #000;"><?php $this->order_number(); ?></td>
                </tr>         
                <tr class="invoice-date">
                    <th style="text-align:right;"><?php _e( '請求日', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                    <td style="text-align:right; border-bottom:1px solid #000;"><?php $this->invoice_date(); ?></td>
                </tr>                
                <?php if (isset($formatted_deadline_date) && !empty($formatted_deadline_date)): ?>
                    <tr class="payment-deadline">                    
                        <th style="text-align:right;"><?php _e( 'お支払期限', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                        <td style="text-align:right; border-bottom:1px solid #000;">
                            <?php echo $formatted_deadline_date; ?>
                        </td>
                    </tr>
                <?php endif; ?>

                <?php if (isset($discount) && !empty($discount)): ?>
                    <tr class="disc">                    
                        <th style="text-align:right;"><?php _e( '割引', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                        <td style="text-align:right; border-bottom:1px solid #000;">
                            <?php 
                                // Convert discount to decimal
                                $formatted_discount = $discount / 100;
                                // Format the discount to 1 decimal place
                                echo number_format($formatted_discount, 1); 
                            ?>
                        </td>
                    </tr> 
                <?php endif; ?>
                
                <?php
                /*
                
                <tr class="order-date">
                    <th style="text-align:right;"><?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                    <td style="text-align:right; border-bottom:1px solid #000;"><?php $this->order_date(); ?></td>
                </tr>
                */
                ?>
                <?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
            </table>
        </td>
    </tr>

    <tr>
        <td class="table-separator" style="padding:12px 0;" colspan="2">
        </td>
    </tr>

    <tr class="invoice-amount">
        <td colspan="2">
            <p style="display:inline; vertical-align:bottom;>
                <span style="display:inline-block; margin-right:24px; font-size:1.2em;"><?php _e('ご請求金額', 'woocommerce-pdf-invoices-packing-slips'); ?></span>
                <span style="font-size:1.5em;">
                    <?php
                        foreach( $printTotals as $key => $total ) 
                        {
                            if ( $key == 'order_total' )
                            {
                                echo $total['value'];
                            }
                        }               
                    ?>
                </span>
            </p>
        </td>
    </tr>

    <tr>
        <td class="table-separator" style="padding:6px 0;" colspan="2">
        </td>
    </tr>

    <tr>
        <td colspan="2">
            <?php do_action( 'wpo_wcpdf_before_order_details', $this->export->template_type, $this->export->order ); ?>

            

            <?php do_action( 'wpo_wcpdf_after_order_details', $this->export->template_type, $this->export->order ); ?>
        </td>
    </tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->export->template_type, $this->export->order ); ?>

<table class="order-details">
    <thead>
        <tr>
            <th class="product"><?php _e('品目', 'woocommerce-pdf-invoices-packing-slips'); ?></th>
            
            <?php 
                if(!$isGift) {
                    ?>
                        <th class="retail-price"><?php _e('上代', 'woocommerce-pdf-invoices-packing-slips'); ?></th>
                        <th class="price-after-disc"><?php _e('単価', 'woocommerce-pdf-invoices-packing-slips'); ?></th>
                    <?php
                }
            ?>

            <th class="quantity"><?php _e('数量', 'woocommerce-pdf-invoices-packing-slips'); ?></th>
            
            <?php 
                if(!$isGift) {
                    ?>
                        <th class="price"><?php _e('価格', 'woocommerce-pdf-invoices-packing-slips'); ?></th>
                    <?php
                }
            ?>
        </tr>
    </thead>

    <tbody>
        <?php $items = $this->order->get_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
        <?php

            if(has_term('exclude-from-catalog', 'product_visibility', $item->get_product_id() )) {
                continue;
            }

            ?>



        <tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->export->template_type, $this->export->order, $item_id ); ?>">
            <td class="product">
                <?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
                <span class="item-name"><?php echo $item->get_name(); ?></span>
                <?php do_action( 'wpo_wcpdf_before_item_meta', $this->export->template_type, $item, $this->export->order  ); ?>
                <dl class="meta">
                    <?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
                    <?php $product = $item->get_product(); ?>
                    <?php if( !empty( $product->get_sku() )) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo  $product->get_sku(); ?></dd><?php endif; ?>
                    <!-- <?php if( !empty( $product->get_weight() ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $product->get_weight(); ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?> -->
                </dl>
                <?php do_action( 'wpo_wcpdf_after_item_meta', $this->export->template_type, $item, $this->export->order  ); ?>
            </td>
            <td class="retail-price">
                <?php
                    $realQty = $this->order->get_qty_refunded_for_item( $item_id ) + $item->get_quantity();

                    echo wc_price($product->get_regular_price());
                    $oldPrice += ($product->get_regular_price() * $realQty);
                ?>
            </td>
            <td class="price-after-disc">
                <?php   
                    echo wc_price($item->get_total() / $item->get_quantity());
                    $newPrice += (($item->get_total() / $item->get_quantity()) * $realQty);
                ?>
            </td>
            <td class="quantity">
                <?php 
                    $item_qty_refunded = $this->order->get_qty_refunded_for_item( $item_id );
                    
                    if($item_qty_refunded == 0)
                        echo $item->get_quantity(); 
                    else
                        echo "<s>" . $item->get_quantity() . "</s>&nbsp;&nbsp;&nbsp;" . ($item->get_quantity() + $item_qty_refunded); 
                ?>
            </td>

            <?php if(!$isGift): ?>
            <td class="price">
                <?php 
                    $item_total_refunded = $this->order->get_total_refunded_for_item( $item_id );

                    if($item_total_refunded == 0)
                        echo wc_price($item->get_total()); 
                    else
                        echo "<s>" . wc_price($item->get_total()) . "</s>&nbsp;&nbsp;&nbsp;" . wc_price($item->get_total() - $item_total_refunded); 
                ?>
            </td>
            <?php endif ?>

        </tr>
        <?php endforeach; endif; ?>
    </tbody>

    <?php if(!$isGift): ?>
    <tfoot>
        <tr class="no-borders">
            <td class="no-borders" colspan="2">

            </td>
            <td class="no-borders" colspan="3">
                <table class="totals">
                    <tfoot>
                        <?php  
                        

                        if($this->order->get_total_refunded() != 0) {
                            $cartRefund = array("cart_refund" => array("label" => "Refund", "value" => wc_price($this->order->get_total_refunded())));

                            // Insert $cartRefund after "cart_subtotal"
                            $printTotals = array_slice($printTotals, 0, 1, true) + $cartRefund + array_slice($printTotals, 1, null, true);

                            // Change order total
                            $total_paid = $order->get_total();
                            $total_refunded = $order->get_total_refunded();
                            $total_tax_paid = $order->get_total_tax();
                            $total_tax_refunded = $order->get_total_tax_refunded();

                            $net_amount = $total_paid - $total_refunded;
                            $net_tax_amount = $total_tax_paid - $total_tax_refunded;
                            $net_amount_formatted = wc_price( $net_amount );

                            $printTotals["order_total"]["value"] = $net_amount_formatted . " (includes " . wc_price($net_tax_amount) . " 消費税)";
                        }

                        /*
                        foreach( $printTotals as $key => $total ) : ?>
                        <tr class="<?php echo $key; ?>">
                            <td class="no-borders"></td>
                            <th class="description" style="font-weight:bold;"><?php echo $total['label']; ?></th>
                            <td class="price"><span class="totals-price"><?php echo $total['value']; ?></span></td>
                        </tr>
                        <?php endforeach; ?>
                        */

                        ?>
                        <tr class="oldprice">
                            <td class="no-borders"></td>
                            <th class="description" style="font-weight:bold;">小売価格</th>
                            <td class="price"><span class="totals-price"><?php echo wc_price($oldPrice); ?></span></td>
                        </tr>
                        <tr class="newprice"> 
                            <td class="no-borders"></td>
                            <th class="description" style="font-weight:bold;">割引</th>
                            <td class="price"><span class="totals-price"><?php echo wc_price($newPrice - $oldPrice); ?></span></td>
                        </tr>
                        <tr class="subtotal" style="border-top: 2px dashed;">
                            <td class="no-borders"></td>
                            <th class="description" style="font-weight:bold;">小計</th>
                            <td class="price"><span class="totals-price"><?php echo wc_price($newPrice); ?></span></td>
                        </tr>
                        <tr class="shipping">
                            <td class="no-borders"></td>
                            <th class="description" style="font-weight:bold;">送料</th>
                            <td class="price"><span class="totals-price"><?php echo wc_price($order->get_shipping_total()); ?></span></td>
                        </tr> 
                        <tr class="tax">
                            <td class="no-borders"></td>
                            <th class="description" style="font-weight:bold;">消費税（10％対象）</th>
                            <td class="price"><span class="totals-price"><?php echo wc_price($order->get_total_tax()); ?></span></td>
                        </tr>
                        <tr class="total">
                            <td class="no-borders"></td>
                            <th class="description" style="font-weight:bold;">合計</th>
                            <td class="price"><span class="totals-price"><?php echo wc_price($order->get_total()); ?></span></td>
                        </tr>
                        
                    </tfoot>
                </table>
            </td>
        </tr>
    </tfoot>
    <?php endif ?>
</table>

<table style="width:100%;">   

    <tr class="shipping-note">
        <td style="border:1px solid #000; padding:6px; width:100%;" colspan="2">
            <h3 style="font-size:1.2em; font-weight:bold; padding-bottom:2px;"><?php _e('特記事項', 'woocommerce-pdf-invoices-packing-slips'); ?></h3>
            <p>
                <?php 
                    $specified_time = $order->get_meta( 'specified_time' );
                    if ( $specified_time ) {
                        echo "指定時間: " . $specified_time;
                    }

                    if (( $specified_time ) && ( $order->get_customer_note() ) ){
                        echo "<br>";
                    }

                    if ( $order->get_customer_note() ) {
                        echo $order->get_customer_note();
                    }
                ?>
            </p>
        </td>
    </tr>

    <tr>
        <td class="table-separator" style="padding:6px 0;" colspan="2">
        </td>
    </tr>    

    <?php   
        if ( $b2b_role ) 
        {
            ?>
                <tr>
                    <td class="bank1" style="border:1px solid #000; padding:6px; width:50%;">
                        金融機関：　三菱東京ＵＦＪ銀行　中目黒支店
                        お振込み手数料は貴社ご負担にてお願い致します。
                        口座番号：　普通口座　０２２１５６４
                        口座名義：　アンビエントラウンジジャパン(カ
                        お振込み手数料は貴社ご負担にてお願い致します。                        
                    </td>

                    <td class="bank2" style="border:1px solid #000; padding:6px; width:50%;">
                        銀行名: ゆうちょ銀行
                        支店名: ◯四八(ゼロヨンハチ)支店
                        科目: 普通預金
                        口座番号: 35684601
                        受取口座名義: アンビエントラウンジジヤパン(カ
                        お振込み手数料は貴社ご負担にてお願い致します。
                    </td>
                </tr>
            <?php
        }
    ?>
</table>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->export->template_type, $this->export->order ); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<?php $this->footer(); ?>
</div><!-- #letter-footer -->
<?php endif; ?>