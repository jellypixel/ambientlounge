<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<!--<table class="head container">-->
<!--	<tr>-->
<!--		<td class="header">-->
<!--		--><?php
//		if( $this->get_header_logo_id() ) {
//			$this->header_logo();
//		} else {
//			echo apply_filters( 'wpo_wcpdf_packing_slip_title', __( 'Packing Slip', 'woocommerce-pdf-invoices-packing-slips' ) );
//		}
//		?>
<!--		</td>-->
<!--		<td class="shop-info">-->
<!--			<div class="shop-name"><h3>--><?php //$this->shop_name(); ?><!--</h3></div>-->
<!--			<div class="shop-address">--><?php //$this->shop_address(); ?><!--</div>-->
<!--		</td>-->
<!--	</tr>-->
<!--</table>-->

<?php
use AtumLevels\Models\BOMModel;
 
//To see if it's gift shipping or not
$isGift = get_post_meta($this->order->get_id(),'is_a_gift',true) == '1' ? true:false;
//To see if it's b2b shipping or not
$isB2B = strpos($this->order->get_status(),'b2b') !== false;
//To see if it's COD or not
$isCOD = $this->order->get_payment_method() == "cod"? true: false;
//Get custom time
$specifiedTime = get_post_meta($this->order->get_id(),'specified_time',true)? get_post_meta($this->order->get_id(),'specified_time',true):"";

?>
<p class="header" style="text-align: center"><?php $this->header_logo();?></p>
<?php

if($isB2B){
    echo '<h1 style="font-size:2em; text-align: center; color:red;">DO NOT SEND WITH PRODUCT</h1>';
    echo '<h1 style="font-size:2em; text-align: center; color:red; margin-bottom:20px;">製品に添付しないでください</h1>';
}
if($isGift){
    echo '<h1 style="font-size:2em; text-align: center; color:red; margin-bottom:20px;">Gift</h1>';
}
if($isCOD){
    echo '<h1 style="font-size:2em; text-align: center; color:red; margin-bottom:20px;">COD 代金引換</h1>';
}

?>

<h1 class="document-type-label">
	<?php if( $this->get_header_logo_id() ) echo apply_filters( 'wpo_wcpdf_packing_slip_title', __( 'Packing Slip', 'woocommerce-pdf-invoices-packing-slips' ) ); ?>
</h1>


<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

<table class="order-data-addresses" style="table-layout: fixed;">
	<tr> 
        <td class="address shipping-address" style="word-wrap: break-word; text-wrap: wrap;">
             <h3><?php _e( 'Shipping Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
            <?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
            <?php $this->shipping_address(); ?>
            <?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?>
            <?php if ( isset($this->settings['display_email']) ) { ?>
                <div class="billing-email"><?php $this->billing_email(); ?></div>
            <?php } ?>
            <?php if ( isset($this->settings['display_phone']) ) { ?>
                <div class="billing-phone"><?php $this->billing_phone(); ?></div>
            <?php } ?>
        </td>
        <td class="address billing-address" style="word-wrap: break-word; text-wrap: wrap;">
            <?php if ( true ){ ?>
                <h3><?php _e( 'Billing Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
                <?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
                <?php $this->billing_address(); ?>
                <?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>
            <?php } ?>
        </td>
        <td class="order-data">
            <table>
                <?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
                <tr class="order-number" style="font-weight: 800; font-size: 20px;">
                    <th><?php _e( 'Order Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                    <td><?php $this->order_number(); ?></td>
                </tr>
                <tr class="order-date">
                    <th><?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                    <td><?php $this->order_date(); ?></td>
                </tr>
                <!--
                <tr class="shipping-method">
                    <th><?php _e( 'Shipping Method:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                    <td><?php $this->shipping_method(); ?></td>
                </tr>
                -->
                <tr class="preferred-time">
                    <th><?php _e( 'Preferred Time:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                    <td><?php echo $specifiedTime; ?></td> 
                </tr>
                <?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
            </table>
        </td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>

<table class="order-details">
    <thead>
    <tr>
        <th class="product"><?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
        <th class="quantity"><?php _e('Quantity', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
    </tr>
    </thead>
	<tbody>
	<?php $items = $this->order->get_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>

    <?php //remove warehouse product
        // if(has_term( 'Warehouse', 'product_cat', $item['product_id']) ) continue;

    ?>


        <tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->export->template_type, $this->export->order, $item_id ); ?>">
            <td class="product">
                <?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
               
                 <span class="item-name"><?php echo $item->get_name(); ?></span>
                <?php do_action( 'wpo_wcpdf_before_item_meta', $this->export->template_type, $item, $this->export->order  ); ?>
                <dl class="meta">
                    <?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
                    <?php $product = $item->get_product(); ?>
                    <?php if( !empty( $product->get_sku() )) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo  $product->get_sku(); ?></dd><?php endif; ?>
                    <?php if( !empty( $product->get_weight() ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $product->get_weight(); ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
                    <br>
                    <dt class="sku">
                        <?php
                                /*
                                $metaargs = array(
                                    'before'       => '',
                                    'after'        => '',
                                    'separator'    => '',
                                    'echo'         => true,
                                    'autop'        => true,
                                    'label_before' => '',
                                    'label_after'  => ':',
                                );
                                wc_display_item_meta($item, $metaargs); 
                            */

                            // Only for Delux
                            /*
                            $deluxMeta = array_values($item->get_all_formatted_meta_data());
                            if($deluxMeta[0]->value == "delux") {
                                // cover, base, filling, size
                                $prop = $deluxMeta[3]->display_value . " / " . $deluxMeta[2]->display_value . " / " . $deluxMeta[4]->display_value . " / " . $deluxMeta[1]->display_value;
                                
                                $prop = str_replace("\r", "", $prop);
                                $prop = str_replace("\n", "", $prop);
                                $prop = str_replace("<br>", "", $prop);
                                $prop = str_replace("<p>", "", $prop);
                                $prop = str_replace("</p>", "", $prop);

                                echo $prop;
                            }
                            */

                            $deluxMeta = $item->get_all_formatted_meta_data();
                            $printProp = "";

                            // Show each property if exist
                            // Cover Selection
                            $filtered_properties = array_values(array_filter(
                                $deluxMeta,
                                function($obj){ 
                                    return $obj->key === 'pa_cover-selection';
                            }));

                            if(sizeof($filtered_properties) != 0) {
                                $tmp = explode('・', $filtered_properties[0]->display_value);
                                
                                if(!strpos($item->get_name(), "Delux")) 
                                    $printProp .=  $tmp[0] . " / ";
                                else {

                                    if(strpos($item->get_name(), "グレー")) {
                                        $printProp .=  "ウルフグレー / ";
                                    }

                                    if(strpos($item->get_name(), "ベージュ")) {
                                        $printProp .=  "ラビットベージュ / ";
                                    }

                                    if(strpos($item->get_name(), "ブルー")) {
                                        $printProp .=  "シープオーガニック / ";
                                    }
                                    
                                }
                            }

                             // Bed Selection
                             $filtered_properties = array_values(array_filter(
                                $deluxMeta,
                                function($obj){ 
                                    return $obj->key === 'pa_bed-selection';
                            }));

                            if(sizeof($filtered_properties) != 0) {
                                $tmp = explode('・', $filtered_properties[0]->display_value);

                                if(count($tmp) > 1)
                                    $tmp[0] = $tmp[1];

                                $printProp .=  $tmp[0] . " / ";
                            }

                            // Filling
                            $filtered_properties = array_values(array_filter(
                                $deluxMeta,
                                function($obj){ 
                                    return $obj->key === 'pa_filling';
                            }));

                            if(sizeof($filtered_properties) != 0) {
                                if(strpos($item->get_name(), "Delux")) {
                                    $printProp .=  "プレミアム / ";
                                } else {
                                    $printProp .=  $filtered_properties[0]->display_value . " / ";
                                }
                            }
                            
                            // Size
                            $filtered_properties = array_values(array_filter(
                                $deluxMeta,
                                function($obj){ 
                                    return $obj->key === 'pa_size';
                            }));

                            if(sizeof($filtered_properties) != 0) {
                                $tmp = preg_replace('/\([^)]*\)/', '', $filtered_properties[0]->display_value); 
                                $printProp .=  $tmp . " / ";
                            }
 
                            // The rest of the attributes
                            foreach ($deluxMeta as $deluxItem) {
                                if (
                                    ($deluxItem->key != "pa_cover-selection") && 
                                    ($deluxItem->key != "pa_bed-selection") &&
                                    ($deluxItem->key != "pa_filling") &&
                                    ($deluxItem->key != "pa_size") 
                                ) {
                                   $printProp .= $deluxItem->display_value . " / ";
                                }
                            }

                            // Cleanup
                            $printProp = str_replace("\r", "", $printProp);
                            $printProp = str_replace("\n", "", $printProp);
                            $printProp = str_replace("<br>", "", $printProp);
                            $printProp = str_replace("<p>", "", $printProp);
                            $printProp = str_replace("</p>", "", $printProp);
                            $printProp = rtrim($printProp, " // ");

                            /*
                            if(strpos($product->get_sku(), "Swatch")) {
                                $printProp = "";
                            }

                            if(strpos($product->get_sku(), "Cover")) {
                                $printProp = "";
                            }
                            */

                            echo $printProp;
                        ?>
                    </dt>
                </dl>
                <?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
			</td>
			<td class="quantity">
                <?php 
                    // Check first chained product or not
                    $chained = false;
                    $main_product_id = !empty($item['variation_id']) ? $item['variation_id'] :  $item['product_id'];
                    if(!empty($main_product_id)) {

                        $_product = wc_get_product($main_product_id);
                        $main_id = $_product->get_id();
                        if($_product->get_parent_id())
                            $main_id = $product->get_parent_id();

                        if(get_field("enable_product_parts_packingslip", $main_id)) {
                            $chained = true;
                        }

                    }

                    if(!$chained) {
                        $item_qty_refunded = $this->order->get_qty_refunded_for_item( $item_id );

                        if($item_qty_refunded == 0)
                            echo $item->get_quantity(); 
                        else
                            echo "<s>" . $item->get_quantity() . "</s>&nbsp;&nbsp;&nbsp;" . ($item->get_quantity() + $item_qty_refunded); 
                    }
                ?>
            </td>
		</tr>

    <!-- chained products -->
    <?php
        $main_product_id = !empty($item['variation_id']) ? $item['variation_id'] :  $item['product_id'];

        if(empty($main_product_id)) continue;

        $_product = wc_get_product($main_product_id);
        $no_of_main  = intval($item['quantity']);

        if(!$_product) continue;
        $chained_products = [];
        for($i= 1; $i<=10; $i++){
            $product_id = get_post_meta($main_product_id,'chained_product_' . $i, true);
            $quantity = get_post_meta($main_product_id,'chained_product_' . $i . '_quantity', true);
            if($product_id && $quantity){
                $chained_product = wc_get_product($product_id);
                if(!$chained_product) continue;
                $cp_name = $chained_product->get_title();
                $cp_sku = $chained_product->get_sku();

                $chained_products[] = ['product_id' => $product_id, 'quantity' => intval($quantity) * $no_of_main , 'name'=> $cp_name,'sku' => $cp_sku ];
            }
        }

        if(empty($chained_products)){
            $parent_id = $_product->get_parent_id();

            if(!$parent_id ) continue;
            for($i= 1; $i<=10; $i++){
                $product_id = get_post_meta($parent_id,'chained_product_' . $i, true);
                $quantity = get_post_meta($parent_id,'chained_product_' . $i . '_quantity', true);

                if($product_id && $quantity){
                    $chained_product = wc_get_product($product_id);
                    if(!$chained_product) continue;
                    $cp_name = $chained_product->get_title();
                    $cp_sku = $chained_product->get_sku();
                    $chained_products[] = ['product_id' => $product_id, 'quantity' => intval($quantity) * $no_of_main, 'name'=> $cp_name,'sku' => $cp_sku ];
                }
            }
        }

        // atum parts. NOTE THAT THIS WILL OVERWRITE CHAIN PRODUCTS.
        $main_id = $_product->get_id();
		if($_product->get_parent_id())
			$main_id = $product->get_parent_id();

        if(get_field("enable_product_parts_packingslip", $main_id)) {
            $atum_product_parts = BOMModel::get_linked_bom( $main_product_id, 'product_part' );  
            if (!empty($atum_product_parts)) {

                $chained_products[] = array();

                // $atum_product_parts will contain an array of product parts
                foreach ($atum_product_parts as $part) {

                    $bomdetail = wc_get_product($part->bom_id);

                    $attrKeywords = get_field("enable_product_parts_packingslip_keyword", $main_id);
                    if($attrKeywords ) {

                        $attrKeywords = explode(",", $attrKeywords);

                        foreach ($attrKeywords as $attrKeyword){
                            if (stripos($item->get_name(), $attrKeyword)) { 
                                $chained_products[] = ['product_id' => $part->bom_id, 'quantity' =>$part->qty * $no_of_main, 'name'=> $bomdetail->get_title(),'sku' => $bomdetail->get_sku() ];
                            }
                        }

                    } else {
                        $chained_products[] = ['product_id' => $part->bom_id, 'quantity' =>$part->qty * $no_of_main, 'name'=> $bomdetail->get_title(),'sku' => $bomdetail->get_sku() ];
                    }
                    
                }
                
            }  
        }

    ?>
        <?php foreach($chained_products as $item): 
 
            if($item['name'] != "") {
            ?>
            <tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?>">
                <td class="product">
                    <?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
                    <span class="item-name" style="padding-left:25px;"><?php echo $item['name']; ?></span>

                    <dl class="meta" style="padding-left:25px;">
                        <?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
                        <?php if( !empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo $item['sku']; ?></dd><?php endif; ?>
                        <?php if( !empty( $item['weight'] ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
                    </dl>
                </td>
                <td class="quantity"><?php echo $item['quantity']; ?></td>
            </tr>
        <?php } endforeach; ?>

        <!-- Empty Row -->
        <tr>
            <td class="product">
                <span class="item-name" style="padding-left:25px;"></span>

                <dl class="meta" style="padding-left:25px;">
                </dl>
            </td>
            <td class="quantity"></td>
        </tr>
	<?php endforeach; endif; ?>
	</tbody>
</table>


<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>

<div class="customer-notes">
	<?php if ( $this->get_shipping_notes() ) : ?>
        <h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
		<?php $this->shipping_notes(); ?>
	<?php endif; ?>
</div>

<?php if ( $this->get_footer() ): ?>
	<div id="footer">
		<?php $this->footer(); ?>
	</div><!-- #letter-footer -->
<?php endif; ?>

<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>