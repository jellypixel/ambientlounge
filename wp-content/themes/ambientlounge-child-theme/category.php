<?php
	get_header();

    $cat = '';
    if ( isset( $_GET['cat'] ) ) 
    {
        $cat = $_GET['cat'];
    } else {
        $obj = get_queried_object();
        $cat = $obj->term_id;
    }

    $allCatClass = 'active';
    $categories = get_categories();
    foreach($categories as $category) 
    {
        if ( $cat == $category->term_id ) {
            $allCatClass = '';
        }
    }

    $blogURL = get_post_type_archive_link( 'post' );

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
?>
    <div class="content-area">
        <header class="entry-header">
            <h1><?php echo get_the_archive_title(); ?></h1>
        </header>

        <div class="entry-content">    

            <div class="blog-category-wrapper">
                <form action="" method="get">
                    <div class="blog-category-inner">
                        
                        <a href="<?php echo $blogURL . '?cat=all'; ?>" title="<?php _e( 'すべて', 'ambientlounge' ); ?>">
                            <div class="blog-category <?php echo $allCatClass; ?>" cat="-1"><?php _e( 'すべて', 'ambientlounge' ); ?></div>
                        </a>
                        <?php                            
                            foreach($categories as $category) 
                            {
                                ?>
                                    <a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo $category->name ?>">
                                        <div class="blog-category <?php echo ( $cat == $category->term_id) ? "active" : "" ?>" cat="<?php echo $category->term_id ?>"><?php echo $category->name ?></div>
                                    </a>
                                <?php                                
                            }
                        ?>                     
                    </div>                    
                    <input type="hidden" name="cat" id="cat" value="">                        
                </form>
            </div>                

            <div class="blog-wrapper">
                <?php		                
                    $query = new WP_Query( $args );
                    
                    if ( have_posts() ) 
                    {
                        while ( have_posts() ) 
                        {
                            the_post();

                            global $post;
							$postThumb = get_the_post_thumbnail_url( $post, 'mobile' );	

                            ?>
                                <div class="post-card">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <div class="post-card-thumb" style="background-image:url(<?php echo $postThumb; ?>);">
                                        </div>
                                    </a>
                                    <div class="post-card-description">
                                        <div class="post-card-title">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <h3><?php the_title(); ?></h3>
                                            </a>
                                        </div>
                                        <div class="post-card-content">
                                            <?php
                                                $excerpt = get_the_excerpt(); 
                                                echo $excerpt;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php                        
                        }
                    } 
                    wp_reset_postdata();			
                ?>
            </div>

            <div class="paging-numbered">
                <?php	                
                    echo paginate_links();
                ?>
            </div>

        </div>
    </div>

<?php
	get_footer();
?>