<?php
/**
 * Custom function to send emails to SendGrid via AutomateWoo action
 * @param $workflow AutomateWoo\Workflow
 */
function automatewoo_addtosendgrid( $workflow ) {
    $customer = $workflow->data_layer()->get_customer();

    // The data layer will return a valid data item or false
    if ( ! $customer ) {
        return;
    }

    $email = $customer->get_email();
    $firstname = $customer->get_first_name();
    $lastname = $customer->get_last_name();

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.sendgrid.com/v3/marketing/contacts",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "PUT",
        CURLOPT_POSTFIELDS => "{\"list_ids\":[\"7a4923b9-a2c3-450f-a14b-fcab0ecf0869\"],\"contacts\":[{\"email\":\"".$email."\",\"first_name\":\"".$firstname."\",\"".$lastname."\":\"string (optional)\"}]}",
        CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "content-type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
}