<?php
/**
 * This is an example trigger that is triggered via a WordPress action and includes a user data item.
 * Trigger with: do_action('my_custom_action', $user_id );
 */
class OnEventBooked extends AutomateWoo\Trigger {
    /** @var array - Define which data items are set by this trigger, this determines which rules and actions will be available */
    public $supplied_data_items = array( 'order','customer');
    /**
     * Set up the trigger
     */
    public function init() {
        $this->title = __( 'On order saved', 'automatewoo-custom' );
        $this->group = __( 'On order saved', 'automatewoo-custom' );
    }
    /**
     * Add any fields to the trigger (optional)
     */
    public function load_fields() {}
    /**
     * Defines when the trigger is run
     */
    public function register_hooks() {

        add_action( 'on_event_booked', array( $this, 'catch_hooks' ) );
    }
    /**
     * Catches the action and calls the maybe_run() method.
     *
     * @param $user_id
     */
    public function catch_hooks( $order_id ) {

        // get/create customer object from the user id

        $order = wc_get_order($order_id);

        $customer = AutomateWoo\Customer_Factory::get_by_user_id( $order->get_customer_id() );

        $this->maybe_run(array(
            'order' => $order,
            'customer' => $customer
          //  'organizer' => $organizer
        ));


    }
    /**
     * Performs any validation if required. If this method returns true the trigger will fire.
     *
     * @param $workflow AutomateWoo\Workflow
     * @return bool
     */
    public function validate_workflow( $workflow ) {
        // Get objects from the data layer
        $customer = $workflow->data_layer()->get_customer();
        // do something...
        return true;
    }
}