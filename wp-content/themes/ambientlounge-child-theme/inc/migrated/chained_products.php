<?php
/*
 * Chained product
 */
add_action('add_meta_boxes_product','warehouse_metabox');
add_action( 'woocommerce_product_after_variable_attributes', 'warehouse_metabox_variation', 10, 3 );

function warehouse_metabox($post){
    add_meta_box( 'warehouse_chained_product', 'Warehouse chained product', 'chained_warehouse_product', null, 'normal', 'high',[$post]);
}

function warehouse_metabox_variation( $loop, $variation_data, $variation ){
    chained_warehouse_product($variation);
}
function chained_warehouse_product($post){
    $i = 1;
    $wh_products = [];
    $product = wc_get_product($post->ID);
    $variation_prefix = $product instanceof WC_Product_Variation ? 'variation_' . $product->get_id() . '_' : '';
    while($meta = get_post_meta($post->ID,'chained_product_'.$i, true ) ){
        if(empty($meta)) continue;
        $wh_products[$i]['product_id'] = $meta;
        $wh_products[$i]['quantity'] = get_post_meta($post->ID,'chained_product_'.$i .'_quantity', true);
        $i++;
    }

    if(!$wh_products){
        $wh_products[1]['product_id'] = '';
        $wh_products[1]['quantity'] = 1;
    }

    $args           = array(
        'post_type'      => 'product',
        'posts_per_page' => -1,
        'post_status'    => 'active'
    );
    $posts = get_posts($args);
    ?>
    <div class="warehouse-chained-product-wrapper <?= $variation_prefix ? "variation_product" : "products" ?>">

        <?php
        if($variation_prefix){
            echo '<h2>Warehouse Chained product</h2>';
        }

        foreach($wh_products as $index => $p) {?>
            <div class="row">
                <div class="warehouse-chained-product-item chained-product">
                    <select class="rwmb-post rwmb-select_advanced" name="<?=$variation_prefix ?>chained_product[]" id="<?=$variation_prefix ?>chained_product_<?= $index ?>"
                            data-options='{"allowClear":true,"placeholder":"Select a product","width":"100%" }'>
                        <option value=""></option>
                        <?php
                        foreach($posts as $post) {

                            $product = wc_get_product($post->ID);
                            if($product instanceof WC_Product_Variable){
                                $children = $product->get_children();
                                foreach($children as $child_id){
                                    $selected = $p['product_id'] == $child_id ? 'selected' : '';
                                    echo '<option value="' . $child_id . '" ' . $selected . '>' . get_the_title($child_id) . '</option>';
                                }
                            }else{
                                $selected = $p['product_id'] == $product->get_id() ? 'selected' : '';
                                echo '<option value="' . $product->get_id() . '" ' . $selected . '>' . get_the_title($product->get_id()) . '</option>';
                            }

                        }
                        ?>
                    </select>
                </div>
                <div class="warehouse-chained-product-item product-quantity">
                    <input type="number" min="1" max="9999" name="<?=$variation_prefix ?>chained_product_quantity[]" placeholder="Quantity" value="<?= $p['quantity'] ?>">
                </div>

                <div class="warehouse-chained-product-item action">
                    <a class="chained-warehouse-button button add button-primary">Add</a>
                    <a class="chained-warehouse-button button remove button-delete">Remove</a>
                </div>


            </div>
            <?php
        } ?>
    </div>
    <?php
}

//save the fields made by above function
add_action('save_post', 'save_chained_products');
add_action( 'woocommerce_save_product_variation', 'save_chained_products', 10 );
function save_chained_products( $post_id ) {

    $product = wc_get_product($post_id);
    if(empty($product)) return;

    $variation_prefix = $product instanceof WC_Product_Variation ? 'variation_' . $product->get_id() . '_'  : '';
    $product_ids = isset($_POST[ $variation_prefix.'chained_product']) ? $_POST[ $variation_prefix.'chained_product'] : [];
    $qtys = isset($_POST[ $variation_prefix.'chained_product_quantity']) ? $_POST[ $variation_prefix.'chained_product_quantity'] : [];
    //clear the chained products
    for($j=1; $j<=20; $j++){
        delete_post_meta($post_id, 'chained_product_' . ($j)  );
        delete_post_meta($post_id, 'chained_product_' . ($j). '_quantity');
    }
    $i=0;

    while(!empty($product_ids[$i]) && !empty($qtys[$i])){
        update_post_meta($post_id, 'chained_product_' . ($i + 1) , $product_ids[$i]);
        update_post_meta($post_id, 'chained_product_' . ($i + 1). '_quantity', intval($qtys[$i]) );
        $i++;
    }

}

//reduce chained product( warehouse stock)
add_action( 'woocommerce_reduce_order_stock', function($order){
    $order_items    = $order->get_items();

    foreach ( $order_items as $item_id => $order_item) {

        $_product = $order_item->get_product();

        $no_of_main  = $order_item->get_quantity();

        $chained_products = [];
        for($i= 1; $i<=10; $i++){
            $product_id = get_post_meta($_product->get_id(),'chained_product_' . $i, true);
            $quantity = get_post_meta($_product->get_id(),'chained_product_' . $i . '_quantity', true);
            if($product_id && $quantity){
                $chained_products[] = ['product_id' => $product_id, 'quantity' => intval($quantity)];
            }
        }


        //if it has no chained products look at its parent product
        if(empty($chained_products)){
            for($i= 1; $i<=10; $i++){
                $product_id = get_post_meta($_product->get_parent_id(),'chained_product_' . $i, true);
                $quantity = get_post_meta($_product->get_parent_id(),'chained_product_' . $i . '_quantity', true);
                if($product_id && $quantity){
                    $chained_products[] = ['product_id' => $product_id, 'quantity' => intval($quantity)];
                }
            }
        }

        foreach($chained_products as $c_product){
            $product = wc_get_product($c_product['product_id']);
            if(!$product) continue;
            $new_stock    = wc_update_product_stock( $product, $c_product['quantity'] * $no_of_main, 'decrease' );
            $item_name    = $_product->get_title() ? $product->get_title() : $product->get_id();
            $note         = sprintf( __( 'Item %1$s stock reduced from %2$s to %3$s.', 'woocommerce' ), $item_name, $new_stock + $c_product['quantity'] * $no_of_main, $new_stock );
            $order->add_order_note( $note );
        }
    }

});