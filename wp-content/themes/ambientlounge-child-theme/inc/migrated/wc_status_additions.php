<?php
function register_new_order_statuses() {
    register_post_status( 'wc-b2b-q', array(
        'label'                     => 'B2B: Quoting',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'B2B Quote <span class="count">(%s)</span>', 'B2B Quotes <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-b2b-i', array(
        'label'                     => 'B2B: Invoiced',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'B2B invoice <span class="count">(%s)</span>', 'B2B invoices <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-b2b-or', array(
        'label'                     => 'B2B: Order received',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'B2B received order <span class="count">(%s)</span>', 'B2B received orders <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-b2b-op', array(
        'label'                     => 'B2B: Order processing',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'B2B processing order <span class="count">(%s)</span>', 'B2B processing orders <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-b2b-rfp', array(
        'label'                     => 'B2B: Ready for pickup',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'B2B ready for pickup <span class="count">(%s)</span>', 'B2B ready for pickup <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-b2b-ap', array(
        'label'                     => 'B2B: Awaiting payment',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'B2B awaiting payment <span class="count">(%s)</span>', 'B2B awaiting payment <span class="count">(%s)</span>' )
    ) );


    register_post_status( 'wc-b2b-comp', array(
        'label'                     => 'B2B: Complete',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'B2B completed <span class="count">(%s)</span>', 'B2B completed <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-b2c-received', array(
        'label'                     => 'B2C: Received',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'B2C received <span class="count">(%s)</span>', 'B2C received <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-defect', array(
        'label'                     => 'Defect',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Defect <span class="count">(%s)</span>', 'Defects <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-preparing-to-ship', array(
        'label'                     => 'Preparing to ship',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Preparing to ship <span class="count">(%s)</span>', 'Preparing to ship <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-b2b-pts', array(
        'label'                     => 'B2B: Preparing to ship',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'B2B: Preparing to ship <span class="count">(%s)</span>', 'B2B: Preparing to ship <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-yoyaku-included', array(
        'label'                     => '予約商品を含む',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( '予約商品を含む <span class="count">(%s)</span>', '予約商品を含む <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-puo-processing', array(
        'label'                     => 'Pop up order processing',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Pop up order processing <span class="count">(%s)</span>', 'Pop up order processing <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-puo-pts', array(
        'label'                     => 'Pop up preparing to ship',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Pop up preparing to ship <span class="count">(%s)</span>', 'Pop up preparing to ship <span class="count">(%s)</span>' )
    ) );

    register_post_status('wc-b2b-onloan', array(
        'label' => __( 'B2B: On Loan', 'woocommerce' ),
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('B2B: On Loan <span class="count">(%s)</span>', 'B2B: On Loan <span class="count">(%s)</span>')
    ));

    register_post_status('wc-preorder', array(
        'label' => __( 'Pre-Order', 'woocommerce' ),
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('Pre-Order <span class="count">(%s)</span>', 'Pre-Order <span class="count">(%s)</span>')
    ));

    register_post_status('wc-repair-estimate', array(
        'label' => __( 'Repair Estimate', 'woocommerce' ),
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('Repair Estimate <span class="count">(%s)</span>', 'Repair Estimate <span class="count">(%s)</span>')
    ));
 
    /*

    register_post_status('wc-cod-processing', array(
        'label' => __( 'COD: Processing', 'woocommerce' ),
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('COD: Processing <span class="count">(%s)</span>', 'COD: Processing <span class="count">(%s)</span>')
    ));

    register_post_status('wc-cod-preparing', array(
        'label' => __( 'COD: Preparing', 'woocommerce' ),
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('COD: Preparing <span class="count">(%s)</span>', 'COD: Preparing <span class="count">(%s)</span>')
    ));

    register_post_status('wc-cod-waitpayment', array(
        'label' => __( 'COD: Awaiting Payment', 'woocommerce' ),
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('COD: Awaiting Payment <span class="count">(%s)</span>', 'COD: Awaiting Payment <span class="count">(%s)</span>')
    ));

    register_post_status('wc-cod-completed', array(
        'label' => __( 'COD: Completed', 'woocommerce' ),
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('COD: Completed <span class="count">(%s)</span>', 'COD: Completed <span class="count">(%s)</span>')
    ));

    */
}


add_action( 'init', 'register_new_order_statuses' );

//Register bulk actions
function register_new_bulk_actions( $bulk_actions ) {

    $bulk_actions['mark_preparing_to_ship'] = 'Preparing to ship';
    return $bulk_actions;

}

add_filter( 'bulk_actions-edit-shop_order', 'register_new_bulk_actions' );


function bulk_process_custom_status() {

    // if an array with order IDs is not presented, exit the function
    if( !isset( $_REQUEST['post'] ) && !is_array( $_REQUEST['post'] ) )
        return;

    foreach( $_REQUEST['post'] as $order_id ) {

        $order = new WC_Order( $order_id );
        $order->update_status( 'preparing-to-ship', '', true );
    }

    // of course using add_query_arg() is not required, you can build your URL inline
    $location = add_query_arg( array(
        'post_type' => 'shop_order',
        'mark_preparing_to_ship' => 1, // markED_awaiting_shipment=1 is just the $_GET variable for notices
        'changed' => count( $_REQUEST['post'] ), // number of changed orders
        'ids' => join(',',  $_REQUEST['post'] ),
        'post_status' => 'all'
    ), 'edit.php' );

    wp_redirect( admin_url( $location ) );
    exit;

}

add_action( 'admin_action_mark_preparing_to_ship', 'bulk_process_custom_status' );

//Admin notice
function custom_order_status_notices() {

    global $pagenow, $typenow;

    if( $typenow == 'shop_order'
        && $pagenow == 'edit.php'
        && isset( $_REQUEST['mark_preparing_to_ship'] )
        && $_REQUEST['mark_preparing_to_ship'] == 1
        && isset( $_REQUEST['changed'] ) ) {

        $message = sprintf( _n( 'Order status changed.', '%s order statuses changed.', $_REQUEST['changed'] ), number_format_i18n( $_REQUEST['changed'] ) );
        echo "<div class=\"updated\"><p>{$message}</p></div>";

    }

}

add_action('admin_notices', 'custom_order_status_notices');


// Add to list of WC Order statuses
function add_new_order_statuses_to_order_statuses( $order_statuses ) {

    $order_statuses['wc-b2b-q'] = 'B2B: Quoting';
    $order_statuses['wc-b2b-i'] = 'B2B: Invoiced';
    $order_statuses['wc-b2b-or'] = 'B2B: Order received';
    $order_statuses['wc-b2b-op'] = 'B2B: Order processing'; 
    $order_statuses['wc-b2b-rfp'] = 'B2B: Ready for pickup';
    $order_statuses['wc-b2b-ap'] = 'B2B: Awaiting payment';
    $order_statuses['wc-b2b-comp'] = 'B2B: Complete';
    $order_statuses['wc-b2c-received'] = 'B2C: Received';
    $order_statuses['wc-defect'] = 'Defect';
    $order_statuses['wc-preparing-to-ship'] = 'Preparing to ship';
    $order_statuses['wc-b2b-pts'] = 'B2B: Preparing to ship';
    $order_statuses['wc-b2b-onloan'] = 'B2B: On Loan';
    $order_statuses['wc-preorder'] = 'Pre-Order';
    $order_statuses['wc-repair-estimate'] = 'Repair Estimate';
    $order_statuses['wc-yoyaku-included'] = '予約商品を含む';
    $order_statuses['wc-puo-processing'] = 'Pop up order processing';
    $order_statuses['wc-puo-pts'] = 'Pop up preparing to ship'; 

    /*
    $order_statuses['wc-cod-processing'] = 'COD: Processing';
    $order_statuses['wc-cod-preparing'] = 'COD: Preparing';
    $order_statuses['wc-cod-waitpayment'] = 'COD: Awaiting Payment';
    $order_statuses['wc-cod-completed'] = 'COD: Completed';
    */
    //Note: we can't use over 20 characters as status id
    return $order_statuses;
}
add_filter( 'wc_order_statuses', 'add_new_order_statuses_to_order_statuses' );

// Custom order status for COD
add_action('woocommerce_thankyou_cod', 'action_woocommerce_thankyou_cod', 10, 1);
function action_woocommerce_thankyou_cod($order_id)
{
    $order = wc_get_order($order_id);
    $order->update_status('wc-cod-processing');
}