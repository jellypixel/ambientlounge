
<?php


class WFOCU_Rule_Filling extends WFOCU_Rule_Base {
    public $supports = array( 'order' );

    public function __construct() {
        parent::__construct( 'funnel_skip' );
    }

    public function get_possible_rule_operators() {
        $operators = array(
            'if' => __( 'If', 'woofunnels-upstroke-one-click-upsell' ),
        );

        return $operators;
    }

    public function get_possible_rule_values() {
        return array(
            'diy-set' => "DIY Set",
            'pre-filled' => "Pre filled",
            //'products' => __( 'Products in offer are already present in original order', 'woofunnels-upstroke-one-click-upsell' ),
        );
    }

    public function get_condition_input_type() {
        return 'Select';
    }

    public function is_match( $rule_data, $env = 'order' ) {
        global $woocommerce;
        if ( 'diy-set' === $rule_data['condition'] ) {

            $cart_contents = $woocommerce->cart->get_cart();

            foreach($cart_contents as $item){
                if(isset($item['filling-key']) &&  $item['filling-key'] === 'diy-set'){
                    return true;
                }
            }
            return false;
        }

        else if ( 'pre-filled' === $rule_data['condition'] ) {

            $cart_contents = $woocommerce->cart->get_cart();

            foreach($cart_contents as $item){
                if(isset($item['filling-key']) && $item['filling-key'] === 'pre-filled'){
                    return true;
                }
            }
            return false;
        }

        return false;

    }

}
