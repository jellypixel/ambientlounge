<?php
/**
 * Slider Card Item Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create class attribute allowing for custom "className" and "align" values.
$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

/*$template = array(
    array( 'core/paragraph', array(
        'placeholder' => 'Add Title',
        'className' => 'accordion-item-title',
        'lock' => array(
            'move'   => true,
            'remove' => true,
        ),        
    ) )
);*/

?>

<li class="slider-card-item <?php echo esc_attr($classes); ?>">    
    <?php echo '<InnerBlocks />'; ?>      
</li>