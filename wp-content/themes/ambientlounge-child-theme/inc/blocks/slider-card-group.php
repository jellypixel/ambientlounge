<?php
/**
 * Slider Card Group Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create class attribute allowing for custom "className" and "align" values.
$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$allowed_blocks = array( 'acf/slider-card-item' );

$slide_count = get_field('slide_shown') ?: '1';
?>
<div id="slider-card-<?php echo uniqid(); ?>" class="slider-card-block-wrapper flexslider<?php echo esc_attr($classes); ?>" data-slide-count="<?php echo $slide_count; ?>">
    <ul class="slides">
        <?php echo '<InnerBlocks allowedBlocks="' . esc_attr( wp_json_encode( $allowed_blocks ) ) . '" />'; ?>
    </ul>

    <?php
        if( !is_admin() && $GLOBALS['sliderCardBlockRun'] == 0 )
        {
            // Lightbox Element
            ?>
                <div class="lightbox-overlay">
                    <div class="lightbox-wrapper slider-card-lightbox">                        
                        <div class="lightbox-inner">
                            
                        </div>
                        <div class="lightbox-footer">
                            <div class="lightbox-footer-left">
                                <a href="#" class="lightbox-close">取消</a>
                            </div>
                        </div>                        
                    </div>
                </div>
            <?php

            // Script
            ?>
                <script>
                    var $j = jQuery.noConflict();

                    $j(window).load(function(){
                        /* Block - Slider Card Group */
                            // Flexslider
                                $j('.slider-card-block-wrapper.flexslider').each(function() {

                                    var slideCount = $j( this ).attr( 'data-slide-count' );
                                    if ( slideCount == '' || slideCount == 0 ) { slideCount = 1; }    
                                    
                                    var minMaxItems;
                                    if ( slideCount > 3 ) {
                                        minMaxItems = ( window.innerWidth < 768 ) ? 1 : ( window.innerWidth < 992 ) ? 3 : slideCount;
                                    }
                                    else {
                                        minMaxItems = ( window.innerWidth < 768 ) ? 1 : slideCount;
                                    }

                                    var sliderCardEl = $j( this );
                                    sliderCardEl.flexslider({
                                        animation: "slide",
                                        animationLoop: false,
                                        itemWidth: 210,
                                        itemMargin: 32,
                                        minItems: minMaxItems, 
                                        maxItems: minMaxItems,
                                        prevText: '',
                                        nextText: '',
                                        controlNav: true,
                                        directionNav: true,
                                        slideshow: false
                                    });            

                                    /*$j( window ).resize( function() {	
                                        sliderCardEl.flexslider.vars.minItems = ( window.innerWidth < 768 ) ? 1 : slideCount;
                                        sliderCardEl.flexslider.vars.maxItems = ( window.innerWidth < 768 ) ? 1 : slideCount;
                                    });*/
                                });

                            // Hide Lightbox Content
                                $j('.slider-card-item' ).each( function() {
                                    var separatorEl = $j( this ).children( 'hr:first-of-type' );

                                    // Make element before (presumably paragraph) display inline
                                    separatorEl.prev().css({ 'display' : 'inline' });
                                    // Add readmore link
                                    separatorEl.before( '<a href="#" class="slider-card-readmore" title="続きを読む">続きを読む</a>' );
                                    // Wrap all content after separator
                                    separatorEl.nextAll().addBack().wrapAll( '<div class="slider-lightbox-content"></div>' );
                                });

                            // Call Lightbox
                                var lightboxOverlay = $j( '.lightbox-overlay' );
			                    var lightboxEl = $j( '.lightbox-wrapper' );
                                var lightboxInner = $j( '.lightbox-inner' );

                                $j( 'body' ).on( 'click', '.slider-card-block-wrapper .slider-card-item a.slider-card-readmore', function(e) {
                                    e.preventDefault();       

                                    openLightboxSliderItem( $j( this ) );
                                });

                                lightboxOverlay.click( function(e) {
                                    if ($j(e.target).has('.lightbox-wrapper').length) {
                                        closeLightbox();
                                    }
                                })

                                $j( '.lightbox-close').click( function(e) {
                                    e.preventDefault();
                                    closeLightbox();
                                })

                                function openLightboxSliderItem( callerEl ) {
                                    // Parent
                                    var sliderItemEl = callerEl.closest( '.slider-card-item' );
                                    // Get content until readmore                                                                        
                                    var contentUntilExcerpt = sliderItemEl.clone();
                                    contentUntilExcerpt.find( '.slider-card-readmore, .slider-lightbox-content' ).remove();                                    
                                    contentUntilExcerpt.children().last().remove();
                                    // Get extra content
                                    var contentExtra = sliderItemEl.children( '.slider-lightbox-content' ).clone();
                                    contentExtra.find( 'hr:first-child' ).remove();
                                    //var contentExtra = sliderItemEl.children( '.slider-lightbox-content' ).children().not(':first');
                                    // Merge Contnet
                                    var allContent = contentUntilExcerpt.html() + contentExtra.html();

                                    lightboxInner.append( allContent );

                                    lightboxOverlay.fadeIn( 200, function() {
                                        lightboxEl.fadeIn(400);
                                    });
                                }

                                function closeLightbox() {
                                    lightboxEl.fadeOut( 200, function() {
                                        lightboxInner.empty();
                                        lightboxOverlay.fadeOut(400);
                                    });
                                }                                
                    });
                </script>
            <?php
        }

        $GLOBALS['sliderCardBlockRun']++;
    ?>
</div>

