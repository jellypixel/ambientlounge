<?php
    /**
    * Direct Sale Block Template.
    *
    * @param   array $block The block settings and attributes.
    * @param   string $content The block inner HTML (empty).
    * @param   bool $is_preview True during AJAX preview.
    * @param   (int|string) $post_id The post ID this block is saved to.
    */

    // Create class attribute allowing for custom "className" and "align" values.
    $classes = '';
    if( !empty($block['className']) ) {
        $classes .= sprintf( ' %s', $block['className'] );
    }
    if( !empty($block['align']) ) {
        $classes .= sprintf( ' align%s', $block['align'] );
    }

    $product_id = trim( get_field( 'product_id' ) ) ?: '-1';
    $image_field = get_field( 'image' ) ?: '';
    $custom_name = get_field( 'custom_name' ) ?: '';
    $direct_to_cart_field = get_field( 'direct_to_cart' ) ?: '';
    $from_prefix = get_field( 'from_prefix' ) ?: false;        
    $add_to_cart_button = get_field( 'add_to_cart_button' );
    if ( is_null( $add_to_cart_button ) ) {
        $add_to_cart_button = true;
    }
    $badge = get_field( 'badge' ) ?: '';
  
    // Check if product exist
    $product_post = get_post($product_id);   
    
    if ( $product_post && ( $product_post->post_type === 'product' || $product_post->post_type === 'product_variation' ) )
    {   
        $product = wc_get_product( $product_id ); 
        
        if(!$product->is_in_stock()) {
            $classes .= "disabled";
        }

        // Name
        if ( !empty( $custom_name ) ) {
            $product_name = $custom_name;
        }
        else {
            $product_name = $product->get_name();    
        }            

        // Permalink
        $permalink = get_permalink( $product_id );
        if ( !empty( $direct_to_cart_field ) )
        {
            $permalink = site_url() . '/offers/?item=' . $product_id . '&quantity=1';
        }

        // Thumb
        if ( empty( $image_field ) )
        {                                
            $thumbURL = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'fullhd' )[0];
        }
        else
        {
            $thumbURL = $image_field;
        }

        // Price
        $price = $product->get_price_html();
    }
?>

<div class="directsale-block-wrapper <?php echo esc_attr($classes); ?> <?php echo $cardClass; ?>">
    <?php 
        // Badge
        if ( !empty( $badge ) )
        {
            ?>
                <a href="<?php echo $permalink; ?>" title="<?php echo $product_name; ?>">
                    <img class="directsale-badge" src="<?php echo $badge; ?>">
                </a>
            <?php
        }

        if ( $product ) 
        {
            if ( $product->is_on_sale() ) {
                $regular_price = (float) $product->get_regular_price();
                $sale_price    = (float) $product->get_sale_price();
            
                if ( $regular_price > 0 ) {
                    $discount_percentage = ( ( $regular_price - $sale_price ) / $regular_price ) * 100;
                    echo '<div class="directsale-block-percentage">' . round( $discount_percentage ) . '% OFF</div>';
                } 
            } 

            ?>                
                <div class="directsale-block-thumb">
                    <a href="<?php echo $permalink; ?>" title="<?php echo $product_name; ?>">
                        <img src="<?php echo $thumbURL; ?>" alt="<?php echo $product_name; ?>">	
                    </a>
                </div>                
                <div class="directsale-block-title">
                    <a href="<?php echo $permalink; ?>" title="<?php echo $product_name; ?>">
                        <?php echo $product_name; ?>
                    </a>
                </div>                
                <?php         
                    // From Prefix
                    $classPrefix = '';
                    if ( $from_prefix )  {
                        $classPrefix = 'from-prefix';
                    }
                ?>
                <div class="directsale-block-price <?php echo $classPrefix; ?>">
                    <?php echo $price; ?>
                </div>
                <?php
                    if ( $add_to_cart_button )
                    {
                        ?>
                            <div class="directsale-block-action">
                                <a href="<?php echo $permalink; ?>" title="<?php echo $product_name; ?>">
                                    <button class="std-button">
                                        <?php _e( 'お買い物カゴに追加', 'ambientlounge' ); ?>
                                    </button>
                                </a>    
                            </div>
                        <?php
                    }
                ?>

            <?php
        } 
    ?>
</div>