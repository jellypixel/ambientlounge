<?php
/**
 * Hardcoded Price Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create class attribute allowing for custom "className" and "align" values.
$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$price = get_field( 'price' ) ?: '0';
$sale = get_field( 'sale', 'option');
$discount_rate = get_field( 'discount_rate', 'option' );

if ( $sale ) {
    $sale_price = $price - ( $price * $discount_rate /100 );
}
?>
<div class="hprice-block-wrapper <?php echo esc_attr($classes); ?>">
    <?php
        if ( $sale )
        {
            echo 
                '<div class="hprice-original"><div class="hprice-original-inner">¥' . number_format( $price ) . '</div></div>
                <div class="hprice-sale">¥' . number_format( $sale_price ) . '</div>
                <div class="hprice-tax-included">(税込)から</div>';
        }
        else {
            echo '<div class="hprice-normal">¥' . number_format( $price ) . ' (税込)から</div>';
        }
    ?>
</div>