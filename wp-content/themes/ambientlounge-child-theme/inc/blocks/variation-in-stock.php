<?php
/**
 * Variation in Stock Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create class attribute allowing for custom "className" and "align" values.
$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$product_id = get_field( 'product' ) ?: '';
/*$gray_background_field = get_field( 'gray_background' ) ?: '';

$grayClass = '';
if ( $gray_background_field ) {
    $grayClass = 'gray-bg';
}*/

$product = wc_get_product( $product_id );

if ($product->is_type( 'variable' )) 
{
    $variations = $product->get_available_variations();
    $variations_id = wp_list_pluck( $variations, 'variation_id' );
    ?>
        <div class="variationinstock-block-outer">
            <?php
                foreach( $variations_id as $variation_id ) 
                {     
                    $variable_product = wc_get_product( $variation_id );
                    if( $variable_product && $variable_product->has_enough_stock(1) ) 
                    {
                        // Name
                        $product_name = $variable_product->get_name();

                        // Permalink
                        $permalink = site_url() . '/offers/?item=' . $variation_id . '&quantity=1';

                        // Thumb        
                        $thumbURL = wp_get_attachment_image_src( get_post_thumbnail_id( $variation_id ), 'fullhd' )[0];

                        // Price
                        $price = $variable_product->get_price_html();
                        ?>
                            <div class="variationinstock-block-wrapper <?php echo esc_attr($classes); ?> <?php //echo $grayClass; ?>">
                                <div class="variationinstock-block-thumb">
                                    <a href="<?php echo $permalink; ?>" title="<?php echo $product_name; ?>">
                                        <img src="<?php echo $thumbURL; ?>" alt="<?php echo $product_name; ?>">	
                                    </a>
                                </div>                
                                <div class="variationinstock-block-title">
                                    <a href="<?php echo $permalink; ?>" title="<?php echo $product_name; ?>">
                                        <?php echo $product_name; ?>
                                    </a>
                                </div>                
                                <div class="variationinstock-block-price">
                                    <?php echo $price; ?>
                                </div>
                                <div class="variationinstock-block-action">
                                    <a href="<?php echo $permalink; ?>" title="<?php echo $product_name; ?>">
                                        <button class="std-button">
                                            <?php _e( 'お買い物カゴに追加', 'ambientlounge' ); ?>
                                        </button>
                                    </a>    
                                </div>
                            </div>                            
                        <?php
                    }                            
                }    
            ?>
        </div
    <?php
}    
?>
