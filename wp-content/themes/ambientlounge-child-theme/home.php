<?php
	get_header();

    $cat = '';
    if ( isset( $_GET['cat'] ) ) 
    {
        $cat = $_GET['cat'];
    }

    $allCatClass = 'active';
    $categories = get_categories();
    foreach($categories as $category) 
    {
        if ( $cat == $category->term_id ) {
            $allCatClass = '';
        }
    }

    $blogURL = get_post_type_archive_link( 'post' );

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;	   
?>
    <div class="content-area">
        <header class="entry-header">
            <h1><?php _e( 'Blog', 'ambientlounge' ); ?></h1>
        </header>

        <div class="entry-content">        
            <div class="blog-category-wrapper">
                <form action="" method="get">
                    <div class="blog-category-inner">
                        
                        <a href="<?php echo $blogURL . '?cat=all'; ?>" title="<?php _e( 'すべて', 'ambientlounge' ); ?>">
                            <div class="blog-category <?php echo $allCatClass; ?>" cat="-1"><?php _e( 'すべて', 'ambientlounge' ); ?></div>
                        </a>
                        <?php                            
                            foreach($categories as $category) 
                            {
                                ?>
                                    <a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo $category->name ?>">
                                        <div class="blog-category <?php echo ( $cat == $category->term_id) ? "active" : "" ?>" cat="<?php echo $category->term_id ?>"><?php echo $category->name ?></div>
                                    </a>
                                <?php                                
                            }
                        ?>                     
                    </div>                    
                    <input type="hidden" name="cat" id="cat" value="">                        
                </form>
            </div>

            <div class="blog-wrapper">
                <?php		                
                    $args = array(
                        'post_type' => array( 'post' ),
                        'paged' => $paged
                    );

                    if ( !empty( $cat ) ) {
                        $args['cat'] = $cat;
                    }
                    
                    $query = new WP_Query( $args );
                    
                    if ( $query->have_posts() ) 
                    {
                        while ( $query->have_posts() ) 
                        {
                            $query->the_post();

                            global $post;
							$postThumb = get_the_post_thumbnail_url( $post, 'mobile' );	

                            ?>
                                <div class="post-card">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <div class="post-card-thumb" style="background-image:url(<?php echo $postThumb; ?>);">
                                        </div>
                                    </a>
                                    <div class="post-card-description">
                                        <div class="post-card-title">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <h3><?php the_title(); ?></h3>
                                            </a>
                                        </div>
                                        <div class="post-card-content">
                                            <?php
                                                $excerpt = get_the_excerpt(); 
                                                echo $excerpt;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php                        
                        }
                    } 
                    wp_reset_postdata();			
                ?>
            </div>

            <div class="paging-numbered">
                <?php	                
                    echo paginate_links( array(
                        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                        'total'        => $query->max_num_pages,
                        'current'      => max( 1, get_query_var( 'paged' ) ),
                        'format'       => '?paged=%#%',
                        'show_all'     => false,
                        'type'         => 'plain',
                        'end_size'     => 2,
                        'mid_size'     => 1,
                        'prev_next'    => true,
                        'prev_text'    => sprintf( '%1$s', __( '<svg width="100%" height="100%" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
    <g transform="matrix(-1,0,0,1,12.05,-1.85)"><path d="M4.111,2.682L10.828,9.4C10.828,9.4 4.111,16.118 4.111,16.118C3.916,16.313 3.916,16.629 4.111,16.825C4.306,17.02 4.623,17.02 4.818,16.825L11.889,9.754C12.084,9.558 12.084,9.242 11.889,9.046L4.818,1.975C4.623,1.78 4.306,1.78 4.111,1.975C3.916,2.171 3.916,2.487 4.111,2.682Z" style="fill:rgb(205,176,125);"/></g></svg>
', 'generatepress' ) ),
                        'next_text'    => sprintf( '%1$s', __( '<svg width="100%" height="100%" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">    <g transform="matrix(1,0,0,1,-3.95,-1.85)"><path d="M4.111,2.682L10.828,9.4C10.828,9.4 4.111,16.118 4.111,16.118C3.916,16.313 3.916,16.629 4.111,16.825C4.306,17.02 4.623,17.02 4.818,16.825L11.889,9.754C12.084,9.558 12.084,9.242 11.889,9.046L4.818,1.975C4.623,1.78 4.306,1.78 4.111,1.975C3.916,2.171 3.916,2.487 4.111,2.682Z" style="fill:rgb(205,176,125);"/></g></svg>
', 'generatepress' ) ),
                        'add_args'     => false,
                        'add_fragment' => '',
                    ) );
                ?>
            </div>

        </div>
    </div>

<?php
	get_footer();
?>