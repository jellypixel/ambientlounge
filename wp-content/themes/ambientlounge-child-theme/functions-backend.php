<?php
include_once get_theme_file_path( 'inc/migrated/chained_products.php' );
include_once get_theme_file_path( 'inc/migrated/wc_status_additions.php' );
include_once get_theme_file_path( 'inc/migrated/automatewoo/automatewoo_addtosendgrid.php' );

/************************ WCOFU **************************/

    add_action('init',function(){
        if(class_exists('WFOCU_Rule_Base')){
            include_once get_theme_file_path( 'inc/migrated/WFOCU_Rule_Filling.php' );
            include_once get_theme_file_path( 'inc/migrated/WFOCU_Gateway_Integration_Furikomi.php' );
        }
    });

    add_filter('wfocu_wfocu_rule_get_rule_types', function($types){
        $types['Custom'] = [
            "filling" => "Filling"
        ];
        return $types;
    });

    add_filter( 'wfocu_wc_get_supported_gateways', function($gateways){
        $gateways['furikomi'] = 'WFOCU_Gateway_Integration_Furikomi';
        return $gateways;
    });

/************************ AutomateWoo **************************/

    //Add custom triggers for automatewoo
    add_filter('automatewoo/triggers', 'add_custom_triggers');

    function add_custom_triggers($triggers)
    {
        include_once 'inc/migrated/automatewoo/OnB2BOrderSaved.php';
        include_once 'inc/migrated/automatewoo/OnB2COrderSaved.php';

        $triggers['OnB2BOrderSaved'] = 'OnB2BOrderSaved';
        $triggers['OnB2COrderSaved'] = 'OnB2COrderSaved';

        return $triggers;
    }

    //on status changed
    add_action( 'woocommerce_order_status_changed', function($order_id, $old_status, $new_status ){

        do_action('on_b2b_order_saved',$order_id);
        do_action('on_b2c_order_saved',$order_id);

    },PHP_INT_MAX,3);

    //on order saved
    add_action( 'save_post_shop_order', function($order_id ){

        do_action('on_b2b_order_saved',$order_id);
        do_action('on_b2c_order_saved',$order_id);

    },PHP_INT_MAX);

/************************ Meta Boxes **************************/
add_filter( 'rwmb_meta_boxes', 'ambient_product_meta_boxes' );
function ambient_product_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'id' => 'b2b_options',
        'title' => esc_html__( 'B2B Options', 'ambientlounge' ),
        'post_types' => array('shop_order' ),
        'context' => 'side',
        'priority' => 'low',
        'autosave' => 'false',
        'fields' => array(
            array(
                'id' => 'b2b_shipping_date',
                'type' => 'date',
                'name' => esc_html__( 'Shipping due date', 'ambientlounge' )
            ),
        ),
    );

    $meta_boxes[] = array(
        'id' => 'b2c_options',
        'title' => esc_html__( 'B2C Options', 'ambientlounge' ),
        'post_types' => array('shop_order' ),
        'context' => 'side',
        'priority' => 'low',
        'autosave' => 'false',
        'fields' => array(
            array(
                'id' => 'b2c_shipping_date',
                'type' => 'date',
                'name' => esc_html__( 'Shipping due date', 'ambientlounge' )
            ),
        ),
    );

    $meta_boxes[] = array(
        'id' => 'referral_data',
        'title' => esc_html__( 'Store referral', 'metabox-online-generator' ),
        'post_types' => array('shop_order' ),
        'context' => 'side',
        'priority' => 'default',
        'autosave' => 'false',
        'fields' => array(
            array(
                'id' => 'referral_store',
                'type' => 'text',
                'name' => esc_html__( 'Referring store', 'metabox-online-generator' ),
            ),
        ),
    );


    return $meta_boxes;
}

/************************CSV exporter **************************/
    function wc_csv_export_modify_column_headers( $column_headers ) {
        $new_headers = array(
            'customer_code' => 'お客様管理番号',
            'okurijou_type'=> '送り状種類',
            'cool' => 'クール区分',
            'denpyou_bango' => '伝票番号',
            'shipping_date'=>'出荷予定日',
            'delivered_date'=> 'お届け予定日',
            'delivered_time'=> '配達可能時間帯',
            'send_to_code' => 'お届け先コード',
            'phone' => 'お届け先電話番号',
            'phone_branch' => 'お届け先電話番号枝番',
            'postcode'=> 'お届け先郵便番号',
            'full_address' => 'お届先住所',
            'shipping_address_option'=>'お届け先アパート・マンション名',
            'shipping_company_1' => 'お届け先会社・部門１',
            'shipping_company_2' => 'お届け会社・部門２',
            'full_name' => 'お届け先名',
            'full_name_kana' => 'お届け先名(ｶﾅ)',
            'Keisho' => '敬称',
            'client_code'=> 'ご依頼主コード',
            'client_phone' => 'ご依頼主電話番号',
            'client_phone_brunch' => 'ご依頼主電話番号枝番',
            'client_postcode' => 'ご依頼主郵便番号',
            'client_full_address' => 'ご依頼主住所',
            'client_address_option' => 'ご依頼主アパートマンション',
            'client_name' => 'ご依頼主名',
            'client_name_kana'=> 'ご依頼主名(ｶﾅ)',
            'hinmei_code' => '品名コード1',
            'hinmei' => '品名１',
            'hinmei_code2' => '品名コード２',
            'hinmei2' => '品名',
            'niatsukai' => '荷扱い１',
            'niatsukai2'=> '荷扱い２',
            'kiji' => '記事',
            'correct_hikitsugi_gaku' => 'ｺﾚｸﾄ代金引換額（税込)',
            'naisyouhizeigakutou' => '内消費税額等',
            'tomeoki' => '止置き',
            'eigyousho_code' => '営業所コード',
            'hakkoumaisu' => '発行枚数',
            'kosuukuchi_hyouji_flag' => '個数口表示フラグ',
            'seikyuusaki_code' => '請求先顧客コード' ,
            'seikyuusaki_bunrui_code' => '請求先分類コード',
            'unchinkanri_code' =>  '運賃管理番号'
        );

        foreach($new_headers as $key => $val){
            $new_headers[$key] = mb_convert_encoding($val,'UTF-8','UTF-8');
        }

        return $new_headers;
    }

    add_filter( 'wc_customer_order_csv_export_order_headers', 'wc_csv_export_modify_column_headers' );

    function wc_csv_export_modify_row_data( $order_data, $order, $csv_generator ) {
        $data = get_post_meta( $order->id);
        foreach ( $order->get_items() as $item_id => $item ) {
            $product[] = $item['name'];//$order->get_product_from_item($item);

        };
        $product_name = count($product)>=2 ? implode('、',$product) : $product[0];
        $full_name = $data["_shipping_last_name"][0].' '.$data["_shipping_first_name"][0];
        $full_name_kana = $data["_shipping_yomigana_last_name"][0].' '.$data["_shipping_yomigana_first_name"][0];
        $states = WC()->countries->get_states('JP');
        $pref = $states[$data["_shipping_state"][0]];
        $full_address = mb_convert_encoding($pref.$data["_shipping_city"][0].$data["_shipping_address_1"][0],'UTF-8','UTF-8');
        $building =  mb_convert_encoding($data["_shipping_address_2"][0],'UTF-8','UTF-8');
        $postcode =  str_replace('"','',$data['_shipping_postcode'][0]);
        $postcode = str_replace('-','',$postcode);
        $shipping_phone = !empty($data['_shipping_phone'][0])?$data['_shipping_phone'][0]:'';
        $custom_data = array(
            'shipping_date'=> (new DateTime())->format('Y/m/d'),
            'okurijou_type' => '0',
            'phone' => str_replace('"','',$data['_shipping_phone'][0]),
            'full_name' => mb_convert_encoding(str_replace('"','',$full_name),'UTF-8','UTF-8'),
            'full_name_kana' => mb_convert_encoding(mb_convert_kana(str_replace('"','',$full_name_kana),"kh"),'UTF-8','UTF-8'),
            'postcode' => $postcode,
            'full_address' => str_replace('"','',$full_address),
            'shipping_address_option' => str_replace('"','',$building),
            'client_code'=> '',
            'client_phone' => '0120-100-057',
            'client_phone_brunch' => '',
            'client_postcode' => '3410042',
            'client_full_address' => mb_convert_encoding('埼玉県三郷市谷口320-2','UTF-8','UTF-8'),
            'client_address_option' => '',
            'client_name' => mb_convert_encoding('株式会社信光物流','UTF-8','UTF-8'),
            'hinmei' => mb_convert_encoding($product_name,'UTF-8','UTF-8'),
            'seikyuusaki_code' => '0489593482',
            'unchinkanri_code' =>  '01',
            'hakkoumaisu' => '1'
        );

        $new_order_data   = array();
        $one_row_per_item = false;

        if ( version_compare( wc_customer_order_csv_export()->get_version(), '4.0.0', '<' ) ) {
            // pre 4.0 compatibility
            $one_row_per_item = ( 'default_one_row_per_item' === $csv_generator->order_format || 'legacy_one_row_per_item' === $csv_generator->order_format );
        } elseif ( isset( $csv_generator->format_definition ) ) {
            // post 4.0 (requires 4.0.3+)
            $one_row_per_item = 'item' === $csv_generator->format_definition['row_type'];
        }
        if ( $one_row_per_item ) {
            foreach ( $order_data as $data ) {
                $new_order_data[] = array_merge( (array) $data, $custom_data );
            }
        } else {
            $new_order_data = array_merge( $order_data, $custom_data );
        }
        return $new_order_data;
    }
    add_filter( 'wc_customer_order_csv_export_order_row', 'wc_csv_export_modify_row_data', 10, 3 );

    add_filter( 'wc_customer_order_csv_export_enable_bom', function(){return true;} );

/*********************** Adds shipping phone number display to back end ***********************/
    add_filter( 'woocommerce_admin_shipping_fields' , 'additional_admin_shipping_fields' );
    function additional_admin_shipping_fields( $fields ) {
        $fields['phone'] = array(
            'label' => __( 'Telephone', 'woocommerce' ),
        );
        return $fields;
    }

/*********************** Add 'sama' to formatted address **************************/
    add_filter( 'woocommerce_order_formatted_shipping_address','add_sama');
    add_filter( 'woocommerce_order_formatted_billing_address','add_sama');
    function add_sama($data){

        if(isset($data['first_name'])) {
            $data['first_name'] .= '様';
        }
        return $data;
    }

    add_filter( 'woocommerce_credit_card_form_fields',function($default_fields){
        if(get_user_agent() == 'ipad' || get_user_agent() == 'ios' || get_user_agent() == 'android'){
            $default_fields = str_replace('type="text"','type="tel"',$default_fields);
        }
        return $default_fields;
    });

/*********************** Add billing and shipping fields to order admin **************************/
    add_action( 'woocommerce_admin_order_data_after_billing_address', function($order){
        $yomigana_last = get_post_meta( $order->get_id(),'_billing_yomigana_last_name', true);
        $yomigana_first = get_post_meta( $order->get_id(),'_billing_yomigana_first_name', true);
        echo "<div class='address'><p><strong>フリガナ:</strong><br>".$yomigana_last . ' '. $yomigana_first ."</p></div>";
    },30);

    add_action( 'woocommerce_admin_order_data_after_shipping_address', function($order){
        $yomigana_last = get_post_meta( $order->get_id(),'_shipping_yomigana_last_name', true);
        $yomigana_first = get_post_meta( $order->get_id(),'_shipping_yomigana_first_name', true);
        echo "<div class='address'><p><strong>フリガナ:</strong><br>".$yomigana_last.' '. $yomigana_first ."</p></div>";
    },30);

    add_filter("woocommerce_admin_billing_fields",function($fields){
        unset($fields['first_name']);
        unset($fields['last_name']);
        $insert =
            array(
                'last_name' => array(
                    'label' => __( 'Last Name', 'woocommerce' ),
                    'show'  => false
                ),
                'first_name' => array(
                    'label' => __( 'First Name', 'woocommerce' ),
                    'show'  => false
                ),
                'yomigana_last_name' => array(
                    'label' => 'フリガナ（姓）',
                    'show'  => false
                ),
                'yomigana_first_name' => array(
                    'label' => 'フリガナ（名）',
                    'show'  => false
                ),
                'company' => array(
                    'label' => 'Building Name',
                    'show'  => true
                )
            );

        return array_merge($insert,$fields);


    },100);

    add_filter("woocommerce_admin_shipping_fields",function($fields){
        unset($fields['first_name']);
        unset($fields['last_name']);
        $insert =
            array(
                'last_name' => array(
                    'label' => __( 'Last Name', 'woocommerce' ),
                    'show'  => false
                ),
                'first_name' => array(
                    'label' => __( 'First Name', 'woocommerce' ),
                    'show'  => false
                ),
                'yomigana_last_name' => array(
                    'label' => 'フリガナ（姓）',
                    'show'  => false
                ),
                'yomigana_first_name' => array(
                    'label' => 'フリガナ（名）',
                    'show'  => false
                ),
                'company' => array(
                    'label' => 'Building Name',
                    'show'  => true
                )
            );

        return array_merge($insert,$fields);


    },100);

/*********************** Emails **************************/

    // Extra templates for AutomateWoo promo emails
    add_filter( 'automatewoo_email_templates', 'custom_automatewoo_email_templates' );
    function custom_automatewoo_email_templates( $templates ) {
        $templates['no-header'] = 'Mail with no headline';
        return $templates;
    }

    // Add 備考
    add_action('woocommerce_email_before_order_table',function($order){

        if ( $notes = $order->get_customer_order_notes() ){
            $output = '<p>[備考]</p>';
            $output .= '<ul>';
            foreach ( $notes as $note ){
                $output.= "<li>" . wpautop( wptexturize( $note->comment_content ) ) . "</li>";
            }
            $output .= '</ul>';
            echo $output;
        }
    },25);

    //display shipping address in the mails
    add_filter('woocommerce_order_needs_shipping_address','__return_true');

/************************ Gift option settings **************************/
    add_action('woocommerce_before_order_notes', 'gift_option');

    function gift_option( $checkout ) {
        echo '<div id="gift_option"><h3>';

        woocommerce_form_field( 'is_a_gift', array(
            'type'          => 'checkbox',
            'class'         => array('input-checkbox'),
            'label'         => __('納品書不要'),
            'required'  => false,
        ), $checkout->get_value( 'is_a_gift' ));

        echo '</h3></div>';
    }
    add_action('woocommerce_checkout_update_order_meta', 'gift_option_update_order_meta');

    function gift_option_update_order_meta( $order_id ) {
        if ($_POST['is_a_gift']) update_post_meta( $order_id, 'is_a_gift', esc_attr($_POST['is_a_gift']));
    }

    // Add gift checkbox to order page
    add_action( 'add_meta_boxes', 'add_gift_metabox' );
    function add_gift_metabox()
    {
        add_meta_box( 'gift_meta_box', "Gift option", 'show_gift_metabox', 'shop_order', 'normal', 'core' );
    }

    function show_gift_metabox()
    {
        global $post;
        $checked = get_post_meta( $post->ID, 'is_a_gift', true )  ? 'checked' : '';

        ?>

        <div>
            <table>
                <tr>
                    <td><label>Is a Gift?</label></td>
                    <td> <input type="checkbox" name="is_a_gift" <?= $checked ?> value="1">
                        <input type="hidden" name="is_a_gift_nonce" value="<?= wp_create_nonce() ?>">
                    </td>
                </tr>
            </table>
        </div>
    <?php
    }
    add_action( 'save_post', 'save_gift_option_admin' );
    function save_gift_option_admin($post_id){
        if(!isset($_POST['is_a_gift_nonce']))  return;

        $is_a_gift = isset($_POST['is_a_gift']);
        update_post_meta($post_id,'is_a_gift',$is_a_gift);

    }

/************************ Hide shipping rates when free shipping is available. **************************/

    add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );
    function my_hide_shipping_when_free_is_available( $rates ) {
        $free = array();
        foreach ( $rates as $rate_id => $rate ) {
            if ( 'free_shipping' === $rate->method_id ) {
                $free[ $rate_id ] = $rate;
                break;
            }
        }
        return ! empty( $free ) ? $free : $rates;
    }

/************************ Show product variation on title. **************************/
 
    add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_true' );

/************************ Order CSV Export **************************/

    //For order csv export
    add_filter('woe_fetch_order','order_csv_export_prettier_format',20,2);
    function order_csv_export_prettier_format($row,$order){

        foreach($row['products'] as &$product){
            $product['name'] = mb_convert_kana(str_replace(' ','', $product['name']),'AS');
        }

        $row['shipping_last_name'] = $order->get_shipping_last_name() . '　' . $order->get_shipping_first_name();

        $states = WC()->countries->get_states( 'JP' );
        $pref_code = $order->get_shipping_state();
        $pref_name = $states[$pref_code] ?? $pref_code;
        $city = $order->get_shipping_city();
        $address = $row['shipping_address'];
        $row['shipping_address'] = $pref_name . $city . $address;

        return $row;

    }
    
    add_action('woe_fetch_order_row',function($row,$order_id){
        $limit = 15;
        foreach($row['products'] as &$product){
            if(mb_strlen($product['name'], 'UTF-8') > $limit){
              $product['name'] = mb_substr($product['name'],0, $limit);
            }
        }
    
        return $row;
    },20,2);

/************************ REST API Modifications **************************/

    // Get cart collaterals and apply coupon using ajax
    function get_cart_collaterals_callback($request) {
        ob_start();
        woocommerce_cart_totals();
        $result = ob_get_clean();

        return $result;
    }

    function calculate_shipping_callback($request) {
        $address['country']  = isset( $_POST['calc_shipping_country'] ) ? wc_clean( wp_unslash( $_POST['calc_shipping_country'] ) ) : '';
        $address['state']    = isset( $_POST['calc_shipping_state'] ) ? wc_clean( wp_unslash( $_POST['calc_shipping_state'] ) ) : '';
        $address['postcode'] = isset( $_POST['calc_shipping_postcode'] ) ? wc_clean( wp_unslash( $_POST['calc_shipping_postcode'] ) ) : '';
        $address['city']     = isset( $_POST['calc_shipping_city'] ) ? wc_clean( wp_unslash( $_POST['calc_shipping_city'] ) ) : '';
        WC()->customer->set_billing_location( $address['country'], $address['state'], $address['postcode'], $address['city'] );
        WC()->customer->set_shipping_location( $address['country'], $address['state'], $address['postcode'], $address['city'] );
        ob_start();

        $packages = WC()->cart->get_shipping_packages();
        WC()->shipping()->calculate_shipping($packages);

        $packages = WC()->shipping()->get_packages();
        foreach ($packages as $key => $package) {
            $method = wc_get_chosen_shipping_method_for_package($key, $package);
        }

        WC()->customer->set_calculated_shipping();

        woocommerce_cart_totals();
        $result = ob_get_clean();

        return $result;
    }

    function apply_coupon_callback($request) {
    $coupon_code = $_POST['coupon_code'];

    include_once WC_ABSPATH . 'includes/wc-cart-functions.php';
    include_once WC_ABSPATH . 'includes/class-wc-cart.php';
    require_once WC_ABSPATH . 'includes/wc-notice-functions.php';

    if ( is_null( WC()->cart ) ) {
        wc_load_cart();
    }

    ob_start();
    WC()->cart->apply_coupon( sanitize_text_field($coupon_code) );
    wc_print_notices();
    $result = ob_get_clean();
        return $result;
    }

    add_action( 'rest_api_init', function () {
    register_rest_route( 'cart', '/apply-coupon', array(
        'methods' => 'POST',
        'callback' => 'apply_coupon_callback'
    ) );
    register_rest_route( 'cart', '/calculate-shipping', array(
        'methods' => 'POST',
        'callback' => 'calculate_shipping_callback'
    ) );
    register_rest_route( 'cart', '/get-collaterals', array(
        'methods' => 'GET',
        'callback' => 'get_cart_collaterals_callback'
    ) );
    } );

/************************ Modify Woo Frontend **************************/

    // Product tabs
    add_filter( 'woocommerce_product_tabs', 'reorder_product_tabs', 98 );

    function reorder_product_tabs( $tabs ) {

        unset( $tabs['additional_information'] );

        $tabs['description']['priority'] = 5;

        if(isset($tabs['選べる注文方法']))
            $tabs['選べる注文方法']['priority'] = 10;
        if(isset($tabs['お手入れ']))
            $tabs['お手入れ']['priority'] = 15;

        return $tabs;
    }

    // Rename and remove order dropdowns
    add_filter( 'woocommerce_catalog_orderby', 'rename_default_sorting_options' );
    function rename_default_sorting_options( $options ){

        unset( $options[ 'rating' ] ); // remove
        $options[ 'menu_order' ] = 'おすすめ'; // rename

        return $options;
    }

    // Remove add to cart message
    // add_filter('wc_add_to_cart_message', '__return_false');

    // Remove tag from variation name
    add_filter('woocommerce_json_search_found_products','removeTagFromVariationName',20);
    function removeTagFromVariationName($products){
    $rtn = [];
    foreach($products as $key => $product){
        $rtn[$key] = preg_replace('/<span.*<\/span>/i','',$product);
    }
    return $rtn;
    }

    // Add logout URL to amazon pay parameters
    add_filter('woocommerce_amazon_pa_widgets_params','add_logout_url');
    function add_logout_url($params){
    $params['redirect_url'] = $params['logout_url'];
    return $params;
    }

/************************ Modify Checkout Fields **************************/

    add_filter('woocommerce_checkout_fields','remove_field_when_amazon_pay',20);
    function remove_field_when_amazon_pay($fields){

        $fields['billing']['billing_phone']['required'] = true; 

        if( isset($_REQUEST['amazon_payments_advanced'])){
            unset($fields['billing']['billing_yomigana_first_name'] );
            unset($fields['billing']['billing_yomigana_last_name']);
            unset($fields['shipping']['shipping_yomigana_first_name']);
            unset($fields['shipping']['shipping_yomigana_last_name']);
            $fields['billing']['billing_state']['required'] = false;
            $fields['billing']['billing_city']['required'] = false;
            $fields['shipping']['shipping_city']['required'] = false;
        }

        if(WC()->session->chosen_payment_method == "amazon_payments_advanced") {
            unset($fields['billing']['billing_yomigana_first_name'] );
            unset($fields['billing']['billing_yomigana_last_name']);
            unset($fields['shipping']['shipping_yomigana_first_name']);
            unset($fields['shipping']['shipping_yomigana_last_name']);
            $fields['billing']['billing_state']['required'] = false;
            $fields['billing']['billing_city']['required'] = false;
            $fields['shipping']['shipping_city']['required'] = false;
        }

        if(!empty($_POST['payment_request_type'])) {

            if($_POST['payment_request_type'] == "google_pay") {
                unset($fields['billing']['billing_yomigana_first_name'] );
                unset($fields['billing']['billing_yomigana_last_name']);
                unset($fields['shipping']['shipping_yomigana_first_name']);
                unset($fields['shipping']['shipping_yomigana_last_name']);
                $fields['billing']['billing_phone']['required'] = false;
                $fields['billing']['billing_state']['required'] = false;
                $fields['billing']['billing_city']['required'] = false;
                $fields['shipping']['shipping_city']['required'] = false;
                $fields['shipping']['shipping_phone']['required'] = false;
            }
        
            if($_POST['payment_request_type'] == "apple_pay") {
                unset($fields['billing']['billing_yomigana_first_name'] );
                unset($fields['billing']['billing_yomigana_last_name']);
                unset($fields['shipping']['shipping_yomigana_first_name']);
                unset($fields['shipping']['shipping_yomigana_last_name']);
                $fields['billing']['billing_phone']['required'] = false;
                $fields['billing']['billing_state']['required'] = false;
                $fields['billing']['billing_city']['required'] = false;
                $fields['shipping']['shipping_city']['required'] = false;
                $fields['shipping']['shipping_phone']['required'] = false;
            }

        }

    return $fields;
    }

/************************ Custom Order Status **************************/

    // Register new custom order status B2B 予約商品
    add_action('init', 'register_reserved_order_statuses');
    function register_reserved_order_statuses() {
        register_post_status('wc-test-reserved ', array(
            'label' => __( 'B2B 予約商品', 'woocommerce' ),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('B2B 予約商品 <span class="count">(%s)</span>', 'B2B 予約商品 <span class="count">(%s)</span>')
        ));
    }

    // Add new custom order status to list of WC Order statuses
    add_filter('wc_order_statuses', 'add_reserved_order_statuses');
    function add_reserved_order_statuses($order_statuses) {
        $new_order_statuses = array();


        foreach ($order_statuses as $key => $status) {
            $new_order_statuses[$key] = $status;
            if ('wc-processing' === $key) {
                $new_order_statuses['wc-test-reserved'] = __('B2B 予約商品', 'woocommerce' );
            }
        }
        return $new_order_statuses;
    }

    // Adding new custom status to admin order list bulk dropdown
    add_filter( 'bulk_actions-edit-shop_order', 'reserved_dropdown_bulk_actions_shop_order', 50, 1 );
    function reserved_dropdown_bulk_actions_shop_order( $actions ) {
        $new_actions = array();


        foreach ($actions as $key => $action) {
            if ('mark_processing' === $key) {
                $new_actions['mark_b2b-ap'] = __( 'Change status to B2B: Awaiting payment', 'woocommerce' );
                $new_actions['mark_b2b-or'] = __( 'Change status to B2B: Order received', 'woocommerce' );
                $new_actions['mark_b2b-op'] = __( 'Change status to B2B: Order processing', 'woocommerce' );
                $new_actions['mark_b2b-pts'] = __( 'Change status to B2B: Preparing to ship', 'woocommerce' );
                $new_actions['mark_b2b-comp'] = __( 'Change status to B2B: Complete', 'woocommerce' );
            }
            
            $new_actions[$key] = $action;
        }
        return $new_actions; 
    }
    add_action( 'woocommerce_order_status_b2b-op', 'wc_maybe_reduce_stock_levels' );

    // Register new custom order status Pop up 予約商品
    add_action('init', 'register_popureserved_order_statuses');
    function register_popureserved_order_statuses() {
        register_post_status('wc-test-popureserved ', array(
            'label' => __( 'Pop up 予約商品', 'woocommerce' ),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Pop up 予約商品 <span class="count">(%s)</span>', 'Pop up 予約商品 <span class="count">(%s)</span>')
        ));
    }

    // Add new custom order status to list of WC Order statuses
    add_filter('wc_order_statuses', 'add_popureserved_order_statuses');
    function add_popureserved_order_statuses($order_statuses) {
        $new_order_statuses = array();


        foreach ($order_statuses as $key => $status) {
            $new_order_statuses[$key] = $status;
            if ('wc-processing' === $key) {
                $new_order_statuses['wc-test-popureserved'] = __('Pop up 予約商品', 'woocommerce' );
            }
        }
        return $new_order_statuses;
    }

    // Adding new custom status to admin order list bulk dropdown
    add_filter( 'bulk_actions-edit-shop_order', 'popureserved_dropdown_bulk_actions_shop_order', 50, 1 );
    function popureserved_dropdown_bulk_actions_shop_order( $actions ) {
        $new_actions = array();


        foreach ($actions as $key => $action) {
            if ('mark_processing' === $key)
                $new_actions['mark_test-popureserved'] = __( 'Change status to Pop up 予約商品', 'woocommerce' );

            $new_actions[$key] = $action;
        }
        return $new_actions;
}

/************************ Tracking **************************/

    /* Google Tag Manager */
    add_action('wp_footer', 'criteo_scripts');
    function criteo_scripts(){

        $email = "";
        if ( is_user_logged_in() ) { 
            $current_user = wp_get_current_user();
            $email = $current_user->user_email;
        }

        // Jelly Pixel
        if(is_cart()) {
            ?>
                <!-- JellyPixel Universal dataLayer -->
                <script type="text/javascript">
                    var dataLayer = dataLayer || [];
                    dataLayer.push({
                        event: 'jellypixel_cart',
                        jellypixel: {
                            email: '<?php echo $email; ?>' //can be empty string if email not known
                        }
                    });
                </script>
                <!-- END JellyPixel Universal dataLayer -->
            <?php
        } else {

            if(!is_checkout()) {
                ?>
                    <!-- JellyPixel Universal dataLayer -->
                    <script type="text/javascript">
                        var dataLayer = dataLayer || [];
                        dataLayer.push({
                            event: 'jellypixel_universal',
                            jellypixel: {
                                email: '<?php echo $email; ?>' //can be empty string if email not known
                            }
                        });
                    </script>
                    <!-- END JellyPixel Universal dataLayer -->
                <?php
            }
        }

        // Criteo
        if(is_front_page()) {
            ?>
                <!-- Criteo Homepage dataLayer -->
                <script type="text/javascript">
                    var dataLayer = dataLayer || [];
                    dataLayer.push({
                        event: 'crto_homepage',
                        crto: {
                            email: '<?php echo $email; ?>' //can be empty string if email not known
                        }
                    });
                </script>
                <!-- END Criteo Homepage dataLayer -->
            <?php
        } else if(is_product_category()) {
            global $wp_query;
            $category_id = $wp_query->get_queried_object_id();

            $query = new WP_Query( array(
                'post_type'      => 'product',
                'post_status'    => 'publish',
                'posts_per_page' => -1,
                'tax_query'      => array( array(
                    'taxonomy'   => 'product_cat',
                    'field'      => 'term_id',
                    'terms'      => array( $category_id ),
                ) )
            ) );
            
            $itemsJSON = array();
            while ( $query->have_posts() ) : $query->the_post();
                $itemsJSON[] = "" . get_the_ID() . "";
            endwhile;

            ?>
                <!-- Criteo Category dataLayer -->
                <script type="text/javascript">
                    var dataLayer = dataLayer || [];
                    dataLayer.push({
                        event: 'crto_listingpage',
                        crto: {
                            email: '<?php echo $email; ?>',
                            products: <?php echo json_encode($itemsJSON); ?>
                        }
                    });
                </script>
                <!-- END Criteo Category dataLayer -->
            <?php
        } else if(is_product()) {

            // Product ID
            global $product;
            $id = $product->get_id();

            //Attribute SLUG ID
            $attributeID = "";

            $queries = explode("&", $_SERVER['QUERY_STRING']);
            foreach($queries as $query) {

                $queryComponent = explode("=", $query);

                if (strpos($queryComponent[0], 'attribute_') !== false) {
                    $queryComponent[0] = str_replace("attribute_", "", $queryComponent[0]);

                    // Make this only work for pa_color
                    if($queryComponent[0] == "pa_color") {

                        $queryComponent[0] = trim($queryComponent[0]);
                        $queryComponent[1] = trim($queryComponent[1]);

                        $terms = wc_get_product_terms( $id, $queryComponent[0], array( 'fields' => 'all' ) );
                        if(!empty($terms))
                        {
                            foreach($terms as $term) {
                                if($term->slug == $queryComponent[1]) {
                                    $attributeID .= "-" . $term->term_id;
                                }
                            }
                        }
                    }

                }
            }

            ?>
                <!-- Criteo Product dataLayer -->
                <script type="text/javascript">
                    var dataLayer = dataLayer || [];
                    dataLayer.push({
                        event: 'crto_productpage',
                        crto: {
                            email: '<?php echo $email; ?>',
                            products: '<?php echo $id . $attributeID; ?>'
                        }
                    });
                </script>
                <!-- END Criteo Product dataLayer -->
            <?php
        } else if (is_checkout() && empty( is_wc_endpoint_url('order-received') ) ) {
            global $woocommerce;
            $items = $woocommerce->cart->get_cart();

            $itemsJSON = array();
            foreach($items as $item => $values) { 

                // Get attribute ID
                $attributeID = "";

                $queries = $values['variation'];
                foreach($queries as $key => $value) {

                    $queryComponent = array($key, $value);
                    $queryComponent[0] = str_replace("attribute_", "", $queryComponent[0]);

                    // Make this only work for pa_color
                    if($queryComponent[0] == "pa_color") {
                        $terms = wc_get_product_terms( $values['product_id'], $queryComponent[0], array( 'fields' => 'all' ) );
                        if(!empty($terms))
                        {
                            foreach($terms as $term) {
                                if($term->slug == $queryComponent[1]) {
                                    $attributeID .= "-" . $term->term_id;
                                }
                            }
                        }
                    }

                }

                // Get Price
                $price = get_post_meta($values['product_id'] , '_price', true);

                // Final data
                $itemsJSON[] = array("id" => $values['product_id'] . $attributeID, "price" => $price, "quantity" => $values['quantity']);
            } 

            ?>
                <!-- Criteo Basket/Cart dataLayer -->
                <script type="text/javascript">
                    var dataLayer = dataLayer || [];
                    dataLayer.push({
                        event: 'crto_basketpage',
                        crto: {             
                            email: '<?php echo $email; ?>',
                            products: <?php echo json_encode($itemsJSON); ?>
                        }
                    });
                </script>
                <!-- END Criteo Basket/Cart dataLayer -->
            <?php
        } else if ( is_checkout() && !empty( is_wc_endpoint_url('order-received') ) ) {

            global $wp_query;
            $order = wc_get_order( $wp_query->query_vars[ 'order-received' ] );
            
            if($order) {

                update_post_meta($order->id, 'tracker-' . $order->get_order_number(), 1);

                $items = $order->get_items();

                $itemsJSON = array(); $itemsJSONTwitter = array(); $productsCSV = "";
                foreach($items as $item) { 

                    // Get attribute ID
                    $attributeID = "";

                    $queries = wc_get_product_variation_attributes($item['variation_id']);
                    foreach($queries as $key => $value) {

                        $queryComponent = array($key, $value);
                        $queryComponent[0] = str_replace("attribute_", "", $queryComponent[0]);

                        // Make this only work for pa_color
                        if($queryComponent[0] == "pa_color") {
                            $terms = wc_get_product_terms( $item['product_id'], $queryComponent[0], array( 'fields' => 'all' ) );
                            if(!empty($terms))
                            {
                                foreach($terms as $term) {
                                    if($term->slug == $queryComponent[1]) {
                                        $attributeID .= "-" . $term->term_id;
                                    }
                                }
                            }
                        }

                    }
                    
                    //($item->get_total() + $item->get_total_tax())
                    $productsCSV .= $item['product_id'] . "/";
                    $itemsJSON[] = array("id" => $item['product_id'] . $attributeID, "price" => $item->get_total(), "quantity" => $item->get_quantity());
                    $itemsJSONTwitter[] = array("content_name" => $item['product_id'] . $attributeID, "content_id" => $item['product_id'] . $attributeID, "content_price" => $item->get_total(), "num_items" => $item->get_quantity());
                } 

                $productsCSV = rtrim($productsCSV, "/");

                ?>
                    <!-- Criteo Sales dataLayer -->
                    <script type="text/javascript">
                        var dataLayer = dataLayer || [];
                        dataLayer.push({
                            event: 'crto_transactionpage',
                            jellypixel:{
                                revenue: '<?php echo $order->get_total(); ?>',
                                products: '<?php echo $productsCSV; ?>',
                                productstwitter: <?php echo json_encode($itemsJSONTwitter); ?>
                            },
                            crto: {
                                email: '<?php echo $email; ?>',
                                transactionid: '<?php echo $order->id; ?>',
                                products: <?php echo json_encode($itemsJSON); ?>
                            }
                        });
                    </script>
                    <!-- END Criteo Sales dataLayer -->
                <?php
            }
        }
    };

/************************ Referral Factory **************************/

    /* Referral Factory Endpoint */
    add_action( 'rest_api_init', function () {
        register_rest_route( 'referralfactory', '/add', array(
                'methods' => 'POST',
                'callback' => 'add_referral',
            ) );
    } );
    
    function add_referral( $req ) {
    
        $req = $req->get_params();
    
        global $wpdb;
        $response = $wpdb->insert('referral_factory', array(
            'referrer_code' => $req['referrer_code'],
            'referral_code' => $req['code'],
            'referral_email' => $req['email'],
        ));
    
        $res = new WP_REST_Response($response);
        $res->set_status(200);
    
        return ['req' => $res];
    }
    
    add_action( 'woocommerce_thankyou', function($order_id){
    
        if ( !$order_id ) return;
    
        $order = wc_get_order($order_id); 
        $billing_email  = $order->get_billing_email();
    
        //Get referral code (if exist)
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM referral_factory where referral_email = '" . $billing_email . "'" );
        foreach ( $result as $referral )
        {
            $ch = curl_init();
    
            curl_setopt($ch, CURLOPT_URL, "https://referral-factory.com/webhook/bfZBQ7pUoGjR7Bbg8wVF/qualify?code=" . $referral->referral_code);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
            $return = curl_exec($ch);
            curl_close ($ch);
        }
        
    }, 10, 1);    

/************************ Others **************************/

    // Redirect all 404 to Homepage
    add_action('template_redirect', 'redirect_404_to_homepage', 999);
    function redirect_404_to_homepage(){

        if(!is_404())
            return;

        wp_redirect(home_url());
        exit;

    }

    /* JP: Disable Checkout Data Prefilling */
    add_filter('woocommerce_checkout_get_value','jp_clear_checkout_fields', 1, 2);
    function jp_clear_checkout_fields($value, $input) {

        if(is_user_logged_in()) {
            return $value;
        } else {
            return "";
        }

    }

    /* JP: Programmatically clears the cache for all products included in an order when the payment has been completed. */
    add_action('woocommerce_thankyou', 'jp_clean_product_cache_after_order');
    function jp_clean_product_cache_after_order($order_id) {
        if (!$order_id) {
            return;
        }

        // Get the order object
        $order = wc_get_order($order_id);

        // Check if cache clearing has already been done for this order
        if ($order->get_meta('_kinsta_cache_cleared') === 'yes') {
            return;
        }

        // Collect unique product paths that need cache clearing
        $paths_to_clear = [];

        $items = $order->get_items();
        foreach ($items as $item) {
            // Only proceed if the item quantity is greater than 0
            if ($item->get_quantity() > 0) {
                // Get the product and variation IDs
                $product_id = $item->get_product_id();
                $variation_id = $item->get_variation_id();

                // Only fetch stock data instead of entire product objects
                $stock_quantity = ($variation_id) 
                    ? get_post_meta($variation_id, '_stock', true) 
                    : get_post_meta($product_id, '_stock', true);

                // If stock quantity is less than or equal to 2, mark for cache clearing
                if ($stock_quantity !== '' && $stock_quantity <= 2) {
                    $product_path = get_path_from_permalink(get_permalink($product_id));
                    if (!in_array($product_path, $paths_to_clear, true)) {
                        $paths_to_clear[] = $product_path;
                    }
                }
            }
        }

        // Perform cache-clearing requests in a single batch
        foreach ($paths_to_clear as $path) {
            $mobile_url = get_site_url() . '/kinsta-clear-mobile-cache' . $path . '*';
            $desktop_url = get_site_url() . '/kinsta-clear-cache' . $path . '*';

            // Send HTTP requests
            wp_remote_get($mobile_url);
            wp_remote_get($desktop_url);
        }

        // Mark the order as having cleared the cache
        $order->update_meta_data('_kinsta_cache_cleared', 'yes');
        $order->save();
    }

    function get_path_from_permalink($url) {
        $parsed_url = wp_parse_url($url);
        return isset($parsed_url['path']) ? $parsed_url['path'] : '';
    }

    /* JP: Remove bulk cancel from order table */
    add_filter( 'bulk_actions-edit-shop_order', 'jp_custom_shop_order_bulk_actions', 999 );
    function jp_custom_shop_order_bulk_actions( $actions ) {
        unset( $actions['mark_cancelled'] );
        return $actions;
    }

    /* JP: Reorder quick action */
    function add_custom_admin_js() {
    ?>
    <script>
    (function($) {
        $(document).ready(function() {
        var select = $('#bulk-action-selector-top');
        var options = select.find('option');

        // Remove all options
        select.empty();

        // Add options in the desired order
        select.append(options.filter('[value="-1"]')); // Bulk actions
        // select.append(options.filter('[value="woe_export_selected_orders"]')); // Export as CSV
        select.append(options.filter('[value="invoice"]')); // PDF Invoice
        select.append(options.filter('[value="packing-slip"]')); // PDF Packing Slip
        select.append(options.filter('[value="mark_processing"]')); // Change status to processing
       
        // Change the label for mark_preparing_to_ship
        var preparingToShipOption = options.filter('[value="mark_preparing_to_ship"]');
        preparingToShipOption.text("Change status to preparing to ship");
        select.append(preparingToShipOption);

        select.append(options.filter('[value="mark_completed"]')); // Change status to completed
        select.append(options.filter('[value="mark_on-hold"]')); // Change status to on-hold

        select.append(options.filter('[value="mark_b2b-ap"]')); // Change status to B2B: Awaiting payment
        select.append(options.filter('[value="mark_b2b-or"]')); // Change status to B2B: Order received
        select.append(options.filter('[value="mark_b2b-op"]')); // Change status to B2B: Order processing
        select.append(options.filter('[value="mark_b2b-pts"]')); // Change status to B2B: Preparing to ship
        select.append(options.filter('[value="mark_b2b-comp"]')); // Change status to B2B: Complete

        // select.append(options.filter('[value="mark_test-popureserved"]')); // Change status to Pop up 予約商品
        // select.append(options.filter('[value="woe_mark_exported"]')); // Mark exported
        // select.append(options.filter('[value="woe_unmark_exported"]')); // Unmark exported

        select.append(options.filter('[value="trash"]')); // Move to Trash
        select.append(options.filter('[value="cdp_bulk_copy"]')); // コピー
        });
    })(jQuery);
    </script>
    <?php
    }
    add_action( 'admin_footer', 'add_custom_admin_js' );

    /* JP: Add new column to order list */
    add_filter( 'manage_edit-shop_order_columns', 'jp_add_order_categorization_column_header', 20 );
    function jp_add_order_categorization_column_header( $columns ) {

        $new_columns = array();
       
        foreach ( $columns as $column_name => $column_info ) {
       
            $new_columns[ $column_name ] = $column_info;
        
            if ( 'order_date' === $column_name ) {
                $new_columns['order_categorization'] = __( 'Category', 'ambientlounge' );
                $new_columns['order_phone'] = __( 'Phone', 'ambientlounge' );
                $new_columns['order_due'] = __( 'Due Date', 'ambientlounge' );
            }
        }
       
        return $new_columns;
    }

    add_action( 'manage_shop_order_posts_custom_column', 'jp_add_order_categorization_column_content' );
    function jp_add_order_categorization_column_content( $column ) {
        global $post;

        if ( 'order_categorization' === $column ) {
            $order = wc_get_order( $post->ID );
            $items = $order->get_items();

            $categories = array();
            $sizes = array();

            foreach ( $items as $item ) {
                $product_id = $item['product_id'];
                $product = new WC_Product($product_id);

                // Get the category of the product
                $product_categories = wp_get_post_terms( $product_id, 'product_cat', array( 'fields' => 'ids' ) );

                // Get the SKU of the product 
                $product_sku = get_post_meta( $item['variation_id'], '_sku', true );

                // Check if SKU contains specific variations and add them to categories
                if (strpos($product_sku, '-L-') !== false) {
                    $sizes = array_unique(array_merge($sizes, array('L')));
                } else if (strpos($product_sku, '-XL-') !== false) {
                    $sizes = array_unique(array_merge($sizes, array('XL')));
                } else if (strpos($product_sku, '-XXL-') !== false) {
                    $sizes = array_unique(array_merge($sizes, array('XXL')));
                } else if (strpos($product_sku, '-S-') !== false) {
                    $sizes = array_unique(array_merge($sizes, array('S')));
                } else if (strpos($product_sku, '-M-') !== false) {
                    $sizes = array_unique(array_merge($sizes, array('M')));
                } else if (strpos($product_sku, '-XS-') !== false) {
                    $sizes = array_unique(array_merge($sizes, array('XS')));
                }

                $categories = array_merge( $categories, $product_categories );
            }    
            
            $categories = array_unique( $categories );

            // Get categorization
            $settings = get_field("categorizationgrouping_mapping", "option");
            $displayed = array();

            foreach ( $categories as $category ) {

                foreach ($settings as $setting) {

                    if(in_array($category, $setting["category"])) {

                        if(!in_array($setting["name"], $displayed)) {
                            $displayed[] = $setting["name"];
 
                            // Special rule for PET
                            foreach($sizes as $size) {
                                $setting["name"] = $setting["name"] . "-" . $size;
                            }

                            // Special rule for ECO
                            if($setting["show_sku"])  {
                                $modified_sku = preg_replace('/^[\d-]+/', '', $product_sku);

                                if (stripos($modified_sku, '-D-') !== false) {
                                    $modified_sku = "PETBED";
                                } else if (stripos($modified_sku, 'Cover') !== false) { 
                                    $modified_sku = "COVER";
                                } else if (stripos($modified_sku, 'Shampoo') !== false) {
                                    $modified_sku = "SHAMPOO";
                                } else if (stripos($modified_sku, 'Laundry') !== false) {
                                    $modified_sku = "LAUNDRY";
                                } else if (stripos($modified_sku, 'Cushion') !== false) {
                                    $modified_sku = "CUSHION";
                                } else if (stripos($modified_sku, 'Acoustic') !== false) {
                                    $modified_sku = "SOFA";
                                } else if (stripos($modified_sku, 'Butterfly') !== false) {
                                    $modified_sku = "SOFA";
                                } else if (stripos($modified_sku, 'Ottoman') !== false) {
                                    $modified_sku = "SOFA";
                                } else if (stripos($modified_sku, 'Couch') !== false) {
                                    $modified_sku = "SOFA";
                                } else if (stripos($modified_sku, 'Table') !== false) {
                                    $modified_sku = "TABLE";
                                } else if (stripos($modified_sku, 'Blanket') !== false) {
                                    $modified_sku = "BLANKET";
                                } else if (stripos($modified_sku, 'VIP') !== false) {
                                    $modified_sku = "SOFA";
                                }  

                                 $setting["name"] = $setting["name"] . "-" .  $modified_sku;
                            } 

                            echo '<div style="background-color:'.$setting["color"].'; border-radius: 5px;color: #fff;font-weight: 800;padding: 10px;display: inline-block; margin-top:2px; margin-bottom: 2px;">' . $setting["name"] . '</div>';
                        }
                    }

                }

            }
        }

        if ( 'order_phone' === $column ) {
            $order = wc_get_order( $post->ID );
            echo $order->get_billing_phone();
        }

        if ( 'order_due' === $column ) {
            global $wp_roles;
            $order = wc_get_order( $post->ID );
            
            if ( $order ) {
                // Get the customer ID
                $customer_id = $order->get_customer_id();
            
                // Check if the customer ID is valid (non-guest)
                if ( $customer_id ) {

                    // 2. Get the user's roles
                    $user = get_userdata( $customer_id );
                    $roles = $user->roles;
            
                    // 3. Loop through the roles to find one starting with "B2B"
                    $b2b_role = '';
                    foreach ($roles as $role) {
                        
                        $role = translate_user_role( $wp_roles->roles[ $role ]['name'] );
            
                        if (stripos($role, 'B2B') !== FALSE) { // Role starts with "B2B"
                            $b2b_role = $role;
                            break;
                        }
                    }
                    
                    // If we found a B2B role, proceed with extracting information
                    if ($b2b_role) {
                        // Remove the 'wcwp_b2b' part 
                        $b2b_role = str_replace('B2B-', '', $b2b_role);
            
                        // Split the role into components
                        $components = explode('-', $b2b_role);
            
                        // Extract the discount amount
                        $discount = isset($components[0]) ? intval($components[0]) : 0;
            
                        // Extract the shipping type (either 'shipping' or 'freeshipping')
                        $shipping_type = isset($components[1]) ? $components[1] : '';
            
                        // Extract the invoice deadline (last part of the string)
                        $invoice_deadline = isset($components[2]) ? $components[2] : '';
            
                        // Default deadline date (today)
                        $deadline_date = $order->get_date_created();
                        $deadline_date = new DateTime($deadline_date);
            
                        // Handle different deadline cases
                        if (stripos($invoice_deadline, 'EOM') !== false) {
                            // End of month (EOM)
                            $deadline_date->modify('last day of this month');
                            
                            // Check if there's an additional modifier (e.g., +65d, +2m, +1y)
                            if (preg_match('/EOM\+(\d+)([dmy])/', $invoice_deadline, $matches)) {
                                // Matches[1] will contain the number, Matches[2] will contain the unit (d, m, y)
                                $number = (int)$matches[1];
                                $unit = $matches[2]; 
            
                                // Modify the date based on the unit (d = days, m = months, y = years)
                                if ($unit === 'd') {
                                    $deadline_date->modify('+' . $number . ' days');
                                } elseif ($unit === 'm') {
                                    $deadline_date->modify('+' . $number . ' months');
                                } elseif ($unit === 'y') {
                                    $deadline_date->modify('+' . $number . ' years');
                                }
                            } 
                        } 
                        elseif (stripos($invoice_deadline, 'EONM') !== false) {
                            // End of next month (EONM)
                            $deadline_date->modify('last day of next month');
                            
                            // Check if there's an additional modifier (e.g., +2m, +1y)
                            if (preg_match('/EONM\+(\d+)([dmy])/', $invoice_deadline, $matches)) {
                                // Matches[1] will contain the number, Matches[2] will contain the unit (d, m, y)
                                $number = (int)$matches[1];
                                $unit = $matches[2];
            
                                // Modify the date based on the unit (d = days, m = months, y = years)
                                if ($unit === 'd') {
                                    $deadline_date->modify('+' . $number . ' days');
                                } elseif ($unit === 'm') {
                                    $deadline_date->modify('+' . $number . ' months');
                                } elseif ($unit === 'y') {
                                    $deadline_date->modify('+' . $number . ' years');
                                }
                            }
                        }            
            
                        // Format the deadline date in "YYYY年MM月DD日"
                        $formatted_deadline_date = $deadline_date->format('Y年m月d日');
                        echo $formatted_deadline_date;
                    }
                    
                } else {
                    echo '';
                }
            } else {
                echo '';
            }
        }
    }

    /* JP: Make category filterrable in the order column */
    add_filter( 'woocommerce_shop_order_search_results', 'jp_order_search_by_category', 9999, 3 );
    function jp_order_search_by_category($order_ids, $term, $search_fields) {
        global $wpdb;

        if (!empty($search_fields)) {
            $category = get_term_by('slug', wc_clean($term), 'product_cat');
 
            if (!$category) {
                return $order_ids;
            }

            $product_ids = $wpdb->get_col(
                $wpdb->prepare(
                    "SELECT DISTINCT tr.object_id
                    FROM {$wpdb->term_relationships} tr
                    LEFT JOIN {$wpdb->term_taxonomy} tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                    LEFT JOIN {$wpdb->terms} t ON (tt.term_id = t.term_id)
                    WHERE tt.taxonomy = 'product_cat' AND t.slug = %s",
                    $category->slug
                )
            );

            if (empty($product_ids)) {
                return $order_ids;
            }

            $product_ids = array_map('absint', $product_ids);

            $order_ids = $wpdb->get_col(
                $wpdb->prepare(
                    "SELECT DISTINCT order_items.order_id
                    FROM {$wpdb->prefix}woocommerce_order_itemmeta as order_itemmeta
                    LEFT JOIN {$wpdb->prefix}woocommerce_order_items as order_items ON (order_itemmeta.order_item_id = order_items.order_item_id)
                    WHERE order_itemmeta.meta_key IN ('_product_id', '_variation_id') AND order_itemmeta.meta_value IN (" . implode(',', $product_ids) . ") AND order_items.order_item_type = 'line_item'"
                )
            );
        }

        return $order_ids;
    } 

    
    /* JP: Add new template to AutomateWoo */

    add_filter( 'automatewoo/variables/product_templates', 'jp_automatewoo_product_templates' );
    function jp_automatewoo_product_templates( $templates ) {
        $templates['order-table-forshipping.php'] = 'Order Table without Tracking';
        $templates['order-table-forinvoice.php'] = 'Order Table without Price';
        return $templates;
    }  
 
    /* JP: Add shipping phone to automatewoo */
    add_filter( 'automatewoo/variables', 'jp_automatewoo_shipping_phone' );
    function jp_automatewoo_shipping_phone( $variables ) {
        $variables['order']['shipping_phone'] = dirname(__FILE__) . '/automatewoo/email/variable-shipping-phone.php';
	    return $variables;
    }

    /* JP: Add a custom admin bar to help clear cache */
    add_action('wp_before_admin_bar_render', 'custom_admin_bar_menu');
    function custom_admin_bar_menu() {
        global $wp_admin_bar;
    
        // Add a parent menu item
        $wp_admin_bar->add_menu(array(
            'id' => 'custom-clear-cache',
            'title' => 'Dika the Butler',
            'href' => '#', // Dummy href
            'meta' => array('class' => 'custom-clear-cache')
        ));

        // Check if REQUEST_URI ends with a slash
        $base_url = rtrim($_SERVER['REQUEST_URI'], '/');
        $path = parse_url($base_url, PHP_URL_PATH);
        $query = parse_url($base_url, PHP_URL_QUERY);

        if (!empty($query)) {
            $path = $path . ($query ? '?' . $query : '');
            $path = trim($path, "/");
            $path = str_replace('?', '%3F', $path);
            $path = str_replace('=', '%3D', $path);
            $path = trim($path, "/"); 
        } else {
            $path = trim($path, "/");
            $path = $path . "/*";
        }
    
        // Add child menu items 
        $wp_admin_bar->add_menu(array(
            'id' => 'clear-desktop-cache', 
            'parent' => 'custom-clear-cache',
            'title' => 'Step 1: Mobile Cache',
            'href' => 'https://ambientlounge.co.jp/kinsta-clear-mobile-cache/' . $path,
            'meta' => array('target' => '_blank'),
        ));

        $wp_admin_bar->add_menu(array(
            'id' => 'clear-mobile-cache',
            'parent' => 'custom-clear-cache',
            'title' => 'Step 2: Desktop Cache',
            'href' => 'https://ambientlounge.co.jp/kinsta-clear-cache/' . $path,
            'meta' => array('target' => '_blank'),
        ));
    } 

    /* JP: Add shipping phone to woocommerce REST API */   

    add_filter('woocommerce_rest_prepare_shop_order_object', function($response, $order, $request) {

        $shippingPhone = get_post_meta($order->get_id(), '_shipping_phone', true);

        if(!empty($shippingPhone)) {
            $response->data['billing']['phone'] = get_post_meta($order->get_id(), '_shipping_phone', true);
        }

        return $response; 
    }, 10, 3);

     /* JP: Get all thumbnail from selected attribute*/   
    add_action('wp_ajax_get_thumbnail_for_variation', 'get_thumbnail_for_variation');
    add_action('wp_ajax_nopriv_get_thumbnail_for_variation', 'get_thumbnail_for_variation');
    function get_thumbnail_for_variation() {
         // Retrieve data from the Ajax request
        $product_id = $_POST['product_id'];
        $attributes = $_POST['selected_attributes'];

        // Get the product variation ID based on selected attributes
        $product = wc_get_product($product_id);
        $variations = $product->get_available_variations();

        // Loop through variations to find the one that matches the specified attributes
        $all_attributes_match = true;
        foreach ($variations as $variation) {
            $all_attributes_match = true;
            $variation_attributes = $variation['attributes'];
        
            // Check if all attributes match   
            foreach ($attributes as $attribute_name => $attribute_value) {
                if (!isset($variation_attributes[$attribute_name]) || $variation_attributes[$attribute_name] !== $attribute_value) {
                    $all_attributes_match = false;
                    break;
                }
            }
        
            // If all attributes match, get the variation ID and thumbnail
            if ($all_attributes_match) {
                $variation_id = $variation['variation_id'];
                $variation_product = wc_get_product($variation_id);
                $variation_thumbnail = $variation_product->get_image_id();

                // Output the variation ID and thumbnail
                $response = array(
                    'url' => wp_get_attachment_image_url($variation_thumbnail, 'large'),
                    'price' => $variation_product->get_price_html(),
                );
                wp_send_json($response);
        
                // You can use $variation_id and $variation_thumbnail as needed
                break; // Exit the loop once a matching variation is found
            }
        }

        // If no matching variation is found
        if (!$all_attributes_match) {
            $response = array(
                'url' => 'https://ambientlounge.co.jp/wp-content/uploads/2024/04/noimage.jpg',
                'price' => '<span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">¥</span>-</bdi></span> <small class="woocommerce-price-suffix">(税込/送料無料)</small>',
            );
            wp_send_json($response);
        }

        // Always exit after handling Ajax calls.
        wp_die();
    }

    /**
	 * Add default phone number if user hasn't inputted any during checkout
	 */
	function add_default_phone_number( $order_id ) {
		// Get the order object
		$order = wc_get_order( $order_id );
		
		// Check if phone number for BILLING is not provided
		$phone = $order->get_billing_phone();
		if ( empty( $phone ) ) {
			// Set default phone number
			$default_phone_number = '0120-429-332'; // Change this to your desired default phone number
			
			// Update order with default phone number
			$order->set_billing_phone( $default_phone_number );
			$order->save();
		}

		// Check if phone number for SHIPPING is not provided
		$phone = $order->get_shipping_phone();
		if ( empty( $phone ) ) {
			// Set default phone number
			$default_phone_number = '0120-429-332'; // Change this to your desired default phone number
			
			// Update order with default phone number
			$order->set_shipping_phone( $default_phone_number );
			$order->save();
		}
	}
	add_action( 'woocommerce_checkout_update_order_meta', 'add_default_phone_number' );

    /* JP: Modify billing and shipping phone */
    add_action('woocommerce_checkout_update_order_meta', 'update_phone_numbers_after_checkout', 10, 2);
    function update_phone_numbers_after_checkout($order_id, $posted_data) {
        $billing_phone = isset($posted_data['billing_phone']) ? sanitize_text_field($posted_data['billing_phone']) : '';
        $shipping_phone = isset($posted_data['shipping_phone']) ? sanitize_text_field($posted_data['shipping_phone']) : '';

        // Remove dots and dashes from billing phone
        $billing_phone = str_replace(array('.', '-'), '', $billing_phone);

        // Remove dots and dashes from shipping phone
        $shipping_phone = str_replace(array('.', '-'), '', $shipping_phone);

        if (!empty($billing_phone)) {
            update_post_meta($order_id, '_billing_phone', $billing_phone);
        }

        if (!empty($shipping_phone)) {
            update_post_meta($order_id, '_shipping_phone', $shipping_phone);
        }
    } 

    /* Add a verified order button */
        // Add a new column to the orders table
        add_filter('manage_edit-shop_order_columns', 'add_verified_order_column');
        function add_verified_order_column($columns) {
            // Add a new column at the end
            $columns['verified_order'] = __('Verified Order', 'textdomain');
            return $columns;
        }

        // Display the verified order status in the custom column
        add_action('manage_shop_order_posts_custom_column', 'display_verified_order_column', 10, 2);
        function display_verified_order_column($column, $post_id) {
            if ($column === 'verified_order') {
                $order = wc_get_order($post_id);
                $is_verified = $order->get_meta('verified_order');

                // Display a button to toggle the verified status
                $button_text = $is_verified ? __('Unverify', 'textdomain') : __('Verify', 'textdomain');
                $button_class = $is_verified ? 'button unverify-order' : 'button verify-order';
                
                echo '<button class="' . esc_attr($button_class) . '" data-order-id="' . esc_attr($post_id) . '">';
                echo esc_html($button_text);
                echo '</button>';
            }
        }
 
        // Handle the AJAX request to toggle the verified order status
        add_action('wp_ajax_toggle_verified_order_status', 'handle_toggle_verified_order_status');
        function handle_toggle_verified_order_status() {
            // Verify nonce for security
            check_ajax_referer('toggle_verified_order_nonce', 'nonce');

            // Get the order ID from the AJAX request
            $order_id = intval($_POST['order_id']);
            $order = wc_get_order($order_id);

            if (!$order) {
                wp_send_json_error(['error' => __('Order not found', 'textdomain')]);
            }

            // Toggle the verified status
            $is_verified = $order->get_meta('verified_order');
            $order->update_meta_data('verified_order', !$is_verified);
            $order->save();

            wp_send_json_success(['is_verified' => !$is_verified]);
        }

    /* Display real price without discount in WooCommerce */
    add_action( 'woocommerce_admin_order_item_headers', 'pd_admin_order_items_headers' );
    function pd_admin_order_items_headers($order){
        ?>
        <th class="line_customtitle sortable" data-sort="your-sort-option">
            Normal Price (+Tax)
        </th>
        <th class="line_customtitle sortable" data-sort="your-sort-option">
            Normal Price (-Tax)
        </th>
        <?php
    }  

    add_action( 'woocommerce_admin_order_item_values', 'pd_admin_order_item_values', 10000000, 3 );
    function pd_admin_order_item_values( $product, $item, $item_id ) {
        
        ?>
        <td class="line_customtitle">
            <?php 
                if(!empty($product)) {
                    echo $product->get_price_html();
                }
            ?>
        </td>
        <td class="line_customtitle">
            <?php 
                if(!empty($product)) {
                    
                    $tmp = strip_tags($product->get_price_html());
                    $tmp = explode(" ", $tmp); $tmp = $tmp[0];
                    $tmp = substr($tmp, 1); $tmp = str_replace(",", "", $tmp); $tmp = str_replace("yen;", "", $tmp);

                    $tmp = ((float)$tmp) / 1.1;

                    echo $tmp;
                }
            ?>
        </td>
        <style>
            .line_customtitle del {
                text-decoration: none;
            }
            .line_customtitle ins {
                display: none;
            }
            .line_customtitle .woocommerce-price-suffix {
                display: none;
            }
        </style>
        <?php
    } 

    /* JP: Add a custom my account link to wholesale pages */
    add_action( 'woocommerce_account_navigation', 'add_custom_link_to_my_account' );
    function add_custom_link_to_my_account() {
        // Get the current user's data
        $user = wp_get_current_user();
        
        // Check if the user has a role
        if ( empty( $user->roles ) ) {
            return;
        }

        // Get the first role of the user (assuming single-role users)
        $role = $user->roles[0];
        
        // Load global roles
        global $wp_roles;
        if ( isset( $wp_roles->roles[ $role ] ) ) {
            // Translate the role name
            $role_name = translate_user_role( $wp_roles->roles[ $role ]['name'] );
            
            // Check if the role name contains "B2B"
            if ( stripos( $role_name, 'B2B' ) !== false ) {

                if ( stripos( $role_name, 'noorder' ) === false ) {
                    // Split the role name by "-"
                    $parts = explode( '-', $role_name );

                    // Get the last part of the array
                    $last_part = end( $parts );

                    // Create a link to "/text/"
                    $link = esc_url( home_url( '/' . $last_part  . '/' ) );
                    $link_text = esc_html( $last_part );

                    // Output the link
                    echo '<div class="wholesale-link">';
                    echo '<a href="' . $link . '">購入画面</a></div>'; 
                }
            }
        }
    }

    /**
    * New pdf file name for summary document, appending the user name
    */
    add_filter( 'wpo_wcpdf_filename', function( $filename, $document_type, $order_ids, $context, $args ) {
        if ( isset( $_POST['action'] ) &&  'wpo_wcpdf_export_bulk' === $_POST['action'] ) {
            $request = stripslashes_deep( $_POST );
            $user_id = isset( $request['users_filter'] ) ? reset(  $request['users_filter'] ) : 0;
            
            if (  $user_id > 0 ) {
                $user     = get_user_by( 'ID', $user_id );
                $filename = $user->user_login . '-' . $filename;
            }
        }
        return $filename;
    }, 20, 5 );

    /**
     * Customize the summary document
     *
     * @param string $document_type The type of document being generated.
     * @param array  $order_ids     The array of order IDs associated with the document.
     */
    add_action( 'wpo_wcpdf_before_document', 'print_order_id', 10, 2 );
    function print_order_id( $document_type, $order_ids ) {

        if(is_array($order_ids)) {
            $order_ids = $order_ids[0];
        }

        $order = wc_get_order( $order_ids );

        ?>
            <table class="order-data-addresses" style="line-height:1.1; margin-bottom: 0px; width: 100%;">
                <tr>
                    <td style="position:relative;" colspan="2">
                        <?php
                            $titleInvoice = '請求書';
                        ?>
                        <h1 style="font-size:3em; text-align: center; padding:5px 0; margin:0;"><?php echo $titleInvoice; ?></h1>            
                    </td>
                </tr>
 
                <tr>
                    <td class="table-separator" style="padding:6px 0;" colspan="2">
                    </td>
                </tr>

                <tr>
                    <td class="address billing-address" style="line-height:1.2">        
                        <h3 style="font-size:1.2em"><?php _e( '請求先住所', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>    
                            <div>
                                <?php

                                    $billing_first_name = $order->get_billing_first_name();
                                    $billing_last_name = $order->get_billing_last_name();
                                    $billing_address_1 = $order->get_billing_address_1();
                                    $billing_address_2 = $order->get_billing_address_2();
                                    $billing_city = $order->get_billing_city();
                                    $billing_state = $order->get_billing_state();
                                    $billing_postcode = $order->get_billing_postcode();
                                    $billing_country = $order->get_billing_country();

                                    $jp_states_kanji = [
                                        'JP01' => '北海道',
                                        'JP02' => '青森県',
                                        'JP03' => '岩手県',
                                        'JP04' => '宮城県',
                                        'JP05' => '秋田県',
                                        'JP06' => '山形県',
                                        'JP07' => '福島県',
                                        'JP08' => '茨城県',
                                        'JP09' => '栃木県',
                                        'JP10' => '群馬県', // Gunma
                                        'JP11' => '埼玉県',
                                        'JP12' => '千葉県',
                                        'JP13' => '東京都',
                                        'JP14' => '神奈川県',
                                        'JP15' => '新潟県',
                                        'JP16' => '富山県',
                                        'JP17' => '石川県',
                                        'JP18' => '福井県',
                                        'JP19' => '山梨県',
                                        'JP20' => '長野県',
                                        'JP21' => '岐阜県',
                                        'JP22' => '静岡県',
                                        'JP23' => '愛知県',
                                        'JP24' => '三重県',
                                        'JP25' => '滋賀県',
                                        'JP26' => '京都府',
                                        'JP27' => '大阪府',
                                        'JP28' => '兵庫県',
                                        'JP29' => '奈良県',
                                        'JP30' => '和歌山県',
                                        'JP31' => '鳥取県',
                                        'JP32' => '島根県',
                                        'JP33' => '岡山県',
                                        'JP34' => '広島県',
                                        'JP35' => '山口県',
                                        'JP36' => '徳島県',
                                        'JP37' => '香川県',
                                        'JP38' => '愛媛県',
                                        'JP39' => '高知県',
                                        'JP40' => '福岡県',
                                        'JP41' => '佐賀県',
                                        'JP42' => '長崎県',
                                        'JP43' => '熊本県',
                                        'JP44' => '大分県',
                                        'JP45' => '宮崎県',
                                        'JP46' => '鹿児島県',
                                        'JP47' => '沖縄県',
                                    ];
                                    
                                    $billing_state_kanji = $jp_states_kanji[$billing_state] ?? $billing_state;

                                    // Combine billing address into a single string if needed
                                    $billing_address = trim(
                                        $billing_postcode . "<br>" .
                                        $billing_city . ', ' . $billing_state_kanji . ' ' . $billing_address_1 . ' ' . $billing_address_2 . "<br>" .
                                        $billing_last_name . ' ' . $billing_first_name
                                    );

                                    echo $billing_address;

                                ?>
                            </div>
                            <div class="billing-email"><?php $order->get_billing_email(); ?></div>
                            <div class="billing-phone"><?php $order->get_billing_phone(); ?></div>
                    </td>
                    
                    <td style="text-align:right;" class="ambient-logo">
                        <div>
                            <img style="display:inline; max-width:200px; padding:0 0 5px 0;" src="https://www.ambientlounge.co.jp/wp-content/uploads/2016/12/ambient-logo.png">
                            <?php
                                if ( $b2b_role )
                                {
                                    ?>                    
                                        <img style="display:inline; right:0; top:0; max-height:50px;" src="https://ambientlounge.co.jp/wp-content/uploads/2024/12/ambietlounge-seal.png">
                                    <?php
                                }
                            ?>
                        </div>
                        <p style="text-align:right; line-height:1.2;">
                            <?php _e( 'Ambient Lounge Japan株式会社', 'woocommerce-pdf-invoices-packing-slips' ); ?><br>
                            <?php _e( '群馬県利根郡みなかみ町川上32-20', 'woocommerce-pdf-invoices-packing-slips' ); ?><br>
                            <?php _e( 'TEL: 0120-429-332', 'woocommerce-pdf-invoices-packing-slips' ); ?>
                        </p>
                    </td>   
                </tr>

                <tr>
                    <td class="table-separator" style="padding:12px 0;" colspan="2">
                    </td>
                </tr>

                <tr>        
                    <td class="address">
                    </td> 

                    <td class="order-data clearfix" style="text-align:right;">
                        <table style="float:right;">
                            <tr class="registration-number">                    
                                <th style="text-align:right;"><?php _e( '登録番号', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                                <td style="text-align:right; border-bottom:1px solid #000;"><?php _e( 'T3011001104769', 'woocommerce-pdf-invoices-packing-slips' ); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="table-separator" style="padding:12px 0;" colspan="2">
                    </td>
                </tr>
            </table>
        <?php
    }

    add_action( 'wpo_wcpdf_after_document', 'print_order_note', 10, 2 );
    function print_order_note( $document_type, $order_ids ) {
        ?>
            <table style="width:100%;">   
                <tr>
                    <td class="bank1" style="border:1px solid #000; padding:6px; width:50%;">
                        金融機関：　三菱東京ＵＦＪ銀行　中目黒支店
                        お振込み手数料は貴社ご負担にてお願い致します。
                        口座番号：　普通口座　０２２１５６４
                        口座名義：　アンビエントラウンジジャパン(カ
                        お振込み手数料は貴社ご負担にてお願い致します。                        
                    </td>

                    <td class="bank2" style="border:1px solid #000; padding:6px; width:50%;">
                        銀行名: ゆうちょ銀行
                        支店名: ◯四八(ゼロヨンハチ)支店
                        科目: 普通預金
                        口座番号: 35684601
                        受取口座名義: アンビエントラウンジジヤパン(カ
                        お振込み手数料は貴社ご負担にてお願い致します。
                    </td>
                </tr>
            </table>
        <?php
    }

    // Display customer role in the WooCommerce single order screen
    add_action('woocommerce_admin_order_data_after_order_details', 'add_customer_role_to_order_details');
    function add_customer_role_to_order_details($order) {
        // Get the customer ID from the order
        $customer_id = $order->get_user_id();
        global $wp_roles;

        if ($customer_id) {
            $user = get_userdata($customer_id);
            if ($user && !empty($user->roles)) {
                
                $b2b_role = '';
                foreach ($user->roles as $role) {
                    
                    $role = translate_user_role( $wp_roles->roles[ $role ]['name'] );
            
                    if (stripos($role, 'B2B') !== FALSE) { // Role starts with "B2B"
                        $b2b_role = $role;
                        break;
                    }
                }

                echo '<p class="form-field form-field-wide wc-customer-user"><strong>' . __('Customer Role', 'textdomain') . ':</strong> ' . esc_html($b2b_role) . '</p>';
            }
        } else {
            echo '<p class="form-field form-field-wide wc-customer-user"><strong>' . __('Customer Role', 'textdomain') . ':</strong> ' . __('Guest', 'textdomain') . '</p>';
        }
    } 

    /**
     * Hide the invoice date and number on the Summary document - WPO PDF Invoices & Packing Slips
     */
    add_action( 'wpo_wcpdf_custom_styles', function( $document_type, $document ) {
        ?>
            table.summary-details .invoice-date, table.summary-details .invoice-number {
                display: none;
            }
        <?php
    }, 10 , 2 ); 

    /**
     * Add b2b class if logged in as b2b customers 
     * */ 
    function add_b2b_customer_body_class($classes) {
        global $b2b_cust;
        global $b2b_link;

        $b2b_cust = false;
    
        if (is_user_logged_in()) {
            $current_user = wp_get_current_user();
            global $wp_roles;
    
            foreach ($current_user->roles as $role) {
                $translated_role = translate_user_role($wp_roles->roles[$role]['name']);
    
                if (stripos($translated_role, 'B2B') !== false) {
                    $classes[] = 'b2b_customer';
                    $b2b_cust = true; // Set global variable to true

                    if ( stripos( $translated_role, 'noorder' ) === false ) {
                        // Split the role name by "-"
                        $parts = explode( '-', $translated_role );
    
                        // Get the last part of the array
                        $last_part = end( $parts );
                        $b2b_link = $last_part;
                        
                    } else {
                        $b2b_link = '';
                    }

                    break;
                }
            }
        }
    
        return $classes;
    }
    add_filter('body_class', 'add_b2b_customer_body_class');
    
    