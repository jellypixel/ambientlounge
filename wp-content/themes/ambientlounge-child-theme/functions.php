<?php
	/*	 	  
	 * WORDPRESS: Functions
	 * Extend Wordpress built in functions and GeneratePress's functions.
	 */
	 
	/* 
	 * Table of contents:
	 * 1. Constants
	 * 2. Register Style/Script	
	 * 3. Prevent Render Blocking 
	 * 4. GeneratePress - Default Settings 
	 * 5. Theme Setup	  
	 *    a. Dequeue Leaks
	 *	  b. Theme Support
	 *    c. Image Size
	 *    d. Sidebar
	 *	  e. Excerpt
	 * 6. Theme Customizer
	 * 7. Gutenberg - Register Block Style
	 * 8. WPCF7
	 * 9. ACF Options Page
	 * 10. ACF Blocks 
	 * 11. Custom Post Taxonomy
	 * 12. Template Compare - AJAX load child page
	 * 13. Woocommerce
	 * 14. JudgeMe
	 * 15. Paidy
	 * 16. Offer Redirect
	 * 17. Output color data to footer
	 */

	// Constants
		define( 'THEMEDIR', get_template_directory() . '/' );
		define( 'THEMEURI', get_stylesheet_directory_uri() . '/' );

	// Domain
		$GLOBALS['currDomain'] = 'https://ambientlounge.co.jp';
		
	// Register Style/Script
		// Frontend
			add_action( 'wp_enqueue_scripts', 'enqueue_list', 9000 );
		
			function enqueue_list() {
				// Style											
					wp_enqueue_style( 'flexslider-css',  THEMEURI . ( $css_path = 'src/vendor/flex/css/flexslider.css' ) );
					wp_enqueue_style( 'font-notosansjp', 'https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;700&display=swap' );					
					wp_enqueue_style( 'default-css',  THEMEURI . ( $css_path = '/dist/style.min.css' ), array( 'generate-style' ), get_version( $css_path ) );					
					
				// Script			
					wp_enqueue_script( 'jquery' );					
					wp_enqueue_script( 'flexslider-js', THEMEURI . 'src/vendor/flex/js/jquery.flexslider-min.js',  array( 'jquery' ), null, true );				
					
					// Default
					wp_enqueue_script( 'default-js', THEMEURI . ( $js_path = '/src/js/script.js' ), array( 'jquery' ), get_version( $js_path ), true );


					// Woocommerce
					if ( is_woocommerce() ) {					
						wp_enqueue_script( 'woocommercegeneral-js', THEMEURI . ( $js_sp_path = '/src/js/woocommerce-general.js' ), array( 'jquery' ), get_version( $js_sp_path ), true );	
					}

					// Special Product Configurator - Single Product Pet Lounge
					if ( is_product() ) {						
						wp_enqueue_style( 'choices-css', 'https://cdnjs.cloudflare.com/ajax/libs/choices.js/3.0.4/styles/css/choices.min.css', array(), get_version( $css_path ) );
						wp_enqueue_style( 'dogfinder-css', THEMEURI . ( $css_sc_path = '/src/vendor/sizechart/dogfinder.css' ), array(), get_version( $css_sc_path ) );

						wp_enqueue_script( 'choices-js', 'https://cdnjs.cloudflare.com/ajax/libs/choices.js/3.0.4/choices.min.js', array( 'jquery' ), get_version( $js_sp_path ), true);
						wp_enqueue_script( 'dogfinder-select-functionality', THEMEURI . ( $js_sc_path = '/src/vendor/sizechart/main_dev.js' ), array( 'jquery' ), get_version( $js_sc_path ), true);

						wp_enqueue_script( 'singleproduct-js', THEMEURI . ( $js_sp_path = '/src/js/single-product.js' ), array( 'jquery' ), get_version( $js_sp_path ), true );	
					}

					if(is_shop() || is_product_category()) {
						wp_enqueue_script( 'archiveproduct-js', THEMEURI . ( $js_ap_path = '/src/js/archive-product.js' ), array( 'jquery' ), get_version( $js_ap_path ), true );	
					}

					if(is_checkout()) {
						wp_enqueue_script( 'checkout-js', THEMEURI . ( $js_ch_path = '/src/js/checkout.js' ), array( 'jquery' ), get_version( $js_ch_path ), true );
					}

					if(is_cart()) {
						wp_enqueue_script( 'cart-js', THEMEURI . ( $js_ca_path = '/src/js/cart.js' ), array( 'jquery' ), get_version( $js_ca_path ), true );
					}

					if(is_page_template('template-offers.php')) {
						wp_enqueue_script( 'offers-js', THEMEURI . ( $js_of_path = '/src/js/offers.js' ), array( 'jquery' ), get_version( $js_of_path ), true );	
					}

					wp_add_inline_script('default-js', 'const HC =' . json_encode(array(
						'ajaxurl' => admin_url('admin-ajax.php'),
						'nonce' => wp_create_nonce('ajax-search-nonce'),
					)), 'before');
	
			}		
	 
		// Backend
			add_action('admin_enqueue_scripts', 'admin_enqueue_list', 22);
			
			function admin_enqueue_list() {		
				wp_enqueue_style( 'admin-css', THEMEURI . ( $cssadmin_path = '/dist/style-admin.min.css' ), array(), get_version( $cssadmin_path ) );
				
				wp_enqueue_script( 'admin-js', THEMEURI . ( $js_adm_path = '/src/js/admin.js' ), array( 'jquery' ), get_version( $js_adm_path ), true );
				wp_localize_script('admin-js', 'verifiedOrderAjax', [
					'ajax_url' => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('toggle_verified_order_nonce'),
				]);
			}

			add_action( 'enqueue_block_editor_assets', 'block_editor_scripts' );

			function block_editor_scripts() {
				wp_enqueue_script( 'block-editor', THEMEURI . 'src/js/editor.js', array( 'wp-blocks', 'wp-dom' ) );
			}			

		// Replaces cache busting versioning
			function get_version($relative_file_path) {
				return date("ymd-Gis", filemtime( get_stylesheet_directory() . $relative_file_path ));
			}
			
	// Prevent Render Blocking
		add_filter( 'style_loader_tag', 'add_google_font_stylesheet_attributes', 10, 2 );
		
		function add_google_font_stylesheet_attributes( $html, $handle ) {
			if ( 'font-notosansjp' === $handle ) 
			{
				return str_replace( "rel='stylesheet'", "rel='stylesheet' media='print' onload=\"this.media='all'\"", $html );
			}
			
			return $html;
		}

	// Include legacy functions
	include_once get_theme_file_path( 'functions-backend.php' );
		
	// GeneratePress - Default Settings
		if ( ! function_exists( 'generate_get_defaults' ) ) {
			/**
			 * Set default options
			 *
			 * @since 0.1
			 */
			 
			function generate_get_defaults() {
				
				return apply_filters(
					'generate_option_defaults',
					array(
						'hide_title' => '',
						'hide_tagline' => true,
						'logo' => '',
						'inline_logo_site_branding' => false,
						'retina_logo' => '',
						'logo_width' => '',
						'top_bar_width' => 'full',
						'top_bar_inner_width' => 'contained',
						'top_bar_alignment' => 'right',
						'container_width' => '944',
						'container_alignment' => 'text',
						'header_layout_setting' => 'fluid-header',
						'header_inner_width' => 'contained',
						'nav_alignment_setting' => is_rtl() ? 'right' : 'left',
						'header_alignment_setting' => is_rtl() ? 'right' : 'left',
						'nav_layout_setting' => 'fluid-nav',
						'nav_inner_width' => 'contained',
						'nav_position_setting' => 'nav-float-right',
						'nav_drop_point' => '',
						'nav_dropdown_type' => 'hover',
						'nav_dropdown_direction' => is_rtl() ? 'left' : 'right',
						'nav_search' => 'disable',
						'content_layout_setting' => 'separate-containers',
						'layout_setting' => 'right-sidebar',
						'blog_layout_setting' => 'right-sidebar',
						'single_layout_setting' => 'right-sidebar',
						'post_content' => 'excerpt',
						'footer_layout_setting' => 'fluid-footer',
						'footer_inner_width' => 'contained',
						'footer_widget_setting' => '3',
						'footer_bar_alignment' => 'right',
						'back_to_top' => '',
						'background_color' => 'var(--base-2)',
						'text_color' => 'var(--contrast)',
						'link_color' => 'var(--accent)',
						'link_color_hover' => 'var(--contrast)',
						'link_color_visited' => '',
						'font_awesome_essentials' => true,
						'icons' => 'svg',
						'combine_css' => true,
						'dynamic_css_cache' => true,
						'structure' => 'flexbox',
						'underline_links' => 'always',
						'font_manager' => array(),
						'typography' => array(),
						'google_font_display' => 'auto',
						'use_dynamic_typography' => true,
						'global_colors' => array(
							array(
								'name' => __( 'Black', 'generatepress' ),
								'slug' => 'black',
								'color' => '#000000',
							),	
							array(
								'name' => __( 'Dark', 'generatepress' ),
								'slug' => 'dark',
								'color' => '#181818',
							),
							array(
								'name' => __( 'Secondary Dark', 'generatepress' ),
								'slug' => 'secondarydark',
								'color' => '#888888',
							),
							array(
								'name' => __( 'Secondary', 'generatepress' ),
								'slug' => 'secondary',
								'color' => '#b2bbc4',
							),							
							array(
								'name' => __( 'Hairline', 'generatepress' ),
								'slug' => 'hairline',
								'color' => '#d2d2d7',
							),
							array(
								'name' => __( 'Light Gray', 'generatepress' ),
								'slug' => 'lightgray',
								'color' => '#f5f5f7',
							),
							array(
								'name' => __( 'White', 'generatepress' ),
								'slug' => 'white',
								'color' => '#ffffff',
							),
							array(
								'name' => __( 'Link', 'generatepress' ),
								'slug' => 'link',
								'color' => '#0066cc',
							),
							array(
								'name' => __( 'Pale Blue', 'generatepress' ),
								'slug' => 'paleblue',
								'color' => '#f7f9fb',
							),
							array(
								'name' => __( 'Green', 'generatepress' ),
								'slug' => 'green',
								'color' => '#7fb34d',
							),
							array(
								'name' => __( 'Red', 'generatepress' ),
								'slug' => 'red',
								'color' => '#f56300',
							)							
						)
					)
				);
			}
		}

	// Theme Setup
		add_action( 'after_setup_theme', 'initial_setup', 12 );
	
		function initial_setup()
		{	
			// Dequeue Leaks
				if ( !is_admin() ) {
					add_action( 'wp_enqueue_scripts', 'dequeue_leaks');
					
					function dequeue_leaks() {
						wp_deregister_style('common');
					}				
				}
		
			// Theme Support
				if ( function_exists( 'add_theme_support' ) ) {		
					add_theme_support( 'post-thumbnails', array( 'post' ) );
					add_theme_support( 'editor-styles' );
					add_theme_support( 'align-wide' );	
					
					// Non square custom logo
					add_theme_support( 'custom-logo', array(
						 'flex-height' => true,
						 'flex-width' => true
					) );						
			
					// Editor - Font Size
					add_theme_support(
						'editor-font-sizes',
						array(
							array(
								'name'      => __( 'H6', 'generatepress' ),
								'shortName' => __( 'H6', 'generatepress' ),
								'size'      => 12,
								'slug'      => 'H6',
							),
							array(
								'name'      => __( 'H5', 'generatepress' ),
								'shortName' => __( 'H5', 'generatepress' ),
								'size'      => 14,
								'slug'      => 'H5',
							),
							array(
								'name'      => __( 'Paragraph', 'generatepress' ),
								'shortName' => __( 'paragraph', 'generatepress' ),
								'size'      => 17,
								'slug'      => 'paragraph',
							),
							array(
								'name'      => __( 'H4 ', 'generatepress' ),
								'shortName' => __( 'Normal', 'generatepress' ),
								'size'      => 19,
								'slug'      => 'H4',
							),
							array(
								'name'      => __( 'H3 ', 'generatepress' ),
								'shortName' => __( 'H3', 'generatepress' ),
								'size'      => 24,
								'slug'      => 'H3',
							),
							array(
								'name'      => __( 'H2 ', 'generatepress' ),
								'shortName' => __( 'H2', 'generatepress' ),
								'size'      => 44,
								'slug'      => 'H2',
							),
							array(
								'name'      => __( 'H1 ', 'generatepress' ),
								'shortName' => __( 'H1', 'generatepress' ),
								'size'      => 68,
								'slug'      => 'H1',
							)
						)
					);
				}
		
			// Image Size
				if ( function_exists( 'add_image_size' ) ) {
					add_image_size( 'fullhd', 1920 );
					add_image_size( 'macbook', 1680 );
					add_image_size( 'laptop', 1366 );
					add_image_size( 'ipad', 1024 );
					add_image_size( 'tablet', 768 );
					add_image_size( 'mobile', 576 );	
				}
				
			// Sidebar
				register_sidebar( array(
					'name'          => 'Payment',
					'id'            => 'payment',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );

				register_sidebar( array(
					'name'          => 'Social',
					'id'            => 'social',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );

				register_sidebar( array(
					'name'          => 'Copyright',
					'id'            => 'copyright',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );

				register_sidebar( array(
					'name'          => 'Collection Filter',
					'id'            => 'collection_filter',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );
				
			// Excerpt
				add_filter( 'excerpt_length', 'custom_excerpt_length', 99 );

				function custom_excerpt_length( $length ) {
					if ( is_singular( 'post' ) )
					{
						return 100;
					}
					else if ( is_home() ) {
						return 25;
					}
					else if ( is_category() ) {
						return 100;
					}
					else {
						return 5;
					}
				}				
		}	

	// Theme Customizer		
		// Alternate Logo
		//add_action( 'customize_register', 'themecustomizer_alternate_logo' );
		
		function themecustomizer_alternate_logo($wp_customize)
		{ 
			$wp_customize->add_setting( 'alternate_logo', array(
				'default' => get_theme_file_uri('img/selectlogo.png'), // Default
				'sanitize_callback' => 'esc_url_raw'
			));
		 
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'alternate_logo_control', array(
				'label' => 'Alternate Logo',
				'priority' => 9,
				'section' => 'title_tagline',
				'settings' => 'alternate_logo',
				'button_labels' => array(
									  'select' => 'Select logo',
									  'remove' => 'Remove',
									  'change' => 'Change logo',
								   )
			)));		 
		}

		// Ambient Functions
		add_action( 'customize_register', 'themecustomizer_ambientfunctions' );
		
		function themecustomizer_ambientfunctions($wp_customize)
		{ 
			$wp_customize->add_section( 'ambient_section', array(
				'title' => __( 'Ambient Functions' ),
				'description' => __( '' ),
				'panel' => '', 
				'priority' => 22,
				'capability' => 'edit_theme_options',
				'theme_supports' => '',
			) );
			
			function sanitize_select( $input, $setting ) {
				$input = sanitize_key( $input );
				$choices = $setting->manager->get_control( $setting->id )->choices;	
				return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
			}
			
			// Discount Text
			$wp_customize->add_setting( 'discount_text', array(
				'capability' => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_select',
				'default' => 'no',
			) );
			
			$wp_customize->add_control( 'discount_text', array(
				'type' => 'select',
				'section' => 'ambient_section', 
				'label' => __( 'Enable discount text' ),
				'description' => __( '' ),
				'choices' => array(
					'yes' => __( 'Yes' ),
					'no' => __( 'No' ),
				),
			) );	 			
		}

	// Metabox		
		// PAGE Metabox
		add_action( 'add_meta_boxes', 'add_page_metabox' );
		add_action( 'save_post', 'page_metabox_save' );	
		
		function add_page_metabox() 
		{		
			add_meta_box(
				'page_metabox', 
				'Page Options',
				'page_metabox_html', 
				'page'
			);		
		}	
		
		function page_metabox_html( $post ) 
		{
			$show_page_title = get_post_meta( $post->ID, '_show_page_title_meta_key', true );
			$container_width = get_post_meta( $post->ID, '_container_width_meta_key', true );
			$background_color = get_post_meta( $post->ID, '_background_color_meta_key', true );

			if ( empty( $background_color ) ) {
				$background_color = 'white';	
			}

			if ( empty( $container_width ) ) {
				$container_width = 'normal';	
			}

			/*$transparent_menu = get_post_meta( $post->ID, '_transparent_menu_meta_key', true );
			
			if ( empty( $transparent_menu ) ) {
				$transparent_menu = 'no';	
			}*/
			?>
	            <div class="metabox-row">
                    <label for="show_page_title_field">Show Page Title</label>
                    <select name="show_page_title_field" id="show_page_title_field" class="postbox">
                        <option value="yes" <?php selected( $show_page_title, 'yes' ); ?>>Yes</option>
                        <option value="no" <?php selected( $show_page_title, 'no' ); ?>>No</option>
                    </select>
                </div>		

				<div class="metabox-row">
                    <label for="container_width_field">Container Width</label>
                    <select name="container_width_field" id="container_width_field" class="postbox">
                        <option value="widest" <?php selected( $container_width, 'widest' ); ?>>Widest 1440px</option>
						<option value="wider" <?php selected( $container_width, 'wider' ); ?>>Wider 1264px</option>
                        <option value="normal" <?php selected( $container_width, 'normal' ); ?>>Normal 944px</option>
						<option value="slim" <?php selected( $container_width, 'slim' ); ?>>Slim 656px</option>
                    </select>
                </div>	

				<div class="metabox-row">
                    <label for="background_color_field">Background Color</label>
                    <select name="background_color_field" id="background_color_field" class="postbox">
                        <option value="white" <?php selected( $background_color, 'white' ); ?>>White</option>
                        <option value="gray" <?php selected( $background_color, 'gray' ); ?>>Gray</option>
                    </select>
                </div>						
                
				<?php
				/*
                <div class="metabox-row">
                    <label for="transparent_menu_field">Transparent Header</label>
                    <select name="transparent_menu_field" id="transparent_menu_field" class="postbox">
                        <option value="yes" <?php selected( $transparent_menu, 'yes' ); ?>>Yes</option>
                        <option value="no" <?php selected( $transparent_menu, 'no' ); ?>>No</option>
                    </select>
                    <div class="metabox-description">
	                    Enabling this will transform header in this page into transparent header without background color and overlayed in front of content. Best paired with image/cover as the first block.<br>
                        Alternate logo will be used if you upload it in theme customizer (Appearance - Customize - Site identity).                        
                    </div>
                </div>
				*/
				?>
			<?php
		}		
		
		function page_metabox_save( $post_id ) 
		{
			if ( array_key_exists( 'show_page_title_field', $_POST ) ) {
				update_post_meta(
					$post_id,
					'_show_page_title_meta_key',
					$_POST['show_page_title_field']
				);
			}

			if ( array_key_exists( 'background_color_field', $_POST ) ) {
				update_post_meta(
					$post_id,
					'_background_color_meta_key',
					$_POST['background_color_field']
				);
			}

			if ( array_key_exists( 'container_width_field', $_POST ) ) {
				update_post_meta(
					$post_id,
					'_container_width_meta_key',
					$_POST['container_width_field']
				);
			}
			
			/*if ( array_key_exists( 'transparent_menu_field', $_POST ) ) {
				update_post_meta(
					$post_id,
					'_transparent_menu_meta_key',
					$_POST['transparent_menu_field']
				);
			}*/
		}

	// Gutenberg - Register Block Style
		// Block - Cover
		register_block_style(
			'core/cover',
			array(
				'name'         => 'cover-highlight-left',
				'label'        => 'Highlight Left',
				'style_handle' => 'highlight-left-style',
			)
		);

		register_block_style(
			'core/cover',
			array(
				'name'         => 'cover-highlight-center',
				'label'        => 'Highlight Center',
				'style_handle' => 'highlight-center-style',
			)
		);

		register_block_style(
			'core/cover',
			array(
				'name'         => 'cover-highlight-right',
				'label'        => 'Highlight Right',
				'style_handle' => 'highlight-right-style',
			)
		);

		// Block - Heading
		register_block_style(
			'core/heading',
			array(
				'name'         => 'gradient-purple-blue',
				'label'        => 'Gradient Purple/Blue',
				'style_handle' => 'gradient-purple-blue-style',
			)
		);

		register_block_style(
			'core/heading',
			array(
				'name'         => 'gradient-pink',
				'label'        => 'Gradient Pink',
				'style_handle' => 'gradient-pink',
			)
		);

		// Block - Columns
		register_block_style(
			'core/columns',
			array(
				'name'         => 'slim',
				'label'        => 'Slim',
				'style_handle' => 'slim-style',
			)
		);

		// Block - Column
		register_block_style(
			'core/column',
			array(
				'name'         => 'card',
				'label'        => 'Card',
				'style_handle' => 'card-style',
			)
		);

		register_block_style(
			'core/column',
			array(
				'name'         => 'note',
				'label'        => 'Note',
				'style_handle' => 'note-style',
			)
		);

		// Block - Row
		register_block_style(
			'core/row',
			array(
				'name'         => 'slim-row',
				'label'        => 'Slim',
				'style_handle' => 'slim-row-style',
			)
		);

		// Block - Stack
		register_block_style(
			'core/stack',
			array(
				'name'         => 'slim-stack',
				'label'        => 'Slim',
				'style_handle' => 'slim-stack-style',
			)
		);

		// Block - Group
		register_block_style(
			'core/group',
			array(
				'name'         => 'slim-group',
				'label'        => 'Slim',
				'style_handle' => 'slim-group-style',
			)
		);

		// Block - Image
		register_block_style(
			'core/image',
			array(
				'name'         => 'square-border',
				'label'        => 'Square Border',
				'style_handle' => 'square-border-style',
			)
		);

		// Block - Spacer
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer188',
				'label'        => 'Spacer 188px',
				'style_handle' => 'spacer188-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer112',
				'label'        => 'Spacer 112px',
				'style_handle' => 'spacer112-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer80',
				'label'        => 'Spacer 80px',
				'style_handle' => 'spacer80-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer64',
				'label'        => 'Spacer 64px',
				'style_handle' => 'spacer64-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer40',
				'label'        => 'Spacer 40px',
				'style_handle' => 'spacer40-style',
			)
		);
		register_block_style(
			'core/spacer',
			array(
				'name'         => 'spacer32',
				'label'        => 'Spacer 32px',
				'style_handle' => 'spacer32-style',
			)
		);

		// Block - Button
		register_block_style(
			'core/button',
			array(
				'name'         => 'blue',
				'label'        => 'Blue',
				'style_handle' => 'blue-style',
				'is_default'   => true
			)
		);
		
		register_block_style(
			'core/button',
			array(
				'name'         => 'black',
				'label'        => 'Black',
				'style_handle' => 'black-style',
			)
		);

		register_block_style(
			'core/button',
			array(
				'name'         => 'white',
				'label'        => 'White',
				'style_handle' => 'white-style',
			)
		);

		register_block_style(
			'core/button',
			array(
				'name'         => 'gray',
				'label'        => 'Gray',
				'style_handle' => 'gray-style',
			)
		);

		register_block_style(
			'core/button',
			array(
				'name'         => 'nonclickable',
				'label'        => 'Non Clickable',
				'style_handle' => 'nonclickable-style',
			)
		);

		// Block - Gallery
		register_block_style(
			'core/gallery',
			array(
				'name'         => 'default',
				'label'        => 'Default',
				'style_handle' => 'default-style',
				'is_default'   => true
			)
		);

		register_block_style(
			'core/gallery',
			array(
				'name'         => 'vertical',
				'label'        => 'Vertical',
				'style_handle' => 'vertical-style'
			)
		);

		// Block - Table
		register_block_style(
			'core/table',
			array(
				'name'         => 'simple',
				'label'        => 'Simple',
				'style_handle' => 'simple-style',
			)
		);

	// WPCF7
		// Validate Email
		add_filter( 'wpcf7_validate_email*', 'custom_email_confirmation_validation_filter', 20, 2 );

		function custom_email_confirmation_validation_filter( $result, $tag ) 
		{
			if ( 'email-address-confirmation' == $tag->name ) {
				$your_email = isset( $_POST['email-address'] ) ? trim( $_POST['email-address'] ) : '';
				$your_email_confirm = isset( $_POST['email-address-confirmation'] ) ? trim( $_POST['email-address-confirmation'] ) : '';
		
				if ( $your_email != $your_email_confirm ) {
					$result->invalidate( $tag, "Email address doesn't match." );
				}
			}

			return $result;
		}	

	// ACF Rules - Show on parent page template
		add_filter('acf/location/rule_types', 'acf_location_rules_types');
		function acf_location_rules_types( $choices ) {

			$choices['Parent']['parent_template'] = 'Parent Template';

			return $choices;

		}

		add_filter('acf/location/rule_values/parent_template', 'acf_location_rules_values_parent_template');
		function acf_location_rules_values_parent_template( $choices ) {

			$templates = get_page_templates();

			if ( $templates ) {
				foreach ( $templates as $template_name => $template_filename ) {

					$choices[ $template_filename ] = $template_name;

				}
			}

			return $choices;
		}

		add_filter('acf/location/rule_match/parent_template', 'acf_location_rules_match_parent_template', 10, 3);
		function acf_location_rules_match_parent_template( $match, $rule, $options ) {

			$selected_template = $rule['value'];

			global $post;
			$template = get_page_template_slug( $post->post_parent );

			if( $rule['operator'] == "==" ) {

				$match = ( $selected_template == $template );

			} elseif($rule['operator'] == "!=") {

				$match = ( $selected_template != $template );

			}

			return $match;
		}

	// ACF Options Page
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page();
		}

	// ACF BLOCKS
		// Ambient Lounge Category Block
			add_filter( 'block_categories', 'ambientlounge_block_categories' );

			function ambientlounge_block_categories( $categories ) {
				$category_slugs = wp_list_pluck( $categories, 'slug' );
				return in_array( 'ambientlounge', $category_slugs, true ) ? $categories : array_merge(
					$categories,
					array(
						array(
							'slug'  => 'ambientlounge',
							'title' => __( 'Ambient Lounge', 'ambientlounge' ),
							'icon'  => null,
						),
					)
				);
			}

		// Blocks
			add_action('acf/init', 'acf_init_block_types');

			function acf_init_block_types() 
			{	
				if( function_exists('acf_register_block_type') ) 
				{
					// Accordion Group
					acf_register_block_type(array(
						'name'              => 'accordion-group',
						'title'             => 'Accordion',
						'description'       => 'Create accordion.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'list-view',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template' => 'inc/blocks/accordion-group.php',
					));

					// Accordion Item
					acf_register_block_type(array(
						'name'              => 'accordion-item',
						'title'             => 'Accordion Item',
						'description'       => 'Create accordion item.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'align-center',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template' => 'inc/blocks/accordion-item.php',
					));

					// Hardcoded Price
					acf_register_block_type(array(
						'name'              => 'direct-sale',
						'title'             => 'Direct Sale',
						'description'       => 'Display direct sale.',
						'category'          => 'widgets',
						'mode'              => 'preview',
						'icon'				=> 'paperclip',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/direct-sale.php'
					));

					// Hardcoded Price
					acf_register_block_type(array(
						'name'              => 'hardcoded-price',
						'title'             => 'Hardcoded Price',
						'description'       => 'Display hardcoded price.',
						'category'          => 'widgets',
						'mode'              => 'preview',
						'icon'				=> 'tag',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/hardcoded-price.php'
					));

					// Hearts
					acf_register_block_type(array(
						'name'              => 'hearts',
						'title'             => 'Hearts',
						'description'       => 'Display rating with heart icons.',
						'category'          => 'widgets',
						'mode'              => 'preview',
						'icon'				=> 'heart',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/hearts.php'
					));

					// Highlight Product - SCRAP
					acf_register_block_type(array(
						'name'              => 'highlight-product',
						'title'             => 'Highlight Product',
						'description'       => 'Show and highlight a product.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'megaphone',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template' => 'inc/blocks/highlight-product.php',
					));

					// Product Card Group
					acf_register_block_type(array(
						'name'              => 'product-card-group',
						'title'             => 'Product Card Group',
						'description'       => 'Show single product.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'columns',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/product-card-group.php'
					));

					// Product Card
					acf_register_block_type(array(
						'name'              => 'product-card',
						'title'             => 'Product Card',
						'description'       => 'Show single product.',
						'category'          => 'widgets',
						'mode'              => 'preview',
						'icon'				=> 'star-filled',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/product-card.php'
					));

					// Product Card - Upsell
					acf_register_block_type(array(
						'name'              => 'product-card-upsell',
						'title'             => 'Product Card Upsell',
						'description'       => 'Show single product - upsell version.',
						'category'          => 'widgets',
						'mode'              => 'preview',
						'icon'				=> 'star-empty',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/product-card-upsell.php'
					));

					// Slider Card Group
					$GLOBALS['sliderCardBlockRun'] = 0;

					acf_register_block_type(array(
						'name'              => 'slider-card-group',
						'title'             => 'Slider Card Group',
						'description'       => 'Create slider/carousel.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'list-view',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template' => 'inc/blocks/slider-card-group.php',
					));

					// Slider Card Item
					acf_register_block_type(array(
						'name'              => 'slider-card-item',
						'title'             => 'Slider Card Item',
						'description'       => 'Create slide.',
						'category'          => 'formatting',
						'mode'              => 'preview',
						'icon'				=> 'align-center',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template' => 'inc/blocks/slider-card-item.php',
					));

					// Variation in Stock
					acf_register_block_type(array(
						'name'              => 'variation-in-stock',
						'title'             => 'Variation in Stock',
						'description'       => 'Display variation in stock.',
						'category'          => 'widgets',
						'mode'              => 'preview',
						'icon'				=> 'screenoptions',
						'supports'          => array(
							'align' => true,
							'mode' => false,
							'jsx' => true
						),
						'render_template'	=> 'inc/blocks/variation-in-stock.php'
					));
				}
			}
	
	// Custom Post Taxonomy
		// CPT - Retail		
			add_action('init', 'register_cpt_retail');
			
			function register_cpt_retail() {
				register_post_type(
					'retail',
					array(
						'labels' => array(
							'name' 			=> _x( 'Retails', 'pepperit' ),
							'singular_name' => _x( 'Retail', 'pepperit' ),
							'menu_name' 	=> __( 'Retails', 'pepperit' ),					
							'add_new'		=> __( 'Add New Retail', 'pepperit' ),
							'add_new_item'	=> __( 'Add New Retail', 'pepperit' ),
							'edit_item'		=> __( 'Edit Retail', 'pepperit' ),					
							'search_items'	=> __( 'Search Retails', 'pepperit' ),
							'not_found'		=> __( 'No Retail found', 'pepperit' ),
							'not_found_in_trash' => __( 'No Retail found in Trash', 'pepperit' ),					
						),
						'public'			=> true,
						'has_archive' 		=> false,
						'rewrite' 			=> array( 
							'slug' 			=> 'retail',
							'with_front' 	=> false
						),
						'show_in_rest'		=> true,
						'supports' 			=> array( 
												'title', 
												'editor', 										
												'thumbnail',
												'revisions'
											),
						'menu_position' 	=> 5,
						'menu_icon' 		=> 'dashicons-location',
						'can_export' 		=> true,
					)
				);
				
				// Add day archive (and pagination)
				add_rewrite_rule("retail/([0-9]{4})/([0-9]{2})/([0-9]{2})/page/?([0-9]{1,})/?",'index.php?post_type=retail&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]','top');
				add_rewrite_rule("retail/([0-9]{4})/([0-9]{2})/([0-9]{2})/?",'index.php?post_type=retail&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]','top');
				
				// Add month archive (and pagination)
				add_rewrite_rule("retail/([0-9]{4})/([0-9]{2})/page/?([0-9]{1,})/?",'index.php?post_type=retail&year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]','top');
				add_rewrite_rule("retail/([0-9]{4})/([0-9]{2})/?",'index.php?post_type=retail&year=$matches[1]&monthnum=$matches[2]','top');
				
				// Add year archive (and pagination)
				add_rewrite_rule("retail/([0-9]{4})/page/?([0-9]{1,})/?",'index.php?post_type=retail&year=$matches[1]&paged=$matches[2]','top');
				add_rewrite_rule("retail/([0-9]{4})/?",'index.php?post_type=retail&year=$matches[1]','top');
				
			}	
			
			// CPT Retail - Columns List
			add_filter( 'manage_retail_posts_columns', 'cpt_retail_columns' );
			
			function cpt_retail_columns( $retailColumns )
			{
				$retailColumns = array(
					'cb' => '<input type="checkbox">',
					//'article_featured_image' => __( 'Featured Image', 'pepperit' ),
					'title' => __( 'Title', 'pepperit' ),			
					'author' => __( 'Author', 'pepperit' ),
					'comments' => '<span class="vers"><div title="Comments" class="comment-grey-bubble"></div></span>',
					'date' => __( 'Date', 'pepperit' ) 
				);
				
				return $retailColumns;
			}	
			
			// CPT Retail - Custom Columns
			add_action( 'manage_posts_custom_column', 'cpt_retail_custom_columns' );
			
			function cpt_retail_custom_columns( $retailColumns )
			{
				global $post;
			
				switch ( $retailColumns )
				{
					case 'retail_featured_image':			
						if ( has_post_thumbnail() )
						{
							the_post_thumbnail( 'medium' );
						}				
						break;
				}		
			} 
			
			// CPT Retail - Category
			add_action( 'init', 'cpt_retail_category', 0 );
		
			function cpt_retail_category() {	 
				$labels = array(
					'name' => _x( 'Retail Categories', 'pepperit' ),
					'singular_name' => _x( 'Category', 'pepperit' ),
					'search_items' =>  __( 'Search Categories' ),
					'all_items' => __( 'All Categories' ),
					'parent_item' => __( 'Parent Category' ),
					'parent_item_colon' => __( 'Parent Category:' ),
					'edit_item' => __( 'Edit Category' ), 
					'update_item' => __( 'Update Category' ),
					'add_new_item' => __( 'Add New Category' ),
					'new_item_name' => __( 'New Category Name' ),
					'menu_name' => __( 'Categories' ),
				); 	
				
				register_taxonomy(
					'retail_category',
					array('retail'), 
					array(
						'hierarchical' => true,
						'labels' => $labels,
						'show_ui' => true,
						'show_admin_column' => true,
						'show_in_rest'      => true,
						'query_var' => true,
						'rewrite' => array( 'slug' => 'retail-category' ),
					)
				);
			}
			
			// CPT Retail - Location
			add_action( 'init', 'cpt_retail_location', 0 );
		
			function cpt_retail_location() {	 
				$labels = array(
					'name' => _x( 'Retail Locations', 'pepperit' ),
					'singular_name' => _x( 'Location', 'pepperit' ),
					'search_items' =>  __( 'Search Locations' ),
					'all_items' => __( 'All Locations' ),
					'parent_item' => __( 'Parent Location' ),
					'parent_item_colon' => __( 'Parent Location:' ),
					'edit_item' => __( 'Edit Location' ), 
					'update_item' => __( 'Update Location' ),
					'add_new_item' => __( 'Add New Location' ),
					'new_item_name' => __( 'New Location Name' ),
					'menu_name' => __( 'Locations' ),
				); 	
				
				register_taxonomy(
					'retail_location',
					array('retail'), 
					array(
						'hierarchical' => true,
						'labels' => $labels,
						'show_ui' => true,
						'show_admin_column' => true,
						'show_in_rest'      => true,
						'query_var' => true,
						'rewrite' => array( 'slug' => 'retail-location' ),
					)
				);
			}

	// Template Compare - AJAX load child page
		add_action('wp_ajax_get_child_page', 'load_child_page');           // for logged in user
		add_action('wp_ajax_nopriv_get_child_page', 'load_child_page');    // if user not logged in

		function load_child_page()
		{	
			$page_id = $_POST[ 'page_id' ];

			if ( !empty( $page_id ) )
			{
				$args = array(
					'page_id' => $page_id
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						$query->the_post();
						
						the_content();
					}
				} 
				wp_reset_postdata();	
			}		

			exit;
		}

	// Woocommerce
		// Enable Gutenberg in single product
			add_filter('use_block_editor_for_post_type', 'wplook_activate_gutenberg_products', 10, 2);

			function wplook_activate_gutenberg_products($can_edit, $post_type){
				if($post_type == 'product'){
					$can_edit = true;
				}
				
				return $can_edit;
			}			

		// Custom Cart is empty message
			remove_action( 'woocommerce_cart_is_empty', 'wc_empty_cart_message', 10 );
			add_action( 'woocommerce_cart_is_empty', 'custom_empty_cart_message', 10 );

			function custom_empty_cart_message() {
				$html  = '<div class="cart-empty">';
				$html .= wp_kses_post( apply_filters( 'wc_empty_cart_message', __( '現在、カートには何もはいっていません', 'ambientlounge' ) ) );
				echo $html . '</div>';
			}

		// Change Labels in dashboard
			add_filter ( 'woocommerce_account_menu_items', 'dashboard_custom_label' );

			function dashboard_custom_label() {
				$myorder = array(									
					'dashboard'          => __( 'ダッシュボード', 'woocommerce' ),
					'orders'             => __( '注文履歴', 'woocommerce' ),
					'edit-address'       => __( '住所', 'woocommerce' ),
					'payment-methods'    => __( '決済方法', 'woocommerce' ),
					'edit-account'       => __( 'アカウント詳細', 'woocommerce' ),
					'customer-logout'    => __( 'ログアウト', 'woocommerce' ),
				);

				return $myorder;
			}

		// Change Address labels
			add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );

			function wc_billing_field_strings( $translated_text, $text, $domain ) {
				switch ( $translated_text ) 
				{
					case 'Billing address' :
						$translated_text = __( '請求先住所', 'ambientlounge' );
						break;

					case 'Shipping address' :
						$translated_text = __( 'お届け先住所', 'ambientlounge' );
						break;
				}
				return $translated_text;
			}

		// Move Amazon Pay + Google Pay into single div
			/*add_action( 'woocommerce_checkout_init', 'move_amazon_pay', 11 );

			function move_amazon_pay() {
				remove_action( 'woocommerce_before_checkout_form', array( wc_apa(), 'checkout_message' ), 5 );
				add_action( 'woocommerce_checkout_before_customer_details', array( wc_apa(), 'checkout_message' ), 1 );
			}*/

		// Global - Remove variation name
		/*
			add_filter( 'woocommerce_cart_item_name', 'change_cart_variation_name_format', 10, 3 );
			function change_cart_variation_name_format( $item_name, $cart_item, $cart_item_key ) {
				$product = $cart_item['data'];
				$variation_name = '';
				if ( $product->is_type( 'variation' ) ) {
					$attributes = $product->get_variation_attributes();
					$variation_attributes = array();

					foreach ( $attributes as $key => $value ) 
					{
						$taxonomy = str_replace( 'attribute_', '', $key );
						$term = get_term_by( 'slug', $value, $taxonomy );
						$variation_attributes[] = $term->name;
					}

					if ( !empty( $variation_attributes ) )
					{
						$variation_name = rtrim(implode( ' / ', $variation_attributes ), ' / ');
					}
				}
				return $product->get_name() . $variation_name; 
			}
		*/

		// Shop - Product per page - 1000
		add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

		function new_loop_shop_per_page( $cols ) {
			// $cols contains the current number of products per page based on the value stored on Options –> Reading
			// Return the number of products you wanna show per page.
			$cols = 1000;
			return $cols;
		}

		// Shop - Remove OrderBy Rating/Date
		add_filter( "woocommerce_catalog_orderby", "my_woocommerce_catalog_orderby", 20 );
		
		function my_woocommerce_catalog_orderby( $orderby ) {
			unset($orderby["rating"]);
			unset($orderby["date"]);
			return $orderby;
		}

		// Single - Product gallery add navigation arrow
			add_filter( 'woocommerce_single_product_carousel_options', 'single_product_gallery_flexslider_option' );

			function single_product_gallery_flexslider_option( $options ) {
				$options['directionNav'] = true;
				$options['prevText'] = '';
				$options['nextText'] = '';
				return $options;
			}

		// Single - Variation Swatchers Mod
			$colorFlag = 0;

			add_filter('tawcvs_swatch_html','jp_variation_swatchers_mod',30,4);

			function jp_variation_swatchers_mod($html, $term, $attrType, $args)
			{
				global $product, $colorFlag;

				if(empty($product))
					return $html;

				// Attributes
				$name         = $term->name;
				$slug         = $term->slug;
				$description  = $term->description;
				$label        = get_term_meta( $term->term_id, 'label', true );
				$subtext      = get_term_meta( $term->term_id, 'subtext', true );
				$promotion        = get_term_meta( $term->term_id, 'promotion', true );
				$selected     = sanitize_title( $args['selected'] ) == $term->slug ? 'selected' : '';

				$attach_id = get_term_meta( $term->term_id, 'image', true );
				if ( ! empty( $attach_id ) ) {
					$image_url = wp_get_attachment_image_url( $attach_id, 'large' );
					$image_url = apply_filters( 'tawcvs_product_swatch_image_url', $image_url, $args );
				} 
				
				// Custom looks
				if($term->taxonomy === 'pa_color') {
					$waterresistantClass = 'nonwaterresistant-color';
					$html = "";

					if ( preg_match_all("/waterresistant/i", esc_attr( $term->slug ), $matches) ) 
					{
						$waterresistantClass = 'waterresistant-color';

						if($colorFlag == 0) {
							$html .= 
								'<div class="interiorplus-header swatch-item-wrapper waterresistant-color">
									<div class="interiorplus-icon">
										<img src="' . get_theme_file_uri('img/woocommerce-general/producticons.svg') . '">
									</div>
									<div class="interiorplus-description">
										<div class="interiorplus-title">
											' . __( 'インテリア＋プラス', 'ambientlounge' ) . '																				
										</div>
										<div class="interiorplus-subtitle">
											' . __( '撥水性/褪色防止/引っ掻きに強い/ペットフレンドリー', 'ambientlounge' ) . '										
										</div>
									</div>
								</div>';

							$colorFlag++;
						}
					}
 
					$swatchLabel = "";
					if(!empty($promotion)) {
						$swatchLabel  = "<div class='swatch-promotionlabel'>" . htmlspecialchars($promotion) . "</div>";
					}

					$html  .= sprintf('				
						<div class="swatch-item-wrapper ' . $waterresistantClass . '">	
							<div class="swatch swatch-%s %s" title="%s" data-value="%s"> ' . $swatchLabel . '							
								<div class="swatch-image" style="background-image:url( ' . $image_url . ' );"></div>
							</div>
							<div class="swatch-item-label">
								' . $name . '
							</div>
						</div>'
					,
						esc_attr( $term->slug ),
						$selected,
						esc_attr( $name ),
						esc_attr( $term->slug )
					);
				}

				if($term->taxonomy === 'pa_product-type'){
				
					if(empty($subtext))
						$subtext = '';
					else {
						$subtext = '<div class="swatch-sub">'.str_replace("%", "%%", $subtext).'</div>';
					}

					$html  = sprintf('
					
						<div class="swatch swatch-%s %s" title="%s" data-value="%s" swatch-producttype>
							'.$subtext.'
							<div class="swatch-label">
							'.$label.'
							</div>
							<div class="swatch-image">
								<img src="'.$image_url.'">
							</div>
						</div>
						'
					,
						esc_attr( $term->slug ),
						$selected,
						esc_attr( $name ),
						esc_attr( $term->slug )
					);

					if(!empty($promotion)) {
						$html .= '
							<div class="swatch-promotion">
								<div class="inner-promotion">
								'.$promotion.'
								</div>
							</div>
						';
					}
				}

				if($term->taxonomy === 'pa_bed-selection'){
					if(empty($subtext))
						$subtext = '';
					else {
						$subtext = '<div class="swatch-subtext">'. $subtext . '</div>';
					}

					$html  = sprintf('
					
						<div class="swatch swatch-%s %s" title="%s" data-value="%s">
							<div class="swatch-image">
								<img src="'.$image_url.'">
							</div>
							<div class="swatch-label">
								' . $label . $subtext . '
							</div>
						</div>'
					,
						esc_attr( $term->slug ),
						$selected,
						esc_attr( $name ),
						esc_attr( $term->slug )
					);
				}

				if($term->taxonomy === 'pa_cover-selection'){
					if(empty($subtext))
						$subtext = '';
					else {
						$subtext = '<div class="swatch-subtext">'. $subtext . '</div>';
					}
 
					if(!empty($promotion)) {
						$swatchLabel  = "<div class='swatch-promotionlabel'>" . htmlspecialchars($promotion) . "</div>";
					}

					$html  = sprintf('
					
						<div class="swatch swatch-%s %s" title="%s" data-value="%s">
							<div class="swatch-image">
								<img src="'.$image_url.'">
							</div> 
							<div class="swatch-text">
								'.$swatchLabel.'
								<div class="swatch-label">
									' . $label . $subtext . '
								</div>
								<div class="swatch-description">
								'.$description.'
								</div>
							</div>
						</div>'
					,
						esc_attr( $term->slug ),
						$selected,
						esc_attr( $name ),
						esc_attr( $term->slug )
					);
				}

				if($term->taxonomy === 'pa_deluxe_info'){
					$descriptions = explode('|',$term->description);

					if(empty($subtext))
						$subtext = '';
					else {
						$subtext = '<div class="swatch-subtext">'. $subtext . '</div>';
					}

					$html  = sprintf('
					
						<div class="swatch swatch-%s %s" title="%s" data-value="%s">
							<div class="swatch-image">
								<img src="'.$image_url.'">
							</div>
							<div class="swatch-text">
								<div class="swatch-label">
									'.$descriptions[0] .  $subtext . '
								</div>
								<div class="swatch-description">
								'.$descriptions[1].'
								</div>
							</div>
						</div>'
					,
						esc_attr( $term->slug ),
						$selected,
						esc_attr( $name ),
						esc_attr( $term->slug )
					);
				}

				if($term->taxonomy === 'pa_filling'){
					$descriptions = explode('|',$term->description);

					$html  = sprintf('
					
						<div class="swatch swatch-%s %s" title="%s" data-value="%s">
							<div class="swatch-image">
								<img src="'.$image_url.'">
							</div>
							<div class="swatch-text">
								<div class="swatch-label">
								'.$name.'
								</div>
								<div class="swatch-description">
								'.$descriptions[0].'
								</div>
							</div>
						</div>'
					,
						esc_attr( $term->slug ),
						$selected,
						esc_attr( $name ),
						esc_attr( $term->slug )
					);
				}

				return $html;
			}

		// Single - Remove post content from first of tabs since we put post content above the tabs
			add_filter( 'woocommerce_product_tabs', 'remove_product_tabs', 98, 1 );

			function remove_product_tabs( $tabs ) {
				unset( $tabs['description'] );
				return $tabs;
			}

		// Price - Show normal price too when it's sale
			/* JP: Sale Price in Category */

			if (!function_exists('my_commonPriceHtml')) {

				function my_commonPriceHtml($price_amt, $regular_price, $sale_price) {
					$html_price = '<p class="price">';
					//if product is in sale
					if (($price_amt == $sale_price) && ($sale_price != 0)) {
						$html_price .= '<del>' . wc_price($regular_price) . '</del>';
						$html_price .= '<ins>' . wc_price($sale_price) . '</ins>';
					}
					//not is sale
					else if (($price_amt == $regular_price) && ($regular_price != 0)) {
						$html_price .= '<ins>' . wc_price($regular_price) . '</ins>';
					}
					$html_price .= '</p>';
					return $html_price;
				}

			}

			add_filter('woocommerce_get_price_html', 'my_simple_product_price_html', 100, 2);

			function my_simple_product_price_html($price, $product) {
				if ($product->is_type('simple')) {
					$regular_price = $product->get_regular_price();
					$sale_price = $product->get_sale_price();
					$price_amt = $product->get_price();
					return my_commonPriceHtml($price_amt, $regular_price, $sale_price);
				} else {
					return $price;
				}
			}

			add_filter('woocommerce_variation_sale_price_html', 'my_variable_product_price_html', 10, 2);
			add_filter('woocommerce_variation_price_html', 'my_variable_product_price_html', 10, 2);

			function my_variable_product_price_html($price, $variation) {
				$variation_id = $variation->variation_id;
				//creating the product object
				$variable_product = new WC_Product($variation_id);

				$regular_price = $variable_product->get_regular_price();
				$sale_price = $variable_product->get_sale_price();
				$price_amt = $variable_product->get_price();

				return my_commonPriceHtml($price_amt, $regular_price, $sale_price);
			}

			add_filter('woocommerce_variable_sale_price_html', 'my_variable_product_minmax_price_html', 10, 2);
			add_filter('woocommerce_variable_price_html', 'my_variable_product_minmax_price_html', 10, 2);

			function my_variable_product_minmax_price_html($price, $product) {
				$variation_min_price = $product->get_variation_price('min', true);
				$variation_max_price = $product->get_variation_price('max', true);
				$variation_min_regular_price = $product->get_variation_regular_price('min', true);
				$variation_max_regular_price = $product->get_variation_regular_price('max', true);

				if (($variation_min_price == $variation_min_regular_price) && ($variation_max_price == $variation_max_regular_price)) {

					// $html_min_max_price = $price; this will output range. We are using kara now.
					
					if($variation_min_price != $variation_max_price)
						$html_min_max_price = wc_price($variation_min_price) . '<span class="kara">から</span>';
					else
						$html_min_max_price = wc_price($variation_min_price);
						
				} else {

					/* Old one where we are using range.
					if($variation_min_price != $variation_max_price) {
						$html_price .= '<del>' . wc_price($variation_min_regular_price) . ' - ' . wc_price($variation_max_regular_price) . '</del>';
						$html_price .= '<ins>' . wc_price($variation_min_price) . ' - ' . wc_price($variation_max_price) . '</ins>';
						$html_min_max_price = $html_price;
					} else {
						$html_price .= '<del>' . wc_price($variation_min_regular_price) . '</del>';
						$html_price .= '<ins>' . wc_price($variation_min_price) . '</ins>';
						$html_min_max_price = $html_price;
					}
					*/

					// We are using から now

					if($variation_min_price != $variation_max_price) {
						$html_price .= '<del>' . wc_price($variation_min_regular_price) . '<span class="kara">から</span></del>';
						$html_price .= '<ins>' . wc_price($variation_min_price) . '<span class="kara">から</span></ins>';
						$html_min_max_price = $html_price;
					} else {
						$html_price .= '<del>' . wc_price($variation_min_regular_price) . '</del>';
						$html_price .= '<ins>' . wc_price($variation_min_price) . '</ins>';
						$html_min_max_price = $html_price;
					}
					
				}

				return $html_min_max_price;
			}

		// View Thank You Page
			/**
			* @snippet       View Thank You Page @ Edit Order Admin
			* @how-to        Get CustomizeWoo.com FREE
			* @author        Rodolfo Melogli
			* @compatible    WooCommerce 6
			* @donate $9     https://businessbloomer.com/bloomer-armada/
			*/
			
			add_filter( 'woocommerce_order_actions', 'bbloomer_show_thank_you_page_order_admin_actions', 9999, 2 );
			
			function bbloomer_show_thank_you_page_order_admin_actions( $actions, $order ) {
				if ( $order->has_status( wc_get_is_paid_statuses() ) ) {
					$actions['view_thankyou'] = 'Display thank you page';
				}
				return $actions;
			}
			
			add_action( 'woocommerce_order_action_view_thankyou', 'bbloomer_redirect_thank_you_page_order_admin_actions' );
			
			function bbloomer_redirect_thank_you_page_order_admin_actions( $order ) {
				$url = $order->get_checkout_order_received_url();
				add_filter( 'redirect_post_location', function() use ( $url ) {
					return $url;
				});
			}

		// Thank you page - update billing phone with modal
			add_action('wp_ajax_update_order_billing_phone', 'update_order_billing_phone');
			add_action('wp_ajax_nopriv_update_order_billing_phone', 'update_order_billing_phone'); // Handle requests from non-logged-in users if needed
			function update_order_billing_phone() {
				// Get the order ID and billing phone from the request
				$order_id = isset($_POST['order_id']) ? intval($_POST['order_id']) : 0;
				$billing_phone = isset($_POST['billing_phone']) ? sanitize_text_field($_POST['billing_phone']) : '';

				// Load the order
				$order = wc_get_order($order_id);

				if ($order && $billing_phone) {
					// Update the billing phone
					$order->set_billing_phone($billing_phone);
					$order->set_shipping_phone($billing_phone);
					$order->save(); // Save the changes

					wp_send_json_success('Order billing phone updated.');
				} else {
					wp_send_json_error('Invalid order ID or phone number.');
				}

				wp_die(); // All AJAX handlers should call wp_die() to end the request properly
			}
			
	// JudgeMe
		add_action( 'woocommerce_single_product_summary', 'customJudgemePosition', 7 );
		
		function customJudgemePosition(){
			echo do_shortcode('[jgm-preview-badge]');
		}

	// Paidy
		// Add Paidy to all Single Product
		add_filter( 'woocommerce_before_add_to_cart_button','jp_add_paidyhtml' );
		function jp_add_paidyhtml( $classes ) {

			global $product;

			if(empty($product))
				return;
				
			echo '<div class="_paidy-promotional-messaging" data-amount="'. $product->get_price() .'" data-alignment="center"></div>';
		}


		// Add Paidy script to Footer
		add_action('wp_footer', 'jp_add_paidyscript'); 
		function jp_add_paidyscript(){
			global $product;

			if(empty($product))
				return;

			echo '<script src="https://cdn.paidy.com/promotional-messaging/general/paidy-opt-v2.js" defer="defer"></script>';
		}

	// Offer Redirect
		function redirect_to_offer_page($url) {
			$product_id = isset($_REQUEST['product_id']) ? intval($_REQUEST['product_id']) : 0;
			$variation_id = isset($_REQUEST['variation_id']) ? intval($_REQUEST['variation_id']) : 0;

			// Check if the upsell_redirect post meta is set to true
			$upsell_redirect = get_post_meta($product_id, 'upsell_redirect', true);
		
			// Only redirect if upsell_redirect is true
			if ($upsell_redirect === '1') {

				if ($variation_id)
					$offer_url = home_url('/offers/?item=' . $variation_id);
				else
					$offer_url = home_url('/offers/?item=' . $product_id);

				return $offer_url;
			}
		
			// If upsell_redirect is false or not set, return the original URL
			return $url;
		}
		
		add_filter('woocommerce_add_to_cart_redirect', 'redirect_to_offer_page', 10, 1);

	// Output color data to footer
		add_action( 'wp_footer', 'output_image_urls' );
		function output_image_urls() {
			if ( is_product_category() ) { // Only run on product category archive pages
				$attributes = wc_get_attribute_taxonomies();
				$image_urls = array();
				foreach ( $attributes as $attribute ) {
					if ( $attribute->attribute_name == 'color' ) { // 'pa_color' is the attribute slug
						$terms = get_terms( array(
							'taxonomy' => 'pa_color',
							'hide_empty' => false,
						) );
						foreach ( $terms as $term ) {
							$attach_id = get_term_meta( $term->term_id, 'image', true );
							if ( ! empty( $attach_id ) ) {
								$image_url = wp_get_attachment_image_url( $attach_id, 'large' );
								$image_url = apply_filters( 'tawcvs_product_swatch_image_url', $image_url, $args );
								$image_urls[$term->slug] = $image_url;
							} 
						}
					}
				}

				if ( ! empty( $image_urls ) ) {
					printf( '<script>window.productAttributeImageUrls = %s;</script>', wp_json_encode( $image_urls ) );
				}
			}
		}

	// Custom shortcode: Manual collection
	add_shortcode('attribute', 'shortcode_attribute'); 

	function shortcode_attribute( $atts, $content = null  ) 
	{ 
		$attribute = shortcode_atts( array(
			'image' => '',
			'name' => '',
			'sub' => '',
			'link' => '',
			'button_text' => '',
			'promotion_text' => '',

			'attr' => 'attribute_pa_size',

			'selection' => '',
			'price' => '',
			'sale' => '',

			'product_id' => 0,
			'attr_selected' => 'attribute_pa_bed-selection=grey-delux',
			'show_selection' => 'yes',
		), $atts );

		// Modify price if product is selected
		if( !empty( $attribute[ 'product_id' ] ) ) {
			$product = wc_get_product( $attribute[ 'product_id' ] );

			// Check if this assumes a variation selected
			if ( $product->is_type( 'variable' ) ) {

				$attribute[ 'selection' ] = array(); $attribute[ 'price' ] = array(); $attribute[ 'sale' ] = array();
				$attr_selected = explode(",", $attribute[ 'attr_selected' ]);

				foreach($product->get_available_variations() as $variation ){

					// Here we check whether it is the same variation
					$check = 0; 
					if(trim($attr_selected[0]) == "none")
						$check = 1;

					foreach($attr_selected as $attrs) {

						if(str_contains($attrs, '=')) {
							$attrs = explode("=", $attrs);
							if($variation['attributes'][$attrs[0]] == $attrs[1]) {
								$check++;
							}
						}
					}

					if($check == count($attr_selected)) {
						
						$key = array_search($variation['attributes'][$attribute[ 'attr' ]], $attribute[ 'selection' ]);

						if($key === false) {

							$attribute[ 'selection' ][] = $variation['attributes'][$attribute[ 'attr' ]];
							$attribute[ 'price' ][] = $variation['display_regular_price'];
							$attribute[ 'sale' ][] = $variation['display_price'];

						} else {

							// Modify Price
							$priceMod = explode("-", $attribute[ 'price' ][$key]);
							$saleMod = explode("-", $attribute[ 'sale' ][$key]);

							$lowPrice = $priceMod[0]; $highPrice = $priceMod[0]; 
							$lowSale = $saleMod[0]; $highSale = $saleMod[0];

							if(is_countable($lowPrice) && count($lowPrice) > 1) {
								$lowPrice = $priceMod[0]; $highPrice = $priceMod[1]; 
								$lowSale = $saleMod[0]; $highSale = $saleMod[1];
							}

							if($lowPrice > $variation['display_regular_price'])
								$lowPrice = $variation['display_regular_price'];

							if($highPrice < $variation['display_regular_price'])
								$highPrice = $variation['display_regular_price'];

							if($lowSale > $variation['display_price'])
								$lowSale = $variation['display_price'];

							if($highSale < $variation['display_price'])
								$highSale = $variation['display_price'];

							// Now check if it is lower / higher than previous selection
							if(str_contains($attribute[ 'price' ][$key], '-')) {
								$prevResult = explode("-", $attribute[ 'price' ][$key]);
								$prevResultSale = explode("-", $attribute[ 'sale' ][$key]);

								if($lowPrice > $prevResult[0])
									$lowPrice = $prevResult[0];

								if($highPrice < $prevResult[1])
									$highPrice = $prevResult[1];

								if($lowSale > $prevResultSale[0])
									$lowSale = $prevResultSale[0];

								if($highSale < $prevResultSale[1])
									$highSale = $prevResultSale[1]; 
							}

							$attribute[ 'price' ][$key] = $lowPrice . "-" . $highPrice;
							$attribute[ 'sale' ][$key] = $lowSale . "-" . $highSale;
						}
					}
				}

				// Try to arrange so it's low to high
				$newAttribute = array(); $newPrice = array(); $newSale = array(); $num = count($attribute[ 'price' ]);
				for($i = 0; $i < $num; $i++) {

					$index = array_search(min($attribute[ 'price' ]), $attribute[ 'price' ]);
					
					$newAttribute[$i] = $attribute[ 'selection' ][$index];
					$newPrice[$i] = $attribute[ 'price' ][$index];
					$newSale[$i] = $attribute[ 'sale' ][$index];

					unset($attribute[ 'selection' ][$index]);
					unset($attribute[ 'sale' ][$index]);
					unset($attribute[ 'price' ][$index]);
				}

				$attribute[ 'selection' ] = implode(",", $newAttribute);
				$attribute[ 'price' ] = implode(",", $newPrice);
				$attribute[ 'sale' ] = implode(",", $newSale);
				
			} else {
				$attribute[ 'selection' ] = "-";
				$attribute[ 'price' ] = $product->get_regular_price();
				$attribute[ 'sale' ] = $product->get_sale_price();
			}

		}

		$currentSelectionTarget = '';
		if ( isset( $_GET[ 'attribute_pa_size' ] ) )
		{
			$currentSelectionTarget = $_GET[ 'attribute_pa_size' ];
		}
		
		$el = '';
		
		if ( !empty( $attribute[ 'name' ] ) && !empty( $attribute[ 'selection' ] ) && !empty( $attribute[ 'price' ] ) && !empty( $attribute[ 'link' ] ) )
		{
			$name = $attribute[ 'name' ];
			$arrSelection = explode( ',', $attribute[ 'selection' ] );
			$arrPrice = explode( ',', $attribute[ 'price' ] );
			$arrSale = explode( ',', $attribute[ 'sale' ] );

			$isSale = false;
			for($z = 0; $z < count($arrSale); $z++) {

				if($arrSale[$z] != $arrPrice[$z]) {
					if(!empty($arrSale[$z])) {
						$isSale = true;
						break;
					}
				}
			}

			if(!$isSale) {
				$arrSale = array();
			}

			if( empty( $attribute[ 'product_id' ] ) ) {
				$isSale = false;
				$arrSale = array();
			}
			
			$selectionAttributeLink = '';
			if ( !empty( $currentSelectionTarget ) )
			{
				foreach( $arrSelection as $selection ) {
					$selectionSlug = explode("|", $selection);
					$selectionName = $selectionSlug[0];
					if(!empty($selectionSlug[1])) 
						$selectionSlug = $selectionSlug[1];
					else
						$selectionSlug = $selectionSlug[0];

					if($selectionName == $currentSelectionTarget) {

						if (strpos($attribute[ 'link' ], '?')) {
							$selectionAttributeLink = '&' . $attribute[ 'attr' ] . '=' . $selectionSlug;
						} else {
							$selectionAttributeLink = '?' . $attribute[ 'attr' ] . '=' . $selectionSlug; 	
						}

						break;
					}
				}
			}
			
			$el .= '<div class="productattribute-wrapper">
						<a href="' . $attribute[ 'link' ] . $selectionAttributeLink . '" attr="'.$attribute[ 'attr' ].'">
							<div class="productattribute-image">';
								if ( !empty( $attribute['image'] ) )
								{
									$el .= '<img src="' . $attribute['image'] . '">';
								}
							
			$el .=		'	</div>
						</a>';
						
			if($attribute[ 'show_selection' ] == "yes") {
				$el .=		'<div class="productattribute-selection">';
				
								$i = 0;
								$selectedFlag = 0;
								foreach( $arrSelection as $selection ) 
								{
									$selectionSlug = explode("|", $selection);
									$selectionName = $selectionSlug[0];
									$selectionName = explode("-", $selectionName); $selectionName = $selectionName[0];

									if(!empty($selectionSlug[1])) 
										$selectionSlug = $selectionSlug[1];
									else
										$selectionSlug = $selectionSlug[0];

									$classSelection = '';
									if ( !empty( $currentSelectionTarget ) )
									{								
										if ( strtolower( $currentSelectionTarget ) == trim( strtolower( $selectionName ) ) )
										{
											$classSelection = 'selected';
											$selectedFlag = $i;
										}
									} else {
										if ( 's' == trim( strtolower( $selectionName ) ) )
										{
											$classSelection = 'selected';
											$selectedFlag = $i;
										}
									}

									$printPrice = $arrPrice[$i]?? "";
									$printSale = $arrSale[$i]?? "";
									
									$el .= '<div class="attribute-selection ' . $classSelection . '" price="' . $printPrice . '" sale="' . $printSale . '" value="' . trim( strtolower( $selectionSlug ) ) . '">
												' . trim( $selectionName ) . '
											</div>';
											
									$i++;
								}
				$el .= 		'</div>';	                
			}
							
			if ( !empty( $attribute['promotion_text'] ) )
			{
				$el .= '<div class="productattribute-promotiontext">
							<h3>' . $attribute['promotion_text'] . '</h3>
						</div>';	
			}
						
			$el .=		'<div class="productattribute-name">';
							$el .= $attribute[ 'name' ];		
							
			$el .= 		'</div>
						<div class="productattribute-price">';
						
							if ( !empty( $currentSelectionTarget ) )
							{
								if (str_contains($arrPrice[$selectedFlag], '-')) { 
									
									$tmpVal = explode('-', $arrPrice[$selectedFlag]);

									if(!$isSale)
										$el .= '¥' . number_format( $tmpVal[0], 0 ) . '-' . '¥' . number_format( $tmpVal[1], 0 ) ;
									else
										$el .= '<del>¥' . number_format( $tmpVal[0], 0 ) . "</del>" . '-<del>¥' . number_format( $tmpVal[1], 0 ) . "</del>";

								} else {
									if(!$isSale)
										$el .= '¥' . number_format( $arrPrice[$selectedFlag], 0 );
									else
										$el .= '<del>¥' . number_format( $arrPrice[$selectedFlag], 0 ) . "</del>";
								}
							}
							else
							{
								$countPrice = count( $arrPrice ); $price = '';
								if ( $countPrice > 0 )
								{
									if ( $countPrice > 1 )
									{
										// check if have first or last item have price range instead singular price
										$firstItemPrice = $arrPrice[0];
										$lastItemPrice = $arrPrice[$countPrice - 1];
										
										if ( strpos( $firstItemPrice, '-' ) !== false ) {
											$arrFirstItemPrice = explode( '-', $firstItemPrice );
											$firstItemPrice = $arrFirstItemPrice[0];
										}
										
										if ( strpos( $lastItemPrice, '-' ) !== false ) {
											$arrLastItemPrice = explode( '-', $lastItemPrice );
											$countLastItemPrice = count( $arrLastItemPrice );
											$lastItemPrice = $arrLastItemPrice[$countLastItemPrice - 1];
										}
			
										if(!$isSale)
											$price = '¥' . number_format( $firstItemPrice, 0 ) . ' - ' . '¥' . number_format( $lastItemPrice, 0 );
										else
											$price = '<del>¥' . number_format( $firstItemPrice, 0 ) . ' - ' . '¥' . number_format( $lastItemPrice, 0 ) . "</del>";
									}
									else
									{
										$tmpPrice = explode("-", $arrPrice[0]);

										if(count($tmpPrice) == 2) {

											if(!$isSale)
												$el .= '¥' . number_format( $tmpPrice[0], 0 ) . ' - ¥' . number_format( $tmpPrice[1], 0 );
											else
												$el .= '<del>¥' . number_format( $tmpPrice[0], 0 ) . " - ¥" . number_format( $tmpPrice[1], 0 ) . "</del>";
										} else {
											if(!$isSale)
												$price = '¥' . number_format( $arrPrice[0], 0 ); 
											else
												$price = '<del>¥' . number_format( $arrPrice[0], 0 ) . "</del>"; 
										}
									}
									
									$el .= $price;
								}
							}	
			$el .= 		'</div>';

			if($isSale) {
				$el .= 		'<div class="productattribute-sale">';
							
								if ( !empty( $currentSelectionTarget ) )
								{
									if (str_contains($arrSale[$selectedFlag], '-')) { 
										$tmpVal = explode('-',$arrSale[$selectedFlag]);
										$el .= '¥' . number_format( $tmpVal[0], 0 ) . '-¥' . number_format( $tmpVal[1], 0 );
									} else {
										$el .= '¥' . number_format( $arrSale[$selectedFlag], 0 );
									}
								}
								else
								{
									$countPrice = count( $arrSale );
									if ( $countPrice > 0 )
									{
										if ( $countPrice > 1 )
										{
											// check if have first or last item have price range instead singular price
											$firstItemPrice = $arrSale[0];
											$lastItemPrice = $arrSale[$countPrice - 1];
											
											if ( strpos( $firstItemPrice, '-' ) !== false ) {
												$arrFirstItemPrice = explode( '-', $firstItemPrice );
												$firstItemPrice = $arrFirstItemPrice[0];
											}
											
											if ( strpos( $lastItemPrice, '-' ) !== false ) {
												$arrLastItemPrice = explode( '-', $lastItemPrice );
												$countLastItemPrice = count( $arrLastItemPrice );
												$lastItemPrice = $arrLastItemPrice[$countLastItemPrice - 1];
											}
					
											$price = '¥' . number_format( $firstItemPrice, 0 ) . ' - ' . '¥' . number_format( $lastItemPrice, 0 );
										}
										else
										{
											if (str_contains($arrSale[0], '-')) { 
												$tmpVal = explode('-',$arrSale[0]);
												$price = '¥' . number_format( $tmpVal[0], 0 ) . '-¥' . number_format( $tmpVal[1], 0 ); 
											} else {
												$price = '¥' . number_format( $arrSale[0], 0 ); 
											}
										}
										
										$el .= $price;
									}
								}
				$el .= '	</div>';
			}

			$el .= '	<div class="productattribute-sub">';
							$el .= $attribute[ 'sub' ];
							
			$el .= '	</div>
						<div class="productattribute-content">';
							$el .= $content;
							
			$el .= '	</div>
						<div class="productattribute-action">';
							if ( !empty( $attribute[ 'link' ] ) && !empty( $attribute[ 'button_text' ] ) )
							{	
								$el .= '<a href="' . $attribute[ 'link' ] . $selectionAttributeLink . '" title="' . $attribute[ 'name' ] . '">
											<button class="button productattribute-button">
												' . $attribute[ 'button_text' ] . '
											</button>
										</a>';
							}
			
			$el .= '    </div>
					</div>';
		}
		else
		{
			return;
		}
		
		return $el;
	} 

	// Direct Sale Shortcode
	add_shortcode('direct_sale', 'shortcode_directsale'); 

	function shortcode_directsale( $atts, $content = null  ) 
	{ 
		$attribute = shortcode_atts( array(
			'id' => '',
			'image' => ''
		), $atts );

		$variable_product = wc_get_product($attribute["id"]);

		if($variable_product) {
			if($variable_product->has_enough_stock(1)) {
				ob_start();
				?>
				<div class="productattribute-wrapper directsale">
					<a href="https://ambientlounge.co.jp/offers/?add-to-cart=<?php echo $attribute["id"]; ?>&quantity=1">
						<div class="productattribute-image">
							<img src="<?php if(empty($attribute["image"])) echo wp_get_attachment_thumb_url( $variable_product->get_image_id() ); else echo $attribute["image"]; ?>">	
						</div>
					</a>
					<div class="productattribute-price">
						<?php echo $variable_product->get_price_html() ?>
					</div>	
				
					<div class="productattribute-content">	
						<?php echo wc_get_formatted_variation( $variable_product, true, false, false ); ?>
					</div>
				
					<div class="productattribute-action">
						<a href="https://ambientlounge.co.jp/offers/?add-to-cart=<?php echo $attribute["id"]; ?>&quantity=1">
							<button class="button productattribute-button">
								お買い物カゴに追加
							</button>
						</a>    
					</div>
				</div>
				<?php
				
				return ob_get_clean();
			}
		}

		return "";
	} 

	// Variations in Stock Shortcode
	add_shortcode('variations_in_stock', 'shortcode_variations_in_stock'); 
	function shortcode_variations_in_stock( $atts, $content = null  ) 
	{ 
		$attribute = shortcode_atts( array(
			'id' => '',
		), $atts );

		$returnHTML = "";
		$product = wc_get_product($attribute["id"]);
		$variations = $product->get_available_variations();
		$variations_id = wp_list_pluck( $variations, 'variation_id' );

		foreach($variations_id as $variation_id) {
			$variable_product = wc_get_product($variation_id);
			if($variable_product) {
				if($variable_product->has_enough_stock(1)) {
					ob_start();
					?>
					<div class="productattribute-wrapper directsale">
						<a href="https://ambientlounge.co.jp/offers/?add-to-cart=<?php echo $variation_id; ?>&quantity=1">
							<div class="productattribute-image">
								<img src="<?php echo wp_get_attachment_thumb_url( $variable_product->get_image_id() ); ?>">	
							</div>
						</a>
						<div class="productattribute-price">
							<?php echo $variable_product->get_price_html() ?>
						</div>	
					
						<div class="productattribute-content">	
							<?php echo wc_get_formatted_variation( $variable_product, true, false, false ); ?>
						</div>
					
						<div class="productattribute-action">
							<a href="https://ambientlounge.co.jp/offers/?add-to-cart=<?php echo $variation_id; ?>&quantity=1">
								<button class="button productattribute-button">
									お買い物カゴに追加
								</button>
							</a>    
						</div>
					</div>
					<?php
					
					$returnHTML .= ob_get_clean();
				}
			}
		}
		
		return $returnHTML;
	}

	// Fix ajax error on woo swatches
		add_filter( 'woo_variation_swatches_global_ajax_variation_threshold_max', 'woo_variation_swatches_global_ajax_variation_threshold_max_edit', 10, 2 );
		function woo_variation_swatches_global_ajax_variation_threshold_max_edit( $size, $product ){
			return 1500;
		}

	// Add a line break in Woocommerce Product Titles
		add_filter( 'the_title', 'custom_product_title', 10, 2 );
		function custom_product_title( $title, $post_id ){
			$post_type = get_post_field( 'post_type', $post_id, true ); 
		
			if( in_array( $post_type, array( 'product', 'product_variation') ) ) {
				$title = str_replace( '|', '<br/>', $title ); // we replace "|" by "<br>"
			}
			return $title;
		}
 
	// TEMP
		add_filter('acf/load_field/name=variation_select', 'acf_load_variation');

		function acf_load_variation( $field ) {
			
			// reset choices
			$field['choices'] = array();    
			
			// get the textarea value from options page without any formatting
			$product_postobject = get_field('product', 1290, false);

			$choices = array( 'a', 'b', 'c' );
			foreach( $choices as $choice ) {
					
				$field['choices'][ $choice ] = 'a';
				
			}

			if ( is_a( $product, 'WC_Product' ) ) {

				if ( $product->is_type( 'variable' ) ) {
					$variations = $product->get_available_variations();    
				}
			}

			// return the field
			return $field;    
		}

	// Exclusions
	// Exclude scripts from JS delay.
	add_filter( 'rocket_delay_js_exclusions', 'np_wp_rocket__exclude_from_delay_js' );
	function np_wp_rocket__exclude_from_delay_js( $excluded_strings = array() ) {

		if(is_woocommerce()) {
			$excluded_strings[] = 'wp-util.min.js(.*)';
			$excluded_strings[] = 'underscore.min.js(.*)';
			$excluded_strings[] = '/woocommerce/assets/js/frontend/(.*)';
			$excluded_strings[] = '/variation-swatches-for-woocommerce/assets/js/frontend(.*)';
			$excluded_strings[] = '/ambientlounge-child-theme/src/js/single-product(.*)';
			$excluded_strings[] = '/ambientlounge-child-theme/src/js/archive-product(.*)';
		}

		if(
			is_page(322) || is_page(15438) || is_page(68120)
		) {
			$excluded_strings[] = 'recaptcha';
		}

		return $excluded_strings;
	}

	// Fix woo breadcrumb bug
	add_filter('woocommerce_get_breadcrumb', 'sp_custom_woocommerce_breadcrumbs', 10, 2);
	function sp_custom_woocommerce_breadcrumbs($crumbs, $breadcrumb) {
		if (is_product()) {
			global $post;
			$primary_category_id = yoast_get_primary_term_id('product_cat', $post->ID);
			if ($primary_category_id) {
				// Fetch the primary category details
				$primary_category = get_term($primary_category_id, 'product_cat');

				if ($primary_category && !is_wp_error($primary_category)) {
					$updated_crumbs = array($crumbs[0]);

					// Traverse up the category hierarchy and add each parent category to the breadcrumbs
					$parent_category = $primary_category;
					while ($parent_category->parent != 0) {
						$parent_category = get_term($parent_category->parent, 'product_cat');
						if ($parent_category && !is_wp_error($parent_category)) {
							array_splice($updated_crumbs, 1, 0, array(array($parent_category->name, get_term_link($parent_category))));
						}
					}

					$updated_crumbs[] = array($primary_category->name, get_term_link($primary_category));
					$updated_crumbs[] = end($crumbs);

					return $updated_crumbs;
				}
			}
		} 
		return $crumbs;
	}

	add_action( 'woocommerce_before_shop_loop_item', 'loop_variable_product_sale_percentage', 25 );
	function loop_variable_product_sale_percentage() {
		global $product;

		if ( $product->is_on_sale() ) {

			if ( ! $product->is_type( 'variable' ) ) {

				$max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;

			} else {

				$max_percentage = 0;

				foreach ( $product->get_children() as $child_id ) {
					$variation = wc_get_product( $child_id );
					$price = $variation->get_regular_price();
					$sale = $variation->get_sale_price();
					if ( $price != 0 && ! empty( $sale ) ) $percentage = ( $price - $sale ) / $price * 100;
					if ( $percentage > $max_percentage ) {
						$max_percentage = $percentage;
					}
				}

			}

			echo "<div class='directsale-block-percentage'>" . round($max_percentage) . "% OFF</div>";

		}
	}