<?php
/*
	Template Name: Delux
*/
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/adjustment.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/font/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/main-dark.all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/lp-dog-delux.css?ver=208"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/top-bar.css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto Sans JP:100,200,300,400,500,600,700|Noto Sans JP:600|Open Sans:600|Open Sans|Open Sans:700&amp;subset=latin-ext&amp;display=swap" media="all" onload="this.media='all'">
<div id="main-content">
    <div class="header-inner"></div>
    <div class="top-bar-new d-flex justify-content-between" id="top-bar-new">
        <div class="top-bar-new-title" id="top-bar-new-title">DELUX</div>
        <a class="btn btn-primary top-bar-new-btn" id="top-bar-new-btn" href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/?attribute_pa_product-type=delux">商品ページを見る</a>
    </div>
    <div class="container-fluid bg-light">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <p class="top-banner-title">DELUX</p>
                <p class="top-banner-text top-banner-heading-lg">DELUXセットで
                    <br>お得にペットラウンジ
                    <br>生活を始めよう</p>
                <p class="top-banner-text top-banner-heading-sm desc-text-lg">当たり前に一緒に“暮らす”が実現した、人とペットのボーダレスな今だからこそ
                    <br>愛犬に贈りたいのはペットラウンジ。
                    <br>取替カバー3種、メンテナンスや快適をさらに向上させるアクセサリー、
                    <br>リピーター続出のthe Shampooがセットになりました。</p>
                <p class="top-banner-text top-banner-heading-sm desc-text-sm">当たり前に一緒に“暮らす”が実現した、人とペットのボーダレスな今だからこそ愛犬に贈りたいのはペットラウンジ。取替カバー3種、メンテナンスや快適をさらに向上させるアクセサリー、リピーター続出のthe Shampooがセットになりました。</p>
                <img class="lp-delux-top-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/ambientloungejp-PetLounge-Delux-Grey-Shampoo.webp" alt="lp-dog">
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <div class="compare-table-container">
                        <p class="compare-table-title">ベッド単体とデラックスセットどっちがお得？</p>
                        <div class="lp-delux-top-table"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--container bg-black start-->
    <section id="lp-delux">
        <div class="container-fluid container-2">
            <div class="container-dark">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="dark-banner-header text-white">セット内容</p>
                    </div>
                </div>
                <div class="row banner-row-dark">
                    <div class="col-xs-12 col-md-6 p-0 order-2 order-md-1">
                        <div class="banner-img-1"></div>
                    </div>
                    <div class="col-xs-12 col-md-6 m-auto text-end banner-title-right order-1 order-md-2">
                        <p class="banner-title-lg text-gradient-blue banner-title-m-left">ペットラウンジ<br>×1</p>
                        <div class="desc-text-v-sm">
                            <img width="75px;" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/box-review.webp"/>
                            <p class="banner-subtitle text-start">フィリング：<br>プレミアム（とても柔らか）</p>
                        </div>
                        <p class="banner-subtitle desc-text-v-lg"><span><img style="vertical-align: bottom; width: 75px;" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/box-review.webp"></span>フィリング：プレミアム（とても柔らか）</p>
                        <p class="banner-subtitle desc-text-lg">プレミアムビーズにウレタンフォームを配合。人用ソファと同じ最高レベルの充填で、
                            <br>フワッと心地よい弾力性と安定した寝心地を目指しました。
                            <br>座り心地も品質も更なる【極上】レベルへ。</p>
                        <p class="banner-subtitle desc-text-sm">プレミアムビーズにウレタンフォームを配合。人用ソファと同じ最高レベルの充填で、フワッと心地よい弾力性と安定した寝心地を目指しました。座り心地も品質も更なる【極上】レベルへ。</p>
                    </div>
                </div>
                <div class="row banner-row-dark" style="margin-bottom: 0">
                    <div class="col-xs-12 col-md-6 m-auto text-start banner-title-left">
                        <p class="banner-title-lg text-gradient-pink">ベッド<br>取替カバー×3</p>
                        <p class="banner-subtitle pt-3 pt-md-4">洗い替えに便利なカバーが3種類セットに。
                            <br>カバーを交換するだけでインテリアや季節に応じて簡単にアップデート。
                            <br>１台で何通りものデザインや用途が楽しめます。
                            <br>※ベッド本体のお色に合わせたセットになります。
                        </p>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0">
                        <div class="banner-img-2"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p class="dark-banner-header text-gradient-blue">アクセサリー ×5</p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                        <div class="product-panel-dark">
                            <p class="product-panel-title">クールジェルマット</p>
                            <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-bg-dark-1.webp"/>
                            <p class="product-desc">キルトカバー裏面ポケットにすっぽり入るおしゃれなオリジナル保冷ジェル。(S/Mには1点、L/XXLには2点付属)</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                        <div class="product-panel-dark">
                            <p class="product-panel-title">ポケラウンジ</p>
                            <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-bg-dark-2.webp"/>
                            <p class="product-desc">ミニチュアサイズの可愛いお供。愛犬とお揃いを楽しめるアイコニックで使えるキーホルダー。</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                        <div class="product-panel-dark">
                            <p class="product-panel-title">ブランケット</p>
                            <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-bg-dark-3.webp"/>
                            <p class="product-desc">お昼寝やケージ、キャリーのマットなどに重宝するブランケットは耐久性に優れた隠れた名品。</p>
                            <span class="product-sm-panel">※ブランケットS-Lサイズは各ベッドサイズに合わせてお届け。<br>XXLベッドをご注文の方にはLサイズのブランケットをお届けします。</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                        <div class="product-panel-dark">
                            <p class="product-panel-title">ウォッシュネット</p>
                            <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-bg-dark-4.webp"/>
                            <p class="product-desc">バーのお洗濯にピッタリの耐久性に優れたウォッシュネット。ペットラウンジサイズに合わせたサイズをお届け。</p>
                            <span class="product-sm-panel">※L、XXL用のウォッシュネットはブランドロゴ付きのデザインではありません。<br>詳しくは商品ページへ。</span>
                        </div>
                    </div>
                    <div class="col-xs-12 p-0">
                        <div class="product-panel-dark panel-last">
                            <div class="row">
                                <div class="col-xs-12 panel-last-title-lg">
                                    <p class="product-panel-title">the Shampoo</p>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <img class="product-bg-dark-img-last" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-bg-dark-5.webp"/>
                                </div>
                                <div class="col-xs-12 col-md-9 m-auto">
                                    <div class="panel-last-title-sm"><p class="product-panel-title">the Shampoo</p></div>
                                    <p class="product-desc">メディアで話題！水のかわりに国産抽出液でつくったシャンプー。家族みんなでご利用いただけます。</p>
                                    <span class="product-sm-panel">※the Shampooはシュリンクフィルム梱包が未開封の場合のみトライアル返品対象となります。</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--container bg-black end-->

    <!--container filling material start -->
    <div class="container-fluid bg-light p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <p class="light-text-title">フィリングについて</p>
                    <p class="light-text-subtitle">ペットの好みや性格、年齢に合わせて3種類のフィリングをご用意</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-4 text-center">
                    <p class="text-common-bold">ベーシック</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/filling_img_1.webp" alt=""/>
                    <p class="panel-text-bold text-black">柔らかさ</p>
                    <img class="p-2 w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/star_1.webp" alt=""/>
                    <p class="text-grey">硬め、軽量<br>&emsp;</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/recommend_1.webp" alt=""/>
                    <p class="text-common-regular">プレミアムビーズ充填で仕上げました。エントリープライスのペットラウンジ。硬めがお好みのペット達にもおすすめです。</p>
                </div>
                <div class="col-xs-12 col-md-4 text-center">
                    <p class="text-common-bold">スタンダード</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/filling_img_2.webp" alt=""/>
                    <p class="panel-text-bold text-black">柔らかさ</p>
                    <img class="p-2 w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/star_2.webp" alt=""/>
                    <p class="text-grey">柔らかめ、包み込む、体圧分散、関節サポート、<br>弾力性、軽量</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/recommend_2.webp" alt=""/>
                    <p class="text-common-regular text-start">独自開発のハイブリッド充填で仕上げました。反発力と弾力性に優れ、足腰が弱りがちなシニア達の起き上がりを助けます。体が心地よく沈み、柔らか過ぎず、硬過ぎない、最適なフィット感で人気No.1のフィリングです。</p>
                </div>
                <div class="col-xs-12 col-md-4 text-center">
                    <p class="text-common-bold">プレミアム</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/filling_img_3.webp" alt=""/>
                    <p class="panel-text-bold text-black">柔らかさ</p>
                    <img class="p-2 w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/star_3.webp" alt=""/>
                    <p class="text-grey">フカフカ、包み込む、体圧分散、関節サポート、<br>弾力性、形状記憶、衝撃吸収</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/recommend_3.webp" alt=""/>
                    <p class="text-common-regular text-start">独自開発の低反発ハイブリッド充填で仕上げました。まるでエアーベッドのような極上感でペット達はたちまち虜に。自分の体に合わせてじんわりと沈み込み、最高の睡眠をサポート。体圧分散性に加え、反発力と弾力性により、特定部位への負担を避け体の痛みを軽減する働きが期待できます。また、高い衝撃吸収力で元気よく飛び乗るパピー達の関節にも安心です。</p>
                </div>
            </div>
        </div>
    </div>
    <!--container filling material end-->

    <!--container al-intro start-->
    <div class="container-fluid bg-white p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 text-center">
                    <p class="light-text-title">日本品質へのこだわり</p>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-9 py-5"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/al_logo_lg.webp" alt=""/></div>
                <div class="w-100"></div>
                <div class="col-xs-12 col-md-10 p-0 mb-5 text-center">
                    <p class="al-intro-text">ゆったりとした時間が流れ、QOLを大切にするオーストラリア発のアンビエントラウンジが日本に上陸したのは2015年。
                        <br>機能的でユニークなデザインはそのままに、ジャパン社では日本発信の商品開発や日本品質にこだわっています。
                        <br>試行錯誤を繰り返したどり着いた、圧倒的な座り心地のソファやペットベッドはジャパン社だけの独自品質。
                        <br>ローカル産業との取り組みを始め、過疎化が進む "みなかみ町" に新しいビジネスモデルを生み出したいと考えています。
                        <br>ご注文いただいたソファやベッドは全て、地元みなかみ町のスタッフが１点１点心を込めて仕上げています。</p>
                </div>
            </div>
        </div>
    </div>
    <!--container al-intro end-->

    <!--container review start-->
    <div class="container-fluid container-3 p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <div class="col-md-12">
                    <p class="light-text-title">カスタマーレビュー</p>
                </div>
                <div class="col-xs-12 col-md-10 p-0">
                    <div class="row ranking-panel">
                        <div class="col-xs-12 col-md-3 p-0 m-auto"><div class="review-img delux-review-1"></div></div>
                        <div class="col-xs-12 col-md-9 p-0 text-start m-auto">
                            <p class="ranking-panel-text"><span class="ranking-panel-text-bold"> お誕生日プレゼント </span> 3歳のお誕生日プレゼントに、1日の大半を寝て過ごす眠り姫の我が子の為に購入しました。最初から自分の場所だと認識して、使ってくれています。お値段はそれなりですが、一年中使えて、交換カバーやアクセサリーでお手入れも出来るので、良い買い物だったと思います。</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-10 p-0">
                    <div class="row ranking-panel">
                        <div class="col-xs-12 col-md-3 p-0 m-auto"><div class="review-img delux-review-2"></div></div>
                        <div class="col-xs-12 col-md-9 p-0 text-start m-auto">
                            <p class="ranking-panel-text"><span class="ranking-panel-text-bold"> 良かったです </span> 1年中使えるようにと取り替えカバーが付いてるデラックスのMサイズを購入しました。ベッドの上の部分のカバーが取り外しが楽な事、洗濯ネットが付いていてるので安心してお洗濯出来ます。<br>なにより愛犬のトイプードルが気に入ってるようです。</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-10 p-0">
                    <div class="row ranking-panel">
                        <div class="col-xs-12 col-md-3 p-0 m-auto"><div class="review-img delux-review-3"></div></div>
                        <div class="col-xs-12 col-md-9 p-0 text-start m-auto">
                            <p class="ranking-panel-text"><span class="ranking-panel-text-bold"> 最高の癒しのベッドです </span> 届いた初日から気に入ってくれたようで、ぐっすりスヤスヤ気持ちよさそうに寝ています。金額がお高めなので、買うのを迷っていましたが、安物を何回も買い替えたりするよりは、ずっと経済的だし、何よりもワンコにとって最高の睡眠を得られるのであれば、買って後悔なしです。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--container review end-->

    <!--container details start-->
    <div class="container-fluid container-4 p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 text-center">
                    <p class="light-text-title">ベッドのサイズにお悩みですか？</p>
                    <p class="light-text-subtitle">一覧からあなたのペットにあったベッドを探すことができます</p>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/pet_size_s.webp" alt=""/>
                    <h2 class="my-3">S</h2>
                    <h6>小型犬10kg</h6>
                    <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                    <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                    <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト /
                        <br>オーガニックコットン /<br> フーディー</p>
                    <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                    <p class="text-grey-sm">ベーシック / スタンダード /
                        <br>プレミアム</p>
                    <p class="text-price">¥25,900(税込)から</p>
                    <a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/?attribute_pa_product-type=standard&attribute_pa_size=s&attribute_pa_bed-selection=grey-base&attribute_pa_cover-selection=wolfgrey-cover&attribute_pa_filling=be-lux-prime" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/pet_size_m.webp" alt=""/>
                    <h2 class="my-3">M</h2>
                    <h6>中型犬25kg</h6>
                    <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                    <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                    <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト /
                        <br>オーガニックコットン</p>
                    <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                    <p class="text-grey-sm">ベーシック / スタンダード /
                        <br>プレミアム</p>
                    <p class="text-price">¥34,900(税込)から</p>
                    <a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/?attribute_pa_product-type=standard&attribute_pa_size=m&attribute_pa_bed-selection=grey-base&attribute_pa_cover-selection=wolfgrey-cover&attribute_pa_filling=be-lux-prime" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/pet_size_l.webp" alt=""/>
                    <h2 class="my-3">L</h2>
                    <h6>大型犬40kg</h6>
                    <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                    <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                    <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト /
                        <br>オーガニックコットン</p>
                    <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                    <p class="text-grey-sm">ベーシック / スタンダード /
                        <br>プレミアム</p>
                    <p class="text-price">¥46,900(税込)から</p>
                    <a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/?attribute_pa_product-type=standard&attribute_pa_size=l&attribute_pa_bed-selection=grey-base&attribute_pa_cover-selection=wolfgrey-cover&attribute_pa_filling=be-lux-prime" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/pet_size_xxl.webp" alt=""/>
                    <h2 class="my-3">XXL</h2>
                    <h6>超大型犬40kg以上</h6>
                    <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color-1-2.png" style="max-width: 2rem;" alt=""/>
                    <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                    <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト</p>
                    <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                    <p class="text-grey-sm">ベーシック / スタンダード /
                        <br>プレミアム</p>
                    <p class="text-price">¥77,900(税込)から</p>
                    <a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/?attribute_pa_product-type=standard&attribute_pa_size=xxl&attribute_pa_bed-selection=grey-base&attribute_pa_cover-selection=wolfgrey-cover&attribute_pa_filling=be-lux-prime" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 p-0 text-center">
                    <p class="light-text-title">ベッドを使ってくれるか<br>心配ですか？</p>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-4 text-center">
                    <div class="panel-blue p-2 post-details-text">気に入らなければ<b>最大60日以内</b>の返品OK!</div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/step_img_1.webp" alt=""/>
                    <p class="post-desc-text">カラーやフィリングをカスタマイズしてご注文</p>
                    <p class="post-details-text">最短翌日出荷</p>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/step_img_2.webp" alt=""/>
                    <p class="post-desc-text">ご注文を受けてから日本ラボで作成しお届け</p>
                    <p class="post-details-text">日本人スタッフによる<br>最高品質の製造</p>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/step_img_3.webp" alt=""/>
                    <p class="post-desc-text">最大60日のお試しトライアル期間
                    <p class="post-details-text">ソファは30日間、ペットラウンジは60日間。気に入らない場合は、期間内なら全額返金。 
                    <br>※適用条件は「詳しく見る」をクリックしてご確認ください</p>   
                </div>
            </div>
            <div class="row justify-content-center py-5">
                <div class="col-md-3 text-center">
                    <a class="btn btn-primary btn-details-lg" href="https://ambientlounge.co.jp/30days-trial/" target="_self">詳しく見る</a>
                </div>
            </div>
        </div>
    </div>
    <!--container details end-->

    <!--container product white panel start-->
    <section id="lp-delux-product-w">
        <div class="container-fluid bg-light container-5 p-3 p-xl-5">
            <div class="container-dark">
                <div class="row justify-content-center">
                    <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                        <div class="product-panel-white">
                            <p class="light-panel-title">ベッド</p>
                            <p class="light-panel-subtitle">耐久性に優れ、いつも清潔、そして極上の寝心地。</p>
                            <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/pet-lounge/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                            <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-1.webp"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                        <div class="product-panel-white">
                            <p class="light-panel-title">アクセサリー</p>
                            <p class="light-panel-subtitle desc-text-lg">ブランケットやジェルマット、メンテンス用品など<br>いろいろなアイテムが勢ぞろい</p>
                            <p class="light-panel-subtitle desc-text-sm">ブランケットやジェルマット、メンテンス用品などいろいろなアイテムが勢ぞろい</p>
                            <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/pet-accessories/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                            <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-2.webp"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                        <div class="product-panel-white">
                            <p class="light-panel-title">Eco Friendly</p>
                            <p class="light-panel-subtitle">エコロジカルな活動を</p>
                            <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/eco/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                            <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/eco-friendly-product.webp"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                        <div class="product-panel-white">
                            <p class="light-panel-title">The Shampoo</p>
                            <p class="light-panel-subtitle">愛犬のグルーミング商品</p>
                            <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/the-shampoo-intro/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                            <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-4.webp"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--container product white panel end-->
</div>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/float-top-bar.js"></script>
<?php
get_footer();
?>