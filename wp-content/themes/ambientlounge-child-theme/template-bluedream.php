<?php
/*
	Template Name: Blue Dream
*/
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/adjustment.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/main-dark.all.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/lp_bluedream.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/banner-bottom.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/top-bar.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto Sans JP:100,200,300,400,500,600,700|Noto Sans JP:600|Open Sans:600|Open Sans|Open Sans:700&amp;subset=latin-ext&amp;display=swap" media="all" onload="this.media='all'">
    <div class="header-inner"></div>
    <div class="top-bar-new d-flex justify-content-between" id="top-bar-new">
        <div class="top-bar-new-title" id="top-bar-new-title">PET LOUNGE</div>
        <a class="btn btn-primary top-bar-new-btn" id="top-bar-new-btn" href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/?attribute_pa_product-type=standard&attribute_pa_size=s&attribute_pa_bed-selection=blue-base/">商品ページを見る</a>
    </div>
    <section id="banner-top">
        <div class="container-fluid bg-white">
            <div class="container-dark">
                <div class="row justify-content-center text-center">
                    <div class="top-banner">
                        <div class="top-banner-title-lg">PET LOUNGE</div>
                        <div class="top-banner-subtitle">- Japan Limited Edition -</div>
                        <div class="top-banner-heading-lg">史上最高品質のペットラウンジ<br>ブルードリーム</div>
                    </div>
                </div>
                <div class="row justify-content-center text-center">
                    <div class="satisfaction-title">3年で10,000台の販売実績<br>ドッグトレーナーが推奨する愛犬のためのベッド</div>
                    <img class="satisfaction-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog_new/satisfaction.webp"/>
                </div>
                <div class="row justify-content-center text-center">
                    <div class="top-banner-2">
                        <div class="top-banner-heading-2 desc-text-lg">大切な愛犬のためのライフタイムベッド</div>
                        <div class="top-banner-heading-2 desc-text-sm">大切な愛犬のための<br>ライフタイムベッド</div>
                    </div>
                    <div class="top-banner-2-desc px-0">
                        当たり前に一緒に“暮らす”が実現した、
                        <br>人とペットのボーダーレスな今だからこそ
                        <br>愛犬に贈りたいのはペットラウンジ。
                        <br>世界中のドックトレーナーや愛犬家に支持され、
                        <br>愛犬の睡眠と健康を守るための機能を
                        <br>ギュッと詰め込んだ
                        <br>究極のペットベッドです。
                    </div>
                </div>
            </div>
        </div>
    </section>
<!--section blue-dream start-->
<section id="blue-dream">
    <div class="container-fluid">
        <div class="container-dark">
            <div class="row banner-row-light" id="video-panel">
                <div class="col-xs-12 col-md-6 ps-md-0 m-auto">
                    <div class="banner-img-1"></div>
                </div>
                <div class="col-xs-12 col-md-6 p-md-5 text-center text-md-end m-auto">
                    <p class="banner-title-lg text-gradient-blue banner-title-right">史上最高品質の<br>ペットラウンジ</p>
                    <p class="banner-subtitle desc-text-lg">ペットラウンジのコンセプトを徹底的に追求し、これまで以上にお手入れがしやすく、美しく進化した史上最高品質のペットラウンジ ・ブルードリーム。
                        <br>シープオーガニックカバーとのセットアップは、まるでフワフワと雲が浮く青空のよう。
                        <br>ぐっすり眠れる極上ベッドは愛犬の幸せと健康を守ります。</p>
                    <p class="banner-subtitle desc-text-sm">ペットラウンジのコンセプトを徹底的に追求し、
                        <br>これまで以上にお手入れがしやすく、
                        <br>美しく進化した史上最高品質
                        <br>のペットラウンジ ・ブルードリーム。
                        <br>シープオーガニックカバーとのセットアップは、
                        <br>まるでフワフワと雲が浮く青空のよう。
                        <br>ぐっすり眠れる極上ベッドは
                        <br>愛犬の幸せと健康を守ります。</p>
                </div>
            </div>
            <div class="row banner-row-light">
                <div class="col-xs-12 col-md-10 p-md-5 m-auto text-end text-md-center">
                    <p class="banner-title-lg text-gradient-pink">洗濯機で洗える
                        <br>清潔な設計</p>
                    <p class="banner-subtitle desc-text-lg">裏地が完全防水の高機能なカバーは
                        <br>ファスナーでぐるりと取り外して洗濯機で洗えます｡
                        <br>季節や機能でカバーを交換できるから春夏秋冬ずっと快適、ずっと清潔。</p>
                    <p class="banner-subtitle desc-text-sm">裏地が完全防水の高機能なカバーはファスナーで
                        <br>ぐるりと取り外して洗濯機で洗えます｡
                        <br>季節や機能でカバーを交換できるから春夏秋冬ずっと快適、ずっと清潔。</p>
                </div>
                <div class="col-xs-12 col-md-10 m-auto p-0">
                    <div class="banner-img-3"></div>
                </div>
            </div>
            <div class="row banner-row-light">
                <div class="col-xs-12 col-md-6 p-md-5 m-auto text-start order-1">
                    <p class="banner-title-lg text-gradient-blue">お手入れが<br>更に簡単</p>
                    <p class="banner-subtitle">ぐるりと開閉する底面ファスナーでベッド本体も簡単にお洗濯できる仕様になりました。汚れに強い撥水生地にこだわりました。</p>
                </div>
                <div class="col-xs-12 col-md-6 m-auto order-2">
                    <div class="">
                        <img class="w-100 rounded-5" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/Blue-dream.gif" alt=""/>
                    </div>
                </div>
            </div>
            <div class="row banner-row-dark">
                <div class="col-xs-12">
                    <p class="banner-title-lg text-gradient-blue desc-text-lg">清潔・睡眠・体温調整で<br>健康を徹底的にサポート</p>
                    <p class="banner-title-lg text-gradient-blue desc-text-sm">清潔・睡眠・体温調整で健康を徹底的にサポート</p>
                </div>
                <div class="col-xs-12 p-3">
                    <div class="row product-list-lg">
                        <div class="col-md-4 col-xs-12 p-2 p-md-4 p-xl-5 m-auto text-center text-md-start">
                            <p class="panel-text-title">清潔</p>
                            <p class="text-desc-lg banner-desc-m-right desc-text-lg">本体は撥水加工、カバーは洗って清潔に保てるので、<br>ノミ、ダニやバイ菌の温床にならず、<br>おしっこやよだれにも安心です</p>
                            <p class="text-desc-lg banner-desc-m-right desc-text-sm">本体は撥水加工、カバーは洗って清潔に保<br>てるので、ノミ、ダニやバイ菌の温床にな<br>らず、おしっこやよだれにも安心です</p>
                        </div>
                        <div class="col-md-8 col-xs-12 p-0 m-auto">
                            <img class="w-100" style="border-radius: 15px;" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/product_img_lg_1.webp" alt=""/>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 p-3">
                    <div class="row product-list-lg">
                        <div class="col-md-7 col-xs-12 p-0 m-auto order-2 order-md-1">
                            <img class="w-100" style="border-radius: 15px;" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/product_img_lg_2.webp" alt=""/>
                        </div>
                        <div class="col-md-5 col-xs-12 p-2 p-md-4 p-xl-5 m-auto text-center text-md-start order-1 order-md-2">
                            <p class="panel-text-title text-center text-md-end">睡眠</p>
                            <p class="text-desc-lg text-end desc-text-lg">眠りが浅い犬達がぐっすり熟睡できるように、<br>独自開発の充填は人用マットレスと同グレード<br>健康を左右する良質な睡眠を<br>パピーからシニアまで末長くサポートします</p>
                            <p class="text-desc-lg text-center desc-text-sm">眠りが浅い犬達がぐっすり熟睡できるよう<br>に、独自開発の充填は人用マットレスと同<br>グレード健康を左右する良質な睡眠をパ<br>ピーからシニアまで末長くサポートします</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 p-3">
                    <div class="row product-list-lg">
                        <div class="col-md-4 col-xs-12 p-2 p-md-4 p-xl-5 m-auto text-center text-md-start">
                            <p class="panel-text-title">体温調整</p>
                            <p class="text-desc-lg banner-desc-m-right desc-text-lg">通気性に優れた生地で内部に湿気をため込まず、<br>睡眠環境を最適な温度に保ちます</p>
                            <p class="text-desc-lg banner-desc-m-right desc-text-sm">通気性に優れた生地で内部に湿気をため込まず、睡眠環境を最適な温度に保ちます</p>
                        </div>
                        <div class="col-md-8 col-xs-12 p-0 m-auto">
                            <img class="w-100" style="border-radius: 32px;" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/bluedream-banner-2.webp" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--section blue-dream end-->
    <!--container colors start-->
    <div class="container-fluid bg-white p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 p-0 text-center">
                    <p class="light-text-title">インテリアや季節に合わせて<br>あなたらしさが進む<br>豊富なバリエーション</p>
                </div>
                <div class="col-xs-12 p-0 text-center">
                    <p class="light-text-subtitle pb-xl-5">季節、デザイン、機能で選べるバリエーションが豊富だから、インテリアのアップデートや衣替えも楽しめます。</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-3 col-xs-6 p-0 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/bluedream-color-1.webp" alt=""></div>
                <div class="col-md-3 col-xs-6 p-0 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/bluedream-color-2.webp" alt=""></div>
                <div class="col-md-3 col-xs-6 p-0 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/bluedream-color-3.webp" alt=""></div>
                <div class="col-md-3 col-xs-6 p-0 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/bluedream-color-4.webp" alt=""></div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-3 col-xs-6 p-0 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/bluedream-color-5.webp" alt=""/></div>
                <div class="col-md-3 col-xs-6 p-0 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/bluedream-color-6.webp" alt=""/></div>
                <div class="col-md-3 col-xs-6 p-0 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/bluedream-color-7.webp" alt=""/></div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 p-0 text-center lh-lg title-m-top">
                    <p class="light-text-title desc-text-lg">洗練されたデザインで<br>ワンランク上のライフスタイルを</p>
                    <p class="light-text-title desc-text-sm">洗練されたデザインで<br>ワンランク上の<br>ライフスタイルを</p>
                </div>
                <div class="col-xs-12 p-0 text-center">
                    <p class="light-text-subtitle">世界中の愛犬家やセレブリティに愛される高級ペットベッドは家族みんなの幸せの中心にあります</p>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid bg-white">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 p-0">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/blue_dream/bluedream-banner-family.webp" alt=""/>
                </div>
            </div>
            <div class="row justify-content-center panel-customer-voice">
                <div class="customer-voice-title"><img class="instagram-logo" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog_new/instagram-logo.webp"/>CUSTOMER VOICE</div>
                <div class="customer-voice-subtitle">みんなで奪い合い！</div>
                <div class="customer-voice-content desc-text-lg">我が家では、ペットラウンジをリビングの真ん中に置いてあります。
                    <br>ここからは、みんなの動きがみれるし、なにより気持ち良いらしく、
                    <br>くろしろ2、ペットラウンジの奪い合いです。ペットラウンジで寝ている子が、水飲み等でスッとその場からいなくなると、
                    <br>さっと誰かが陣取りします（笑）実は、我が家では、わんこだけでなく、人もこのペットラウンジ争奪戦に参加してます🤣
                    <br>普通はひとり、もしくは1ワンだけが使いますが、みんなそれぞれ、
                    <br>どうしてもここで寝たいという気持ちが強くなると、みんなで一緒に寝るという、超密な状態となります🤣
                    <br>ペットラウンジは、あまりに気持ち良く、一旦そこに寝てしまうと、
                    <br>もう起き上がりたくなくなります。我が家では、ペットラウンジを「人とわんこをダメにするやつ」と呼んでいます🤣
                    <br>もうペットラウンジなしでは生活できません（笑）
                    <br><br>@tamanegi.qoo.riku
                </div>
                <div class="customer-voice-content desc-text-sm">我が家では、ペットラウンジを
                    <br>リビングの真ん中に置いてあります。
                    <br>ここからは、みんなの動きがみれるし、
                    <br>なにより気持ち良いらしく、
                    <br>くろしろ2、ペットラウンジの奪い合いです。
                    <br>ペットラウンジで寝ている子が、
                    <br>水飲み等でスッとその場からいなくなると、
                    <br>さっと誰かが陣取りします（笑）
                    <br>実は、我が家では、わんこだけでなく、
                    <br>人もこのペットラウンジ争奪戦に参加してます🤣
                    <br>普通はひとり、もしくは1ワンだけが使いますが、
                    <br>みんなそれぞれ、
                    <br>どうしてもここで寝たいという気持ちが強くなると、
                    <br>みんなで一緒に寝るという、超密な状態となります🤣
                    <br>ペットラウンジは、あまりに気持ち良く、
                    <br>一旦そこに寝てしまうと、
                    <br>もう起き上がりたくなくなります。
                    <br>我が家では、ペットラウンジを
                    <br>「人とわんこをダメにするやつ」と呼んでいます🤣
                    <br>もうペットラウンジなしでは生活できません（笑）
                    <br><br>@tamanegi.qoo.riku</div>
                <div class="customer-voice-img"></div>
            </div>
        </div>
    </div>
    <!--container colors end-->

    <!--container filling material start -->
    <div class="container-fluid bg-light p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <p class="light-text-title">フィリングについて</p>
                    <p class="light-text-subtitle">ペットの好みや性格、年齢に合わせて3種類のフィリングをご用意</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-4 text-center">
                    <p class="text-common-bold">ベーシック</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/filling_img_1.webp" alt=""/>
                    <p class="panel-text-bold text-black">柔らかさ</p>
                    <img class="p-2 w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/star_1.webp" alt=""/>
                    <p class="text-grey">硬め、軽量<br>&emsp;</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/recommend_1.webp" alt=""/>
                    <p class="text-common-regular">プレミアムビーズ充填で仕上げました。エントリープライスのペットラウンジ 。硬めがお好みのペット達にもおすすめです。</p>
                </div>
                <div class="col-xs-12 col-md-4 text-center">
                    <p class="text-common-bold">スタンダード</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/filling_img_2.webp" alt=""/>
                    <p class="panel-text-bold text-black">柔らかさ</p>
                    <img class="p-2 w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/star_2.webp" alt=""/>
                    <p class="text-grey">柔らかめ、包み込む、体圧分散、関節サポート、<br>弾力性、軽量</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/recommend_2.webp" alt=""/>
                    <p class="text-common-regular text-start">独自開発のハイブリッド充填で仕上げました。反発力と弾力性に優れ、足腰が弱りがちなシニア達の起き上がりを助けます。体が心地よく沈み、柔らか過ぎず、硬過ぎない、最適なフィット感で人気No.1のフィリングです。</p>
                </div>
                <div class="col-xs-12 col-md-4 text-center">
                    <p class="text-common-bold">プレミアム</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/filling_img_3.webp" alt=""/>
                    <p class="panel-text-bold text-black">柔らかさ</p>
                    <img class="p-2 w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/star_3.webp" alt=""/>
                    <p class="text-grey">フカフカ、包み込む、体圧分散、関節サポート、<br>弾力性、形状記憶、衝撃吸収</p>
                    <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/recommend_3.webp" alt=""/>
                    <p class="text-common-regular text-start">独自開発の低反発ハイブリッド充填で仕上げました。まるでエアーベッドのような極上感でペット達はたちまち虜に。自分の体に合わせてじんわりと沈み込み、最高の睡眠をサポート。体圧分散性に加え、反発力と弾力性により、特定部位への負担を避け体の痛みを軽減する働きが期待できます。また、高い衝撃吸収力で元気よく飛び乗るパピー達の関節にも安心です。</p>
                </div>
            </div>
        </div>
    </div>
    <!--container filling material end-->

    <!--container al-intro start-->
    <div class="container-fluid bg-white p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 text-center">
                    <p class="light-text-title">日本品質へのこだわり</p>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-7 py-5"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/al_logo_lg.webp" alt=""/></div>
                <div class="w-100"></div>
                <div class="col-xs-12 col-md-10 text-center">
                    <p class="al-intro-text">ゆったりとした時間が流れ、QOLを大切にするオーストラリア発のアンビエントラウンジが日本に上陸したのは2015年。
                        <br>機能的でユニークなデザインはそのままに、ジャパン社では日本発信の商品開発や日本品質にこだわっています。
                        <br>試行錯誤を繰り返したどり着いた、圧倒的な座り心地のソファやペットベッドはジャパン社だけの独自品質。
                        <br>ローカル産業との取り組みを始め、過疎化が進む "みなかみ町" に新しいビジネスモデルを生み出したいと考えています。
                        <br>ご注文いただいたソファやベッドは全て、地元みなかみ町のスタッフが１点１点心を込めて仕上げています。</p>
                </div>
            </div>
        </div>
    </div>
    <!--container al-intro end-->

    <!--container review start-->
    <div class="container-fluid bg-light p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <div class="col-md-12 p-0">
                    <p class="light-text-title">カスタマーレビュー</p>
                </div>
                <div class="col-xs-12 p-0 col-md-10">
                    <div class="row review-panel">
                        <div class="col-xs-12 col-md-3 p-0 m-auto"><div class="review-img bluedream-review-1"></div></div>
                        <div class="col-xs-12 col-md-9 p-0 text-start m-auto">
                            <p class="review-panel-text"><span class="review-panel-text-bold">ふわふわ！</span> カバーもお洗濯できるし、とにかく体にフィットして包み込んでくれるような自然な沈み方が気持ち良さそうです。<br>ベージュも持っていますが、柔らかいブルードリームの方がうちの子はお気に入り！</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 p-0 col-md-10">
                    <div class="row review-panel">
                        <div class="col-xs-12 col-md-3 p-0 m-auto"><div class="review-img bluedream-review-2"></div></div>
                        <div class="col-xs-12 col-md-9 p-0 text-start m-auto">
                            <p class="review-panel-text"><span class="review-panel-text-bold">インテリアにマッチ </span>  落ち着いた青いカラーがとってもすてきで一目惚れです♡早速飛び乗りしばらく動きませんでした🤣笑<br>ルナもアリスも気に入ったようで順番に使ったり、一緒に使ったりしています。</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 p-0 col-md-10">
                    <div class="row review-panel">
                        <div class="col-xs-12 col-md-3 p-0 m-auto"><div class="review-img bluedream-review-3"></div></div>
                        <div class="col-xs-12 col-md-9 p-0 text-start m-auto">
                            <p class="review-panel-text"><span class="review-panel-text-bold">待望のオーガニック素材 </span>  やわらかいクッションが大好きなので、直ぐに気に入ってくれました✨ オーガニックコットンでできており、保温性も高くこれから寒くなる季節にぴったりです。最近は起きると気持ち良さそうにへそ天しながら二度寝しているのでこちらまで嬉しくなります♡</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--container review end-->

    <!--container details start-->
    <div class="container-fluid container-4">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 text-center">
                    <p class="light-text-title desc-text-lg">ベッドのサイズにお悩みですか？</p>
                    <p class="light-text-title desc-text-sm">ベッドのサイズに<br>お悩みですか？</p>
                    <p class="light-text-subtitle">一覧からあなたのペットにあったベッドを探すことができます</p>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/pet_size_s.webp" alt=""/>
                    <h2 class="my-3">S</h2>
                    <h6>小型犬10kg</h6>
                    <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                    <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                    <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト /
                        <br>オーガニックコットン /<br> フーディー</p>
                    <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                    <p class="text-grey-sm">ベーシック / スタンダード /
                        <br>プレミアム</p>
                    <p class="text-price">¥32,900(税込)から</p>
                    <a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/?attribute_pa_product-type=standard&attribute_pa_size=s&attribute_pa_bed-selection=blue-base" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/pet_size_m.webp" alt=""/>
                    <h2 class="my-3">M</h2>
                    <h6>中型犬25kg</h6>
                    <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                    <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                    <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト /
                        <br>オーガニックコットン</p>
                    <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                    <p class="text-grey-sm">ベーシック / スタンダード /
                        <br>プレミアム</p>
                    <p class="text-price">¥48,900(税込)から</p>
                    <a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/?attribute_pa_product-type=standard&attribute_pa_size=m&attribute_pa_bed-selection=blue-base" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/pet_size_l.webp" alt=""/>
                    <h2 class="my-3">L</h2>
                    <h6>大型犬40kg</h6>
                    <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                    <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                    <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト /
                        <br>オーガニックコットン</p>
                    <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                    <p class="text-grey-sm">ベーシック / スタンダード /
                        <br>プレミアム</p>
                    <p class="text-price">¥59,900(税込)から</p>
                    <a href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/?attribute_pa_product-type=standard&attribute_pa_size=l&attribute_pa_bed-selection=blue-base" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="support-panel">
                    <div class="support-subtitle">お試しいただける</div>
                    <div class="support-title">サポートサービス</div>
                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-md-3 text-center support-list">
                            <p class="support-list-title">１年間の<br>製品保証</p>
                            <p class="support-list-text">アンビエントラウンジでは全ての商品に対し、商品到着より１年間、製造過程における製品不良があった場合には
                                返送料弊社負担の上、修理等の対応を承ります。</p>
                            <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog_new/support-img-1.webp"/>
                        </div>
                        <div class="col-xs-12 col-md-3 text-center support-list">
                            <p class="support-list-title">送料無料</p>
                            <p class="support-list-text">大型ソファも対象！サイト内全ての商品が一部地域を除き、送料無料です。沖縄及び北海道・九州地方の一部離島については送料が発生いたします。ご注文の翌々営業日までに送料についてのご案内メールをお送りいたします。</p>
                            <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog_new/support-img-2.webp"/>
                        </div>
                        <div class="col-xs-12 col-md-3 text-center support-list">
                            <p class="support-list-title">60日間<br>返金保証</p>
                            <p class="support-list-text">フィリング（充填材）は日本トップメーカー（アキレス）との共同開発。大自然に囲まれる群馬県みなかみのラボでご注文を受けてから１点１点専門スタッフによって手作業で仕上げられています。</p>
                            <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog_new/support-img-3.webp"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 text-center">
                    <p class="light-text-title">ベッドを使ってくれるか<br>心配ですか？</p>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-4 text-center">
                    <div class="panel-blue p-2 post-details-text">気に入らなければ<b>最大60日以内</b>の返品OK!</div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-3 col-xs-12 text-center">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/step_img_1.webp" alt=""/>
                    <p class="post-desc-text">カラーやフィリングをカスタマイズしてご注文</p>
                    <p class="post-details-text">最短翌日出荷</p>
                </div>
                <div class="col-md-3 col-xs-12 text-center">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/step_img_2.webp" alt=""/>
                    <p class="post-desc-text">ご注文を受けてから日本ラボで作成しお届け</p>
                    <p class="post-details-text">日本人スタッフによる<br>最高品質の製造</p>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                    <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/step_img_3.webp" alt=""/>
                    <p class="post-desc-text">最大60日のお試しトライアル期間
                    <p class="post-details-text">ソファは30日間、ペットラウンジは60日間。気に入らない場合は、期間内なら全額返金。 
                    <br>※適用条件は「詳しく見る」をクリックしてご確認ください</p>   
                </div>
            </div>
            <div class="row justify-content-center py-3">
                <div class="col-md-3 text-center">
                    <a class="btn btn-primary btn-details-lg" href="https://ambientlounge.co.jp/30days-trial/" target="_self">詳しく見る</a>
                </div>
            </div>
        </div>
    </div>
    <!--container details end-->

    <!--container lookbook start-->
    <div class="container-fluid container-5 p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 text-center">
                    <p class="light-text-title">LOOKBOOK</p>
                </div>
                <div class="col-md-8 col-xs-12 text-center my-3">
                    <p class="light-text-subtitle">気になるお部屋のレイアウトや、どんなワンちゃんが利用しているか...などなど。ルックブックでチェックしてみましょう。</p>
                </div>
                <div class="col-xs-12 p-0">
                    <div class="row justify-content-center">
                        <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/lookbook_img_1.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/lookbook_img_2.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/lookbook_img_3.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/lookbook_img_4.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/lookbook_img_5.webp" style="width:100%" alt=""/></div>
                        <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/lookbook_img_6.webp" style="width:100%" alt=""/></div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center py-5">
                <div class="col-md-3 text-center">
                    <a class="btn btn-primary btn-details-lg" href="https://ambientlounge.co.jp/lookbook/?size=dog" target="_self">LOOKBOOKを見る</a>
                </div>
            </div>
        </div>
    </div>
    <!--container lookbook end-->
    <script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/jquery-3.6.1.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/float-top-bar.js"></script>
<?php
get_footer();
?>