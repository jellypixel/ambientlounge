<?php
/*
	Template Name: Sofa-Sample
*/
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/adjustment.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/main-dark.all.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/sofa-sample.css?v=3"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/top-bar.css?v=2"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto Sans JP:100,200,300,400,500,600,700|Noto Sans JP:600|Open Sans:600|Open Sans|Open Sans:700&amp;subset=latin-ext&amp;display=swap" media="all" onload="this.media='all'">
<body>
<style>
    table tr:nth-child(2n) td{
        background-color: #fff;
    }
    td{
        border-top: 0;
    }
</style>
<div class="header-inner"></div>
<div class="top-bar-new d-flex swatch-table-header justify-content-between" id="top-bar-new">
    <div class="top-bar-new-title" id="top-bar-new-title"></div>
    <a class="btn btn-primary top-bar-new-btn" id="top-bar-new-btn" href="/products/swatch/sofa-swatch-order/">生地サンプルを請求</a>
</div>
<div id="main-content">
    <div class="container-fluid bg-white pb-5">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <p class="top-banner-text top-banner-heading-lg">生地サンプル
                    <br>Sofa</p>
                <p class="top-banner-text top-banner-heading-sm desc-text-lg">家族と一緒にじっくり悩みたいソファ生地。
                    <br>高感度でリッチな風合いのものから、ペットとの暮らしに最適な高機能生地まで幅広くラインアップ。</p>
                <p class="top-banner-text top-banner-heading-sm desc-text-sm">家族と一緒にじっくり悩みたいソファ生地。
                    <br>高感度でリッチな風合いのものから、ペットとの暮ら
                    <br>しに最適な高機能生地まで幅広くラインアップ。</p>
            </div>
        </div>
    </div>

    <!--container bg-light start-->
    <div class="container-fluid bg-light py-5">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <div class="col-lg-8 col-md-10">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-5 col-xs-12 order-lg-1 order-2">
                            <img class="w-100 p-2" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/sofa-banner-top.webp"/>
                        </div>
                        <div class="col-lg-7 col-xs-12 order-lg-2 order-1 text-lg-start m-auto text-center p-2">
                            <p class="sofa-sample-top-heading">アンビエントラウンジの
                                <br>こだわり生地</p>
                            <p class="sofa-sample-top-text text-lg-start text-center">形崩れやよれに強いキルティング仕様。
                                <br>生地自体に重厚感がうまれ、柔らかい座り心地が実現します。</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xs-12 pt-5 px-2">
                    <p class="sample-panel-title mb-4">キルティングのメリット</p>
                    <div class="product-panel-white">
                        <div class="row justify-content-center text-center checked-panel">
                            <div class="col-md-4 col-xs-7 p-2">
                                <p class="checked-text"><img class="icon-checked" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/icon-check.png"/>クッション性</p>
                            </div>
                            <div class="col-md-4 col-xs-5 p-2">
                                <p class="checked-text"><img class="icon-checked" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/icon-check.png"/>耐久性</p>
                            </div>
                            <div class="col-md-4 col-xs-12 p-2">
                                <p class="checked-text"><img class="icon-checked" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/icon-check.png"/>︎シワになりにくい</p>
                            </div>
                            <div class="col-md-4 col-xs-8 p-2">
                                <p class="checked-text"><img class="icon-checked" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/icon-check.png"/>形崩れしにくい</p>
                            </div>
                            <div class="col-md-4 col-xs-4 p-2">
                                <p class="checked-text"><img class="icon-checked" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/icon-check.png"/>軽量</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xs-12 pt-5 px-2">
                    <p class="sample-panel-title mb-4">２種類をご用意</p>
                    <div class="product-panel-white">
                        <div class="row justify-content-center text-start">
                            <div class="col-md-5 col-xs-12 p-2 my-auto">
                                <p class="sample-panel-title text-md-start text-center">インテリア</p>
                                <p class="sofa-sample-top-text">重厚感と弾力感のある丈夫な織りのファブリックコレクション。カラーによって様々な色や素材が折り重なった存在感あふれる風合いが楽しめます。</p>
                            </div>
                            <div class="col-md-4 col-xs-12 my-auto">
                                <img class="interior-img-1" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/sofa-interior-1.webp"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xs-12 px-2">
                    <div class="product-panel-white">
                        <div class="row justify-content-center text-center">
                            <div class="col-md-5 col-xs-12 p-2 my-auto">
                                <p class="sample-panel-title text-md-start text-center">インテリア＋プラス</p>
                                <p class="sofa-sample-top-text">ペットやお子様との暮らしに最適。引っ掻きや汚れに強い撥水高機能ファブリック</p>
                            </div>
                            <div class="col-md-4 col-xs-12 my-auto">
                                <img class="interior-img-2" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/sofa-interior-2.webp"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--container bg-light end-->

    <!--container bg-dark start-->
    <div class="container-fluid bg-black">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <p class="ref-spec-title text-white">カラーバリエーション</p>
                <div class="col-lg-8 col-md-10 col-xs-12 px-3">
                    <p class="ref-spec-subtitle text-white">シックなモノトーン</p>
                    <div class="sofa-banner-panel sofa-banner-bg-1">
                        <div class="row justify-content-center text-center">
                            <div class="col-lg-6 col-md-4 col-xs-12 p-0">
                                <div class="sofa-banner-img-1"></div>
                            </div>
                            <div class="col-lg-6 col-md-8 col-xs-12 p-2 m-auto">
                                <div class="row sofa-color-panel">
                                    <p class="sofa-color-title">インテリア</p>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-soft-grey.webp" />
                                        <p class="sofa-color-caption">ソフトグレー</p>
                                    </div>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-dark-grey.webp" />
                                        <p class="sofa-color-caption">ダークグレー</p>
                                    </div>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-light-grey.webp" />
                                        <p class="sofa-color-caption">ライトグレー</p>
                                    </div>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-black.webp" />
                                        <p class="sofa-color-caption">ブラック</p>
                                    </div>
                                </div>
                                <div class="row sofa-color-panel">
                                    <p class="sofa-color-title">インテリア＋プラス</p>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-wr-silver.webp" />
                                        <p class="sofa-color-caption">シルバー・
                                            <br>インテリアプラス</p>
                                    </div>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-wr-black.webp" />
                                        <p class="sofa-color-caption">ブラック・
                                            <br>インテリアプラス</p>
                                    </div>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-wr-bordergrey.webp" />
                                        <p class="sofa-color-caption">ボーダーグレー・
                                            <br>インテリアプラス</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center text-center bg-dark-grey">
                <div class="col-lg-8 col-md-10 col-xs-12 px-3">
                    <p class="ref-spec-subtitle text-white">温かみのあるブラウン</p>
                    <div class="sofa-banner-panel sofa-banner-bg-2">
                        <div class="row justify-content-center text-center">
                            <div class="col-lg-6 col-md-8 col-xs-12 m-auto order-md-1 order-2 p-2">
                                <div class="row sofa-color-panel-left">
                                    <div class="col-md-6 col-xs-12 p-2">
                                        <div class="row justify-content-center">
                                            <p class="sofa-color-title">インテリア</p>
                                            <div class="col-md-6 col-xs-6 p-2">
                                                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-brown.webp" />
                                                <p class="sofa-color-caption">ブラウン</p>
                                            </div>
                                            <div class="col-md-6 col-xs-6 p-2">
                                                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-beige.webp" />
                                                <p class="sofa-color-caption">ベージュ</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 p-2">
                                        <div class="row justify-content-center">
                                            <p class="sofa-color-title">インテリア＋プラス</p>
                                            <div class="col-md-6 col-xs-6 p-2">
                                                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-wr-lightbeige.webp" />
                                                <p class="sofa-color-caption">ライトベージュ・
                                                    <br>インテリアプラス</p>
                                            </div>
                                            <div class="col-md-6 col-xs-6 p-2">
                                                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-wr-beige.webp" />
                                                <p class="sofa-color-caption">ベージュ・
                                                    <br>インテリアプラス</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-4 col-xs-12 order-md-2 order-1 p-0">
                                <div class="sofa-banner-img-2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center text-center">
                <div class="col-lg-8 col-md-10 col-xs-12 px-3">
                    <p class="ref-spec-subtitle text-white">多彩なアクセントカラー</p>
                    <div class="sofa-banner-panel sofa-banner-bg-3">
                        <div class="row justify-content-center text-center">
                            <div class="col-lg-6 col-md-4 col-xs-12 p-0">
                                <div class="sofa-banner-img-3"></div>
                            </div>
                            <div class="col-lg-6 col-md-8 col-xs-12 p-2 m-auto">
                                <div class="row sofa-color-panel">
                                    <p class="sofa-color-title">インテリア</p>
                                    <div class="col-lg-2p4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-purple.webp" />
                                        <p class="sofa-color-caption">パープル</p>
                                    </div>
                                    <div class="col-lg-2p4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-pink.webp" />
                                        <p class="sofa-color-caption">ピンク</p>
                                    </div>
                                    <div class="col-lg-2p4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-red.webp" />
                                        <p class="sofa-color-caption">レッド</p>
                                    </div>

                                    <div class="col-lg-2p4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-blue.webp" />
                                        <p class="sofa-color-caption">ブルー</p>
                                    </div>
                                    <div class="col-lg-2p4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-lime.webp" />
                                        <p class="sofa-color-caption">ライム</p>
                                    </div>
                                </div>
                                <div class="row sofa-color-panel">
                                    <p class="sofa-color-title">インテリア＋プラス</p>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-wr-red.webp" />
                                        <p class="sofa-color-caption">レッド・
                                            <br>インテリアプラス</p>
                                    </div>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-wr-blue.webp" />
                                        <p class="sofa-color-caption">ボーダーブルー・
                                            <br>インテリアプラス</p>
                                    </div>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-wr-borderred.webp" />
                                        <p class="sofa-color-caption">ボーダーレッド・
                                            <br>インテリアプラス</p>
                                    </div>
                                    <div class="col-md-3 col-xs-4 p-2">
                                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/color-wr-lime.webp" />
                                        <p class="sofa-color-caption">ライム・
                                            <br>インテリアプラス</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--container bg-dark end-->

    <!--container bg-light start-->
    <div class="container-fluid bg-light">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <div class="col-xs-12">
                    <p class="ref-spec-title">素材一覧</p>
                </div>
                <div class="col-xs-12">
                    <p class="ref-spec-subtitle">インテリア</p>
                </div>
                <div class="col-xs-12 p-0">
                    <table class="table table-borderless ref-spec-table-2">
                        <tr class="bg-light text-start">
                            <th>カラー</th>
                            <th>素材</th>
                        </tr>
                        <tr class="bg-white text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-beige.webp"/>ベージュ</th>
                            <td class="swatch-table-desc table-text-lg">ポリエステル 87% / アクリル 13%</td>
                            <td class="swatch-table-desc table-text-sm">ポリエステル 87% /<br> アクリル 13%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-black.webp"/>ブラック</th>
                            <td class="swatch-table-desc table-text-lg">ポリエステル 70% / ビスコース 30%</td>
                            <td class="swatch-table-desc table-text-sm">ポリエステル 70% /<br> ビスコース 30%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-light-grey.webp"/>ライトグレー</th>
                            <td class="swatch-table-desc table-text-lg">ポリエステル 56% / ビスコース 29% / コットン 15%</td>
                            <td class="swatch-table-desc table-text-sm">ポリエステル 56% /<br> ビスコース 29% /<br> コットン 15%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-dark-grey.webp"/>ダークグレー</th>
                            <td class="swatch-table-desc table-text-lg">ポリエステル 90% / アクリル 10%</td>
                            <td class="swatch-table-desc table-text-sm">ポリエステル 90% /<br> アクリル 10%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-lime.webp"/>ライム</th>
                            <td class="swatch-table-desc">ポリエステル 100%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-blue.webp"/>ブルー</th>
                            <td class="swatch-table-desc">ポリエステル 100%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-purple.webp"/>パープル</th>
                            <td class="swatch-table-desc table-text-lg">ポリエステル 63% / ビスコース 37%</td>
                            <td class="swatch-table-desc table-text-sm">ポリエステル 63% /<br> ビスコース 37%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-pink.webp"/>ピンク</th>
                            <td class="swatch-table-desc table-text-lg">ポリエステル 63% / ビスコース 37%</td>
                            <td class="swatch-table-desc table-text-sm">ポリエステル 63% /<br> ビスコース 37%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-red.webp"/>レッド</th>
                            <td class="swatch-table-desc">ポリエステル 100%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-brown.webp"/>ブラウン</th>
                            <td class="swatch-table-desc table-text-lg">ポリエステル 87% / アクリル 13%</td>
                            <td class="swatch-table-desc table-text-sm">ポリエステル 87% /<br> アクリル 13%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-soft-grey.webp"/>ンフトグレー</th>
                            <td class="swatch-table-desc table-text-lg">ビスコース 75% / ポリエステル 14% / リネン 11%</td>
                            <td class="swatch-table-desc table-text-sm">ビスコース 75% /<br> ポリエステル 14% /<br> リネン 11%</td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-12">
                    <p class="ref-spec-subtitle">インテリア＋プラス</p>
                </div>
                <div class="col-xs-12 p-0">
                    <table class="table table-borderless ref-spec-table-2">
                        <tr class="bg-light text-start">
                            <th>カラー</th>
                            <th>素材</th>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="d-flex swatch-table-header"><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-water-resistant-blue.webp"/>ボーダーブルー・
                                <br>インテリアプラス</th>
                            <td class="swatch-table-desc">原着アクリル100%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th class="d-flex swatch-table-header"><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-water-resistant-borderred.webp"/>ボーダーレッド・
                                <br>インテリアプラス</th>
                            <td class="swatch-table-desc">原着アクリル100%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="d-flex swatch-table-header"><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-water-resistant-bordergrey.webp"/>ボーダーグレー・
                                <br>インテリアプラス</th>
                            <td class="swatch-table-desc">原着アクリル100%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th class="d-flex swatch-table-header"><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-water-resistant-beige.webp"/>ベージュ・
                                <br>インテリアプラス</th>
                            <td class="swatch-table-desc">原着アクリル100%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="d-flex swatch-table-header"><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-water-resistant-black.webp"/>ブラック・
                                <br>インテリアプラス</th>
                            <td class="swatch-table-desc">原着アクリル100%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th class="d-flex swatch-table-header"><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-water-resistant-lime.webp"/>ライム・
                                <br>インテリアプラス</th>
                            <td class="swatch-table-desc">原着アクリル100%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="d-flex swatch-table-header"><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-water-resistant-silver.webp"/>シルバー・
                                <br>インテリアプラス</th>
                            <td class="swatch-table-desc">原着アクリル100%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th class="d-flex swatch-table-header"><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-water-resistant-red.webp"/>レッド・
                                <br>インテリアプラス</th>
                            <td class="swatch-table-desc">原着アクリル100%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="d-flex swatch-table-header"><img class="swatch-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-sofa/swatch-water-resistant-lightbeige.webp"/>ライトベージュ・
                                <br>インテリアプラス</th>
                            <td class="swatch-table-desc">原着アクリル100%</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/js/float-top-bar.js"></script>
<?php
get_footer();
?>