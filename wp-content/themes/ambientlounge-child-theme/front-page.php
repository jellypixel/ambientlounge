<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/bootstrap.min.css"/>
<?php
    get_header();
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/adjustment.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/font/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/main-dark.all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/top.css"/>

<div class="container-fluid">
    <div class="container-dark">
        <div class="frontpage-slider-wrapper">
            <?php echo do_shortcode( '[smartslider3 slider="2"]' ); ?>
            <?php echo do_shortcode( '[smartslider3 slider="3"]' ); ?>
        </div>
    </div>
</div>

<section id="top-banner">
    <div class="container-fluid p-0">
        <div class="container-dark top-container">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-6 p-0">
                    <a class="text-white text-decoration-none" href="/sofa/">
                        <div class="top-banner-img-1">
                            <p class="top-link-text">SOFA･MODULAR<img class="chevron-right-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/arrow-right.png"></p>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-6 p-0">
                    <a class="text-white text-decoration-none" href="/pet/">
                        <div class="top-banner-img-2">
                            <p class="top-link-text">PET LOUNGE<img class="chevron-right-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/arrow-right.png"></p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="frontpage-content-wrapper normal-container-width gray-bg">
    <div class="site-content">
        <div class="entry-content">
            <?php the_content(); ?>        
        </div>
    </div>
</div>

<!--container al-intro start-->
<div class="container-fluid bg-white p-3">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="col-xs-12 px-5 text-center">
                <p class="light-text-title">アンビエントラウンジについて</p>
            </div>
            <div class="col-xs-12 col-md-10 p-3 text-center">
                <p class="al-intro-desc desc-text-lg">わたしたちは " 家族をつなぐ " インテリアブランドとして誕生しました
                    <br>人に優しく、ペットに優しい
                    <br>全ての家族にとって快適で安全な商品をお届けします</p>

                <p class="al-intro-desc desc-text-sm">わたしたちは " 家族をつなぐ "
                    <br>インテリアブランドとして誕生しました
                    <br>人に優しく、ペットに優しい
                    <br>全ての家族にとって
                    <br>快適で安全な商品をお届けします
                </p>
            </div>
            <div class="col-xs-12 col-md-10 p-2 text-center">
                <div class="desc-text-lg">
                    <p class="al-intro-link">
                        <a href="/about-us/">ブランドストーリー <i class="bi-chevron-right"></i></a>
<!--                        <a href="#">アンビエントラウンジジャパン <i class="bi-chevron-right"></i></a>-->
                        <a href="/stories/">ブログ <i class="bi-chevron-right"></i></a>
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-md-10 p-2 text-center">
                <div class="desc-text-lg">
                    <p class="al-intro-link">
                        <a href="/sustainability/">サステナビリティの取り組み <i class="bi-chevron-right"></i></a>
                        <a href="/futusato/">ふるさと納税 <i class="bi-chevron-right"></i></a>
                    </p>
                </div>
                <div class="desc-text-sm">
                    <p class="al-intro-link"><a href="/about-us/">ブランドストーリー <i class="bi-chevron-right"></i></a></p>
                    <br><p class="al-intro-link"><a href="/stories/">ブログ <i class="bi-chevron-right"></i></a></p>
                    <br><p class="al-intro-link"><a href="/sustainability/">サステナビリティの取り組み <i class="bi-chevron-right"></i></a></p>
                    <br><p class="al-intro-link"><a href="/furusato/">ふるさと納税 <i class="bi-chevron-right"></i></a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid bg-white p-3">
    <div class="container-dark mb-5">
        <div class="row justify-content-center">
            <div class="col-xs-12 text-center">
                <p class="light-text-title">日本品質へのこだわり</p>
            </div>
            <div class="col-md-4 col-sm-5 col-xs-7 p-2 py-5"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/al_logo_lg.webp" alt=""/></div>
            <div class="w-100"></div>
            <div class="col-xs-12 col-md-10 p-2 text-center">
                <p class="al-intro-text">ゆったりとした時間が流れ、QOLを大切にするオーストラリア発のアンビエントラウンジが日本に上陸したのは2015年。
                    <br>機能的でユニークなデザインはそのままに、ジャパン社では日本発信の商品開発や日本品質にこだわっています。
                    <br>試行錯誤を繰り返したどり着いた、圧倒的な座り心地のソファやペットベッドはジャパン社だけの独自品質。
                    <br>ローカル産業との取り組みを始め、過疎化が進む "みなかみ町" に新しいビジネスモデルを生み出したいと考えています。
                    <br>ご注文いただいたソファやベッドは全て、地元みなかみ町のスタッフが１点１点心を込めて仕上げています。</p>
            </div>
        </div>
    </div>
</div>
<!--container al-intro end-->

<!--container lookbook start-->
<div class="container-fluid container-5 bg-light p-3">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="col-xs-8 col-md-12 p-2 order-2 order-md-1">
                <p class="au_design_title">オリジナルの生活スタイルを</p>
            </div>
            <div class="col-xs-4 col-md-2 p-2 my-auto text-end order-1 order-md-2">
                <img class="au_design_logo" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/au_design_logo.webp" alt="...">
            </div>
            <div class="col-xs-12 col-sm-8 col-md-6 p-2 order-3 order-md-3">
                <p class="al-intro-text text-start">ゆったりと時間が流れるオーストラリアの生活の中でも、模様替えは頻繁。
                    <br>気分に合わせていつでも生活空間を替えたいけど、普通の家具は重くて面倒。
                    <br>ビーズクッションもあるけど、オシャレじゃない。
                    <br>そんな願いを叶えたのが軽量で美しいプレミアムソフトソファ。</p>
            </div>
            <div class="col-md-12 p-0 mt-3 order-4">
                <div class="row justify-content-center">
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_1.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_2.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_3.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_4.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_5.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_6.webp" style="width:100%" alt=""/></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center py-5">
            <div class="col-md-3 text-center"><a  class="btn btn-primary btn-details-lg" href="https://ambientlounge.co.jp/lookbook/">LOOKBOOKを見る</a></div>
        </div>
    </div>
</div>
<!--container lookbook end-->

<div class="container-fluid bg-white p-3">
    <div class="container-dark store-info-container">
        <div class="row justify-content-center">
            <div class="col-xs-12 text-center">
                <p class="store-info-title">取り扱い店舗のご案内</p>
            </div>
            <div class="col-xs-12 col-md-10 text-center">
                <p class="store-info-text desc-text-lg">現物に実際に座って検討したいというお客様のために、
                    <br>お取り扱いのある店舗をご案内させていただきます。</p>
                <p class="store-info-text desc-text-sm">現物に実際に座って検討したいというお客様のために、お取り扱いのある店舗をご案内させていただきます。</p>
            </div>
            <div class="col-xs-12 col-md-10 text-center">
                <a class="store-info-link" href="https://ambientlounge.co.jp/retail-locations/">取扱店舗一覧 <i class="bi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/bootstrap.bundle.min.js"></script>
<?php
    get_footer();
?>