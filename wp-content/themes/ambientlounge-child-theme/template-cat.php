<?php
/*
	Template Name: Cat
*/
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/adjustment.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/font/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/main-dark.all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/banner-bottom.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/lp-cat.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/top-bar.css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto Sans JP:100,200,300,400,500,600,700|Noto Sans JP:600|Open Sans:600|Open Sans|Open Sans:700&amp;subset=latin-ext&amp;display=swap" media="all" onload="this.media='all'">
<div class="header-inner"></div>
<div class="top-bar-new d-flex justify-content-between" id="top-bar-new">
    <div class="top-bar-new-title" id="top-bar-new-title">PET LOUNGE</div>
    <a class="btn btn-primary top-bar-new-btn" id="top-bar-new-btn" href="https://ambientlounge.co.jp/products/cat-bed/cat/">商品ページを見る</a>
</div>
<section id="banner-top">
    <div class="container-fluid bg-white">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <div class="top-banner">
                    <div class="top-banner-title-lg">PET LOUNGE</div>
                    <div class="top-banner-subtitle">耐久性に優れ、いつも清潔、そして極上の寝心地。</div>
                    <div class="top-banner-heading-lg">創ったのは､最高の家族の<br>最高の居場所</div>
                </div>
            </div>
            <div class="row justify-content-center text-center">
                <p class="satisfaction-title desc-text-lg">3年で10,000台の販売実績<br>動物専門家が推奨する愛猫のためのベッド</p>
                <p class="satisfaction-title desc-text-sm">3年で10,000台の販売実績
                    <br>動物専門家が推奨する
                    <br>愛猫のためのベッド
                </p>
                <img class="satisfaction-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog_new/satisfaction.webp"/>
            </div>
        </div>
    </div>
</section>
<!--container bg-black start-->
<div class="container-fluid bg-grey">
    <div class="container-dark pt-5">
        <div class="row banner-row-light">
            <div class="col-xs-12 col-md-6 p-0 order-2 order-md-1">
                <div class="banner-img-1"></div>
            </div>
            <div class="col-xs-12 col-md-6 banner-text-1 order-1 order-md-2">
                <p class="banner-title-lg text-gradient-blue desc-text-lg">極上の隠れ家
                    <br>フーディー
                    <br>ラウンジ</p>
                <p class="banner-title-lg text-gradient-blue desc-text-sm">極上の隠れ家
                    <br>フーディーラウンジ</p>
                <p class="banner-subtitle desc-text-lg">暗くて狭いところが大好きな
                    <br>猫達の習性を熟知したフーディーで
                    <br>自分だけの秘密のお部屋を。
                    <br>安心して落ち着けるベッドに滑り込むと
                    <br>たちまちぐっすり。
                    <br>ゆらゆら揺れるボンボンで
                    <br>遊びも楽しいベッドです。</p>
                <p class="banner-subtitle desc-text-sm">暗くて狭いところが大好きな猫達の習性を熟知した
                    <br>フーディーで自分だけの秘密のお部屋を。
                    <br>安心して落ち着けるベッドに滑り込むと
                    <br>たちまちぐっすり。
                    <br>ゆらゆら揺れるボンボンで遊びも楽しいベッドです。
                </p>
            </div>
        </div>
        <div class="row banner-row-light">
            <div class="col-xs-12 col-md-6 banner-text-2">
                <p class="banner-title-lg text-gradient-pink banner-desc-m-right">オールシーズン
                    <br>清潔な設計
                </p>
                <p class="banner-subtitle desc-text-lg">裏地が完全防水の高機能なカバーは
                    <br>ファスナーでぐるりと取り外して
                    <br>洗濯機で洗えます｡
                    <br>季節や機能でカバーを交換できるから
                    <br>春夏秋冬ずっと快適、ずっと清潔。</p>
                <p class="banner-subtitle desc-text-sm">裏地が完全防水の高機能なカバーは
                    <br>ファスナーでぐるりと取り外して洗濯機で洗えます｡
                    <br>季節や機能でカバーを交換できるから
                    <br>春夏秋冬ずっと快適、ずっと清潔。</p>
            </div>
            <div class="col-xs-12 col-md-6 m-auto">
                <img class="banner-img-2" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/change-cover.gif" alt=""/>
            </div>
        </div>
        <div class="row banner-row-light banner-padding">
            <div class="col-xs-12 col-md-5 m-auto order-2">
                <img class="cat-img-float" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/cat-circle.webp" alt=""/>
            </div>
            <div class="col-xs-12 col-md-7 p-0 order-1">
                <p class="banner-title-lg text-gradient-pink">ひっかきに強い
                    <br>圧倒的な耐久性</p>
                <p class="banner-subtitle desc-text-lg">
                    <br>本体ベース表面にはアウトドア用品にも
                    <br>用いられる 1200 デニールの
                    <br>高密度ソフトナイロンを使用し、
                    <br>簡単には壊されない強度な設計を実現。
                    <br>さらに裏地には ラバーシートを
                    <br>張り合わせダブルのコーティングで
                    <br>汚れや接水性にも強いベッドが誕生しました。
                </p>
                <p class="banner-subtitle desc-text-sm">本体ベース表面にはアウトドア用品にも用いられる1200 デニールの高密度ソフトナイロンを使用し、簡単には壊されない強度な設計を実現。さらに裏地には ラバーシートを張り合わせダブルのコーティングで汚れや接水性にも強いベッドが誕生しました。</p>
            </div>
            <div class="col-xs-12 p-0 order-3">
                <div class="row banner-small panel-float">
                    <div class="col-sm-3 col-xs-5 p-1 m-auto"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/zip_img.webp" alt=""/></div>
                    <div class="col-sm-9 col-xs-7 p-1 text-start m-auto">
                        <p class="panel-text-bold">安全のために</p>
                        <p class="panel-text">本体底にあるファスナーには、誤って噛んだりしないように、スライダートップをあえて取り付けない設計になっています。お手元のゼムクリップなどをご利用になって簡単に開けることができます。</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row banner-row-dark banner-padding">
            <div class="col-xs-12 p-0">
                <p class="banner-title-lg text-gradient-blue desc-text-lg">清潔・睡眠・体温調整で<br>健康を徹底的にサポート</p>
                <p class="banner-title-lg text-gradient-blue desc-text-sm">清潔・睡眠・体温調整で健康を徹底的にサポート</p>
            </div>
            <div class="col-xs-12 p-0">
                <div class="row product-list-lg">
                    <div class="col-md-4 col-xs-12 p-2 p-md-4 p-xl-5 m-auto text-center text-md-start">
                        <p class="panel-text-title">清潔</p>
                        <p class="text-desc-lg banner-desc-m-right desc-text-lg">本体は撥水加工、カバーは洗って清潔に保てるので、<br>ノミ、ダニやバイ菌の温床にならず、<br>おしっこやよだれにも安心です</p>
                        <p class="text-desc-lg banner-desc-m-right desc-text-sm">本体は撥水加工、カバーは洗って清潔に保てるので、ノミ、ダニやバイ菌の温床にならず、おしっこやよだれにも安心です</p>
                    </div>
                    <div class="col-md-8 col-xs-12 p-0 m-auto">
                        <img class="w-100" style="border-radius: 15px;" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/product_img_lg_1.webp" alt=""/>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 p-0">
                <div class="row product-list-lg">
                    <div class="col-md-7 col-xs-12 p-0 m-auto order-2 order-md-1">
                        <img class="w-100" style="border-radius: 15px;" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/product_img_lg_2.webp" alt=""/>
                    </div>
                    <div class="col-md-5 col-xs-12 p-2 p-md-4 p-xl-5 m-auto text-center text-md-start order-1 order-md-2">
                        <p class="panel-text-title text-center text-md-end">睡眠</p>
                        <p class="text-desc-lg text-end desc-text-lg">眠りが浅い犬達がぐっすり熟睡できるように、<br>独自開発の充填は人用マットレスと同グレード<br>健康を左右する良質な睡眠を<br>パピーからシニアまで末長くサポートします</p>
                        <p class="text-desc-lg text-center desc-text-sm">眠りが浅い犬達がぐっすり熟睡できるよう
                            に、独自開発の充填は人用マットレスと同
                            グレード健康を左右する良質な睡眠をパ
                            ピーからシニアまで末長くサポートします</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 p-0">
                <div class="row product-list-lg">
                    <div class="col-md-4 col-xs-12 p-2 p-md-4 p-xl-5 m-auto text-center text-md-start">
                        <p class="panel-text-title">体温調整</p>
                        <p class="text-desc-lg banner-desc-m-right desc-text-lg">通気性に優れた生地で内部に湿気をため込まず、<br>睡眠環境を最適な温度に保ちます</p>
                        <p class="text-desc-lg banner-desc-m-right desc-text-sm">通気性に優れた生地で内部に湿気をため込まず、睡眠環境を最適な温度に保ちます</p>
                    </div>
                    <div class="col-md-8 col-xs-12 p-0 m-auto">
                        <img class="w-100" style="border-radius: 32px;" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/product_img_lg_3.webp" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--container bg-black end-->

<!--container colors start-->
<div class="container-fluid bg-white p-3 p-xl-5">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="col-xs-12 p-0 text-center">
                <p class="light-text-title">インテリアや季節に合わせて
                    <br>あなたらしさが進む
                    <br>豊富なバリエーション</p>
            </div>
            <div class="col-xs-12 p-0 text-center">
                <p class="light-text-subtitle pb-xl-5">季節、デザイン、機能で選べるバリエーションが豊富だから、インテリアのアップデートや衣替えも楽しめます。</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-3 col-xs-6 p-2 my-2 product-img-flex-end"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lp-cat-color-1.webp" alt=""></div>
            <div class="col-md-3 col-xs-6 p-2 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lp-cat-color-2.webp" alt=""></div>
            <div class="col-md-3 col-xs-6 p-2 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lp-cat-color-3.webp" alt=""></div>
            <div class="col-md-3 col-xs-6 p-2 my-2 product-img-flex-end"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lp-cat-color-4.webp" alt=""></div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-3 col-xs-6 p-2 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lp-cat-color-5.webp" alt=""/></div>
            <div class="col-md-3 col-xs-6 p-2 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lp-cat-color-6.webp" alt=""/></div>
            <div class="col-md-3 col-xs-6 p-2 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lp-cat-color-7.webp" alt=""/></div>
            <div class="col-md-3 col-xs-6 p-2 my-2"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lp-cat-color-8.webp" alt=""/></div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xs-12 p-0 text-center lh-lg title-m-top">
                <p class="light-text-title desc-text-lg">洗練されたデザインで<br>ワンランク上のライフスタイルを</p>
                <p class="light-text-title desc-text-sm">洗練されたデザインで<br>ワンランク上の<br>ライフスタイルを</p>
            </div>
            <div class="col-xs-12 p-0 text-center">
                <p class="light-text-subtitle">世界中の愛犬家やセレブリティに愛される高級ペットベッドは家族みんなの幸せの中心にあります</p>
            </div>
            <div class="col-xs-12 col-md-8 p-0 text-center">
                <img class="cat-banner-family" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/cat-banner-family.webp" alt="Cat Banner"/>
            </div>
        </div>

    </div>
</div>
<!--container colors end-->

<!--container filling material start -->
<div class="container-fluid bg-light p-3 p-xl-5">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="col-xs-12 p-0 text-center">
                <p class="light-text-title">フィリングについて</p>
                <p class="light-text-subtitle">ペットの好みや性格、年齢に合わせて3種類のフィリングをご用意</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xs-12 col-md-4 p-3 text-center">
                <p class="text-common-bold">ベーシック</p>
                <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/filling_img_1.webp" alt=""/>
                <p class="panel-text-bold text-black">柔らかさ</p>
                <img class="p-2 w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/star_1.webp" alt=""/>
                <p class="text-grey">硬め、軽量<br>&emsp;</p>
                <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/recommend_1.webp" alt=""/>
                <p class="text-common-regular text-start">プレミアムビーズ充填で仕上げました。エントリープライスのペットラウンジ。硬めがお好みのペット達にもおすすめです。</p>
            </div>
            <div class="col-xs-12 col-md-4 p-3 text-center">
                <p class="text-common-bold">スタンダード</p>
                <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/filling_img_2.webp" alt=""/>
                <p class="panel-text-bold text-black">柔らかさ</p>
                <img class="p-2 w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/star_2.webp" alt=""/>
                <p class="text-grey">柔らかめ、包み込む、体圧分散、関節サポート、<br>弾力性、軽量</p>
                <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/recommend_2.webp" alt=""/>
                <p class="text-common-regular text-start">独自開発のハイブリッド充填で仕上げました。反発力と弾力性に優れ、足腰が弱りがちなシニア達の起き上がりを助けます。体が心地よく沈み、柔らか過ぎず、硬過ぎない、最適なフィット感で人気No.1のフィリングです。</p>
            </div>
            <div class="col-xs-12 col-md-4 p-3 text-center">
                <p class="text-common-bold">プレミアム</p>
                <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/filling_img_3.webp" alt=""/>
                <p class="panel-text-bold text-black">柔らかさ</p>
                <img class="p-2 w-25" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/star_3.webp" alt=""/>
                <p class="text-grey">フカフカ、包み込む、体圧分散、関節サポート、<br>弾力性、形状記憶、衝撃吸収</p>
                <img class="p-2 w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/recommend_3.webp" alt=""/>
                <p class="text-common-regular text-start">独自開発の低反発ハイブリッド充填で仕上げました。まるでエアーベッドのような極上感でペット達はたちまち虜に。自分の体に合わせてじんわりと沈み込み、最高の睡眠をサポート。体圧分散性に加え、反発力と弾力性により、特定部位への負担を避け体の痛みを軽減する働きが期待できます。また、高い衝撃吸収力で元気よく飛び乗る<span class="text-orange">子猫</span>達の関節にも安心です。</p>
            </div>
        </div>
    </div>
</div>
<!--container filling material end-->

<!--container al-intro start-->
<div class="container-fluid bg-white p-3 p-xl-5">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="col-xs-12 text-center">
                <p class="light-text-title">日本品質へのこだわり</p>
            </div>
            <div class="col-md-4 col-sm-5 col-xs-7 px-0 py-5"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/al_logo_lg.webp" alt=""/></div>
            <div class="w-100"></div>
            <div class="col-xs-12 col-md-10 p-0 mb-5">
                <p class="al-intro-text text-start text-md-center">ゆったりとした時間が流れ、QOLを大切にするオーストラリア発のアンビエントラウンジが日本に上陸したのは2015年。
                    <br>機能的でユニークなデザインはそのままに、ジャパン社では日本発信の商品開発や日本品質にこだわっています。
                    <br>試行錯誤を繰り返したどり着いた、圧倒的な座り心地のソファやペットベッドはジャパン社だけの独自品質。
                    <br>ローカル産業との取り組みを始め、過疎化が進む "みなかみ町" に新しいビジネスモデルを生み出したいと考えています。
                    <br>ご注文いただいたソファやベッドは全て、地元みなかみ町のスタッフが１点１点心を込めて仕上げています。</p>
            </div>
        </div>
    </div>
</div>
<!--container al-intro end-->

<!--container ranking start-->
<div class="container-fluid container-3 p-3 p-xl-5">
    <div class="container-dark">
        <div class="row justify-content-center text-center">
            <div class="col-xs-12 p-0">
                <p class="light-text-title">カスタマーレビュー</p>
            </div>
            <div class="col-xs-12 col-md-10 p-0">
                <div class="row ranking-panel">
                    <div class="col-xs-12 col-md-3 p-2 m-auto"><div class="review-img lp-cat-review-1"></div></div>
                    <div class="col-xs-12 col-md-9 p-2 text-start m-auto">
                        <p class="ranking-panel-text"><span class="ranking-panel-text-bold">ふわふわ！</span> カバーもお洗濯できるし、とにかく体にフィットして包み込んでくれるような自然な沈み方が気持ち良さそうです。<br>ベージュも持っていますが、柔らかいブルードリームの方がうちの子はお気に入り！</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-10 p-0">
                <div class="row ranking-panel">
                    <div class="col-xs-12 col-md-3 p-2 m-auto"><div class="review-img lp-cat-review-2"></div></div>
                    <div class="col-xs-12 col-md-9 p-2 text-start m-auto">
                        <p class="ranking-panel-text"><span class="ranking-panel-text-bold">インテリアにマッチ </span>  落ち着いた青いカラーがとってもすてきで一目惚れです♡早速飛び乗りしばらく動きませんでした🤣笑<br>ルナもアリスも気に入ったようで順番に使ったり、一緒に使ったりしています。</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-10 p-0">
                <div class="row ranking-panel">
                    <div class="col-xs-12 col-md-3 p-2 m-auto"><div class="review-img lp-cat-review-3"></div></div>
                    <div class="col-xs-12 col-md-9 p-2 text-start m-auto">
                        <p class="ranking-panel-text"><span class="ranking-panel-text-bold">待望のオーガニック素材 </span>  やわらかいクッションが大好きなので、直ぐに気に入ってくれました✨ オーガニックコットンでできており、保温性も高くこれから寒くなる季節にぴったりです。最近は起きると気持ち良さそうにへそ天しながら二度寝しているのでこちらまで嬉しくなります♡</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--container ranking end-->

<!--container details start-->
<div class="container-fluid container-4">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="col-xs-12 text-center">
                <p class="light-text-title desc-text-lg">ベッドのサイズにお悩みですか？</p>
                <p class="light-text-title desc-text-sm">ベッドのサイズに<br>お悩みですか？</p>
                <p class="light-text-subtitle">一覧からあなたのペットにあったベッドを探すことができます</p>
            </div>
            <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/size-img-s.webp" alt=""/>
                <h2 class="my-3">S</h2>
                <h6>子猫1頭</h6>
                <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト /
                    <br>オーガニックコットン /<br> フーディー</p>
                <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                <p class="text-grey-sm">ベーシック / スタンダード /
                    <br>プレミアム</p>
                <p class="text-price">¥25,900(税込)から</p>
                <a href="https://ambientlounge.co.jp/products/cat-bed/cat/?attribute_pa_product-type=standard&attribute_pa_size-cat=s-cat&attribute_pa_bed-selection=grey-base&attribute_pa_cover-selection=wolfgrey-cover&attribute_pa_filling=be-lux-prime" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
            </div>
            <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/size-img-m.webp" alt=""/>
                <h2 class="my-3">M</h2>
                <h6>成猫1頭</h6>
                <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト /
                    <br>オーガニックコットン</p>
                <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                <p class="text-grey-sm">ベーシック / スタンダード /
                    <br>プレミアム</p>
                <p class="text-price">¥34,900(税込)から</p>
                <a href="https://ambientlounge.co.jp/products/cat-bed/cat/?attribute_pa_product-type=standard&attribute_pa_size-cat=m-cat&attribute_pa_bed-selection=grey-base&attribute_pa_cover-selection=wolfgrey-cover&attribute_pa_filling=be-lux-prime" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
            </div>
            <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/size-img-l.webp" alt=""/>
                <h2 class="my-3">L</h2>
                <h6>多頭飼い主向け</h6>
                <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト /
                    <br>オーガニックコットン</p>
                <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                <p class="text-grey-sm">ベーシック / スタンダード /
                    <br>プレミアム</p>
                <p class="text-price">¥46,900(税込)から</p>
                <a href="https://ambientlounge.co.jp/products/cat-bed/cat/?attribute_pa_product-type=standard&attribute_pa_size-cat=l-cat&attribute_pa_bed-selection=grey-base&attribute_pa_cover-selection=wolfgrey-cover&attribute_pa_filling=be-lux-prime" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
            </div>
            <div class="col-lg-2 col-md-4 col-xs-5 m-1 text-center p-details">
                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/size-img-xl.webp" alt=""/>
                <h2 class="my-3">XXL</h2>
                <h6>多頭飼い主向け</h6>
                <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/color_list.webp" style="max-width: 3rem;" alt=""/>
                <p class="text-grey-sm mt-2"><strong>カバー</strong></p>
                <p class="text-grey-sm text-grey-sm-mh">ファーカバー / キルト</p>
                <p class="text-grey-sm mt-2"><strong>フィリング</strong></p>
                <p class="text-grey-sm">ベーシック / スタンダード /
                    <br>プレミアム</p>
                <p class="text-price">¥77,900(税込)から</p>
                <a href="https://ambientlounge.co.jp/products/cat-bed/cat/?attribute_pa_product-type=standard&attribute_pa_size-cat=xxl-cat&attribute_pa_bed-selection=grey-base&attribute_pa_cover-selection=wolfgrey-cover&attribute_pa_filling=be-lux-prime" class="link-primary text-decoration-none"><p class="buy-link">購入<i class="bi bi-chevron-right"></i></p></a>
            </div>
        </div>
        <div class="row text-center">
            <div class="col">
                <p class="light-text-title desc-text-lg">ペットラウンジを３D体験!</p>
                <p class="light-text-title desc-text-sm">ペットラウンジを<br>３D体験!</p>
                <p class="light-text-subtitle">クリック＆ドラッグで360度ご覧いただけます</p>
            </div>
        </div>
        <div class="row justify-content-center p-0">
            <div class="col-xs-12 col-md-9">
                <div class="cloudimage-360" data-folder="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/dog-lp-grey/"
                     data-filename="dog_lp_grey_{index}.jpg"
                     data-amount="20"
                     data-bottom-circle="true"
                     data-lazyload>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="row ar-panel-lg">
                <div class="col-xs-4 p-0">
                    <div class="ar-icon"></div>
                </div>
                <div class="col-xs-8 p-0 text-start my-auto">
                    <div><p class="ar-panel-title">拡張現実(AR)で見る</p></div>
                    <div><p class="ar-panel-subtitle">ベッドを自分のお部屋に置いてチェック</p></div>
                    <div class="ar-panel-btn-group">
                        <a class=""><img class="ar-btn-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/icons/ar-btn-s.png"></a>
                        <a class=""><img class="ar-btn-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/icons/ar-btn-m.png"></a>
                        <a class=""><img class="ar-btn-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/icons/ar-btn-l.png"></a>
                        <a class=""><img class="ar-btn-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/icons/ar-btn-xxl.png"></a>
                    </div>
                    <p class="ar-panel-caption">※iPhoneはSafari、AndroidはChromeで開いてください。</p>
                </div>
            </div>
            <div class="row ar-panel-sm">
                <div class="col-xs-3 p-2">
                    <div class="ar-icon"></div>
                </div>
                <div class="col-xs-9 p-2 text-start">
                    <div><p class="ar-panel-title">拡張現実(AR)で見る</p></div>
                    <div><p class="ar-panel-subtitle">ベッドを自分のお部屋に置いてチェック</p></div>
                </div>
                <div class="col-xs-12 p-0">
                    <div class="ar-panel-btn-group">
                        <a class="btn btn-primary ar-button ar-other mobile" href="https://ambientloungestaging.kinsta.cloud/scene-viewer.php?url=<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-s/gltf/Dog Pet Lounge_GLTF.gltf" rel="ar">Sサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                        <a class="btn btn-primary ar-button ar-other mobile" href="https://ambientloungestaging.kinsta.cloud/scene-viewer.php?url=<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-m/gltf/scene.gltf" rel="ar">Mサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                        <a class="btn btn-primary ar-button ar-other mobile" href="https://ambientloungestaging.kinsta.cloud/scene-viewer.php?url=<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-l/gltf/scene.gltf" rel="ar">Lサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                        <a class="btn btn-primary ar-button ar-other mobile" href="https://ambientloungestaging.kinsta.cloud/scene-viewer.php?url=<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-xxl/gltf/scene.gltf" rel="ar">XXLサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>

                        <a class="btn btn-primary ar-button ar-android mobile" href="intent://arvr.google.com/scene-viewer/1.0?file=<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-s/gltf/Dog Pet Lounge_GLTF.gltf&utm_source=devtools#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=https://developers.google.com/ar;end;" rel="ar">Sサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                        <a class="btn btn-primary ar-button ar-android mobile" href="intent://arvr.google.com/scene-viewer/1.0?file=<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-m/gltf/scene.gltf&utm_source=devtools#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=https://developers.google.com/ar;end;" rel="ar">Mサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                        <a class="btn btn-primary ar-button ar-android mobile" href="intent://arvr.google.com/scene-viewer/1.0?file=<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-l/gltf/scene.gltf&utm_source=devtools#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=https://developers.google.com/ar;end;" rel="ar">Lサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                        <a class="btn btn-primary ar-button ar-android mobile" href="intent://arvr.google.com/scene-viewer/1.0?file=<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-xxl/gltf/scene.gltf&utm_source=devtools#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=https://developers.google.com/ar;end;" rel="ar">XXLサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>

                        <a class="btn btn-primary ar-button ar-ios mobile" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-s/pet-lounge-s.usdz" rel="ar">Sサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                        <a class="btn btn-primary ar-button ar-ios mobile" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-m/Bed_M_001.usdz" rel="ar">Mサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                        <a class="btn btn-primary ar-button ar-ios mobile" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-l/bed_L.usdz" rel="ar">Lサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                        <a class="btn btn-primary ar-button ar-ios mobile" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/ar_files/pet-lounge-xxl/bed_xxl.usdz" rel="ar">XXLサイズ<img class="ar-icon-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/lp_dog_new/ar-icon-light.png"></a>
                    </div>
                    <p class="ar-panel-caption">※iPhoneはSafari、AndroidはChromeで開いてください。</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid container-4">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="support-panel">
                <div class="support-subtitle">お試しいただける</div>
                <div class="support-title">サポートサービス</div>
                <div class="row justify-content-center">
                    <div class="col-xs-12 col-md-3 text-center support-list">
                        <p class="support-list-title">１年間の<br>製品保証</p>
                        <p class="support-list-text">アンビエントラウンジでは全ての商品に対し、商品到着より１年間、製造過程における製品不良があった場合には
                            返送料弊社負担の上、修理等の対応を承ります。</p>
                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog_new/support-img-1.webp"/>
                    </div>
                    <div class="col-xs-12 col-md-3 text-center support-list">
                        <p class="support-list-title">送料無料</p>
                        <p class="support-list-text">サイト内全ての商品が一部地域を除き、送料無料です。沖縄及び北海道・九州地方の一部離島については送料が発生いたします。ご注文の翌々営業日までに送料についてのご案内メールをお送りいたします。</p>
                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog_new/support-img-2.webp"/>
                    </div>
                    <div class="col-xs-12 col-md-3 text-center support-list">
                        <p class="support-list-title">60日間<br>返金保証</p>
                        <p class="support-list-text">60日間じっくりと使ってみて、気に入らなければ返品OK！愛猫が使ってくれるか…インテリアに合わなかったら….そんな購入前の心配をゼロに。まずは60日のトライアル期間でじっくりと製品をお試しください！</p>
                        <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog_new/support-img-3.webp"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xs-12 text-center">
                <p class="light-text-title">ベッドを使ってくれるか<br>心配ですか？</p>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-4 text-center">
                <div class="panel-blue p-2 post-details-text">気に入らなければ<b>最大60日以内</b>の返品OK!</div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/step_img_1.webp" alt=""/>
                <p class="post-desc-text">カラーやフィリングをカスタマイズしてご注文</p>
                <p class="post-details-text">最短翌日出荷</p>
            </div>
            <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/step_img_2.webp" alt=""/>
                <p class="post-desc-text">ご注文を受けてから日本ラボで作成しお届け</p>
                <p class="post-details-text">日本人スタッフによる<br>最高品質の製造</p>
            </div>
            <div class="col-xs-12 col-md-3 col-sm-8 text-center">
                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp_dog/step_img_3.webp" alt=""/>
                <p class="post-desc-text">最大60日のお試しトライアル期間
                <p class="post-details-text">ソファは30日間、ペットラウンジは60日間。気に入らない場合は、期間内なら全額返金。 
                <br>※適用条件は「詳しく見る」をクリックしてご確認ください</p>   
            </div>
        </div>
        <div class="row justify-content-center py-5">
            <div class="col-md-3 text-center">
                <a class="btn btn-primary btn-details-lg" href="https://ambientlounge.co.jp/30days-trial/" target="_self">詳しく見る</a>
            </div>
        </div>
    </div>
</div>
<!--container details end-->

<!--container lookbook start-->
<div class="container-fluid container-5 p-3 p-xl-5">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="col-xs-12 text-center">
                <p class="light-text-title">LOOKBOOK</p>
            </div>
            <div class="col-md-8 col-xs-12 text-center my-3">
                <p class="light-text-subtitle">気になるお部屋のレイアウトや、どんなワンちゃんが利用しているか...などなど。
                    <br>ルックブックでチェックしてみましょう。</p>
            </div>
            <div class="col-xs-12 p-0">
                <div class="row justify-content-center">
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lookbook_img_1.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lookbook_img_2.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lookbook_img_3.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lookbook_img_4.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lookbook_img_5.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/lp-cat/lookbook_img_6.webp" style="width:100%" alt=""/></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center py-5">
            <div class="col-md-3 text-center">
                <a class="btn btn-primary btn-details-lg" href="https://ambientlounge.co.jp/lookbook/?size=cat" target="_self">LOOKBOOKを見る</a>
            </div>
        </div>
    </div>
</div>
<!--container lookbook end-->

<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/float-top-bar.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/js/lazysizes-5.3.2.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/js/jquery-3.6.1.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/js/js-cloudimage-360-view-2.1.0.min.js"></script>
<script>
    let ua = navigator.userAgent
    let androidVrBtn = document.querySelectorAll(".ar-android.mobile");
    let iosVrBtn = document.querySelectorAll(".ar-ios.mobile");
    let otherVrBtn = document.querySelectorAll(".ar-others.mobile");
    if (/android/i.test(ua)) {
        for (let i = 0; i < androidVrBtn.length; i++) {
            androidVrBtn[i].classList.add('show');
        }
    }
    else if (/iPad|iPhone|iPod/.test(ua)) {
        for (let i = 0; i < iosVrBtn.length; i++) {
            iosVrBtn[i].classList.add('show');
        }
    }
    else {
        for (let i = 0; i < otherVrBtn.length; i++) {
            otherVrBtn[i].classList.add('show');
        }
    }
</script>
<?php
get_footer();
?>