<?php
/**
 * The template for displaying single posts.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php generate_do_microdata( 'article' ); ?>>
	<div class="inside-article">		
		<?php
			if ( generate_show_entry_header() ) :
				?>
				<header <?php generate_do_attr( 'entry-header' ); ?>>
					<?php
					/**
					* generate_before_entry_title hook.
					*
					* @since 0.1
					*/
					do_action( 'generate_before_entry_title' );

					if ( generate_show_title() ) {
						$params = generate_get_the_title_parameters();

						the_title( $params['before'], $params['after'] );
					}

					/**
					* generate_after_entry_title hook.
					*
					* @since 0.1
					*
					* @hooked generate_post_meta - 10
					*/
					do_action( 'generate_after_entry_title' );
					?>
				</header>
				<?php
			endif;
		?>

		<div class="slim-container">
			<?php
				/**
				* generate_before_content hook.
				*
				* @since 0.1
				*
				* @hooked generate_featured_page_header_inside_single - 10
				*/
				do_action( 'generate_before_content' );

				/**
				* generate_after_entry_header hook.
				*
				* @since 0.1
				*
				* @hooked generate_post_image - 10
				*/
				do_action( 'generate_after_entry_header' );

				$itemprop = '';

				if ( 'microdata' === generate_get_schema_type() ) {
					$itemprop = ' itemprop="text"';
				}
				?>

				<div class="entry-content"<?php echo $itemprop; // phpcs:ignore -- No escaping needed. ?>>
					<?php
						the_content();

						wp_link_pages(
							array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'generatepress' ),
								'after'  => '</div>',
							)
						);
					?>
				</div>

			<?php
				/**
				* generate_after_entry_content hook.
				*
				* @since 0.1
				*
				* @hooked generate_footer_meta - 10
				*/
				//do_action( 'generate_after_entry_content' );

				/**
				* generate_after_content hook.
				*
				* @since 0.1
				*/
				do_action( 'generate_after_content' );
			?>
		</div>
	</div>

	<div class="single-footer">
		<div class="container">
			<div class="prevnextpost-wrapper">				
				<?php
					$prevPost = get_adjacent_post( false, '', true);
					$nextPost = get_adjacent_post( false, '', false);
					$prevPostID = $prevPost->ID;
					$nextPostID = $nextPost->ID;

					$arrPrevNextPostID = array();

					if ( !empty( $nextPost ) ) { array_push( $arrPrevNextPostID, $nextPostID ); }
					if ( !empty( $prevPost ) ) { array_push( $arrPrevNextPostID, $prevPostID ); }
					

					if ( !empty( $arrPrevNextPostID ) ) 
					{					
						$args = array(
							'post__in' => $arrPrevNextPostID,
						);

						$query = new WP_Query( $args );

						if ( $query->have_posts() ) 
						{
							while ( $query->have_posts() ) 
							{
								$query->the_post();

								global $post;
								$postThumb = get_the_post_thumbnail_url( $post, 'mobile' );	
								?>
									<div class="prevnextpost">
										<div class="prevnextpost-navigation">
											<?php
												if ( $post->ID == $prevPostID ) 
												{
													?>
														<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
															<div class="prevpost-navigation">
																次の記事
															</div>
														</a>
													<?php
												}
												else if ( $post->ID == $nextPostID ) 
												{
													?>
														<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
															<div class="nextpost-navigation">
																前の記事
															</div>
														</a>
													<?php
												}
											?>
										</div>
										<div class="post-card">
											<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
												<div class="post-card-thumb" style="background-image:url(<?php echo $postThumb; ?>);">
												</div>
											</a>
											<div class="post-card-description">
												<div class="post-card-title">
													<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
														<h3><?php the_title(); ?></h3>
													</a>
												</div>
												<div class="post-card-content">
													<?php 
														$excerpt = get_the_excerpt(); 

														/*$limitChar = 100;
														$excerpt = mb_substr( $excerpt, 0, $limitChar );*/

														echo $excerpt;
													?>
												</div>
											</div>
										</div>
									</div>
								<?php
							}
						} 
						wp_reset_postdata();
					}
				?>
			</div>
		</div>
	</div>
</article>
