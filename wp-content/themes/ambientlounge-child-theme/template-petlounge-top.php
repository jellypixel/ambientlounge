
<?php
/*
	Template Name: Pet Lounge Top
*/
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/adjustment.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/font/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/main-dark.all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/petlounge-top.css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto Sans JP:100,200,300,400,500,600,700|Noto Sans JP:600|Open Sans:600|Open Sans|Open Sans:700&amp;subset=latin-ext&amp;display=swap" media="all" onload="this.media='all'">

<section id="banner-bottom">
    <!--container bg-black start-->
    <div class="container-fluid container-2">
        <div class="container-dark">
            <div class="row banner-bottom-row-dark">
                <div class="col-xs-12 col-md-8 banner-text-box box-1 order-1 order-md-2">
                    <p class="banner-bottom-title text-gradient-blue">犬 ペットラウンジ</p>
                    <p><a class="btn btn-primary details-btn" href="https://ambientlounge.co.jp/pet-lounge/">詳細を見る</a></p>
                    <p><a class="buy-link text-decoration-none" href="https://ambientlounge.co.jp/products/dog-bed/pet-lounge/">購入<i class="bi bi-chevron-right"></i></a></p>
                </div>
                <div class="col-xs-12 col-md-4 p-0 order-2 order-md-1">
                    <div class="banner-bottom-img-1"></div>
                </div>
            </div>
            <div class="row banner-bottom-row-dark">
                <div class="col-xs-12 col-md-7 banner-text-box box-2">
                    <p class="banner-bottom-title text-gradient-pink">猫 ペットラウンジ</p>
                    <p><a class="btn btn-primary details-btn" href="https://ambientlounge.co.jp/cat/">詳細を見る</a></p>
                    <p><a class="buy-link text-decoration-none" href="https://ambientlounge.co.jp/products/cat-bed/cat/">購入<i class="bi bi-chevron-right"></i></a></p>
                </div>
                <div class="col-xs-12 col-md-5 p-0">
                    <div class="banner-bottom-img-2"></div>
                </div>
            </div>
        </div>
    </div>
    <!--container bg-black end-->
</section>

<!--container product white panel start-->
<section id="lp-delux-product-w">
    <div class="container-fluid bg-light p-3 p-xl-5">
        <div class="container-dark">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                    <div class="product-panel-white">
                        <p class="light-panel-title">DELUX</p>
                        <p class="light-panel-subtitle">犬ペットラウンジ<br>取替カバー3種、メンテナンスや快適をさらに向上させるアクセサリー、リピーター続出のthe Shampooがセットになりました。</p>
                        <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/delux/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                        <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa-top/product-1-new.webp"/>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                    <div class="product-panel-white">
                        <p class="light-panel-title">アクセサリー</p>
                        <p class="light-panel-subtitle desc-text-lg">ブランケットやジェルマット、メンテンス用品など<br>いろいろなアイテムが勢ぞろい</p>
                        <p class="light-panel-subtitle desc-text-sm">ブランケットやジェルマット、メンテンス用品などいろいろなアイテムが勢ぞろい</p>
                        <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/pet-accessories/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                        <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-2.webp"/>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                    <div class="product-panel-white">
                        <p class="light-panel-title">Eco Friendly</p>
                        <p class="light-panel-subtitle">エコロジカルな活動を</p>
                        <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/eco/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                        <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/eco-friendly-product.webp"/>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                    <div class="product-panel-white">
                        <p class="light-panel-title">The Shampoo</p>
                        <p class="light-panel-subtitle">愛犬のグルーミング商品</p>
                        <a class="light-panel-link text-decoration-none" href="https://ambientlounge.co.jp/the-shampoo-intro/">さらに詳しく<i class="bi bi-chevron-right"></i></a>
                        <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/delux_dog/product-4.webp"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--container product white panel end-->

<!--container lookbook start-->
<div class="container-fluid p-3 p-xl-5">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="col-xs-8 col-md-12 order-2 order-md-1">
                <p class="au_design_title">オリジナルの生活スタイルを</p>
            </div>
            <div class="col-xs-4 col-md-2 my-auto text-end order-1 order-md-2">
                <img class="au_design_logo" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/au_design_logo.webp" alt="...">
            </div>
            <div class="col-xs-12 col-sm-8 col-md-6 p-2 order-3 order-md-3">
                <p class="al-intro-text text-start">ゆったりと時間が流れるオーストラリアの生活の中でも、模様替えは頻繁。
                    <br>気分に合わせていつでも生活空間を替えたいけど、普通の家具は重くて面倒。
                    <br>ビーズクッションもあるけど、オシャレじゃない。
                    <br>そんな願いを叶えたのが軽量で美しいプレミアムソフトソファ。</p>
            </div>
            <div class="col-md-12 p-0 mt-5 order-4">
                <div class="row justify-content-center">
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_1.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_2.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_3.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_4.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 mt-md-5 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_5.webp" style="width:100%" alt=""/></div>
                    <div class="col-md-4 col-xs-6 p-2"><img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/life_img_6.webp" style="width:100%" alt=""/></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center py-5">
            <div class="col-md-3 text-center"><a href="https://ambientlounge.co.jp/lookbook/" class="btn btn-primary btn-details-lg">もっと見る</a></div>
        </div>
    </div>
</div>
<!--container lookbook end-->
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/bootstrap.bundle.min.js"></script>
<?php
get_footer();
?>