<?php
/**
 * The template for displaying the header.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/* Bg Color */
$background_color = get_post_meta( $post->ID, '_background_color_meta_key', true );

$bgClass = '';
if ( !empty( $background_color) ) {
	$bgClass = $background_color . '-bg';
}

/* Container Width */
$container_width = get_post_meta( $post->ID, '_container_width_meta_key', true );

$containerWidthClass = '';
if ( !empty( $container_width) ) {
	$containerWidthClass = $container_width . '-container-width';
}
 
/* ProductCat Sofa & Product Butterfly */
global $post;
$terms = get_the_terms( $post->ID, 'product_cat' );
$sofaCatID = 281; // 281 - Sofa parent product cat
$flagSofa = false;
$productCatSofaClass = '';

foreach ($terms as $term) {
	if ( $term->term_id == $sofaCatID || get_ancestors($term->term_id, 'product_cat')[0] == $sofaCatID ) {
		$flagSofa = true;
		break;
	}
}

if ( $flagSofa ==  true ) {
	$productCatSofaClass = 'productcat-sofa';
}


?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<script>
		var currDomain = '<?php echo $GLOBALS['currDomain']; ?>';
	</script>
</head>

<body <?php body_class( $bgClass . ' ' . $containerWidthClass . ' ' . $productCatSofaClass ); ?> <?php generate_do_microdata( 'body' ); ?>>
	<?php
		/**
		* wp_body_open hook.
		*
		* @since 2.3
		*/
		do_action( 'wp_body_open' ); // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedHooknameFound -- core WP hook.

		/**
		* generate_before_header hook.
		*
		* @since 0.1
		*
		* @hooked generate_do_skip_to_content_link - 2
		* @hooked generate_top_bar - 5
		* @hooked generate_add_navigation_before_header - 5
		*/
		do_action( 'generate_before_header' );	
		
		function before_logo() {
			//return 'a';	
		}
		add_action( 'generate_before_logo_hook', 'before_logo' );

		/**
		* generate_header hook.
		*
		* @since 1.3.42
		*
		* @hooked generate_construct_header - 10
		*/
		//do_action( 'generate_header' );
		$headerClass = '';
		/*$transparent_menu = get_post_meta( $post->ID, '_transparent_menu_meta_key', true );
		
		if ( $transparent_menu == 'yes' )
		{
			$headerClass .= 'transparentheader';
		}*/	
	?>

	<header <?php generate_do_attr( 'header', array( 'class' => $headerClass . ' ' ) ); ?>>
		<div <?php //generate_do_attr( 'inside-header' ); ?>>
			<div class="header-top">
				<div class="left-menu">
					<div class="burger-wrapper">
						<div class="burger-inner">
							<div class="burgerbar"></div>
							<div class="burgerbar"></div>
							<div class="burgerbar"></div>
						</div>
					</div>
					<nav id="site-navigation" <?php /*class="main-navigation"*/ ?>>
						<div class="inside-navigation">
							<div id="primary-menu" class="main-nav">

								<div class="mobile-primary-menu">		
									<div class="mobile-menu-title" data-target="#sofa">
										<?php _e( 'ソファ', 'ambientlounge' ); ?>
									</div>
									
									<div class="mobile-menu-title active" data-target="#pet">
										<?php _e( 'ペット', 'ambientlounge' ); ?>
									</div>								
								</div>

								<ul class="desktop-primary-menu">
									<li id="sofa" class="menu-title">										
										<a href="#"><?php _e( 'ソファ', 'ambientlounge' ); ?></a>									

										<nav class="megamenu-wrapper megamenu-grey">
											<div class="submenu-title-wrapper">
												<div class="submenu-title active" data-target="#sofalist">ソファ一覧</div>															
												<div class="submenu-title" data-target="#sofa-blog"><?php _e( 'Blog', 'ambientlounge' ); ?></div>									
											</div>

											<div class="submenu-content-wrapper">
												<div class="container">

													<div id="sofalist" class="submenu-content submenu-accordion-wrapper active">
														<div class="submenu-accordion-title">
															<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
															</svg>

															<?php _e( 'ソファ一覧', 'ambientlounge' ); ?>

															<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
															</svg>
														</div>															
														<div class="submenu-accordion-content">															
															<div class="submenu-accordion-title">
																<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
																</svg>

																<?php _e( 'ソファ一覧', 'ambientlounge' ); ?>
															</div>	
															<div class="columns">
																<div class="sub-submenu-accordion-wrapper active">
																	<div class="sub-submenu-accordion-title">
																		<?php _e( '選ぶ理由', 'ambientlounge' ); /* Perlu ganti ga ソファ一覧 */ ?>

																		<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																			<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																		</svg>
																	</div>
																	<div class="sub-submenu-accordion-content">

																		<div class="sofalist-row">
																			<div class="sofalist-column">
																				<div class="sofalist-title">																				
																					<?php _e( '1人掛け', 'ambientlounge' ); ?>
																				</div>

																				<div class="columns card card-columns gap-half padding-half has-white-background-color has-secondarydark-color">
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/1-person/butterfly/" title="<?php _e( 'バタフライ', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/1seater/1バタフライ.svg" alt="<?php _e( 'バタフライ', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'バタフライ', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/1-person/acoustic/" title="<?php _e( 'アクスティック', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">		
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/1seater/2アクスティック.svg" alt="<?php _e( 'アクスティック', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'アクスティック', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/1-person/modular-single/" title="<?php _e( 'モジュラーシングル', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/1seater/3モジュラーシングル.svg" alt="<?php _e( 'モジュラーシングル', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'モジュラーシングル', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/1-person/studio/" title="<?php _e( 'スタジオ', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/1seater/4スタジオ.svg" alt="<?php _e( 'スタジオ', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'スタジオ', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/1-person/avatar/" title="<?php _e( 'アヴァター', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/1seater/5アヴァター.svg" alt="<?php _e( 'アヴァター', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'アヴァター', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/1-person/evolution/" title="<?php _e( 'エヴォリューション', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/1seater/6エヴォリューション.svg" alt="<?php _e( 'エヴォリューション', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'エヴォリューション', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/1-person/zen/" title="<?php _e( 'ゼンラウンジャー', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/1seater/7ゼンラウンジャー.svg" alt="<?php _e( 'ゼンラウンジャー', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'ゼンラウンジャー', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/1-person/modular-corner/" title="<?php _e( 'モジュラーコーナー', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/1seater/8モジュラーコーナー.svg" alt="<?php _e( 'モジュラーコーナー', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'モジュラーコーナー', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																				</div>	
																			</div>
																		</div>

																		<div class="sofalist-row">
																			<div class="sofalist-column">
																				<div class="sofalist-title">																				
																					<?php _e( '2人掛け', 'ambientlounge' ); ?>
																				</div>

																				<div class="columns card card-columns gap-half padding-half has-white-background-color has-secondarydark-color">
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/2-person/modular-twin/" title="<?php _e( 'モジュラーツイン', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/2seater/1モジュラーツイン.svg" alt="<?php _e( 'モジュラーツイン', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'モジュラーツイン', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/2-person/satellite/" title="<?php _e( 'サテライト', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/2seater/2サテライト.svg" alt="<?php _e( 'サテライト', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'サテライト', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>
																			</div>

																			<div class="sofalist-column">
																				<div class="sofalist-title">																				
																					<?php _e( '3人掛け', 'ambientlounge' ); ?>
																				</div>

																				<div class="columns card card-columns gap-half padding-half has-white-background-color has-secondarydark-color">
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/3-person/movie-couch/" title="<?php _e( 'ムービーカウチ', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/3seater/1ムービーカウチ.svg" alt="<?php _e( 'ムービーカウチ', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'ムービーカウチ', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/3-person/quad-couch/" title="<?php _e( 'クワッドカウチ', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/3seater/2クワッドカウチ.svg" alt="<?php _e( 'クワッドカウチ', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'クワッドカウチ', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/3-person/cozy-corner/" title="<?php _e( 'コウズィコーナー', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/3seater/3コウズィコーナー.svg" alt="<?php _e( 'コウズィコーナー', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'コウズィコーナー', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>
																			</div>

																			<div class="sofalist-column">
																				<div class="sofalist-title">																				
																					<?php _e( '大型ソファ', 'ambientlounge' ); ?>
																				</div>

																				<div class="columns card card-columns gap-half padding-half has-white-background-color has-secondarydark-color">
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/3-person/l-sofa/" title="<?php _e( 'エルソファ', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/largesofa/1エルソファ.svg" alt="<?php _e( 'エルソファ', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'エルソファ', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/large-sofa/lounge-max/" title="<?php _e( 'ラウンジマックス', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/largesofa/2ラウンジマックス.svg" alt="<?php _e( 'ラウンジマックス', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'ラウンジマックス', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																																					
																				</div>	
																			</div>																				
																		</div>

																		<div class="sofalist-row">
																			<div class="sofalist-column">
																				<div class="sofalist-title">																				
																					<?php _e( 'オットマン', 'ambientlounge' ); ?>																					
																				</div>

																				<div class="columns card card-columns gap-half padding-half has-white-background-color has-secondarydark-color">
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/ottoman/versa-table/" title="<?php _e( 'ヴァ―サテーブル', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/ottoman/1ヴァ―サテーブル.svg" alt="<?php _e( 'ヴァ―サテーブル', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'ヴァ―サテーブル', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/ottoman/wing-ottoman/" title="<?php _e( 'ウィングオットマン', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/ottoman/2ウィングオットマン.svg" alt="<?php _e( 'ウィングオットマン', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'ウィングオットマン', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/ottoman/twin-ottoman/" title="<?php _e( 'ツインオットマン', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/ottoman/3ツインオットマン.svg" alt="<?php _e( 'ツインオットマン', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'ツインオットマン', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>
																					<div class="column-equal">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/sofa/ottoman/ottoman/" title="<?php _e( 'オットマン', 'ambientlounge' ); ?>">
																							<div class="sofalist-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sofalist/ottoman/4オットマン.svg" alt="<?php _e( 'オットマン', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sofalist-title">
																								<?php _e( 'オットマン', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																	
																				</div>
																			</div>

																			<div class="sofalist-column">
																			</div>																			
																		</div>
																		<div class="sofalist-row">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/collection/sofa/" title="<?php _e( '全ての商品を見る', 'ambientlounge' ); ?>"><?php _e( '全ての商品を見る', 'ambientlounge' ); ?></a>
																		</div>
																	</div>																															
																</div>

																<div class="column-side">
																	<div class="sub-submenu-accordion-wrapper">
																		<div class="sub-submenu-accordion-title column-attr">
																			<div class="title">																			
																				<?php _e( 'モジュラー', 'ambientlounge' ); ?>																				
																			</div>
																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content column-value">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/modular/" title="モジュラーを詳しく見る"><?php _e( 'モジュラーを詳しく見る', 'ambientlounge' ); ?></a>
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/collection/modular/" title="モジュラー一覧"><?php _e( 'モジュラー一覧', 'ambientlounge' ); ?></a>
																		</div>
																	</div>

																	<!--
																	<div class="sub-submenu-accordion-wrapper">
																		<div class="sub-submenu-accordion-title column-attr">
																			<div class="title">																			
																				<?php _e( 'エコプロジェクト', 'ambientlounge' ); ?>																				
																			</div>
																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content column-value">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/collection/eco-campaign/" title="モジュラー一覧"><?php _e( 'エコフレンドリー', 'ambientlounge' ); ?></a>
																		</div>
																	</div>
																	-->

																	<div class="sub-submenu-accordion-wrapper">
																		<div class="sub-submenu-accordion-title column-attr">
																			<div class="title">																			
																				<?php _e( 'ふるさと納税', 'ambientlounge' ); ?>																				
																			</div>
																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content column-value">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/furusato/" title="ふるさと納税"><?php _e( 'ふるさと納税', 'ambientlounge' ); ?><!--<span class="new-menu">&nbsp;New</span>--></a>
																		</div>
																	</div>

																	<div class="sub-submenu-accordion-wrapper">
																		<div class="sub-submenu-accordion-title column-attr">
																			<div class="title">
																				<?php _e( 'アクセサリー', 'ambientlounge' ); ?>																			
																			</div>

																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content column-value">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/sofa-accessories/" title="その他"><?php _e( 'その他', 'ambientlounge' ); ?></a>
																		</div>
																	</div>

																	<div class="sub-submenu-accordion-wrapper">
																		<div class="sub-submenu-accordion-title column-attr">
																			<div class="title">																			
																				<?php _e( 'サイズ選びに迷ったら', 'ambientlounge' ); ?>																				
																			</div>
																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content column-value">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/sofa-compare/" title="サイズを比較する"><?php _e( 'サイズを比較する', 'ambientlounge' ); ?></a>
																		</div>
																	</div>

																	<div class="sub-submenu-accordion-wrapper">
																		<div class="sub-submenu-accordion-title column-attr">
																			<div class="title">
																				<?php _e( '購入について', 'ambientlounge' ); ?>																			
																			</div>

																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content column-value">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/30days-trial/" title="30日間トライアル"><?php _e( '30日間トライアル', 'ambientlounge' ); ?></a>
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/shopping-guide/" title="ご利用ガイド"><?php _e( 'ご利用ガイド', 'ambientlounge' ); ?></a>
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/retail-locations/" title="取扱店舗一覧"><?php _e( '取扱店舗一覧', 'ambientlounge' ); ?></a>
																		</div>
																	</div>																
																</div>
															</div>
														</div>
													</div>

													<div id="mobile-column-side" class="submenu-content">
														<div class="submenu-accordion-content">
															<div id="sofa-modular" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<?php _e( 'モジュラー', 'ambientlounge' ); ?>

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/modular/" title="モジュラーを詳しく見る"><?php _e( 'モジュラーを詳しく見る', 'ambientlounge' ); ?></a>
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/collection/modular/" title="モジュラー一覧"><?php _e( 'モジュラー一覧', 'ambientlounge' ); ?></a>												
																</div>
															</div>

															<!--
															<div id="sofa-modular" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<?php _e( 'エコプロジェクト', 'ambientlounge' ); ?>

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/collection/eco-campaign/" title="モジュラー一覧"><?php _e( 'エコフレンドリー', 'ambientlounge' ); ?></a>															
																</div>
															</div>
															-->

															<div id="sofa-furusato" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<?php _e( 'ふるさと納税', 'ambientlounge' ); ?>

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/furusato" title="ふるさと納税"><?php _e( 'ふるさと納税', 'ambientlounge' ); ?></a>															
																</div>
															</div>

															<div id="sofa-accessories" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<?php _e( 'アクセサリー', 'ambientlounge' ); ?>																			

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/sofa-accessories/" title="その他"><?php _e( 'その他', 'ambientlounge' ); ?></a>
																</div>
															</div>

															<div id="sofa-size" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<?php _e( 'サイズ選びに迷ったら', 'ambientlounge' ); ?>																				

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/sofa-compare/" title="サイズを比較する"><?php _e( 'サイズを比較する', 'ambientlounge' ); ?></a>
																</div>
															</div>

															<div id="sofa-purchase" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<?php _e( '購入について', 'ambientlounge' ); ?>

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/30days-trial/" title="30日間トライアル"><?php _e( '30日間トライアル', 'ambientlounge' ); ?></a>
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/shopping-guide/" title="ご利用ガイド"><?php _e( 'ご利用ガイド', 'ambientlounge' ); ?></a>
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/retail-locations/" title="取扱店舗一覧"><?php _e( '取扱店舗一覧', 'ambientlounge' ); ?></a>
																</div>
															</div>

															<div id="sofa-blog" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<?php _e( 'Blog', 'ambientlounge' ); ?>

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/blog/" title="すべて"><?php _e( 'すべて', 'ambientlounge' ); ?></a>
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/category/brand/" title="ブランド"><?php _e( 'ブランド', 'ambientlounge' ); ?></a>																
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/category/sofa/" title="ソファ"><?php _e( 'ソファ', 'ambientlounge' ); ?></a>
																</div>
															</div>
														</div>
													</div>

													<div id="sofa-blog" class="submenu-content desktop-only">
														<div class="submenu-accordion-content">
															<div id="sofa-blog-section1" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<span>
																		<?php _e( 'カテゴリー', 'ambientlounge' ); ?>
																	</span>

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/blog/" title="すべて"><?php _e( 'すべて', 'ambientlounge' ); ?></a>
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/category/brand/" title="ブランド"><?php _e( 'ブランド', 'ambientlounge' ); ?></a>																
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/category/sofa/" title="ソファ"><?php _e( 'ソファ', 'ambientlounge' ); ?></a>
																</div>
															</div>															
														</div>
													</div>

												</div>
											</div>
										</nav>
									</li>

									<li id="pet" class="menu-title active">										
										<a href="#"><?php _e( 'ペット', 'ambientlounge' ); ?></a>

										<nav class="megamenu-wrapper">
											<div class="submenu-title-wrapper">
												<div class="submenu-title active" data-target="#petlounge"><?php _e( 'ペットラウンジ', 'ambientlounge' ); ?></div>
												<div class="submenu-title" data-target="#reasonforchoosing"><?php _e( '選ぶ理由', 'ambientlounge' ); ?></div>
												<div class="submenu-title" data-target="#pet-blog"><?php _e( 'Blog', 'ambientlounge' ); ?></div>
											</div>
											
											<div class="submenu-content-wrapper">
												<div class="container">

													<div id="petlounge" class="submenu-content submenu-accordion-wrapper active">
														<div class="submenu-accordion-title">
															<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
															</svg>

															<?php _e( 'ペットラウンジ', 'ambientlounge' ); ?>

															<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
															</svg>
														</div>								
														<div class="submenu-accordion-content">		
															<div class="submenu-accordion-title">
																<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
																</svg>

																<?php _e( 'ペットラウンジ', 'ambientlounge' ); ?>
															</div>																											
															<div class="columns gap-one-half">																	
																<div id="petlounge-bedtypes" class="sub-submenu-accordion-wrapper active column-equal desktop-only">
																	<?php	
																		// Pet Lounge Card - Desktop
																		menu_section_petloungecard();	

																		function menu_section_petloungecard() 
																		{
																			?>
																				<div class="sub-submenu-accordion-title">
																					<?php _e( '商品種類', 'ambientlounge' ); ?>

																					<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																						<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																					</svg>
																				</div>
																				<div class="sub-submenu-accordion-content">
																					<div id="petlounge-card" class="columns gap-half columns-down">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/pet-lounge/" title="<?php _e( '犬ペットラウンジ', 'ambientlounge' ); ?>">
																							<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																								<div class="card-image">
																									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-01.svg?v=13" alt="<?php _e( '犬ペットラウンジ', 'ambientlounge' ); ?>">
																								</div>
																								<div class="card-title">
																									<div class="card-title-text">																										
																										<?php _e( '犬ペットラウンジ', 'ambientlounge' ); ?>
																									</div>
																								</div>																								
																							</div>
																						</a>

																						<a href="<?php echo $GLOBALS['currDomain']; ?>/cat" title="<?php _e( '猫ペットラウンジ', 'ambientlounge' ); ?>">
																							<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																								<div class="card-image">
																									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-01.svg?v=13" alt="<?php _e( '猫ペットラウンジ', 'ambientlounge' ); ?>">
																								</div>
																								<div class="card-title">
																									<div class="card-title-text">																										
																										<?php _e( '猫ペットラウンジ', 'ambientlounge' ); ?>
																									</div>
																								</div>																								
																							</div>
																						</a>

																						<?php
																						/*
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/dog-bed/pet-lounge-plus/" title="<?php _e( 'ペットラウンジプラス', 'ambientlounge' ); ?>">
																							<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																								<div class="menu-new"><?php _e( 'NEW', 'ambientlounge' ); ?></div>		
																								<div class="card-image">
																									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-01-slim-1.svg?v=13" alt="<?php _e( 'ペットラウンジプラス', 'ambientlounge' ); ?>">
																								</div>
																								<div class="card-title">
																									<div class="card-title-text">																							
																										<div><?php _e( 'ペットラウンジプラス', 'ambientlounge' ); ?></div>
																									</div>
																								</div>																								
																							</div>
																						</a>
																						*/
																						?>

																						<a href="<?php echo $GLOBALS['currDomain']; ?>/bluedream/" title="<?php _e( '限定ペットラウンジ', 'ambientlounge' ); ?>">
																							<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																								<div class="card-image">
																									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-02.svg?v=10" alt="<?php _e( '限定ペットラウンジ', 'ambientlounge' ); ?>">
																								</div>
																								<div class="card-title">																									
																									<div class="card-title-text">																										
																										<?php _e( '限定ペットラウンジ', 'ambientlounge' ); ?>
																									</div>
																								</div>																								
																							</div>
																						</a>

																						<?php /* ?>
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/delux/" title="<?php _e( 'セット商品 DELUX', 'ambientlounge' ); ?>">
																							<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																								<div class="card-image">
																									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-03.svg?v=10" alt="<?php _e( 'セット商品 DELUX', 'ambientlounge' ); ?>">
																								</div>
																								<div class="card-title">	
																									<div class="card-title-subtext">	
																										<?php _e( 'セット商品', 'ambientlounge' ); ?>
																									</div>																								
																									<div class="card-title-text">																										
																										<?php _e( 'DELUX', 'ambientlounge' ); ?>
																									</div>
																								</div>																								
																							</div>
																						</a>
																						<?php */ ?>

																						<a href="<?php echo $GLOBALS['currDomain']; ?>/products/dog-bed/dog-covers/" title="<?php _e( '取替カバー', 'ambientlounge' ); ?>">
																							<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																								<div class="card-image">
																									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/cover.svg?v=10" alt="<?php _e( '取替カバー', 'ambientlounge' ); ?>">
																								</div>
																								<div class="card-title">																																															
																									<div class="card-title-text">																										
																										<?php _e( '取替カバー', 'ambientlounge' ); ?>
																									</div>
																								</div>																								
																							</div>
																						</a>
																						
																						<?php /* ?>
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/eco/" title="<?php _e( 'エコロジカルな活動を エコフレンドリー', 'ambientlounge' ); ?>">
																							<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																								<div class="card-image">
																									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-04.svg?v=10" alt="<?php _e( 'エコロジカルな活動を エコフレンドリー', 'ambientlounge' ); ?>">
																								</div>
																								<div class="card-title">
																									<div class="card-title-subtext">	
																										<?php _e( 'エコロジカルな活動を', 'ambientlounge' ); ?>
																									</div>																								
																									<div class="card-title-text">																										
																										<?php _e( 'エコフレンドリー', 'ambientlounge' ); ?>
																									</div>
																								</div>																								
																							</div>
																						</a>
																						<?php */ ?>
																						
																					</div>
																				</div>
																			<?php
																		}
																	?>
																</div>

																<?php																			
																	//Pet Lounge Card - Mobile																			
																?>
																<div id="petlounge-card-mobile-wrapper" class="sub-submenu-accordion-wrapper active column-equal mobile-only">
																	<div class="sub-submenu-accordion-title">
																		<?php _e( '商品種類', 'ambientlounge' ); ?>

																		<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																			<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																		</svg>
																	</div>															
																	<div class="sub-submenu-accordion-content">																		
																		<div id="petlounge-card" class="columns gap-half columns-down">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/pet-lounge/" title="<?php _e( '犬ペットラウンジ', 'ambientlounge' ); ?>">
																				<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																					<div class="card-image">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-01.svg?v=10" alt="<?php _e( '犬ペットラウンジ', 'ambientlounge' ); ?>">
																					</div>
																					<div class="card-title">
																						<div class="card-title-text">																										
																							<?php _e( '犬ペットラウンジ', 'ambientlounge' ); ?>
																						</div>
																					</div>																								
																				</div> 
																			</a>

																			<a href="<?php echo $GLOBALS['currDomain']; ?>/cat/" title="<?php _e( '猫ペットラウンジ', 'ambientlounge' ); ?>">
																				<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																					<div class="card-image">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-01.svg?v=10" alt="<?php _e( '猫ペットラウンジ', 'ambientlounge' ); ?>">
																					</div>
																					<div class="card-title">
																						<div class="card-title-text">																										
																							<?php _e( '猫ペットラウンジ', 'ambientlounge' ); ?>
																						</div>
																					</div>																								
																				</div> 
																			</a>

																			<?php
																			/*
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/products/dog-bed/pet-lounge-plus/" title="<?php _e( 'ペットラウンジプラス', 'ambientlounge' ); ?>">
																				<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																					<div class="menu-new"><?php _e( 'NEW', 'ambientlounge' ); ?></div>				
																					<div class="card-image">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-01-slim-1.svg?v=13" alt="<?php _e( 'ペットラウンジプラス', 'ambientlounge' ); ?>">
																					</div>
																					<div class="card-title">
																						<div class="card-title-text">																							
																							<div><?php _e( 'ペットラウンジプラス', 'ambientlounge' ); ?></div>
																						</div>
																					</div>																								
																				</div>
																			</a>
																			*/
																			?>

																			<a href="<?php echo $GLOBALS['currDomain']; ?>/bluedream" title="<?php _e( '限定ペットラウンジ', 'ambientlounge' ); ?>">
																				<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																					<div class="card-image">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-02.svg?v=10" alt="<?php _e( '限定ペットラウンジ', 'ambientlounge' ); ?>">
																					</div>
																					<div class="card-title">																									
																						<div class="card-title-text">																										
																							<?php _e( '限定ペットラウンジ', 'ambientlounge' ); ?>
																						</div>
																					</div>																								
																				</div>
																			</a>

																			<?php /* ?>
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/delux/" title="<?php _e( 'セット商品 DELUX', 'ambientlounge' ); ?>">
																				<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																					<div class="card-image">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-03.svg?v=10" alt="<?php _e( 'セット商品 DELUX', 'ambientlounge' ); ?>">
																					</div>
																					<div class="card-title">	
																						<div class="card-title-subtext">	
																							<?php _e( 'セット商品', 'ambientlounge' ); ?>
																						</div>																								
																						<div class="card-title-text">																										
																							<?php _e( 'DELUX', 'ambientlounge' ); ?>
																						</div>
																					</div>																								
																				</div>
																			</a>
																			<?php */ ?>

																			<a href="<?php echo $GLOBALS['currDomain']; ?>/products/dog-bed/dog-covers/" title="<?php _e( '取替カバー', 'ambientlounge' ); ?>">
																				<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																					<div class="card-image">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/cover.svg?v=10" alt="<?php _e( '取替カバー', 'ambientlounge' ); ?>">
																					</div>
																					<div class="card-title">																																												
																						<div class="card-title-text">																										
																							<?php _e( '取替カバー', 'ambientlounge' ); ?>
																						</div>
																					</div>																								
																				</div>
																			</a>

																			<?php /* ?>
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/eco/" title="<?php _e( 'エコロジカルな活動を エコフレンドリー', 'ambientlounge' ); ?>">
																				<div class="column-equal card radius-half padding-half has-lightgray-background-color">
																					<div class="card-image">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/petlounge/pet-04.svg?v=10" alt="<?php _e( 'エコロジカルな活動を エコフレンドリー', 'ambientlounge' ); ?>">
																					</div>
																					<div class="card-title">
																						<div class="card-title-subtext">	
																							<?php _e( 'エコロジカルな活動を', 'ambientlounge' ); ?>
																						</div>																								
																						<div class="card-title-text">																										
																							<?php _e( 'エコフレンドリー', 'ambientlounge' ); ?>
																						</div>
																					</div>																								
																				</div>
																			</a>
																			<?php */ ?>
																																						
																		</div>
																	</div>																		
																</div>

																<div id="petlounge-sizechart" class="sub-submenu-accordion-wrapper column-equal desktop-only">
																	<div class="sub-submenu-accordion-title">
																		<?php _e( 'サイズ', 'ambientlounge' ); ?>

																		<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																			<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																		</svg>
																	</div>
																	<div class="sub-submenu-accordion-content">
																		<div class="sizechart-columns">
																			<div class="sizechart-column">
																				<div class="sizechart-title">
																					<?php _e( 'XS', 'ambientlounge' ); ?>
																				</div>
																				<div class="sizechart-dog sizechart-xs">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-xs" title="<?php _e( '超小型犬', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_s.svg" alt="<?php _e( '超小型犬', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '超小型犬<br>5 k g', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>
																				<div class="sizechart-cat sizechart-xs">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-xs" title="<?php _e( '小型猫1頭', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_s.svg" alt="<?php _e( '小型猫1頭', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '小型猫1頭', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>																			
																			</div>

																			<div class="sizechart-column">
																				<div class="sizechart-title">
																					<?php _e( 'S', 'ambientlounge' ); ?>
																				</div>
																				<div class="sizechart-dog">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-s" title="<?php _e( '小型犬', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_s.svg" alt="<?php _e( '小型犬', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '小型犬<br>10 k g', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>
																				<div class="sizechart-cat">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-s" title="<?php _e( '成猫1頭', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_s.svg" alt="<?php _e( '成猫1頭', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '成猫1頭', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>																			
																			</div>

																			<div class="sizechart-column">
																				<div class="sizechart-title">
																					<?php _e( 'M', 'ambientlounge' ); ?>
																				</div>
																				<div class="sizechart-dog">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-m" title="<?php _e( '中型犬', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_m.svg" alt="<?php _e( '中型犬', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '中型犬<br>25 k g', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>
																				<div class="sizechart-cat">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-m" title="<?php _e( '成猫2頭', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_m.svg" alt="<?php _e( '成猫2頭', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '成猫2頭', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>																			
																			</div>

																			<div class="sizechart-column">
																				<div class="sizechart-title">
																					<?php _e( 'L', 'ambientlounge' ); ?>
																				</div>
																				<div class="sizechart-dog">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-l" title="<?php _e( '大型犬', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_l.svg" alt="<?php _e( '大型犬', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '大型犬<br>40 k g', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>
																				<div class="sizechart-cat">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-l" title="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_l.svg" alt="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>																			
																			</div>

																			<div class="sizechart-column">
																				<div class="sizechart-title">
																					<?php _e( 'XXL', 'ambientlounge' ); ?>
																				</div>
																				<div class="sizechart-dog">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-xxl" title="<?php _e( '超大型犬', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_xxl.svg" alt="<?php _e( '超大型犬', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '超大型犬<br>40 k g以上', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>
																				<div class="sizechart-cat">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-xxl" title="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																						<div class="sizechart-image">
																							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_xxl.svg" alt="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																						</div>
																						<div class="sizechart-description">
																							<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>
																						</div>
																					</a>
																				</div>																			
																			</div>
																		</div>		
																	</div>
																</div>	

																<div id="petlounge-sizechart-dog-mobile-wrapper" class="sub-submenu-accordion-wrapper column-equal mobile-only">
																	<div class="sub-submenu-accordion-title">
																		<?php _e( 'サイズ・犬', 'ambientlounge' ); ?>

																		<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																			<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																		</svg>
																	</div>
																	<div class="sub-submenu-accordion-content">
																		<div class="sizechart-row">
																			<div class="sizechart-columns">																		
																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'XS', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-dog sizechart-xs">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-xs" title="<?php _e( '超小型犬', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_s.svg" alt="<?php _e( '超小型犬', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '超小型犬<br>5 k g', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																																						
																				</div>

																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'S', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-dog">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-s" title="<?php _e( '小型犬', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_s.svg" alt="<?php _e( '小型犬', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '小型犬<br>10 k g', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																																					
																				</div>

																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'M', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-dog">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-m" title="<?php _e( '中型犬', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_m.svg" alt="<?php _e( '中型犬', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '中型犬<br>25 k g', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																																							
																				</div>
																			</div>

																			<div class="sizechart-columns">	
																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'L', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-dog">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-l" title="<?php _e( '大型犬', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_l.svg" alt="<?php _e( '大型犬', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '大型犬<br>40 k g', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																		
																				</div>

																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'XXL', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-dog">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/?size=size-xxl" title="<?php _e( '超大型犬', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/dog_xxl.svg" alt="<?php _e( '超大型犬', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '超大型犬<br>40 k g以上', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																		
																				</div>
																			</div>
																		</div> 
																	</div>
																</div>	

																<div id="petlounge-sizechart-cat-mobile-wrapper" class="sub-submenu-accordion-wrapper column-equal mobile-only">
																	<div class="sub-submenu-accordion-title">
																		<?php _e( 'サイズ・猫', 'ambientlounge' ); ?>

																		<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																			<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																		</svg>
																	</div>
																	<div class="sub-submenu-accordion-content">
																		<div class="sizechart-row">
																			<div class="sizechart-columns">
																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'XS', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-cat sizechart-xs">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-xs" title="<?php _e( '小型猫1頭', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_s.svg" alt="<?php _e( '小型猫1頭', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '小型猫1頭', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>

																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'S', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-cat">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-s" title="<?php _e( '成猫1頭', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_s.svg" alt="<?php _e( '成猫1頭', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '成猫1頭', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>

																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'M', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-cat">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-m" title="<?php _e( '成猫2頭', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_m.svg" alt="<?php _e( '成猫2頭', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '成猫2頭', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>
																			</div>

																			<div class="sizechart-columns">
																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'L', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-cat">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-l" title="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_l.svg" alt="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>

																				<div class="sizechart-column">
																					<div class="sizechart-title">
																						<?php _e( 'XXL', 'ambientlounge' ); ?>
																					</div>
																					<div class="sizechart-cat">
																						<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-categories/?size=size-xxl" title="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																							<div class="sizechart-image">
																								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sizechart/cat_xxl.svg" alt="<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>">
																							</div>
																							<div class="sizechart-description">
																								<?php _e( '多頭飼い主向け', 'ambientlounge' ); ?>
																							</div>
																						</a>
																					</div>																			
																				</div>
																			</div>
																		</div>	
																	</div>
																</div>																

																<div class="column-equal">

																	<div id="petlounge-colorpurposeaccessories" class="columns gap-full">
																		<?php
																		/*
																		<div class="sub-submenu-accordion-wrapper column">
																			<div class="sub-submenu-accordion-title column-attr">
																				<?php _e( 'カラー', 'ambientlounge' ); ?>

																				<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																					<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																				</svg>
																			</div>
																			<div class="sub-submenu-accordion-content column-value">
																				<a href="#" title="<?php _e( 'グレー', 'ambientlounge' ); ?>"><div class="tone color-gray"><span><?php _e( 'グレー', 'ambientlounge' ); ?></span></div></a>
																				<a href="#" title="<?php _e( 'ベージュ', 'ambientlounge' ); ?>"><div class="tone color-beige"><span><?php _e( 'ベージュ', 'ambientlounge' ); ?></span></div></a>
																				<a href="#" title="<?php _e( 'ブルードリーム', 'ambientlounge' ); ?>"><div class="tone color-bluedream"><span><?php _e( 'ブルードリーム', 'ambientlounge' ); ?></span></div></a>
																			</div>
																		</div>
																		*/
																		?>

																		<div class="sub-submenu-accordion-wrapper column desktop-only">
																			<div class="sub-submenu-accordion-title column-attr">
																				<?php _e( 'アクセサリー', 'ambientlounge' ); ?>

																				<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																					<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																				</svg>
																			</div>
																			<div class="sub-submenu-accordion-content column-value">
																				<?php
																				/*
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/products/dog-bed/dog-covers/" title="<?php _e( '取替カバー', 'ambientlounge' ); ?>"><span><?php _e( '取替カバー', 'ambientlounge' ); ?></span></a>
																				*/
																				?>
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/the-shampoo-intro/" title="<?php _e( 'The Shampoo', 'ambientlounge' ); ?>"><span><?php _e( 'The Shampoo', 'ambientlounge' ); ?></span></a>
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/pet-accessories/" title="<?php _e( 'その他', 'ambientlounge' ); ?>"><span><?php _e( 'その他', 'ambientlounge' ); ?></span></a>

																				<?php
																				/*
																				<a href="#" title="<?php _e( 'その他', 'ambientlounge' ); ?>"><span><?php _e( 'その他', 'ambientlounge' ); ?></span></a>
																				*/
																				?>
																			</div>
																		</div>

																		<div id="petlounge-purpose" class="sub-submenu-accordion-wrapper column">
																			<div class="sub-submenu-accordion-title column-attr">
																				<?php _e( 'ご利用用途', 'ambientlounge' ); ?>

																				<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																					<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																				</svg>
																			</div>
																			<div class="sub-submenu-accordion-content column-value">
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/spring-summer/" title="<?php _e( '春夏商品', 'ambientlounge' ); ?>"><span><?php _e( '春夏商品', 'ambientlounge' ); ?></span></a>
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/autumn-winter/" title="<?php _e( '秋冬商品', 'ambientlounge' ); ?>"><span><?php _e( '秋冬商品', 'ambientlounge' ); ?></span></a>
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/senior" title="<?php _e( 'シニア', 'ambientlounge' ); ?>"><span><?php _e( 'シニア', 'ambientlounge' ); ?></span></a>
																			</div>
																		</div>

																		
																	</div>

																	<div class="columns gap-full">
																		<div class="sub-submenu-accordion-wrapper column column-equal">
																			<div class="sub-submenu-accordion-title column-attr">
																				<?php _e( 'カラー', 'ambientlounge' ); ?>

																				<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																					<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																				</svg>
																			</div>
																			<div class="sub-submenu-accordion-content column-value">
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/grey/" title="<?php _e( 'グレー', 'ambientlounge' ); ?>"><div class="tone color-gray"><span><?php _e( 'グレー', 'ambientlounge' ); ?></span></div></a>
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/beige/" title="<?php _e( 'ベージュ', 'ambientlounge' ); ?>"><div class="tone color-beige"><span><?php _e( 'ベージュ', 'ambientlounge' ); ?></span></div></a>
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/blue/" title="<?php _e( 'ブルードリーム', 'ambientlounge' ); ?>"><div class="tone color-bluedream"><span><?php _e( 'ブルードリーム', 'ambientlounge' ); ?></span></div></a>
																			</div>
																		</div>

																		<div class="sub-submenu-accordion-wrapper column column-equal desktop-only">
																			<div class="sub-submenu-accordion-title column-attr">
																				<?php _e( 'ふるさと納税', 'ambientlounge' ); ?>

																				<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																					<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																				</svg>
																			</div>
																			<div class="sub-submenu-accordion-content column-value">
																				<a href="<?php echo $GLOBALS['currDomain']; ?>/furusato/" title="<?php _e( 'ふるさと納税', 'ambientlounge' ); ?>"><span><?php _e( 'ふるさと納税', 'ambientlounge' ); ?></span></a>
																			</div>
																		</div>
																	</div>

																	

																</div>
															</div>
															<?php
															/*
															<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-categories/" class="soft-link"><?php _e( '全ての商品を見る', 'ambientlounge' ); ?></a>
															*/
															?>
														</div>
													</div>

													<div id="reasonforchoosing" class="submenu-content submenu-accordion-wrapper">
														<div class="submenu-accordion-title">
															<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
															</svg>

															<?php _e( '選ぶ理由', 'ambientlounge' ); ?>

															<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
															</svg>
														</div>															
														<div class="submenu-accordion-content">
															<div class="submenu-accordion-title">
																<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M5.02746 0.437631C5.33805 0.711683 5.36767 1.18563 5.09362 1.49623L2.00204 5.00001L5.09362 8.50379C5.36767 8.81439 5.33805 9.28833 5.02746 9.56239C4.71686 9.83644 4.24291 9.80682 3.96886 9.49622L0.439451 5.49623C0.189288 5.21271 0.189288 4.78731 0.439451 4.50379L3.96886 0.503793C4.24292 0.1932 4.71686 0.163578 5.02746 0.437631Z" fill="#0066CC"/>
																	<path fill-rule="evenodd" clip-rule="evenodd" d="M16.75 5C16.75 5.41421 16.4142 5.75 16 5.75L1.88235 5.75C1.46814 5.75 1.13235 5.41421 1.13235 5C1.13235 4.58579 1.46814 4.25 1.88235 4.25L16 4.25C16.4142 4.25 16.75 4.58579 16.75 5Z" fill="#0066CC"/>
																</svg>

																<?php _e( '選ぶ理由', 'ambientlounge' ); ?>
															</div>	
															<div class="columns">
																<div class="sub-submenu-accordion-wrapper active">
																	<div class="sub-submenu-accordion-title">
																		<?php _e( 'サイズ', 'ambientlounge' ); ?>

																		<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																			<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																		</svg>
																	</div>
																	<div class="sub-submenu-accordion-content columns column-equal card card-columns radius-half has-lightgray-background-color">
																		<div class="column">
																			<div class="title">
																				<?php _e( '選ぶ理由', 'ambientlounge' ); ?>
																			</div>
																			<p><?php _e( 'デザイン性と安全性についてご紹介します', 'ambientlounge' ); ?></p>
																			<div class="columns gap-half">
																				<div class="column-equal">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/dog" title="<?php _e( '犬向け', 'ambientlounge' ); ?>">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/reasonforchoosing/LPDog.webp" alt="<?php _e( '犬向け', 'ambientlounge' ); ?>">
																					</a>
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/dog" title="<?php _e( '犬向け', 'ambientlounge' ); ?>"><?php _e( '犬向け', 'ambientlounge' ); ?></a>
																				</div>
																				<div class="column-equal">
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/cat" title="<?php _e( '猫向け', 'ambientlounge' ); ?>">
																						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/reasonforchoosing/LPCat.webp" alt="<?php _e( '猫向け', 'ambientlounge' ); ?>">
																					</a>
																					<a href="<?php echo $GLOBALS['currDomain']; ?>/cat" title="<?php _e( '猫向け', 'ambientlounge' ); ?>"><?php _e( '猫向け', 'ambientlounge' ); ?></a>
																				</div>
																			</div>
																		</div>
																		<div class="column">
																			<div class="columns card card-columns radius-half gap-half padding-half has-white-background-color has-secondarydark-color">
																				<div class="column-equal">
																					<div class="title h6">
																						<?php _e( 'ベッドカバーについて', 'ambientlounge' ); ?>		
																					</div>
																					<p class="h7"><?php _e( 'お洗濯も簡単。ベッドカバーの魅力をご紹介', 'ambientlounge' ); ?></p>
																					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/reasonforchoosing/LPCover.webp" alt="<?php _e( 'ベッドカバーについて', 'ambientlounge' ); ?>">
																					<div class="columns gap-none">
																						<div class="column-equal">
																							<a href="<?php echo $GLOBALS['currDomain']; ?>/products/dog-bed/dog-covers/" class="link-arrow" title="<?php _e( '犬向け', 'ambientlounge' ); ?>"><?php _e( '犬向け', 'ambientlounge' ); ?></a>
																						</div>
																						<div class="column-equal">
																							<a href="<?php echo $GLOBALS['currDomain']; ?>/products/cat-bed/cat-covers/" class="link-arrow" title="<?php _e( '猫向け', 'ambientlounge' ); ?>"><?php _e( '猫向け', 'ambientlounge' ); ?></a>
																						</div>
																					</div>
																				</div>
																				<div class="column-equal">
																					<div class="title h6">
																						<?php _e( 'フィリングについて', 'ambientlounge' ); ?>		
																					</div>
																					<p class="h7"><?php _e( 'ペットの好みや性格、年齢に合わせて2種類をご用意', 'ambientlounge' ); ?></p>
																					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/reasonforchoosing/LPFilling-2.svg" alt="<?php _e( 'フィリングについて', 'ambientlounge' ); ?>">
																					<div class="columns gap-none">
																						<div class="column-equal">
																							<a href="<?php echo $GLOBALS['currDomain']; ?>/dog-filling" class="link-arrow" title="犬向け"><?php _e( '犬向け', 'ambientlounge' ); ?></a>
																						</div>
																						<div class="column-equal">
																							<a href="<?php echo $GLOBALS['currDomain']; ?>/cat-filling" class="link-arrow" title="猫向け"><?php _e( '猫向け', 'ambientlounge' ); ?></a>
																						</div>
																					</div>	
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="column-side">
																	<div class="sub-submenu-accordion-wrapper">
																		<div class="sub-submenu-accordion-title column-attr">
																			<div class="title">																			
																				<?php _e( 'サイズ選びに迷ったら', 'ambientlounge' ); ?>																				
																			</div>
																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content column-value">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/compare-pet/" title="サイズを比較する"><?php _e( 'サイズを比較する', 'ambientlounge' ); ?></a>
																		</div>
																	</div>

																	<div class="sub-submenu-accordion-wrapper">
																		<div class="sub-submenu-accordion-title column-attr">
																			<div class="title">
																				<?php _e( '購入について', 'ambientlounge' ); ?>																			
																			</div>

																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content column-value">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/30days-trial/" title="60日間トライアル"><?php _e( '60日間トライアル', 'ambientlounge' ); ?></a>
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/shopping-guide/" title="ご利用ガイド"><?php _e( 'ご利用ガイド', 'ambientlounge' ); ?></a>
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/retail-locations/" title="取扱店舗一覧"><?php _e( '取扱店舗一覧', 'ambientlounge' ); ?></a>
																		</div>
																	</div>	
																	
																	<!--
																	<div class="sub-submenu-accordion-wrapper">
																		<div class="sub-submenu-accordion-title column-attr">
																			<div class="title">
																				<?php _e( 'Blog', 'ambientlounge' ); ?>																			
																			</div>

																			<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																			</svg>
																		</div>
																		<div class="sub-submenu-accordion-content">
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/blog/" title="すべて"><?php _e( 'すべて', 'ambientlounge' ); ?></a>
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/category/brand/" title="ブランド"><?php _e( 'ブランド', 'ambientlounge' ); ?></a>																
																			<a href="<?php echo $GLOBALS['currDomain']; ?>/category/pet/" title="ペット"><?php _e( 'ペット', 'ambientlounge' ); ?></a>
																		</div>
																	</div>	
																	--> 
																</div>
															</div>
														</div>
													</div>

													<div id="pet-accessories-mobile" class="submenu-content mobile-only">
														<div class="submenu-accordion-content">
															<div id="pet-blog-section1" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<?php _e( 'アクセサリー', 'ambientlounge' ); ?>

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/the-shampoo-intro/" title="<?php _e( 'The Shampoo', 'ambientlounge' ); ?>"><span><?php _e( 'The Shampoo', 'ambientlounge' ); ?></span></a>
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/pet-accessories/" title="<?php _e( 'その他', 'ambientlounge' ); ?>"><span><?php _e( 'その他', 'ambientlounge' ); ?></span></a>
																</div>
															</div>															
														</div>
													</div>

													<div id="pet-furusato-mobile" class="submenu-content mobile-only">
														<div class="submenu-accordion-content">
															<div id="pet-blog-section1" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<?php _e( 'ふるさと納税', 'ambientlounge' ); ?>

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/furusato/" title="<?php _e( 'ふるさと納税', 'ambientlounge' ); ?>"><span><?php _e( 'ふるさと納税', 'ambientlounge' ); ?></span></a>
																</div>
															</div>															
														</div>
													</div>

													<div id="pet-blog" class="submenu-content desktop-only">
														<div class="submenu-accordion-content">
															<div id="pet-blog-section1" class="sub-submenu-accordion-wrapper">
																<div class="sub-submenu-accordion-title">
																	<span>
																		<?php _e( 'カテゴリー', 'ambientlounge' ); ?>
																	</span>

																	<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M1.16498 0.46967C1.45788 0.176777 1.93275 0.176777 2.22564 0.46967L8.72564 6.96967C9.01854 7.26256 9.01854 7.73744 8.72564 8.03033L2.22564 14.5303C1.93275 14.8232 1.45788 14.8232 1.16498 14.5303C0.872089 14.2374 0.872089 13.7626 1.16498 13.4697L7.13465 7.5L1.16498 1.53033C0.872089 1.23744 0.872089 0.762563 1.16498 0.46967Z" fill="#0066CC"/>
																	</svg>
																</div>
																<div class="sub-submenu-accordion-content">
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/blog/" title="すべて"><?php _e( 'すべて', 'ambientlounge' ); ?></a>
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/category/brand/" title="ブランド"><?php _e( 'ブランド', 'ambientlounge' ); ?></a>																
																	<a href="<?php echo $GLOBALS['currDomain']; ?>/category/pet/" title="ペット"><?php _e( 'ペット', 'ambientlounge' ); ?></a>
																</div>
															</div>															
														</div>
													</div>

												</div>													
											</div>	
										</nav>										
									</li>									
								</ul>

								<div class="mobile-bottom-menu">
									<div class="myaccount-bottom-menu-wrapper">
										<a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>">
											<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M9.67857 18.3571C14.4716 18.3571 18.3571 14.4716 18.3571 9.67857C18.3571 4.88553 14.4716 1 9.67857 1C4.88553 1 1 4.88553 1 9.67857C1 14.4716 4.88553 18.3571 9.67857 18.3571Z" stroke="#2F67B2" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
												<path d="M9.67984 12.9844C7.21127 12.9844 4.98377 14.0162 3.40234 15.6715C4.98377 17.3269 7.20806 18.3587 9.67984 18.3587C12.1516 18.3587 14.3759 17.3269 15.9573 15.6715C14.3759 14.0162 12.1516 12.9844 9.67984 12.9844Z" stroke="#2F67B2" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
												<path d="M9.68089 11.0102C11.3514 11.0102 12.7055 9.65604 12.7055 7.98558C12.7055 6.31512 11.3514 4.96094 9.68089 4.96094C8.01043 4.96094 6.65625 6.31512 6.65625 7.98558C6.65625 9.65604 8.01043 11.0102 9.68089 11.0102Z" stroke="#2F67B2" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
											</svg>
											<?php _e( 'マイアカウント', 'ambientlounge' ); ?>
										</a>
									</div>
									<div class="service-bottom-menu-wrapper">
										<a href="mailto:service@ambientlounge.co.jp">service@ambientlounge.co.jp</a>
										<a href="tel:0120429332">0120429332</a>
									</div>
								</div>
							</div>
						</div>
					</nav>
					<?php
						/**
							* generate_after_header_content hook.
							*
							* @since 0.1
							*
							* @hooked generate_add_navigation_float_right - 5
							*/
							
						// Remove Search
						remove_action( 'generate_menu_bar_items', 'generate_do_navigation_search_button' );
						remove_action( 'generate_inside_navigation', 'generate_navigation_search', 10 );
						
						// Remove Primary Menu
						//remove_action( 'generate_after_header_content', 'generate_add_navigation_float_right', 5 );

						// Remove Mobile Primary Menu
						//remove_action( 'generate_before_navigation', 'generate_do_header_mobile_menu_toggle' );
						
						// Remove Secondary Menu
						remove_action( 'generate_after_header_content', 'generate_do_secondary_navigation_float_right', 7 );
						
						/*
							LIST ACTION HOOK
							$hook_name = 'generate_after_header_content';
							global $wp_filter;
							var_dump( $wp_filter[$hook_name] );
						*/			
						
						// Primary Menu				
						//do_action( 'generate_after_header_content' );
					?>
					
					
				</div>
				
				<div class="logo-wrapper">
					<?php
					// Logo
					/**
						* generate_before_header_content hook.
						*
						* @since 0.1
						*/

					// Remove Logo from generate_before_header_content hook
					remove_action( 'generate_before_header_content', 'generate_do_site_logo', 5 );                        

					$alternate_logo = get_theme_mod( 'alternate_logo' );

					if ( $transparent_menu == 'yes' && !empty( $alternate_logo ) && substr( $alternate_logo, -14 ) != 'selectlogo.png' )
					{
						// Transparent Menu selected, get logo from alternate logo
						?>
							<div class="site-logo">
								<a href="<?php echo site_url(); ?>" rel="home">
									<img src="<?php echo get_theme_mod( 'alternate_logo' ) ?>" class="header-image is-logo-image" alt="<?php echo get_bloginfo( 'name' ); ?>" >
								</a>
							</div>
						<?php

							do_action( 'generate_before_header_content' );
					}
					else
					{
						// Default logo, put logo back to generate_before_header_content hook
						add_action( 'generate_before_header_content', 'generate_do_site_logo', 5 );
						do_action( 'generate_before_header_content' );
					}  
	
					if ( ! generate_is_using_flexbox() ) {
						// Add our main header items.
						generate_header_items();
					}
				?>
				</div>
				
				<div class="right-menu">
					<nav id="secondary-navigation">
						<div class="main-nav">
							<ul class="secondary-menu">
								<li id="menu-search-wrapper">
									<a aria-label="Open Search Bar" href="#"><?php _e( 'Search', 'ambientlounge' ); ?></a>		
								</li>
								<li id="menu-myaccount-wrapper">
									<a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>"><?php _e( 'マイアカウント', 'ambientlounge' ); ?></a>
								</li>
								<li id="menu-cart-wrapper">
									<a href="<?php echo esc_url( wc_get_cart_url() ); ?>"><?php _e( 'お買い物かご', 'ambientlounge' ); ?></a>
									<?php
										$cart_count = WC()->cart->get_cart_contents_count();
										if ($cart_count > 0) {
											echo '<span class="cart-count">' . esc_html($cart_count) . '</span>';
										} 
									?>
								</li>
							</ul>
						</div>
					</nav>

					<?php
						/*
						// Add Search
						//add_action( 'new_search_location', 'generate_do_navigation_search_button', 5 );
						//add_action( 'new_search_location', 'generate_navigation_search', 10 );
						
						// Add Secondary Menu
						//add_action( 'new_secondary_menu_location', 'generate_do_secondary_navigation_float_right' );
					?>
					<div class="header-top-search-wrapper">
						<?php //do_action( 'new_search_location' ); ?>
					</div>
					<?php //do_action( 'new_secondary_menu_location' ); ?>
					<nav id="secondary-navigation" class="secondary-navigation">
						<div class="inside-navigation">
							<div class="main-nav">
								<ul class="secondary-menu">
									<li>
										<a href="#">マイアカウント</a>
									</li>
									<li>
										<a href="#">お買い物かご</a>
									</li>
								</ul>
							</div>
						</div>
					</nav>
						*/
					?>
				</div> 
			</div>

			<div class="header-search-wrapper">
				<div class="header-search-inner">
					<div class="header-search-container">
						<div class="header-search-form-wrapper">
							<form>
								<input type="text" id="header-search-input" name="s" placeholder="Search...">
								<input type="submit" value="消去">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div <?php generate_do_attr( 'page' ); ?>>

		<?php
			/**
			* generate_after_header hook.
			*
			* @since 0.1
			*
			* @hooked generate_featured_page_header - 10
			*/

			// Page Featured Image
			if ( has_post_thumbnail( get_the_ID() ) && is_page() ) 
			{			
				?>
					<div class="featured-image-wrapper">
						<?php 
							// Featured Image
							do_action( 'generate_after_header' ); 

							// Title (only when featured image exist)
							$show_page_title = get_post_meta( $post->ID, '_show_page_title_meta_key', true );
							
							if ( !is_front_page() && !is_checkout() && !is_account_page() && !is_woocommerce() && ( $show_page_title == 'yes' || $show_page_title == '' ) )
							{
								?>									
									<div class="page-heading-wrapper">
										<div class="entry-container">
											<?php
												if ( generate_show_title() ) {
													$params = generate_get_the_title_parameters();

													the_title( $params['before'], $params['after'] );
												}
											?>
										</div>
									</div>
								<?php
							}
						?>
					</div>
				<?php
			}	
		?>	

		<?php
			/* Cart - Woocommerce/cart/ */	
			if ( is_cart() ) 
			{
				$image = get_field( 'image' );
				$title = get_field( 'title' );
				$description = get_field( 'description' );
				$link = get_field( 'link' );

				// Check for b2b users
				$current_user = wp_get_current_user();
				global $wp_roles;

				$b2b_role = '';
                foreach ($current_user->roles as $role) {
                    
                    $role = translate_user_role( $wp_roles->roles[ $role ]['name'] );
            
                    if (stripos($role, 'B2B') !== FALSE) { // Role starts with "B2B"
                        $b2b_role = $role;
                        break;
                    }
                }

				if(empty($b2b_role)) {
				?>
					<a href="<?php echo $link; ?>" title="<?php echo $title; ?>" class="notice-link">
						<div class="notice-wrapper">
							<div class="notice-container">
								<div class="notice-inner">
									<div class="notice-image" style="background-image:url(<?php echo $image; ?>);">
									</div>
									<div class="notice-content">
										<div class="notice-title">
											<?php echo $title; ?>
										</div>
										<div class="notice-description">
											<?php echo $description; ?>
										</div>
									</div>
									<div class="notice-action">
										<svg width="44" height="45" viewBox="0 0 44 45" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M0 0.0749512H43.9998V44.24H0V0.0749512Z" fill="#F7F9FB"/>
											<path fill-rule="evenodd" clip-rule="evenodd" d="M17.2929 13.4178C17.6834 13.0258 18.3166 13.0258 18.7071 13.4178L27.2071 21.9497C27.5976 22.3416 27.5976 22.9772 27.2071 23.3692L18.7071 31.9011C18.3166 32.2931 17.6834 32.2931 17.2929 31.9011C16.9024 31.5091 16.9024 30.8735 17.2929 30.4815L25.0857 22.6594L17.2929 14.8373C16.9024 14.4453 16.9024 13.8098 17.2929 13.4178Z" fill="#B2BBC4"/>
										</svg>
									</div>
								</div>
							</div>
						</div>
					</a>
				<?php		
				}	
			}			

			/* Single Product - Woocommerce/content-single-product */
			if ( is_product() ) 
			{
				global $product;
				
				if ( ! is_a( $product, 'WC_Product' ) ) {
					$product = wc_get_product( get_the_id() );
				}
				
				$thumbID  = $product->get_image_id();
				$thumbURL = wp_get_attachment_image_url( $thumbID, 'mobile' );
				?>
					<div class="single-product-topbar">
						<div class="container">
							<div class="single-product-topbar-left">
								<?php
									if ( !empty( $thumbURL ) ) 
									{
										?>
											<img src="<?php echo $thumbURL; ?>">						
										<?php
									}
								?>
							</div>
							<div class="single-product-topbar-right">
								<div class="single-product-topbar-selectedproduct">									
									<?php _e( '選択中の商品', 'ambientlounge' ); ?>
								</div>
								<div class="single-product-topbar-price">
									<?php 
										$price = $product->get_price(); 
										echo $product->get_price_html();				
									?>
									<small class="woocommerce-price-suffix">(税込/送料無料)</small>
								</div>
								<div class="single-product-topbar-addtocart">
									<?php
										$product = wc_get_product( get_the_ID() );										
									?>
									<a href="#" onclick='jQuery(".single-product-right .single_add_to_cart_button").trigger("click");' class="button"><?php _e( 'カートに追加', 'ambientlounge' ); ?></a>
								</div>
							</div>
						</div>
					</div>
				<?php
			}

			/* Template Offers */
			if ( is_page_template( 'template-offers.php' ) )
			{
				$productID = $_GET["item"];
			
				if( !empty( $productID ) )
				{			
					$product=wc_get_product($productID);
					$productID_parent = $product->get_parent_id();

					$postThumb = get_the_post_thumbnail_url( $productID );
					if(!empty($productID_parent)) {
						$variation = new WC_Product_Variation( $productID );
						$postThumb = $variation->get_image_id();
						$postThumb = wp_get_attachment_image_src($postThumb, 'thumbnail');
						$postThumb = $postThumb[0];
					}

					$args = array(
						'p' => ($productID_parent == 0) ? $productID : $productID_parent,
						'post_type' => array( 'product' )
					);					
					$query = new WP_Query( $args );
						
					if ( $query->have_posts() ) 
					{
						while ( $query->have_posts() ) 
						{
							$query->the_post();

							global $post;	
							$title = get_the_title();

							?>
								<div class="selected-product-topbar">
									<div class="container">
										<div class="selected-product-topbar-left">
											<?php
												if ( !empty( $postThumb ) ) 
												{
													?>
														<img src="<?php echo $postThumb; ?>" alt="<?php echo $title; ?>">								
													<?php
												}
											?>
											<div class="selected-product-topbar-description">
												<div class="selected-product-topbar-title">
													<?php echo $title; ?>
												</div>
												<div class="selected-product-topbar-selectedproduct">
													<?php _e( '選択中の商品', 'ambientlounge' ); ?>
												</div>
												<div class="selected-product-topbar-variation">
													<?php
														$product = wc_get_product( $productID );
														if ( $product->is_type( 'variation' ) )  
														{
															$attributes = $product->get_variation_attributes();
															$variation_attributes = array();

															foreach ( $attributes as $key => $value ) 
															{
																$taxonomy = str_replace( 'attribute_', '', $key );
																$term = get_term_by( 'slug', $value, $taxonomy );
																$variation_attributes[] = $term->name;
															}
														}

														if ( !empty( $variation_attributes ) )
														{
															echo rtrim(implode( ' / ', $variation_attributes ), ' / ');
														}
													?>
												</div>
											</div>								
										</div>
										<div class="selected-product-topbar-right">								
											<div class="selected-product-topbar-addtocart">
												<?php
													$product = wc_get_product( get_the_ID() );										
												?>
												<a href="<?php echo wc_get_cart_url(); ?>"class="button"><?php _e( 'カート', 'ambientlounge' ); ?></a>
											</div>
										</div>
									</div>
								</div>
							<?php
						}
					}
					wp_reset_postdata();				
				}
			}
		?>

		

		<?php
		/**
		 * generate_inside_site_container hook.
		 *
		 * @since 2.4
		 */
		do_action( 'generate_inside_site_container' );
		?>
		<div <?php generate_do_attr( 'site-content' ); ?>>
			<?php
			/**
			 * generate_inside_container hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_inside_container' );
