<?php
/*
	Template Name: The Shampoo
*/
?>

<?php
	get_header();	
?>

	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/css/reset.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/css/common.css?2203230937">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/css/shampoo.css?ver=1.5.8" rel="stylesheet" />
	
    <?php
	/*
    <header id="HEADER">
        <div class="wrap">
            <div class="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/hd_logo.png?1" alt="ambient lounge®"></div>
            <div class="tel"><a href="tel:0120-429-332"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/hd_tel.png?1" alt="0120-429-332 10:00～17：00(土日祝日を除く)"></a></div>
            <div class="contact"><a href="https://ambientlounge.co.jp/contact/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/hd_contact.png?1" alt="お問い合わせ"></a></div>
        </div>
    </header>
	*/
	?>
    
    <div id="HEADER" class="img fv">
        <h1><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/fv_bg.jpg?1" alt="国産シルク美容液で洗うスキンケア発想から生まれたオールインワンシャンプー"></h1>
        <div class="box">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/fv_tit.png?1" alt="愛犬にもわたしにも。the Shampoo MADE IN JAPAN" class="w100">
            <div class="fvwrap">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/fv_point.png?1" alt="シルク抽出液70%配合 ノンシリコン無添加 犬にも人にも使える" class="w100">
                <div class="fvbtnwrap">
                    <a href="#SILK" class="fvbtn fvbtn01"></a>
                    <a href="#POINT01" class="fvbtn fvbtn02"></a>
                    <a href="#POINT02" class="fvbtn fvbtn03"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="img buy">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/buy01_bg.jpg?1" alt="">
        <div class="box">
            <a href="#CV" class="btn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/btn_buy.png?1" alt="商品の購入はこちら"></a>
        </div>
    </div>
    <div class="img conc">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/conc_bg.jpg?1" alt="">
        <div class="box">
            <h2 class="head"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/conc_head.png?1" alt="CONCEPT"></h2>
            <p class="txt">
                 現代社会でアレルギーに悩むのは人も犬も同じです。<br>
                <strong>" 生き物 "</strong>である人も犬も一番大切なことは<br>
                <span class="mark">健康であること</span><br>
                100%国産シルクの恵みを生かして<br>
                無添加で作られた the Shampoo は、<br>
                人と犬が健康的な生活習慣の一部を共有し、<br>
                シンプルでサスティナブルに<br>
                <strong>" 一緒に生きる "</strong>ことを提案します。
            </p>
            <div class="txt t02">
                通常は水が使われるところを<br>
                すべて国産天然シルク抽出液に置き換え、<br>
                １本におよそ30個の国産繭を贅沢に使用した、まさに<br>
                <span class="mark">シルクで洗う</span><br>
                に等しい最高級シャンプー。
            </div>
        </div>
    </div>
    <div class="img silk01" id="SILK">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/silk01_bg.jpg?1" alt="">
        <div class="box">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/silk01_img01.png?1" alt="水の代わりにシルク抽出原液70%配合">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/silk01_100per.png?1" alt="100%">
            <div class="txt">
                <h2>国産シルク美容液で洗う</h2>
                <p>
                    天然ユーカリ精油で<br>
                    清涼感のあるウッディな香りが楽しめます
                </p>
            </div>
            <div class="txt t02">
                <p>
                    丁寧に抽出されたシルク抽出液には皮膚を作る成分に似た<br>
                    アミノ酸が豊富に含まれており、
                </p>
                <h2>強力な保湿作用、抗菌化作用、<br>消炎・修復作用などの効果が期待できます。</h2>
            </div>
        </div>
    </div>
    <div class="img">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/silk02_bg.jpg?2203230935" alt="天然シルク抽出液「保湿作用」「ターンオーバー促進」「修復作用」「抗菌作用」「老化防止」「消炎作用」「紫外線防止」">
    </div>
    <div class="img need">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/need_bg.jpg?1" alt="">
        <div class="box">
            <div class="txt">
                <p>
                    "犬と一緒に暮らすご家族全員で<br>
                    シェアできるシャンプー"で<br>
                    健やかな日々を送って欲しい。
                </p>
                <h2>
                    そんな想いで<br>
                     "最高のシャンプー"<br>
                    「the Shampoo」は<br>
                    誕生しました。
                </h2>
            </div>
        </div>
        <div class="box boxbtn">
            <a href="#CV" class="btn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/btn_buy.png?1" alt="商品の購入はこちら"></a>
        </div>
    </div>
    <div class="img point point01" id="POINT01">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/point01_bg.jpg?1" alt="point01 全犬種対応 「無添加」「無着色」「ノンシリコン」「天然ユーカリ精油使用」">
        <div class="box">
            <div class="txt">
                <h2>
                    毛が絡みやすい<br>
                    ロングコートヘアも<br>
                    <mark>"つやつや・ふわふわ"</mark>に
                </h2>
                <p>
                    犬の皮膚はとっても薄く、人間のおよそ1/3程と言われています。さらにデリケートなシニア犬やアレルギーのある犬にもやさしいオールインシャンプーはコンディショナーいらず。シルクの力で皮膚からしっとりと被毛を蘇らせます。においに敏感な犬たちに優しい、天然ユーカリ精油の自然な香り。
                </p>
            </div>
        </div>
    </div>
    <div class="img point point02" id="POINT02">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/point02_bg.jpg?1" alt="">
        <div class="box">
            <div class="txt">
                <h2>
                    <mark>1つのシャンプーを</mark><br>
                    <mark>みんなでシェアする</mark>
                </h2>
                <br>
                <h2>シンプルでより良い選択</h2>
                <p>
                    無添加で愛犬を含む全てのご家族が心から安心して使える "最高のシャンプー"として誕生した、天然シルク抽出液で洗う the Shampoo は人と犬が健康的な生活習慣でサスティナブルに一緒に生きる事を提案します。
                </p>
            </div>
        </div>
    </div>
    <div class="img point point03">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/point03_bg.jpg?1" alt="">
        <div class="box">
            <div class="txt">
                <h2>
                    <span>良質な国産繭の力で</span><br>
                    <mark>しっとりと艶のある</mark><br>
                    <mark>被毛(髪)や健全な皮膚へと導く</mark>
                </h2>
                <br>
                <br>
                <h2 class="drp01">群馬県北部の大自然から生まれる<br>良質な100％国産繭を使用</h2>
                <br>
                <p class="drp02">
                    広大な桑畑から始まる繭作り。手間ひまを惜しまず、時間をかけて大切に育てられた繭から作られるシルク抽出液が傷んだ皮膚や被毛(髪)をいたわり、修復します。the Shampooを選ぶことは国内でも希少な養蚕業を支えることにつながります。
                </p>
            </div>
        </div>
    </div>
    <div class="img point point04">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/point04_bg.jpg?1" alt="">
        <div class="box">
            <div class="txt">
                <h2>植物由来の<br>合成界面活性剤を配合</h2>
                <br>
                <p>
                    洗浄成分としてココナッツや<br>
                    アブラヤシから採取されるヤシ油やパーム核油等の<br>
                    植物油を合成してつくられています。
                </p>
            </div>
        </div>
        <div class="box boxbtn">
            <a href="#CV" class="btn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/btn_buy.png?1" alt="商品の購入はこちら"></a>
        </div>
    </div>
    <div class="img howto">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/howto_bg.jpg?1" alt="ご利用方法">
        <div class="box">
            <div class="txt">
                <p class="how01">シャンプー前にブラッシングを行い、毛のもつれがないことを確認し、体（髪）をお湯でよく濡らします。</p>
                <p class="how02">適量のシャンプー剤を手に取って泡立ててから体（頭皮）全体を優しくマッサージするように洗います。</p>
                <p class="how03">シルク成分を毛と皮膚に馴染ませるようにパックします。（3分程度推奨）その後、シャンプー剤が残らないように、お湯で十分に洗い流します。<br>効果的にご使用いただくため、コンディショナーの使用は出来る限り避けてください。</p>
            </div>
        </div>
    </div>
    <div class="img voice">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/voice_bg.jpg?1" alt="クチコミ">
        <div class="box">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/voice_head.png?1" alt="">
            <div class="txt">
                <p class="voice01">普段は1週間もすると、フケ、静電気、匂いなどが気になってくるのですが、1週間経った現在もフケもなく、とてもサラサラなのに静電気も気になりません。匂いは驚くほど無臭のままです。</p>
                <p class="voice02">美容師の夫も驚きの仕上がりで、指通りが良く、サラサラに仕上がりました。乾燥肌の子供のフケや痒みが落ち着いて嬉しいです。</p>
                <p class="voice03">泡ぎれがとても良い。白い毛の犬は汚れが落ち真っ白になったと実感します。コンディショナー不使用でこのしっとり感は驚きです！</p>
            </div>
        </div>
    </div>
    <div class="img detail">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/detail_bg.jpg?1" alt="">
        <div class="box">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/detail_head.png?1" alt="">
            <div class="txt">
                <h2>商品名</h2>
                <p>
                    the Shampoo<br>
                    (ペット用シャンプー /ヒト用シャンプー)
                </p>
                <p class="detail01">内容量:300ml  原産国:日本</p>
                <p class="detail02">【使用上の注意】</p>
                <p class="detail03">お肌に異常が生じていないかよく注意して使用してください。お肌に合わないときは、ご使用をおやめください。傷やはれもの、しっしん等、異常のある部位にはお使いにならないでください。目に入った時はこすらず十分に洗い流してください。異常が残る時は専門医にご相談ください。天然由来原料を使用しているため、色や香りにバラつきが生じることがありますが、いずれの場合も品質には問題ありません。</p>
                <p class="detail02">【保管上の注意】</p>
                <p class="detail03">乳幼児の手の届かないところに保管ください。極端に低温または高温の場所や直射日光のあたる場所をさけて保管ください。</p>
                <p class="detail02">【成分】</p>
                <p class="detail03">シルク抽出液70％、ココイルメチルタウリンNa、ココイルグルタミン酸TEA、ヤシ油脂肪酸エタノールアミン、ポリクオタニウム10、グリセリン、エタノール、フェノキシエタノール、AG+、ユーカリ精油</p>
            </div>
        </div>
    </div>
    <div class="img cv" id="CV">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/cv_bg.jpg?1" alt="">
        <div class="box">
            <div class="txt">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/cv_head.png?1" alt="天然シルクで洗う '最高のシャンプー'">
                <div class="cvwrap">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/cv_sample.png?1" alt="[サンプル 15ml]500円（税込）">
                    <a href="https://ambientlounge.co.jp/products/theoriginal/the-shampoo/?attribute_pa_shampoo=shampoo-sample-15ml" class="btn shampoo-internal-product"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/btn_buy_sample.png?1" alt="サンプルを購入する"></a>
                </div>
                <div class="cvwrap">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/cv_one.png?3" alt="[1本 300ml]6,900円（税込）">
                    <a href="https://ambientlounge.co.jp/products/theoriginal/the-shampoo/?attribute_pa_shampoo=shampoo-1x300ml" class="btn shampoo-internal-product"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/btn_buy_one.png?1" alt="1本購入する"></a>
                </div>
                <div class="cvwrap">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/cv_two.png?3" alt="[2本 600ml]900円OFF 13,300円（税込）">
                    <a href="https://ambientlounge.co.jp/products/theoriginal/the-shampoo/?attribute_pa_shampoo=shampoo-2x300ml" class="btn short shampoo-internal-product"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/btn_buy_two.png?1" alt="2本購入する"></a>
                </div>
                <div class="cvwrap">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/cv_five.png?3" alt="[5本 1500ml]4,500円OFF 33,000円（税込）">
                    <a href="https://ambientlounge.co.jp/products/theoriginal/the-shampoo/?attribute_pa_shampoo=shampoo-5x300ml" class="btn short shampoo-internal-product"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/btn_buy_five.png?1" alt="5本購入する"></a>
                </div>
    			
                <?php
				/*
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/cv_foot01.png?1" alt="">
                <a href="tel:0120-429-332"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/cv_foot02.png?1" alt="0120-429-332"></a>
                <a href="https://ambientlounge.co.jp/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/cv_foot03.png?1" alt="www.ambientlounge.co.jp"></a>
				*/
				?>
            </div>
        </div>
    </div>
    
    
    <div class="toTop">
        <a href="#HEADER"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/btn_top.png?1" alt=""></a>
    </div>
    
    
    <div class="stickyselect-wrapper">
        <div class="stickyselect-drop">
        </div>
        <div class="stickyselect-container">
            <div class="stickyselect-input-wrapper">
                <select class="stickyselect">                	
                </select>
                <label>                	
                </label>
            </div>
            <div class="stickyselect-action-wrapper">
                <a href="" title="" class="shampoo-external-cart">カートに入れる</a>
            </div>
        </div>
    </div>
    
    
    <footer>
        <div class="wrap">
            <small><img src="<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images/txt_copy.png?1" alt="Copyright © 2022 Ambient Lounge Japan. All Rights Reserved."></small>
        </div>
    </footer>
        
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    
    <script>
		var $j = jQuery.noConflict();
	
		$j(function(){
			// #で始まるリンクをクリックした場合
			$j('a[href^="#"]:not([href="#"])').click(function(){
				// スクロールの速度（ミリ秒）
				var speed = 400;
				// リンク先を取得してhrefという変数に代入
				var href= $j(this).attr("href");
				// リンク先にidがある場合ターゲットとして値を取得
				var target = $j(href == "#" || href == "" ? 'html' : href);
				// ターゲットの位置を取得し、調整がある場合は位置の調整を行う
				var position = target.offset().top;
				// スクロール実行
				$j('body,html').animate({scrollTop:position}, speed, 'swing');
				return false;
			});
		
			let $jpagetop = $j('.toTop');
			$j(window).on( 'scroll', function () {
				if ( $j(this).scrollTop() < 100 ) {
					$jpagetop.removeClass('is-active');
				} else {
					$jpagetop.addClass('is-active');
				}
				
				// Select - Show when scroll over viewport height
				if ( $j( this ).scrollTop() > $j( this ).height() ) {
					$j( 'body' ).addClass( 'stickyselect-show' );
				}
				else {
					$j( 'body' ).removeClass( 'stickyselect-show' );
				}
			});
			
			// Select - Data
				var data = [
					{
						id: 64526,
						img: '1.jpg',
						text: '１本 - サンプル15ml',
						price: '<span class="normal">¥500</span><!--<span class="original">¥500</span> <span class="disc">¥450</span>--> (税込/送料無料)'			
					},
					{
						id: 64527,
						img: '2.jpg',
						text: '１本 - 300ml',
						price: '<span class="normal">¥4,900</span><!--<span class="original">¥4,900</span> <span class="disc">¥6,211</span>--> (税込/送料無料)',
						selected: true
					},
					{
						id: 64528,
						img: '3.jpg',
						text: '2本 - 600ml 【900円OFF】',
						price: '<span class="normal">¥8,900</span><!--<span class="original">¥8,900</span> <span class="disc">¥11,970</span>--> (税込/送料無料)'
					},
					{
						id: 64529,
						img: '4.jpg',
						text: '5本 - 1500ml 【4500円OFF】',
						price: '<span class="normal">¥20,000</span><!--<span class="original">¥20,000</span> <span class="disc">¥29,700</span>--> (税込/送料無料)'
					}
				];
			
			// Select - Format
				function formatData (product) {
					if (!product.id) {
						return product.text;
					}
					var baseUrl = "<?php echo get_stylesheet_directory_uri(); ?>/lp-assets/the-shampoo/images";
					var $jproduct = $j(
						'<span><img src="' + baseUrl + '/' + product.img + '" class="img-product" /> ' + product.text + '</span>'
					);
					return $jproduct;
				};
			
			// Select - Initialize select2
				$j('.stickyselect').select2({
					data: data,
					templateResult: formatData,
					templateSelection: formatData,
					dropdownPosition: 'above',
					minimumResultsForSearch: -1
				});
				
				$j('.stickyselect').select2('open');
				$j('.stickyselect').select2('close');
				$j('.stickyselect').focusout();
			
			// Select - Initial data on first load
				var currentSelectedData = $j('.stickyselect').select2('data');			
				// Price label
				$j( '.stickyselect-input-wrapper label' ).html( currentSelectedData[0].price );
				// Action URL				
                $j( '.stickyselect-action-wrapper a' ).attr( 'href', 'https://ambientlounge.co.jp/offers/?add-to-cart=' + currentSelectedData[0].id + '&quantity=1&item=' + currentSelectedData[0].id );
			
			// Select - On Change
				$j('.stickyselect').on('select2:select', function (e) {
					var data = e.params.data;
					// Price Label
					$j( '.stickyselect-input-wrapper label' ).html( data.price );
					// Action URL					
                    $j( '.stickyselect-action-wrapper a' ).attr( 'href', 'https://ambientlounge.co.jp/offers/?add-to-cart=' + data.id + '&quantity=1&item=' + data.id );
				});
				
			// Select - Drop on click
				$j( '.stickyselect-drop' ).click( function() {
					$j( 'body' ).toggleClass( 'stickyselect-down' );
				});
		});
		</script>
                
<?php
	get_footer();
?>
