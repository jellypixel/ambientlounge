<?php
/*
	Template Name: Modular
*/
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/adjustment.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/font/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/main-dark.all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/lp-modular.css?123"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/css/flickity.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto Sans JP:100,200,300,400,500,600,700|Noto Sans JP:600|Open Sans:600|Open Sans|Open Sans:700&amp;subset=latin-ext&amp;display=swap" media="all" onload="this.media='all'">

<div class="container-fluid bg-white p-0">
    <div class="container-dark top-container">
        <div class="row justify-content-center text-center">
            <p class="top-banner-title">MODULAR SERIES</p>
            <p class="top-banner-text top-banner-heading-lg">家族とくつろぎの場所を</p>
            <p class="top-banner-text top-banner-heading-sm">"ファスナーでつなぎ合わせて"
                <br>思い通りのインテリアを</p>
            <p class="top-banner-text top-banner-heading-md desc-text-lg">アンビエントラウンジの
                <br>モジュラーシリーズ</p>
            <p class="top-banner-text top-banner-heading-md desc-text-sm">アンビエントラウンジのモジュラーシリーズ</p>
            <img class="modular-top-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/modular-banner-top.webp" alt="top-img-modular">
        </div>
    </div>
</div>
<div class="container-fluid bg-light p-0">
    <div class="container-dark top-container bg-light">
        <div class="row justify-content-center text-center">
            <p class="top-banner-text top-banner-heading-lg">Modular Sofa</p>
            <p class="text-subtitle-dark desc-text-lg">「広がる/繋がるソファ」モジュラーシリーズ</p>
            <p class="text-subtitle-dark desc-text-sm">「広がる/繋がるソファ」<br>モジュラーシリーズ</p>
            <div class="row modular-sofa-intro">
                <div class="col-xs-6 col-md-3 p-2">
                    <img class="modular-img-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/modular-sofa-1.webp" alt="modular-sofa-1">
                    <p class="top-banner-text">1人掛けソファとして</p>
                </div>
                <div class="col-xs-6 col-md-3 p-2">
                    <img class="modular-img-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/modular-sofa-2.webp" alt="modular-sofa-2">
                    <p class="top-banner-text">ファスナーでつながる</p>
                </div>
                <div class="col-xs-6 col-md-3 p-2">
                    <img class="modular-img-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/modular-sofa-3.webp" alt="modular-sofa-3">
                    <p class="top-banner-text">来訪するゲストに</p>
                </div>
                <div class="col-xs-6 col-md-3 p-2">
                    <img class="modular-img-sm" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/modular-sofa-4.webp" alt="modular-sofa-4">
                    <p class="top-banner-text">自分だけのスタイルで</p>
                </div>
            </div>
            <p class="top-banner-text top-banner-heading-sm">ファスナーで自由に取付、取り外し出来る
                <br>アンビエントラウンジ独自開発のユニークなシステムソファ「モジュラーシリーズ」
                <br>ライフスタイルの多様化に対応する「変化」と「進化」が持続する
                <br>サスティナブルなソファコレクション</p>
        </div>
    </div>
</div>
<!--container bg-black start-->
<section id="modular">
    <div class="container-fluid bg-light">
        <div class="container-dark">
            <div class="row bg-black">
                <div class="col-xs-12">
                    <p class="dark-banner-header text-white">製品ラインアップ</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                    <div class="product-panel-dark">
                        <p class="product-panel-title">モジュラーシングル</p>
                        <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/product-img-light-1.webp"/>
                        <p class="product-desc">モジュラーシリーズのベースとなるユニット。家族が増えたとき、リノベーションして空間が広がったとき、モジュラーシングルを足していけば自由自在にリビングスペースをアップデート。</p>
                        <a class="btn btn-primary buy-btn-sm" href="https://ambientlounge.co.jp/products/sofa/1-person/modular-single/" target="_self">購入</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                    <div class="product-panel-dark">
                        <p class="product-panel-title">モジュラーツイン</p>
                        <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/product-img-light-2.webp"/>
                        <p class="product-desc">カップルツイン。パートナーと大喧嘩しても大丈夫、シングルでもツインでもファスナーで自由にセパレート。</p>
                        <a class="btn btn-primary buy-btn-sm" href="https://ambientlounge.co.jp/products/sofa/2-person/modular-twin/" target="_self">購入</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                    <div class="product-panel-dark">
                        <p class="product-panel-title">モジュラーコーナー</p>
                        <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/product-img-light-3.webp"/>
                        <p class="product-desc">モジュラーツインと組み合わせ、L字型やU字型のレイアウトも自由自在に楽しめます。</p>
                        <a class="btn btn-primary buy-btn-sm" href="https://ambientlounge.co.jp/products/sofa/1-person/modular-corner/" target="_self">購入</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                    <div class="product-panel-dark">
                        <p class="product-panel-title">ムービーカウチ</p>
                        <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/product-img-light-4.webp"/>
                        <p class="product-desc">狭いお部屋でも生活の質を向上しながら快適に過ごせます。
                            <br>来客時には真ん中のファスナーを外せば快適な１人掛けが３人分完成。</p>
                        <a class="btn btn-primary buy-btn-sm" href="https://ambientlounge.co.jp/products/sofa/3-person/movie-couch/" target="_self">購入</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                    <div class="product-panel-dark">
                        <p class="product-panel-title">クワッドカウチ</p>
                        <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/product-img-light-5.webp"/>
                        <p class="product-desc">１人から４人まで。シンプルながら飽きのこないデザインで、どんなレイアウトにもマッチ。ツインオットマン(別売)を追加すると、より豪華なお部屋に。</p>
                        <a class="btn btn-primary buy-btn-sm" href="https://ambientlounge.co.jp/products/sofa/3-person/quad-couch/" target="_self">購入</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                    <div class="product-panel-dark">
                        <p class="product-panel-title">コウズィコーナー</p>
                        <img class="" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/product-img-light-6.webp"/>
                        <p class="product-desc">スペースを有効活用。小さなスペースもお洒落にコーディネート。２〜３人で座れるソファは、カップルにも家族にも最適。</p>
                        <a class="btn btn-primary buy-btn-sm" href="https://ambientlounge.co.jp/products/sofa/3-person/cozy-corner/" target="_self">購入</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 pe-md-0">
                    <div class="product-panel-dark">
                        <p class="product-panel-title">ツインオットマン</p>
                        <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/product-img-light-7.webp"/>
                        <p class="product-desc">正方形ボックス型のユニークなツインオットマンは、ファスナーで切り離すと、シングルオットマンとしても、スツールとしても活躍します。</p>
                        <a class="btn btn-primary buy-btn-sm" href="https://ambientlounge.co.jp/products/sofa/ottoman/twin-ottoman/" target="_self">購入</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 p-0 ps-md-0">
                    <div class="product-panel-dark">
                        <p class="product-panel-title">エルソファ</p>
                        <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/product-img-light-8.webp"/>
                        <p class="product-desc">ロフトや地下室のスタジオ、コンパクトなリビングルームに、オリジナルのラウンジエリアを。</p>
                        <a class="btn btn-primary buy-btn-sm" href="https://ambientlounge.co.jp/products/sofa/3-person/l-sofa/" target="_self">購入</a>
                    </div>
                </div>
                <div class="col-xs-12 p-0">
                    <div class="product-panel-dark" style="margin-bottom:0;">
                        <div class="row justify-content-center p-0">
                            <div class="col-xs-12 col-md-6 p-2">
                                <p class="product-panel-title">ラウンジマックス</p>
                                <img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/product-img-light-9.webp"/>
                                <p class="product-desc">ラウンジマックスはモジュラーシリーズのそれぞれのユニットが組み合わさった完成形。両サイドのファスナーを外せば１つずつに切り分けユニット単独での使用も可能。</p>
                                <a class="btn btn-primary buy-btn-sm" href="https://ambientlounge.co.jp/products/sofa/large-sofa/lounge-max/" target="_self">購入</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p class="dark-banner-header text-black">さらに</p>
                </div>
            </div>
            <div class="row banner-row-dark">
                <div class="col-xs-12 col-md-6 px-3 p-md-1 my-auto text-start">
                    <p class="banner-title-lg text-gradient-blue banner-1-title">こだわるのは、
                        <br>おしゃれな空間づくり</p>
                    <p class="banner-subtitle banner-1-desc desc-text-lg">永く愛用していただきたいから、
                        <br>差し色からベーシックまで、和室、
                        <br>洋室どんなお部屋にもマッチする
                        <br>重厚な生地は時を経ても
                        <br>色あせるません。</p>
                    <p class="banner-subtitle banner-1-desc desc-text-sm">
                        永く愛用していただきたいから、
                        <br>差し色からベーシックまで、和室、
                        <br>洋室どんなお部屋にもマッチする
                        <br>重厚な生地は時を経ても
                        <br>色あせるません。
                    </p>
                </div>
                <div class="col-xs-12 col-md-6 p-0">
                    <div class="banner-img-1"></div>
                </div>
            </div>
            <div class="row banner-row-light">
                <div class="col-xs-12 col-md-6 order-md-1 order-2">
                    <div class="banner-img-2"></div>
                    <p class="banner-subtitle text-center banner-2-weight-panel">重さ 約 <span class="banner-title-lg text-gradient-pink align-middle">8kg</span></p>
                </div>
                <div class="col-xs-12 col-md-6 px-3 p-md-1 my-auto text-end order-md-2 order-1">
                    <p class="banner-title-lg text-gradient-pink banner-2-title">軽量で楽々
                        <br>移動可能</p>
                    <p class="banner-subtitle banner-2-desc desc-text-lg">フローリングはもちろん、
                        <br>畳のお部屋でも接地面を傷つけることなく、
                        <br>全ての場面でくつろぎ空間を可能にしました。女性やお子様でも簡単に移動しやすく、
                        <br>コロンと転がして動かして
                        <br>お掃除も楽ちんです。</p>
                    <p class="banner-subtitle banner-2-desc desc-text-sm">フローリングはもちろん、畳のお部屋でも接地面を傷つけることなく、全ての場面でくつろぎ空間を可能にしました。女性やお子様でも簡単に移動しやすく、コロンと転がして動かしてお掃除も楽ちんです。</p>
                </div>
            </div>
            <div class="row banner-row-dark mb-0">
                <div class="col-xs-12 col-md-6 px-3 p-md-1 text-start">
                    <p class="banner-title-lg text-gradient-blue banner-3-title">角のない安全
                        <br>でやさしい作り</p>
                    <p class="banner-subtitle banner-3-desc desc-text-lg">フレームレスであることの最大のメリットは
                        <br>角がないこと。
                        <br>小さなお子様やペット、
                        <br>シニアの方にも安全なソファです。
                    </p>
                    <p class="banner-subtitle banner-3-desc desc-text-sm">フレームレスであることの最大のメリットは角がないこと。小さなお子様やペット、シニアの方にも安全なソファです。</p>
                </div>
                <div class="col-xs-12 col-md-6 p-0">
                    <div class="banner-img-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--container bg-black end-->
<!--container slider start -->
<div class="container-fluid bg-light py-5">
    <div class="container-dark">
        <div class="row justify-content-center text-center bg-white">
            <p class="light-text-title">生地の色バリエーション</p>
            <p class="text-subtitle-dark">豊富な20種類のカラーから<br>
                ずっとかけていたくなる秘密の極上ソファを</p>
            <div class="container carousel-controls p-0">
                <div class="row">
                    <div class="col-xs-6 px-2 text-start text-md-center">
                        <span class="carousel-btn" id="carousel-back"><i class="bi bi-chevron-left" aria-hidden="true"></i> 前の商品</span>
                    </div>
                    <div class="col-xs-6 px-2 text-end text-md-center">
                        <span class="carousel-btn" id="carousel-forward">次の商品 <i class="bi bi-chevron-right" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>
            <div class="row p-0" id="sofa-carousel">
                <div class="carousel sofa-gallery data-flickity p-0">
                    <div class="carousel-cell cell1" data-color="soft-grey">
                        <p class="sofa-img-title">ソフトグレー</p>
                        <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/sofa-slider-1.webp" alt="ソフトグレー">
                        <p><a class="product-details-link text-decoration-none" href="https://ambientlounge.co.jp/products/sofa/1-person/modular-single/?attribute_pa_color=soft-grey&attribute_pa_filling=hybrid" data-color="soft-grey">購入<i class="bi bi-chevron-right"></i></a></p>
                    </div>
                    <div class="carousel-cell cell2" data-color="dark-grey">
                        <p class="sofa-img-title">ダークグレー</p>
                        <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/sofa-slider-2.webp" alt="ダークグレー">
                        <p><a class="product-details-link text-decoration-none" href="https://ambientlounge.co.jp/products/sofa/1-person/modular-single/?attribute_pa_color=dark-grey&attribute_pa_filling=hybrid" data-color="dark-grey">購入<i class="bi bi-chevron-right"></i></a></p>
                    </div>
                    <div class="carousel-cell cell3" data-color="black">
                        <p class="sofa-img-title">ブラック</p>
                        <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/sofa-slider-3.webp" alt="ブラック">
                        <p><a class="product-details-link text-decoration-none" href="https://ambientlounge.co.jp/products/sofa/1-person/modular-single/?attribute_pa_color=black&attribute_pa_filling=hybrid" data-color="black">購入<i class="bi bi-chevron-right"></i></a></p>
                    </div>
                    <div class="carousel-cell cell4" data-color="beige">
                        <p class="sofa-img-title">ベージュ</p>
                        <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/sofa-slider-4.webp" alt="ベージュ">
                        <p><a class="product-details-link text-decoration-none" href="https://ambientlounge.co.jp/products/sofa/1-person/modular-single/?attribute_pa_color=beige&attribute_pa_filling=hybrid" data-color="beige">購入<i class="bi bi-chevron-right"></i></a></p>
                    </div>
                    <div class="carousel-cell cell5" data-color="border-grey-plus">
                        <p class="sofa-img-title">ブラウン</p>
                        <img data-flickity-lazyload="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/sofa-slider-5.webp" alt="ブラウン">
                        <p><a class="product-details-link text-decoration-none" href="https://ambientlounge.co.jp/products/sofa/1-person/modular-single/?attribute_pa_color=boardergrey-waterresistant&attribute_pa_filling=hybrid" data-color="border-grey-plus">購入<i class="bi bi-chevron-right"></i></a></p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center color-panel">
                <div class="col-xs-12 col-md-9 p-3 text-start">
                    <p class="interior-color-title">インテリア</p>
                    <div class="p-0">
                        <figure class="figure">
                            <img class="sofa-color-img" data-selector=".cell1" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/color-btn-1.webp" alt="ソフトグレー">
                            <figcaption class="figure-caption">ソフトグレー</figcaption>
                        </figure>
                        <figure class="figure">
                            <img class="sofa-color-img" data-selector=".cell2" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/color-btn-2.webp" alt="ダークグレー">
                            <figcaption class="figure-caption">ダークグレー</figcaption>
                        </figure>
                        <figure class="figure">
                            <img class="sofa-color-img" data-selector=".cell3" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/color-btn-3.webp" alt="ベージュ">
                            <figcaption class="figure-caption">ブラック</figcaption>
                        </figure>
                        <figure class="figure">
                            <img class="sofa-color-img" data-selector=".cell4" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/color-btn-4.webp" alt="ブラウン">
                            <figcaption class="figure-caption">ベージュ</figcaption>
                        </figure>
                    </div>
                </div>
                <div class="col-xs-12 col-md-9 p-3 text-start d-flex m-auto">
                    <img class="d-flex p-2 interior-plus-black-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/interior-plus-black.png"/>
                    <span class="my-auto">
                        <p class="interior-plus-desc fw-bold">インテリア＋プラス</p>
                        <p class="interior-plus-desc">撥水性/褪色防止/引っ掻きに強い/ペットフレンドリー</p>
                    </span>
                </div>
                <div class="col-xs-12 col-md-9 p-3 text-start">
                    <figure class="figure">
                        <img class="sofa-color-img" data-selector=".cell5" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/color-btn-5.webp" alt="ボーダーグレー・インテリアプラス">
                        <figcaption class="figure-caption">ボーダーグレー・<br>インテリアプラス</figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>
<!--container slider end-->

<!--container spin-view start-->
<div class="container-fluid">
    <div class="container-dark bg-white spin-view-container">
        <div class="row justify-content-center">
            <div class="col-xs-12 text-center">
                <p class="light-text-title">4つのピース<br>無限のレイアウト</p>
                <p class="light-text-subtitle">多様な組み合わせで、お部屋に合わせたレイアウトが自由自在
                    <br>あなただけのオリジナル空間を</p>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-5">
                    <div class="cloudimage-360"
                         data-folder="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/mc/"
                         data-filename="{index}.jpeg"
                         data-amount="25"
                         data-bottom-circle="true"
                         data-lazyload>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5">
                    <div class="cloudimage-360"
                         data-folder="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/ms/"
                         data-filename="{index}.jpeg"
                         data-amount="20"
                         data-bottom-circle="true"
                         data-lazyload>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5">
                    <div class="cloudimage-360"
                         data-folder="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/mt/"
                         data-filename="{index}.jpeg"
                         data-amount="16"
                         data-bottom-circle="true"
                         data-lazyload>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5">
                    <div class="cloudimage-360"
                         data-folder="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/modular/to/"
                         data-filename="{index}.jpeg"
                         data-amount="24"
                         data-bottom-circle="true"
                         data-lazyload>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--container spin-view end-->

<!--container details start-->
<div class="container-fluid bg-light">
    <div class="container-dark">
        <div class="row justify-content-center pb-5">
            <div class="col-xs-12 p-0 text-center">
                <p class="light-text-title">生地の種類</p>
            </div>
            <div class="col-xs-12 col-md-8 p-2 text-center">
                <div class="card details-card p-0">
                    <div class="card-body">
                        <p class="details-card-title">インテリア</p>
                        <p class="details-card-subtitle">重厚感と弾力感のある丈夫な織りのファブリックコレクション。
                            <br>カラーによって様々な色や素材が折り重なった存在感あふれる風合いが楽しめます。</p>
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/material_detail_1.webp" class="card-img-bottom" alt="...">
                </div>
            </div>
            <div class="col-xs-12 col-md-8 p-2 text-center">
                <div class="card details-card p-0">
                    <div class="card-body">
                        <img class="interior-plus-blue-icon" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/interior-plus-blue.webp" alt="interior-plus-blue"/>
                        <p class="details-card-title">インテリアプラス</p>
                        <p class="details-card-subtitle">ペットやお子様との暮らしに最適。
                            <br>引っ掻きや汚れに強い撥水高機能ファブリック</p>
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/sofa_butterfly/material_detail_2.webp" class="card-img-bottom" alt="...">
                </div>
            </div>
        </div>
    </div>
</div>
<!--container details end-->
<!--container al-intro start-->
<div class="container-fluid bg-white p-3 p-xl-5">
    <div class="container-dark">
        <div class="row justify-content-center">
            <div class="col-xs-12 text-center">
                <p class="light-text-title">日本品質へのこだわり</p>
            </div>
            <div class="col-md-4 col-sm-5 col-xs-7 px-0 py-5"><img class="w-100" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/images/icons/al_logo_lg.webp" alt=""/></div>
            <div class="w-100"></div>
            <div class="col-xs-12 col-md-10 p-0">
                <p class="al-intro-text text-start text-md-center">ゆったりとした時間が流れ、QOLを大切にするオーストラリア発のアンビエントラウンジが日本に上陸したのは2015年。
                    <br>機能的でユニークなデザインはそのままに、ジャパン社では日本発信の商品開発や日本品質にこだわっています。
                    <br>試行錯誤を繰り返したどり着いた、圧倒的な座り心地のソファやペットベッドはジャパン社だけの独自品質。
                    <br>ローカル産業との取り組みを始め、過疎化が進む "みなかみ町" に新しいビジネスモデルを生み出したいと考えています。
                    <br>ご注文いただいたソファやベッドは全て、地元みなかみ町のスタッフが１点１点心を込めて仕上げています。</p>
            </div>
        </div>
    </div>
</div>
<!--container al-intro end-->
<section id="banner-bottom">
    <!--container bg-black start-->
    <div class="container-fluid container-2">
        <div class="container-dark">
            <div class="row">
                <div class="col-xs-12">
                    <p class="bottom-banner-header">ベストセラー</p>
                </div>
            </div>
            <div class="row banner-bottom-row-dark">
                <div class="col-xs-12 col-md-6 banner-text-box">
                    <p class="banner-bottom-subtitle">人気No1のソファ</p>
                    <p class="banner-bottom-title text-gradient-blue my-2">バタフライ</p>
                    <p><a class="btn btn-primary details-btn" href="https://ambientlounge.co.jp/butterfly/">詳細を見る</a></p>
                    <p><a class="buy-link text-decoration-none" href="https://ambientlounge.co.jp/products/sofa/1-person/butterfly/">購入<i class="bi bi-chevron-right"></i></a></p>
                </div>
                <div class="col-xs-12 col-md-6 p-0">
                    <div class="banner-bottom-img-1"></div>
                </div>
            </div>
            <div class="row banner-bottom-row-dark mb-0">
                <div class="col-xs-12 col-md-6 px-0 banner-text-box order-1 order-md-2">
                    <p class="banner-bottom-subtitle">つつみこまれる心地よさ</p>
                    <p class="banner-bottom-title text-gradient-pink my-2">アクスティック</p>
                    <p><a class="btn btn-primary details-btn" href="https://ambientlounge.co.jp/acoustic/">詳細を見る</a></p>
                    <p><a class="buy-link text-decoration-none" href="https://ambientlounge.co.jp/products/sofa/1-person/acoustic/">購入<i class="bi bi-chevron-right"></i></a></p>
                </div>
                <div class="col-xs-12 col-md-6 p-0 order-2 order-md-1">
                    <div class="banner-bottom-img-2"></div>
                </div>
            </div>
        </div>
    </div>
    <!--container bg-black end-->
</section>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/lazysizes-5.3.2.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/flickity.pkgd.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>lp-assets/js/js-cloudimage-360-view-2.1.0.min.js"></script>
<script>
    // window.CI360.init();
    var backBtn = document.getElementById("carousel-back");
    var nextBtn = document.getElementById("carousel-forward");
    var flky = new Flickity( '.sofa-gallery', {
        // options, defaults listed
        accessibility: true,
        // enable keyboard navigation, pressing left & right keys
        adaptiveHeight: false,
        // set carousel height to the selected slide
        autoPlay: false,
        // advances to the next cell
        // if true, default is 3 seconds
        // or set time between advances in milliseconds
        // i.e. `autoPlay: 1000` will advance every 1 second
        cellAlign: 'center',
        // alignment of cells, 'center', 'left', or 'right'
        // or a decimal 0-1, 0 is beginning (left) of container, 1 is end (right)
        cellSelector: undefined,
        // specify selector for cell elements
        contain: false,
        // will contain cells to container
        // so no excess scroll at beginning or end
        // has no effect if wrapAround is enabled
        // draggable:false,
        draggable: '>1',
        // enables dragging & flicking
        // if at least 2 cells
        dragThreshold: 3,
        // number of pixels a user must scroll horizontally to start dragging
        // increase to allow more room for vertical scroll for touch devices
        freeScroll: false,
        // enables content to be freely scrolled and flicked
        // without aligning cells
        selectedAttraction: 0.1,
        friction: 0.8,
        // smaller number = easier to flick farther
        groupCells: false,
        // group cells together in slides
        initialIndex: 0,
        // zero-based index of the initial selected cell
        lazyLoad: 2,
        // enable lazy-loading images
        // set img data-flickity-lazyload="src.jpg"
        // set to number to load images adjacent cells
        percentPosition: true,
        // sets positioning in percent values, rather than pixels
        // Enable if items have percent widths
        // Disable if items have pixel widths, like images
        prevNextButtons: false,
        // creates and enables buttons to click to previous & next cells
        pageDots: false,
        // create and enable page dots
        resize: true,
        // listens to window resize events to adjust size & positions
        rightToLeft: false,
        // enables right-to-left layout
        setGallerySize: true,
        // sets the height of gallery
        // disable if gallery already has height set with CSS
        watchCSS: false,
        // watches the content of :after of the element
        // activates if #element:after { content: 'flickity' }
        wrapAround: true,
        // at end of cells, wraps-around to first for infinite scrolling
    });
    backBtn.addEventListener("click",function(){
        flky.previous();
    });
    nextBtn.addEventListener("click",function(){
        flky.next();
    });

    // let buyBtn = document.getElementsByClassName("product-details-link");
    // for (let i = 0; i < buyBtn.length; i++) {
    //     buyBtn[i].addEventListener("click",function(){
    //         let color = buyBtn[i].getAttribute("data-color")
    //         let url = "https://ambientlounge.co.jp/products/sofa/1-person/modular/?attribute_pa_color=";
    //         window.open(url+color,'_self');
    //     })
    // }

    let sofaColorBtn = document.getElementsByClassName("sofa-color-img");
    for (let i = 0; i < sofaColorBtn.length; i++) {
        sofaColorBtn[i].addEventListener("click",function(){
            let selector = sofaColorBtn[i].getAttribute('data-selector')
            flky.selectCell(selector)
        })
    }
</script>
<?php
get_footer();
?>