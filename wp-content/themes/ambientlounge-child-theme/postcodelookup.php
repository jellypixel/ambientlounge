<?php
header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

if(!isset($_GET['postcode'])){
    http_response_code(400);
    echo json_encode(['status'=>'error','message'=>'Please enter a postcode.']);
    return;
}

$postcode = $_GET['postcode']; 

// Build the API URL with the given postcode
$api_url = "https://map.yahooapis.jp/search/zip/V1/zipCodeSearch?detail=full&output=json&query={$postcode}&appid=dj00aiZpPUlsYUtqZENaazdrdCZzPWNvbnN1bWVyc2VjcmV0Jng9YmE-";

// Perform the API request
$response = file_get_contents($api_url);

// Check if the response is successful
if ($response === false) {
    // If request failed, output an error JSON
    $output = [
        "status" => "error",
        "message" => "Failed to retrieve data from Yahoo API"
    ];
} else {
    // Parse the JSON response
    $json_response = json_decode($response, true);

    // Initialize empty variables as fallback
    $kana_state = $kana_city = $kana_address = '';
    $kanji_state = $kanji_city = $kanji_address = '';
    $state_code = '';

    // Check if the response contains the expected structure
    if (isset($json_response['Feature'][0]['Property']['AddressElement'])) {
        $address_elements = $json_response['Feature'][0]['Property']['AddressElement'];

        // Assign values if keys exist, otherwise fallback to empty string
        $kana_state = $address_elements[0]['Kana'] ?? '';
        $kana_city = $address_elements[1]['Kana'] ?? '';
        $kana_address = $address_elements[2]['Kana'] ?? '';
        $kanji_state = $address_elements[0]['Name'] ?? '';
        $kanji_city = $address_elements[1]['Name'] ?? '';
        $kanji_address = $address_elements[2]['Name'] ?? '';
        $state_code = $address_elements[0]['Name'] ?? '';
    }

    // Prepare the data array
    $data = [
        "postcode" => $postcode,
        "kana_state" => $kana_state,
        "kana_city" => $kana_city,
        "kana_address" => $kana_address,
        "kanji_state" => $kanji_state,
        "kanji_city" => $kanji_city,
        "kanji_address" => $kanji_address,
        "state_code" => $state_code
    ];

    // Build the final JSON output 
    $output = [
        "status" => "ok",
        "data" => $data
    ];
}

// Output the JSON response
header('Content-Type: application/json');
echo json_encode($output, JSON_PRETTY_PRINT);