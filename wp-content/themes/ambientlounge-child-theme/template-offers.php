<?php
/*
	Template Name: Offers
*/	

    // Accessible only with product ID
    $productID = $_GET["item"];
    
    if ( empty( $productID ) ) 
    {
        exit;
    }

    $product = wc_get_product($productID);

    if ($product instanceof WC_Product_Variation) {
        $productID = $product->get_parent_id();
    }
?>
<?php
	get_header();

    $show_page_title = get_post_meta( $post->ID, '_show_page_title_meta_key', true );
?>
    <div class="content-area">
        <?php            
            if ( !is_front_page() && !is_checkout() && !is_account_page() && ( $show_page_title == 'yes' || $show_page_title == '' ) &&  !has_post_thumbnail() )
            {
                ?>
                    <div class="page-heading-wrapper">
                        <div class="entry-container">
                            <?php
                                if ( generate_show_title() ) {
                                    $params = generate_get_the_title_parameters();

                                    echo '<h1 class="entry-title" itemprop="headline">';
                                        the_field( 'offer_title', $productID );
                                    echo '</h1>';
                                }
                            ?>
                        </div>
                    </div>
                <?php				
            }		
        ?>

        <div class="entry-content">        
            <?php                                
                $args = array(
                    'p' => $productID,
                    'post_type' => array( 'product' ),
                    /*meta_query' => array(
                        array(
                            'key' => 'offers_$_discount',
                            'value' => '0',
                            'type' => 'NUMERIC',
                            'compare' => '>'
                        )
                    )*/
                );
                
                $query = new WP_Query( $args );
                
                if ( $query->have_posts() ) 
                {
                    while ( $query->have_posts() ) 
                    {
                        $query->the_post();
                    
                        // Check if there's offer
                        if ( have_rows( 'offers' ) ) 
                        {
                            $offerAvailable = 0;
                            $arrOfferProducts = array();

                            // Loop offers
                            while( have_rows( 'offers' ) )
                            {
                                the_row();
                                // Check if the offer is not zero/empty
                                if ( get_sub_field( 'product' ) > 0 )
                                {
                                    $offerAvailable++;

                                    $offerProductID = get_sub_field( 'product' );                                    
                                    array_push( $arrOfferProducts, wc_get_product( $offerProductID ) );
                                }
                            }

                            // Check if there's actually any offer that's not zero/empty
                            if ( $offerAvailable <= 0 ) 
                            {
                                // :(
                                echo 'no available offers';
                            }
                            else
                            {
                                ?>
                                    <section class="related products offers-wrapper">
                                        <div class="card">
                                            <ul class="products columns-3">
                                                <?php
                                                    // Loop all available offers 
                                                    foreach ( $arrOfferProducts as $offerProduct )
                                                    {
                                                        // Make woocommerce product out of this ID
                                                        $post_object = get_post( $offerProduct->get_id() );
                                                        setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

                                                        // Then call woocommerce template
                                                        wc_get_template_part( 'content', 'product-offers' );
                                                        
                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                    </section>
                                <?php
                            }
                        }
                        else 
                        {
                            // :(
                            echo 'no available offers';
                        }
                    }
                } 

                wp_reset_postdata();
            ?>
        </div>
    </div>

<?php
	get_footer();
?>