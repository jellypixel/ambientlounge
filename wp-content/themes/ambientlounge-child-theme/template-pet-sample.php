<?php
/*
	Template Name: Pet-Sample
*/
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/bootstrap.min.css"/>
<?php
get_header();
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/adjustment.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/main-dark.all.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/pet-sample.css?v=2"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/css/top-bar.css?v=2"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto Sans JP:100,200,300,400,500,600,700|Noto Sans JP:600|Open Sans:600|Open Sans|Open Sans:700&amp;subset=latin-ext&amp;display=swap" media="all" onload="this.media='all'">
</head>
<style>
    table tr:nth-child(2n) td{
         background-color: #fff;
    }
    td{
        border-top: 0;
    }
</style>
<body>
<div class="header-inner"></div>
<div class="top-bar-new d-flex justify-content-between" id="top-bar-new">
    <div class="top-bar-new-title" id="top-bar-new-title"></div>
    <a class="btn btn-primary top-bar-new-btn" id="top-bar-new-btn" href="/products/swatch/pet-swatch-order/">生地サンプルを請求</a>
</div>
<div id="main-content">
    <div class="container-fluid bg-grey">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <p class="top-banner-text top-banner-heading-lg">生地サンプル
                    <br>Pet</p>
                <p class="top-banner-text top-banner-heading-sm desc-text-lg">愛犬と一緒に選ぶベッドの生地。
                    <br>大きめサイズの生地サンプルは引っ掻きや噛みつきをテストするのにもちょうどいい。
                    <br>テイラーメイドの様にうちの子だけのベッドを選びましょう。</p>
                <p class="top-banner-text top-banner-heading-sm desc-text-sm">愛犬と一緒に選ぶベッドの生地。
                    <br>大きめサイズの生地サンプルは引っ掻きや噛みつきを
                    <br>テストするのにもちょうどいい。
                    <br>テイラーメイドの様にうちの子だけのベッドを選びま<br>しょう。</p>
            </div>
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-9 p-2 text-center">
                    <img class="w-100 pt-5" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/banner-top.webp" alt="re-banner"/>
                </div>
            </div>
        </div>
    </div>

    <!--container bg-white start-->
    <div class="container-fluid bg-white">
        <div class="container-dark">
            <div class="row justify-content-center text-center">
                <div class="col-xs-12">
                    <p class="ref-spec-title">ベース生地</p>
                </div>
                <div class="col-xs-12 col-lg-8 p-0">
                    <div class="row justify-content-center text-center">
                        <div class="col-xs-12 col-md-10 col-lg-7 p-2">
                            <div class="sample-panel bg-light">
                                <p class="sample-panel-title">グレー・ベージュ</p>
                                <p class="sample-panel-subtitle desc-text-lg">噛んでも引っ掻いても大丈夫！
                                    <br>1200デニール高密度ナイロン仕立ての頑丈スペック</p>
                                <p class="sample-panel-subtitle desc-text-sm">噛んでも引っ掻いても大丈夫！
                                    <br>1200デニール高密度ナイロン
                                    <br>仕立ての頑丈スペック</p>
                                <img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/color-black-pink.webp"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-10 col-lg-5 p-2">
                            <div class="sample-panel bg-light">
                                <img class="icon-new" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/icon-new.webp">
                                <p class="sample-panel-title">ブルードリーム</p>
                                <p class="sample-panel-subtitle">高感度・高機能なのに
                                    <br>柔らかで美しい素材感</p>
                                <img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/color-blue-dream.webp"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <p class="ref-spec-subtitle">素材一覧</p>
                </div>
                <div class="col-xs-12 p-2">
                    <table class="table table-borderless ref-spec-table-1">
                        <tr class="bg-light text-start">
                            <th class="table-1-col1">カラー</th>
                            <th>素材</th>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="table-1-col1">グレー</th>
                            <td>ナイロン100％（撥水・UVカット）</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th class="table-1-col1">ベージュ</th>
                            <td>ナイロン100％（撥水・UVカット）</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th class="table-1-col1">ブルードリーム</th>
                            <td>ポリエステル100%（一部リサイクルポリエステル使用・撥水）</td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-12">
                    <p class="ref-spec-title">カバー生地</p>
                </div>
                <div class="col-xs-12 col-lg-9 p-2">
                    <div class="sample-panel bg-light">
                        <p class="sample-panel-title">エコファー</p>
                        <p class="sample-panel-subtitle">※エコファーとは動物毛皮の代わりに合成繊維を使用した繊維素材です。</p>
                        <div class="row justify-content-center text-center">
                            <div class="col-xs-12 col-md-4 col-lg-3 p-2">
                                <div class="row">
                                    <div class="col-xs-6 col-md-12 p-2 order-md-1 order-2 m-auto">
                                        <p class="sample-img-caption-bold">ウルフグレー</p>
                                        <p class="sample-img-caption">本物の動物の毛の風合いをリアルに再現</p>
                                    </div>
                                    <div class="col-xs-6 col-md-12 p-0 order-md-2 order-1"><img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/material-1.webp"/></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4 col-lg-3 p-2">
                                <div class="row">
                                    <div class="col-xs-6 col-md-12 p-2 order-md-1 order-2 m-auto">
                                        <p class="sample-img-caption-bold">ラビットベージュ</p>
                                        <p class="sample-img-caption">うっとりととろける様な柔らかさが魅力</p>
                                    </div>
                                    <div class="col-xs-6 col-md-12 p-0 order-md-2 order-1"><img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/material-2.webp"/></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4 col-lg-3 p-2">
                                <div class="row">
                                    <div class="col-xs-6 col-md-12 p-2 order-md-1 order-2 m-auto">
                                        <p class="sample-img-caption-bold">バンビグレー</p>
                                        <p class="sample-img-caption">ランクを超えた、極上ラグジュアリー感</p>
                                    </div>
                                    <div class="col-xs-6 col-md-12 p-0 order-md-2 order-1"><img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/material-3.webp"/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-9 p-2">
                    <div class="row justify-content-center text-center">
                        <div class="col-xs-12 col-lg-7 p-0">
                            <div class="product-panel-light">
                                <p class="sample-panel-title">キルト</p>
                                <p class="sample-panel-subtitle">汚れに強く、撥水性のある
                                    <br>高機能ファブリック</p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 p-2">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-12 order-md-1 order-2 p-2 m-auto"><p class="sample-img-caption-bold">グレー</p></div>
                                            <div class="col-xs-6 col-md-12 order-md-2 order-1 p-0"><img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/material-4.webp"/></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 p-2">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-12 order-md-1 order-2 p-2 m-auto"><p class="sample-img-caption-bold">ベージュ</p></div>
                                            <div class="col-xs-6 col-md-12 order-md-2 order-1 p-0"><img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/material-5.webp"/></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-5 p-0">
                            <div class="product-panel-light">
                                <img class="icon-new" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/icon-new.webp"/>
                                <div class="row justify-content-center text-center">
                                    <p class="sample-panel-title">ブルードリーム</p>
                                    <p class="sample-panel-subtitle">環境と皮膚にやさしい
                                        <br>オーガニックコットンブレンド</p>
                                    <div class="row">
                                        <div class="col-xs-6 col-md-12 order-md-1 order-2 p-2 m-auto"><p class="sample-img-caption-bold">シープオーガニック</p></div>
                                        <div class="col-xs-6 col-md-12 order-md-2 order-1 p-0"><img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/material-6.webp"/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <p class="ref-spec-subtitle">素材一覧</p>
                </div>
                <div class="col-xs-12 p-2">
                    <table class="table table-borderless ref-spec-table-2">
                        <tr class="bg-light text-start">
                            <th>カバー生地</th>
                            <th>カラー</th>
                            <th>素材</th>
                        </tr>
                        <tr class="bg-white text-start">
                            <th>エコファー</th>
                            <td>ウルフグレー</td>
                            <td>ポリエステル100%<</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th>エコファー</th>
                            <td>ラビットベージュ</td>
                            <td>ポリエステル100%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th>エコファー</th>
                            <td>バンビグレー</td>
                            <td>ポリエステル100%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th>キルト</th>
                            <td>グレー</td>
                            <td>原着アクリル100%</td>
                        </tr>
                        <tr class="bg-white text-start">
                            <th>キルト</th>
                            <td>ベージュ</td>
                            <td>原着アクリル100%</td>
                        </tr>
                        <tr class="bg-light text-start">
                            <th>オーガニックコットンブレンド</th>
                            <td>シープオーガニック</td>
                            <td>オーガニックコットン80%<br>ポリエステル20％</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--container bg-white end-->

    <!--container product white panel start-->
    <section id="lp-sample-pet">
        <div class="container-fluid container-5 p-3 p-xl-5 bg-light">
            <div class="container-dark">
                <div class="row justify-content-center text-center">
                    <p class="bottom-heading-1">ペットラウンジ 新商品の
                        <br>名称が確定しました</p>
                    <p class="bottom-heading-2">「羊が１匹、羊が２匹・・・」極上ベッドで見る幸せな夢</p>
                    <div class="col-xs-12 col-md-5 col-lg-3 p-0 pe-md-0">
                        <div class="product-panel-white">
                            <img class="icon-new" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/icon-new.webp"/>
                            <p class="light-panel-title">ベッド本体色</p>
                            <p class="light-panel-subtitle">ブルードリーム</p>
                            <img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/pet-lounge-blue.webp"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-5 col-lg-3 p-0 ps-md-0">
                        <div class="product-panel-white">
                            <img class="icon-new" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/icon-new.webp"/>
                            <p class="light-panel-title">カバー</p>
                            <p class="light-panel-subtitle">シープオーガニック</p>
                            <img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/material-6.webp"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-10 col-lg-3 p-0 pe-md-0">
                        <div class="product-panel-white">
                            <img class="icon-new" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/icon-new.webp"/>
                            <p class="light-panel-title">セット名称</p>
                            <p class="light-panel-subtitle">オーガニックブルードリーム</p>
                            <img class="sample-panel-img" src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/images/sample-pet/blue-dream.webp"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--container product white panel end-->
</div>
</body>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . "/"; ?>/lp-assets/js/float-top-bar-new.js"></script>
<?php
get_footer();
?>