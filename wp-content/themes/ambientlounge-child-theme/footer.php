<?php
/**
 * The template for displaying the footer.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

	</div>
</div>

<?php
/**
 * generate_before_footer hook.
 *
 * @since 0.1
 */
do_action( 'generate_before_footer' );
?>

<div <?php generate_do_attr( 'footer' ); ?>>
	<?php
	/**
	 * generate_before_footer_content hook.
	 *
	 * @since 0.1
	 */
	do_action( 'generate_before_footer_content' );

	?>
		<div class="footer-breadcrumb-wrapper">
			<div class="container">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<div class="yoast-breadcrumbs">','</div>' );
					}
				?>
			</div>
		</div>
		<div class="footer-contactinfo-wrapper">
			<div class="container">
				<div class="columns">
					<div class="column-equal">
						<div id="contactinfo-emailconsulting" class="footer-contactinfo">
							<div class="footer-contactinfo-icon">
								<svg width="49" height="49" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M24.35 47.7C37.2458 47.7 47.7 37.2458 47.7 24.35C47.7 11.4542 37.2458 1 24.35 1C11.4542 1 1 11.4542 1 24.35C1 37.2458 11.4542 47.7 24.35 47.7Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									<path d="M34.2468 16H13.7169C12.7742 16 12.0078 16.7664 12.0078 17.7091V31.0905C12.0078 32.0332 12.7742 32.7996 13.7169 32.7996H34.2535C35.1963 32.7996 35.9627 32.0332 35.9627 31.0905V17.7159C35.9627 16.7732 35.1963 16.0068 34.2535 16.0068L34.2468 16Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
									<path d="M12 17.4727L22.7973 24.5601C23.5162 25.0213 24.4386 25.0213 25.1575 24.5601L35.9548 17.4727" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
								</svg>
							</div>
							<div class="footer-contactinfo-content">
								<div class="footer-contactinfo-title">
									<?php _e( 'メールで相談', 'ambientlounge' ); ?>
								</div>
								<a href="<?php echo $GLOBALS['currDomain']; ?>/contact/" title="<?php _e( 'お問合わせ', 'ambientlounge' ); ?>">
									<div class="std-button">
										<?php _e( 'お問合わせ', 'ambientlounge' ); ?>
									</div>
								</a>
								<div class="footer-contactinfo-detail">
									10:00AM – 05:00PM (<?php _e( '土日祝日を除く', 'ambientlounge' ); ?>)
								</div>
							</div>
						</div>
					</div>
					<div class="column-equal">
						<div id="contactinfo-customerservice" class="footer-contactinfo">
							<div class="footer-contactinfo-icon">
								<svg width="49" height="49" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
									<g clip-path="url(#clip0_3147_140402)">
										<path d="M24.35 47.71C37.25 47.71 47.7 37.25 47.7 24.36C47.7 11.47 37.25 1 24.35 1C11.45 1 1 11.46 1 24.35C1 37.24 11.46 47.7 24.35 47.7V47.71Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M29.5204 11.9004H19.1904C18.3399 11.9004 17.6504 12.5899 17.6504 13.4404V34.7104C17.6504 35.5609 18.3399 36.2504 19.1904 36.2504H29.5204C30.3709 36.2504 31.0604 35.5609 31.0604 34.7104V13.4404C31.0604 12.5899 30.3709 11.9004 29.5204 11.9004Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M31.0604 16.2109H17.6504V32.8309H31.0604V16.2109Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M22.6309 14.2012H26.0809" stroke="white" stroke-width="0.8" stroke-linecap="round" stroke-linejoin="round"/>
									</g>
									<g clip-path="url(#clip1_3147_140402)">
										<path d="M24.35 47.71C37.25 47.71 47.7 37.25 47.7 24.36C47.7 11.47 37.25 1 24.35 1C11.45 1 1 11.46 1 24.35C1 37.24 11.46 47.7 24.35 47.7V47.71Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M29.5204 11.9004H19.1904C18.3399 11.9004 17.6504 12.5899 17.6504 13.4404V34.7104C17.6504 35.5609 18.3399 36.2504 19.1904 36.2504H29.5204C30.3709 36.2504 31.0604 35.5609 31.0604 34.7104V13.4404C31.0604 12.5899 30.3709 11.9004 29.5204 11.9004Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M31.0604 16.2109H17.6504V32.8309H31.0604V16.2109Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M22.6309 14.2012H26.0809" stroke="white" stroke-width="0.8" stroke-linecap="round" stroke-linejoin="round"/>
									</g>
									<defs>
										<clipPath id="clip0_3147_140402">
											<rect width="48.71" height="48.71" fill="white"/>
										</clipPath>
										<clipPath id="clip1_3147_140402">
											<rect width="48.71" height="48.71" fill="white"/>
										</clipPath>
									</defs>
								</svg>
							</div>
							<div class="footer-contactinfo-content">
								<div class="footer-contactinfo-title">
									<?php _e( 'カスタマーサービス', 'ambientlounge' ); ?>
								</div>
								<div class="footer-contactinfo-phone">
									<svg width="39" height="12" viewBox="0 0 39 12" fill="none" xmlns="http://www.w3.org/2000/svg">
										<rect width="37.5" height="10" transform="translate(1 1)" fill="black"/>
										<path d="M38.4996 1L21.2402 1V11H38.4996V1Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M28.7773 7.13867V10.9993H30.9607V7.13867" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M30.9607 4.85493V1H28.7773V4.85493" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M38.4989 10.9993V7.13867H30.9609L38.4989 10.9993Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M21.2402 1V4.85493H28.7782L21.2402 1Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M21.2402 10.9993L28.7782 7.13867H21.2402V10.9993Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M30.9609 4.85493H38.4989V1L30.9609 4.85493Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M18.2593 1L1 1L1 11H18.2593V1Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
										<path d="M9.62896 8.97235C11.1996 8.97235 12.4728 7.6402 12.4728 5.99692C12.4728 4.35363 11.1996 3.02148 9.62896 3.02148C8.05837 3.02148 6.78516 4.35363 6.78516 5.99692C6.78516 7.6402 8.05837 8.97235 9.62896 8.97235Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
									</svg>
									0120-429-332
								</div>
								<div class="footer-contactinfo-detail">
									10:00AM – 05:00PM (<?php _e( '土日祝日を除く', 'ambientlounge' ); ?>)
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-links-wrapper">
			<div class="container">
				<div class="footer-links">
					<a href="<?php echo $GLOBALS['currDomain']; ?>/furusato/" title="<?php _e( 'ふるさと納税', 'ambientlounge' ); ?>"><?php _e( 'ふるさと納税', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/about-us/" title="<?php _e( 'ブランドストーリー', 'ambientlounge' ); ?>"><?php _e( 'ブランドストーリー', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/retail-locations/" title="<?php _e( '取扱店舗', 'ambientlounge' ); ?>"><?php _e( '取扱店舗', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/contact/" title="<?php _e( 'お問合せ', 'ambientlounge' ); ?>"><?php _e( 'お問合せ', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/professional/" title="<?php _e( '法人のお客様 – Professional', 'ambientlounge' ); ?>"><?php _e( '法人のお客様 – Professional', 'ambientlounge' ); ?></a>
				</div>
				<div class="footer-links">
					<a href="<?php echo $GLOBALS['currDomain']; ?>/shopping-guide/" title="<?php _e( 'ご利用ガイド', 'ambientlounge' ); ?>"><?php _e( 'ご利用ガイド', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/shopping-guide/?s=shipping-fee" title="<?php _e( '配送方法･送料について', 'ambientlounge' ); ?>"><?php _e( '配送方法･送料について', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/shopping-guide/?s=returns-and-exchange" title="<?php _e( '返品と交換について', 'ambientlounge' ); ?>"><?php _e( '返品と交換について', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/softfurniture-safety/" title="<?php _e( '安全性について', 'ambientlounge' ); ?>"><?php _e( '安全性について', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/product-maintenance/" title="<?php _e( '製品のお手入れ', 'ambientlounge' ); ?>"><?php _e( '製品のお手入れ', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/general-faq/" title="<?php _e( 'よくある質問', 'ambientlounge' ); ?>"><?php _e( 'よくある質問', 'ambientlounge' ); ?></a>
				</div>
				<div class="footer-links">
					<a href="<?php echo $GLOBALS['currDomain']; ?>/company-detail/" title="<?php _e( '会社概要', 'ambientlounge' ); ?>"><?php _e( '会社概要', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/terms-of-service/" title="<?php _e( '利用規約', 'ambientlounge' ); ?>"><?php _e( '利用規約', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/asct/" title="<?php _e( '特定商取引法に基づく表記', 'ambientlounge' ); ?>"><?php _e( '特定商取引法に基づく表記', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/privacy-policy/" title="<?php _e( 'プライバシーポリシー', 'ambientlounge' ); ?>"><?php _e( 'プライバシーポリシー', 'ambientlounge' ); ?></a>
					<a href="<?php echo $GLOBALS['currDomain']; ?>/news/" title="<?php _e( 'NEWS', 'ambientlounge' ); ?>"><?php _e( 'NEWS', 'ambientlounge' ); ?></a>					
				</div>
			</div>			
		</div>
	<?php

	/**
	 * generate_footer hook.
	 *
	 * @since 1.3.42
	 *
	 * @hooked generate_construct_footer_widgets - 5
	 * @hooked generate_construct_footer - 10
	 */
	remove_action( 'generate_footer', 'generate_construct_footer_widgets', 5 );
	

	// Get how many widgets to show.
	$widgets = generate_get_footer_widgets();

	if ( ! empty( $widgets ) && 0 !== $widgets ) :

		// TWI If no footer widgets exist, we don't need to continue.
		if ( ! is_active_sidebar( 'footer-1' ) && ! is_active_sidebar( 'footer-2' ) && ! is_active_sidebar( 'footer-3' ) && ! is_active_sidebar( 'footer-4' ) && ! is_active_sidebar( 'footer-5' ) ) {
			//return;
		}

		// Set up the widget width.
		$widget_width = '';

		if ( 1 === (int) $widgets ) {
			$widget_width = '100';
		}

		if ( 2 === (int) $widgets ) {
			$widget_width = '50';
		}

		if ( 3 === (int) $widgets ) {
			$widget_width = '33';
		}

		if ( 4 === (int) $widgets ) {
			$widget_width = '25';
		}

		if ( 5 === (int) $widgets ) {
			$widget_width = '20';
		}
		
		?>
		<div id="footer-widgets" class="site footer-widgets">
			<div <?php generate_do_attr( 'footer-widgets-container' ); ?>>
				<?php
				/*
				<div class="inside-footer-widgets">
					<?php
						if ( $widgets >= 1 ) {
							generate_do_footer_widget( $widget_width, 1 );
						}
	
						if ( $widgets >= 2 ) {
							generate_do_footer_widget( $widget_width, 2 );
						}						
	
						if ( $widgets >= 3 ) {
							generate_do_footer_widget( $widget_width, 3 );
						}						
	
						if ( $widgets >= 4 ) {
							generate_do_footer_widget( $widget_width, 4 );							
						}
	
						if ( $widgets >= 5 ) {
							generate_do_footer_widget( $widget_width, 5 );
						}

						$widget_classes = sprintf(
							'footer-widget-%s',
							absint( $widget )
						);
					
						if ( ! generate_is_using_flexbox() ) {
							$widget_width = apply_filters( "generate_footer_widget_{$widget}_width", $widget_width );
							$tablet_widget_width = apply_filters( "generate_footer_widget_{$widget}_tablet_width", '50' );
					
							$widget_classes = sprintf(
								'footer-widget-%1$s grid-parent grid-%2$s tablet-grid-%3$s mobile-grid-100',
								absint( $widget ),
								absint( $widget_width ),
								absint( $tablet_widget_width )
							);
						}
						?>
						<div class="<?php echo $widget_classes; // phpcs:ignore ?>">
							<?php dynamic_sidebar( 'footer-' . absint( $widget ) ); ?>
						</div>
						<?php
					?>
				</div>
				*/
				?>

				<?php
					if ( is_active_sidebar( 'payment' ) ) 
					{
						?>
							<div class="payment-sidebar">
								<?php dynamic_sidebar( 'payment' );	?>
							</div>
						<?php
					}

					if ( is_active_sidebar( 'social' ) ) 
					{
						?>
							<div class="social-sidebar">
								<?php dynamic_sidebar( 'social' );	?>
							</div>
						<?php	
					}

					if ( is_active_sidebar( 'copyright' ) ) 
					{
						?>
							<div class="copyright-sidebar">
								<?php /* dynamic_sidebar( 'copyright' ); */	?>
							</div>
						<?php	
					}
				?>

			</div>
		</div>

		<?php
	endif;

	/**
	 * generate_after_footer_widgets hook.
	 *
	 * @since 0.1
	 */
	do_action( 'generate_after_footer_widgets' );
	
	do_action( 'generate_footer' );


	/**
	 * generate_after_footer_content hook.
	 *
	 * @since 0.1
	 */
	do_action( 'generate_after_footer_content' );
	?>
</div>

<?php
/**
 * generate_after_footer hook.
 *
 * @since 2.1
 */
do_action( 'generate_after_footer' );

wp_footer();
?>

<?php
	// Lightbox element for single product
	if ( is_product() ) 
	{
		// Get Content Fillings from post - Dog/Cat/Sofa
		$dog_filling_content_pageID = get_field( 'dog_filling_content', 'options');
		$cat_filling_content_pageID = get_field( 'cat_filling_content', 'options');
		$sofa_filling_content_pageID = get_field( 'sofa_filling_content', 'options');
		
		$dog_filling_content = apply_filters('the_content', $dog_filling_content_pageID->post_content); 
		$cat_filling_content = apply_filters('the_content', $cat_filling_content_pageID->post_content); 
		$sofa_filling_content = apply_filters('the_content', $sofa_filling_content_pageID->post_content);
		
		$el = 
			'<div class="modal-overlay">
				<div class="modal-wrapper">
					<div class="modal-close">
						<svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M32.2344 32.2324L11.7688 11.7668" stroke="#888888"/>
							<path d="M11.7656 32.2324L32.2312 11.7668" stroke="#888888"/>
						</svg>
					</div>
					<div class="modal-content-wrapper">
						<div class="modal-content-inner">                        
						</div>
					</div>
				</div>
			</div>
			<div class="modal-content-list">'

				. $dog_filling_content .
				$cat_filling_content .
				$sofa_filling_content .

				'<div id="content-trial" class="modal-content">
					<div class="modal-title">
						ベッドを使ってくれるか心配ですか？
					</div>
					<div class="modal-subtitle">						
						気に入らなければ最大60日以内の返品OK!
					</div>

					<div class="modal-row">

						<div class="modal-column">
							<div class="modal-column-image">
								<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/modal-trial/colorfilling.svg" alt="カラーやフィリングをカスタマイズしてご注文">
							</div>
							<div class="modal-trial-column-title">
								カラーやフィリングをカスタマイズしてご注文
							</div>
							<div class="modal-trial-column-description">
								最短翌日出荷
							</div>
						</div>

						<div class="modal-column">
							<div class="modal-column-image">
								<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/modal-trial/madebyorder.svg" alt="ご注文を受けてから日ラボで作成しお届け">
							</div>
							<div class="modal-trial-column-title">
								ご注文を受けてから日ラボで作成しお届け
							</div>
							<div class="modal-trial-column-description">
								日本人スタッフによる最高品質の製造
							</div>
						</div>

						<div class="modal-column">
							<div class="modal-column-image">
								<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/modal-trial/trialperiod.svg" alt="最大60日※のお試しトライアル期間">
							</div>
							<div class="modal-trial-column-title">
								最大60日のお試しトライアル期間
							</div>
							<div class="modal-trial-column-description">								
								ソファは30日間、ペットラウンジは60日間。気に入らない場合は、期間内なら全額返金。
								<br>※適用条件は「詳しく見る」をクリックしてご確認ください
							</div>
						</div>

					</div>  

					<div class="modal-cta">
						<a href="https://ambientlounge.co.jp/30days-trial" title="詳しく見る">
							<button class="button">
								詳しく見る
							</button>
						</a>
					</div>
				</div>

				<div id="content-trial30" class="modal-content">
					<div class="modal-title">							
						インテリアに合わないか心配ですか？
					</div>
					<div class="modal-subtitle">						
						気に入らなければ最大30日以内の返品OK!
					</div>

					<div class="modal-row">

						<div class="modal-column">
							<div class="modal-column-image">
								<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/modal-trial/colorfilling.svg" alt="カラーやフィリングをカスタマイズしてご注文">
							</div>
							<div class="modal-trial-column-title">
								カラーやフィリングをカスタマイズしてご注文
							</div>
							<div class="modal-trial-column-description">
								最短翌日出荷
							</div>
						</div>

						<div class="modal-column">
							<div class="modal-column-image">
								<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/modal-trial/madebyorder.svg" alt="ご注文を受けてから日ラボで作成しお届け">
							</div>
							<div class="modal-trial-column-title">
								ご注文を受けてから日ラボで作成しお届け
							</div>
							<div class="modal-trial-column-description">
								日本人スタッフによる最高品質の製造
							</div>
						</div>

						<div class="modal-column">
							<div class="modal-column-image">
								<img src="' . get_stylesheet_directory_uri() . '/img/specialproduct/modal-trial/trialperiod.svg" alt="最大60日※のお試しトライアル期間">
							</div>
							<div class="modal-trial-column-title">
								最大60日のお試しトライアル期間
							</div>
							<div class="modal-trial-column-description">
								ソファは30日間、ペットラウンジは60日間。気に入らない場合は、期間内なら全額返金。
								<br>※適用条件は「詳しく見る」をクリックしてご確認ください
							</div>
						</div>

					</div>

					<div class="modal-cta">
						<a href="https://ambientlounge.co.jp/30days-trial" title="詳しく見る">
							<button class="button">
								詳しく見る
							</button>
						</a>
					</div>
				</div>

				<div id="content-petsize" class="modal-content">
					<div class="modal-title">
						ベッドサイズ
					</div> 
					<div class="modal-subtitle">
						我が家の愛犬にはどのサイズ？
					</div>                

					<div class="modal-description">
						全ての犬たちに万能なサイズガイドはありませんが、犬たちは体格よりも大きなもの好むと言われています
愛犬をよく知る飼い主さまには負けますが一般的な犬種リストをご用意しました。
					</div>
					
					<select id="df-select" class="df-select" data-placeholder="犬種をお選びください">犬種をお選びください</select>
					<section id="dogfinder" data-vc-full-width="true" data-vc-full-width-init="true" class="vc_section vc_custom_1587993713907 vc_section-has-fill dog-small" style="position: relative; left: -455px; box-sizing: border-box; width: 1680px; padding-left: 455px; padding-right: 455px;">
					<div style="" id="sizing" class="normal_height vc_row wpb_row vc_row-fluid">
						<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper"></div>
						</div>
						</div>
					</div>
					<div style="" class="normal_height vc_row wpb_row vc_row-fluid">
						<div class="dogfinder-left wpb_column vc_column_container vc_col-sm-6 hidden">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
							<div class="row vc_row wpb_row vc_inner vc_row-fluid">
								<div class="text-center wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
									<div class="wpb_raw_code wpb_content_element wpb_raw_html">
										<div class="wpb_wrapper">
										<p class="mb-10">
											<strong>あなたの犬種</strong>
										</p>
										<h4 class="breed-name mb-10 mb-md-50" id="df-breed">10kg未満の小型犬</h4>
										</div>
									</div>
									</div>
								</div>
								</div>
							</div>
							<div class="row vc_row wpb_row vc_inner vc_row-fluid">
								<div class="text-center wpb_column vc_column_container vc_col-sm-6">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
									<div class="wpb_raw_code wpb_content_element wpb_raw_html">
										<div class="wpb_wrapper">
										<div class="breed-meta row">
											<div id="df-dog-img" class="text-center">
											<svg width="256" height="58" viewBox="0 0 256 58" fill="none" xmlns="http://www.w3.org/2000/svg">
												<g clip-path="url(#clip0_2081_131441)">
												<path class="dog-small" d="M11.8993 57.43C12.8593 57.43 12.8593 57.32 12.8793 56.96C12.9193 56.3 12.8193 55.82 12.5893 55.53C12.3193 55.19 11.8293 55.1 11.4693 55.03C11.4393 55.03 11.4093 55.02 11.3893 55.02C11.2493 54.79 11.1793 54.62 11.1593 54.51C11.1893 54.51 11.2093 54.5 11.2393 54.49C11.9593 54.34 13.2893 54.07 14.0493 53.44C14.1593 53.47 14.3893 53.58 14.5793 53.67C15.4393 54.1 17.0393 54.89 20.1093 54.97H20.3893C20.5493 54.97 20.8793 54.96 21.0593 54.97C21.0793 55.07 21.0993 55.21 21.1093 55.29C21.1293 55.45 21.1493 55.6 21.1793 55.71C21.2993 56.14 21.4893 56.55 21.7193 56.93C21.7693 57.01 21.8593 57.16 22.0393 57.25C22.2093 57.33 22.3693 57.33 22.4593 57.33C22.5093 57.33 22.5993 57.33 22.6893 57.34C23.3393 57.39 24.1593 57.45 24.3793 56.86C24.4493 56.67 24.4293 56.49 24.4193 56.38C24.3993 56.2 24.3693 55.93 24.1993 55.69C24.0993 55.55 23.9493 55.44 23.7993 55.38C23.7893 55.14 23.8493 55 23.9593 54.89C24.2193 54.62 24.5293 54.41 24.8593 54.19C25.3293 53.88 25.8093 53.57 26.1993 53.03C26.7793 52.22 27.3293 51.32 27.3393 50.27C27.3393 49.63 27.2393 49.11 27.1493 48.64C27.0893 48.32 27.0293 48.01 27.0093 47.72C26.9193 45.82 27.1293 45.8 27.9893 45.71C28.4293 45.66 28.9793 45.61 29.6293 45.35C30.4293 45.03 30.7693 43.62 30.8293 43.34C30.8893 43.04 30.8293 42.72 30.6493 42.48C30.4693 42.23 30.2093 42.06 29.9093 42.02C29.7093 41.99 29.5093 41.97 29.3093 41.94C29.1493 41.92 28.9893 41.9 28.8193 41.88L27.6293 41.71C27.4193 41.68 27.2393 41.55 27.0293 41.4C26.7493 41.21 26.4293 40.98 25.9793 40.93C24.5993 40.79 22.7993 41.27 22.0693 42.48C21.7793 42.95 21.8793 43.6 21.9693 44.22C22.0193 44.52 22.0593 44.82 22.0393 45L22.0093 45.25C21.8993 46.21 21.8293 46.64 21.1693 46.93C20.6593 47.16 19.6793 47.14 18.9693 47.12C18.7593 47.12 18.5693 47.12 18.3993 47.12C18.0793 47.12 17.7693 47.13 17.4593 47.15C16.8793 47.17 16.3293 47.2 15.7593 47.15C15.2193 47.11 14.6393 47.02 14.0293 46.92C12.8293 46.73 11.6093 46.53 10.4993 46.71C9.53929 46.87 8.66929 47.24 7.81929 47.59C6.98929 47.93 6.21929 48.27 5.35929 48.42C3.79929 48.7 0.629287 47.82 0.599287 47.81C0.369287 47.75 0.129287 47.86 0.0292873 48.08C-0.0707127 48.3 -0.000712723 48.56 0.209287 48.69C0.289287 48.75 2.32929 50.03 5.46929 50.04C5.71929 50.04 5.95929 50.02 6.19929 50C6.33929 49.99 6.46929 49.98 6.59929 49.97H6.64929C6.68929 50.03 6.72929 50.13 6.79929 50.23C7.20929 50.9 7.28929 51.62 7.36929 52.26C7.42929 52.77 7.47929 53.21 7.68929 53.54C7.87929 53.83 8.89929 55.81 9.53929 57.07C9.61929 57.23 9.77929 57.33 9.95929 57.34C10.8593 57.37 11.4793 57.39 11.9093 57.39V57.41L11.8993 57.43ZM10.2293 54.03C10.1493 54.28 10.0693 54.86 10.6793 55.71C10.8193 55.9 11.0193 55.94 11.2893 56C11.4293 56.03 11.7593 56.09 11.8393 56.16C11.8493 56.18 11.8893 56.28 11.8993 56.47C11.4993 56.47 10.8793 56.44 10.2693 56.42C9.86929 55.63 8.73929 53.42 8.49929 53.05C8.40929 52.91 8.36929 52.55 8.32929 52.19C8.24929 51.53 8.14929 50.62 7.62929 49.76C7.56929 49.67 7.52929 49.6 7.49929 49.53C7.32929 49.21 7.17929 49.08 6.88929 49.04C7.33929 48.88 7.76929 48.7 8.19929 48.52C9.02929 48.17 9.81929 47.84 10.6593 47.7C11.6093 47.54 12.7693 47.73 13.8793 47.91C14.4793 48.01 15.1093 48.11 15.6993 48.15C16.3193 48.2 16.9193 48.17 17.5093 48.15C17.8093 48.14 18.1093 48.12 18.4193 48.12C18.5793 48.12 18.7593 48.12 18.9493 48.12C19.7993 48.14 20.8593 48.16 21.5693 47.84C22.7593 47.31 22.8693 46.33 22.9793 45.38L23.0093 45.13C23.0493 44.82 22.9893 44.46 22.9293 44.08C22.8693 43.72 22.7893 43.16 22.8993 43C23.4093 42.17 24.8093 41.8 25.8793 41.91C26.0693 41.93 26.2493 42.05 26.4593 42.2C26.7193 42.38 27.0493 42.62 27.4893 42.67L28.6793 42.84C28.8493 42.87 29.0193 42.89 29.1893 42.9C29.3793 42.92 29.5693 42.95 29.7593 42.97C29.7993 42.97 29.8193 43 29.8393 43.02C29.8593 43.04 29.8693 43.08 29.8593 43.13C29.6993 43.9 29.3793 44.38 29.2493 44.44C28.7393 44.65 28.2793 44.7 27.8793 44.74C26.5293 44.88 25.8993 45.23 26.0293 47.76C26.0393 48.12 26.1093 48.46 26.1793 48.83C26.2593 49.27 26.3593 49.73 26.3493 50.26C26.3493 51.05 25.8893 51.78 25.3893 52.46C25.0993 52.85 24.7193 53.1 24.3093 53.38C23.9493 53.62 23.5693 53.87 23.2293 54.22C22.6993 54.79 22.7793 55.49 22.8993 55.95C22.9393 56.11 23.0493 56.23 23.1893 56.29C23.2693 56.32 23.3493 56.33 23.4193 56.32C23.4193 56.33 23.4193 56.35 23.4193 56.37C23.2093 56.38 22.8893 56.36 22.7393 56.35C22.6393 56.35 22.5493 56.34 22.4793 56.34C22.2993 56.03 22.1693 55.73 22.0793 55.41C22.0593 55.35 22.0493 55.25 22.0393 55.16C21.9893 54.78 21.9493 54.38 21.6593 54.16C21.3893 53.95 20.9393 53.95 20.3293 53.97H20.0993C17.2493 53.9 15.8293 53.2 14.9793 52.77C14.3993 52.48 13.8893 52.23 13.4293 52.64C12.8693 53.13 11.6193 53.39 11.0193 53.5C10.5293 53.6 10.2893 53.65 10.2093 53.97V53.99H10.2193L10.2293 54.03Z" fill="#888888"/>
												<path class="dog-medium" d="M47.0302 57.16H49.7302C49.9802 57.16 50.1802 56.97 50.2202 56.72C50.3002 56.02 49.9702 55.34 49.3802 54.97C49.1902 54.88 49.0602 54.83 48.9302 54.78C48.3602 54.55 48.2402 54.49 48.2902 54.01C48.4702 52.05 48.8902 51.54 49.1302 51.4C49.3502 51.28 49.7702 51.06 50.2902 50.8C51.5802 50.14 53.3502 49.24 54.2402 48.62C55.6202 47.48 56.9002 46.27 58.1102 45C59.3302 45.23 64.3002 46.18 65.4602 46.23C66.4402 46.22 67.4102 46.13 68.3602 45.98L68.4102 49.58L68.3202 50.81C68.3202 50.87 68.3202 50.94 68.3502 51L70.3002 56.78C70.3702 56.97 70.5502 57.11 70.7602 57.11H73.8402C74.0302 57.11 74.2002 57 74.2802 56.83C74.3402 56.71 74.8102 55.65 74.0902 55.1C73.7802 54.89 73.4502 54.74 73.1002 54.65L72.2902 52.64C72.5002 51.11 73.1702 47.35 74.0002 46.38C75.8902 44.21 77.6402 41.88 79.2002 39.45C80.0702 38.03 80.6202 36.48 80.8302 34.84C80.9102 34.2 80.9302 33.68 80.9502 33.3C80.9602 33.01 80.9802 32.68 81.0102 32.6C81.2302 32.33 81.5602 32.16 81.9502 32.18C82.6302 32.18 84.8002 31.99 84.8902 31.99C85.0302 31.98 85.1502 31.91 85.2302 31.8L86.2702 30.47C86.4302 30.28 86.4102 30 86.2202 29.82L84.5602 28.22C84.5602 28.22 84.5202 28.19 84.5002 28.17C84.4102 28.11 84.2902 28.03 84.1602 27.96C83.9602 27.84 83.5502 27.59 83.4902 27.45C83.3202 26.93 83.0502 26.44 82.7002 25.99C82.6702 25.96 82.6502 25.93 82.6202 25.91C82.3902 25.73 81.1302 25.16 80.3002 24.79L79.3802 23C79.3002 22.83 79.1202 22.73 78.9302 22.73C78.5302 22.75 78.1802 23 78.0302 23.36C77.9302 23.59 77.7802 24.06 77.6902 24.37C76.9202 24.47 76.1802 24.75 75.5202 25.18C75.0402 25.58 74.5502 26.35 73.9702 27.24C73.4002 28.12 72.7702 29.12 72.2402 29.59L71.9502 29.84C70.8702 30.8 69.0702 32.39 66.9702 32.56C66.0302 32.63 64.1702 32.63 62.2102 32.62C59.0402 32.62 55.4402 32.61 53.9802 32.91C51.7402 33.37 48.7802 34.43 47.3102 36.89C46.3502 38.51 45.3302 41.65 44.5802 43.94C44.2902 44.84 44.0302 45.62 43.8702 46.02C43.0102 47.77 42.7002 49.76 43.0002 51.72C43.1402 52.66 43.4502 53.56 43.9102 54.39C43.9602 54.48 44.0402 54.56 44.1402 54.6C44.3002 54.66 44.5502 54.7 44.8002 54.58C45.0902 54.44 45.3002 54.16 45.4402 53.72C45.4902 56.1 45.6802 56.34 45.7602 56.44C46.0802 56.81 46.5002 57.05 46.9502 57.14C46.9802 57.14 47.0102 57.14 47.0402 57.14L47.0302 57.16ZM49.1902 56.18H47.0802C46.8802 56.13 46.6802 56.02 46.5402 55.87C46.3902 55.33 46.3602 52.38 46.4202 49.71C46.8202 49.12 48.2902 46.92 48.5602 46.29C48.5702 46.26 48.5802 46.23 48.5902 46.2C48.7902 45.23 48.8202 44.22 48.6902 43.24C48.6602 43.02 48.4902 42.85 48.2702 42.82C48.0602 42.79 47.8402 42.92 47.7602 43.12C47.5502 43.63 47.2902 44.13 46.9902 44.61C46.8102 44.88 46.3802 45.99 45.5302 48.24C45.2602 48.95 45.0002 49.62 44.9302 49.79C44.7802 50.13 44.7402 50.71 44.7002 51.64C44.6702 52.28 44.6302 53 44.5302 53.37C44.5302 53.39 44.5202 53.41 44.5102 53.43C44.2402 52.85 44.0602 52.22 43.9602 51.58C43.6902 49.81 43.9602 48.03 44.7602 46.42C44.9502 45.95 45.2002 45.2 45.5002 44.25C46.1902 42.13 47.2302 38.93 48.1402 37.4C49.4202 35.25 52.1202 34.29 54.1602 33.88C55.5202 33.6 59.0702 33.61 62.2002 33.61C64.2702 33.61 66.0602 33.61 67.0402 33.54C69.4702 33.35 71.4202 31.62 72.5902 30.58L72.8702 30.33C73.5002 29.78 74.1502 28.76 74.7802 27.78C75.2602 27.03 75.7602 26.24 76.0802 25.98C76.6502 25.62 77.3502 25.38 78.0702 25.33C78.2702 25.32 78.4502 25.17 78.5102 24.98C78.5602 24.79 78.7002 24.36 78.8102 24.04L79.5102 25.39C79.5602 25.49 79.6502 25.57 79.7402 25.61C80.6402 26.01 81.7102 26.51 81.9702 26.66C82.2202 27 82.4202 27.37 82.5502 27.78C82.7202 28.24 83.2102 28.53 83.6402 28.79C83.7402 28.85 83.8202 28.9 83.8902 28.95L85.2102 30.22L84.5702 31.04C84.0002 31.09 82.4502 31.22 81.9302 31.22C81.2702 31.21 80.6502 31.5 80.2302 32.02C79.9902 32.31 79.9802 32.7 79.9602 33.29C79.9502 33.67 79.9302 34.15 79.8502 34.75C79.6602 36.25 79.1502 37.67 78.3602 38.97C76.8302 41.35 75.1102 43.64 73.2502 45.78C72.0502 47.18 71.3702 52.11 71.2902 52.67C71.2802 52.75 71.2902 52.84 71.3202 52.92L72.2702 55.29C72.3302 55.45 72.4702 55.56 72.6402 55.59C72.9402 55.64 73.2302 55.76 73.4802 55.93C73.4902 55.96 73.4802 56.05 73.4502 56.18H71.0702L69.2602 50.83L69.3402 49.65L69.2902 45.44C69.2902 45.3 69.2302 45.16 69.1102 45.07C69.0002 44.98 68.8602 44.94 68.7102 44.97C67.6302 45.17 66.5302 45.28 65.4302 45.29C64.4702 45.26 60.1502 44.45 58.0002 44.03C57.8402 44 57.6602 44.05 57.5502 44.17C56.3202 45.48 55.0002 46.73 53.6202 47.87C52.8102 48.43 51.0102 49.35 49.8102 49.96C49.2802 50.23 48.8502 50.45 48.6202 50.58C47.8802 51 47.4602 52.07 47.2902 53.95C47.1802 55.17 47.9702 55.48 48.5502 55.71C48.6602 55.76 48.7802 55.8 48.9002 55.85C49.0202 55.93 49.1102 56.05 49.1802 56.2L49.1902 56.18ZM47.6402 45.96C47.5502 46.15 47.3202 46.53 47.0402 46.99C47.3302 46.24 47.6102 45.53 47.7502 45.25C47.7202 45.49 47.6902 45.72 47.6402 45.96Z" fill="#888888"/>
												<path class="dog-large" d="M102.309 56.85H102.339C103.339 56.79 105.319 56.68 105.669 56.68C105.919 56.68 106.149 56.58 106.309 56.4C106.589 56.08 106.539 55.62 106.519 55.54C106.509 55.44 106.459 55.35 106.399 55.27L104.689 53.44L104.729 48.81C105.139 48.6 105.969 48.17 106.859 47.73C107.759 47.28 109.679 45.37 111.909 43.12C112.549 42.46 113.109 41.9 113.439 41.61C115.129 39.63 116.669 37.57 118.059 35.44C118.549 35 119.199 34.79 119.869 34.85C120.659 34.88 129.579 37.43 135.029 39.03C137.639 39.44 140.239 39.6 142.809 39.52C142.829 39.58 142.849 39.68 142.869 39.82C142.989 40.6 144.449 51.83 144.459 51.94C144.459 51.99 144.479 52.03 144.489 52.07C145.149 53.62 146.019 55.07 147.079 56.38C147.169 56.5 147.309 56.56 147.459 56.56H151.349C151.609 56.56 151.829 56.35 151.839 56.09L151.869 55.45C151.869 55.33 151.839 55.21 151.759 55.12L150.339 53.41L150.609 36.75C151.179 35.81 154.849 29.67 155.659 27.82C156.629 24.7 157.399 21.54 157.959 18.39C158.169 17.68 158.799 17.08 159.579 16.9C159.989 16.82 161.519 16.82 162.979 16.82C164.879 16.82 166.679 16.82 167.379 16.68C168.759 16.39 169.839 12.73 170.229 11.17C170.289 10.94 170.169 10.7 169.959 10.6C168.269 9.85004 165.439 8.53004 165.009 8.22004C164.779 8.05004 164.549 7.67004 164.259 7.19004C163.919 6.61004 163.489 5.89004 162.779 5.03004C161.309 3.23004 157.439 3.26004 157.269 3.26004C153.999 3.26004 153.229 3.86004 152.329 4.85004C151.529 5.73004 145.749 11.76 143.249 14.02C141.179 15.88 137.689 17.19 132.879 17.89C130.979 18.17 126.579 18.4 121.929 18.64C116.899 18.91 111.699 19.18 109.489 19.53C105.179 20.22 102.519 24.03 101.319 26.24C100.359 28 98.3991 32.41 97.9891 33.32C96.4791 34.08 95.2091 35.22 94.2991 36.65C93.7891 37.63 93.6991 38.33 94.0091 38.83C94.3491 39.38 94.9791 39.37 95.0591 39.37C95.0891 39.37 95.1291 39.37 95.1591 39.36C97.5791 38.74 99.9191 37.83 102.119 36.62C103.199 35.89 103.959 34.84 104.459 33.89L104.689 39.28L99.3591 45.82C99.2891 45.91 99.2491 46.02 99.2491 46.14L99.3691 53.33C99.3691 53.4 99.3891 53.48 99.4191 53.54C100.059 54.78 100.909 55.86 101.969 56.76C102.059 56.83 102.169 56.88 102.279 56.88H102.309V56.85ZM105.469 55.71C104.809 55.73 103.069 55.83 102.479 55.87C101.609 55.11 100.899 54.2 100.359 53.18L100.239 46.29L105.569 39.75C105.649 39.66 105.689 39.54 105.679 39.42L105.339 31.6C105.329 31.36 105.149 31.16 104.899 31.14C104.669 31.11 104.439 31.27 104.379 31.5C104.379 31.53 103.609 34.43 101.609 35.78C99.5391 36.9 97.3091 37.79 94.9891 38.38C94.9391 38.38 94.8591 38.35 94.8291 38.3C94.8091 38.27 94.6791 38 95.1391 37.12C95.9691 35.83 97.1591 34.78 98.5691 34.1C98.6691 34.05 98.7591 33.96 98.7991 33.86C98.8291 33.81 101.139 28.58 102.169 26.69C103.289 24.64 105.739 21.1 109.629 20.47C111.789 20.13 116.969 19.85 121.979 19.59C126.659 19.34 131.079 19.11 133.029 18.83C138.029 18.1 141.689 16.72 143.909 14.72C146.429 12.45 152.249 6.38004 153.059 5.49004C153.699 4.78004 154.199 4.22004 157.279 4.22004C158.269 4.21004 161.049 4.45004 162.029 5.64004C162.689 6.44004 163.099 7.13004 163.419 7.67004C163.769 8.25004 164.039 8.70004 164.429 9.00004C164.989 9.41004 168.059 10.81 169.189 11.32C168.569 13.61 167.659 15.62 167.189 15.72C166.579 15.84 164.669 15.84 162.989 15.84C161.309 15.84 159.899 15.84 159.389 15.94C158.249 16.2 157.349 17.05 157.019 18.16C156.449 21.33 155.689 24.45 154.749 27.48C153.919 29.34 149.739 36.29 149.699 36.36C149.659 36.43 149.629 36.52 149.629 36.6L149.349 53.57C149.349 53.69 149.389 53.8 149.459 53.89L150.869 55.59H147.689C146.769 54.41 146.009 53.11 145.409 51.74C145.269 50.71 143.939 40.43 143.819 39.66C143.729 39.09 143.549 38.5 142.859 38.54C140.279 38.63 137.729 38.48 135.229 38.09C132.899 37.4 120.889 33.89 119.899 33.89C118.989 33.81 118.039 34.12 117.339 34.78C117.309 34.81 117.289 34.84 117.259 34.87C115.879 36.99 114.359 39.03 112.719 40.95C112.419 41.22 111.859 41.79 111.199 42.46C109.619 44.06 107.239 46.47 106.409 46.88C105.149 47.51 104.009 48.1 104.009 48.1C103.849 48.18 103.749 48.35 103.739 48.53L103.699 53.66C103.699 53.78 103.749 53.91 103.829 54L105.469 55.75V55.71ZM142.929 39.01V39.5C142.929 39.5 142.879 39.5 142.849 39.48L142.929 39.01Z" fill="#888888"/>
												<path class="dog-large" d="M157.009 14.8699C157.079 14.8699 157.159 14.8699 157.229 14.8699C157.349 14.8699 157.479 14.7999 157.559 14.7099C158.469 13.7199 159.139 12.5599 159.559 11.2799C159.979 9.82993 159.939 8.33993 159.439 6.92993C159.349 6.67993 159.079 6.53993 158.819 6.62993C158.569 6.71993 158.429 6.99993 158.519 7.24993C158.949 8.45993 158.979 9.74993 158.619 10.9899C158.269 12.0599 157.709 13.0399 156.959 13.8899C156.019 13.8799 155.169 13.3799 154.689 12.5599C154.039 11.5299 153.529 10.4399 153.159 9.29993C153.069 9.04993 152.799 8.89993 152.549 8.98993C152.299 9.06993 152.149 9.34993 152.239 9.59993C152.629 10.8199 153.179 11.9799 153.859 13.0699C154.509 14.1899 155.699 14.8699 156.989 14.8699H157.009Z" fill="#888888"/>
												<path class="dog-xxl" d="M183.57 57.3H183.6C184.67 57.24 186.79 57.12 187.17 57.12C187.43 57.12 187.66 57.02 187.83 56.83C188.12 56.5 188.07 56.02 188.06 55.93C188.05 55.83 188 55.74 187.94 55.66L186.09 53.69L186.13 48.69C186.56 48.47 187.46 48.01 188.42 47.52C188.73 47.36 189.26 47.2 189.87 47.01C191.84 46.4 194.81 45.48 197.62 42.96C199.86 40.95 201.25 38.28 202.17 36.51C202.55 35.78 202.98 34.96 203.2 34.77C204.4 34.89 213.18 36.91 218.61 38.21C221.38 38.65 224.16 38.81 226.93 38.72C226.95 38.79 226.98 38.9 227.01 39.07C227.15 39.91 228.7 51.93 228.72 52.05C228.72 52.1 228.74 52.14 228.75 52.18C229.46 53.83 230.39 55.38 231.51 56.79C231.6 56.91 231.74 56.97 231.89 56.97H236.05C236.31 56.97 236.52 56.76 236.54 56.5L236.57 55.81C236.57 55.69 236.54 55.57 236.46 55.48L234.93 53.64L235.22 36.23C235.61 35.97 236.23 35.39 237.28 34.16L237.3 34.14C237.34 34.25 237.38 34.39 237.41 34.48C237.55 34.95 237.7 35.44 238.12 35.61C238.27 35.68 238.51 35.71 238.8 35.55C240.14 34.81 240.84 32.33 241.4 30.07C241.47 29.79 241.54 29.48 241.59 29.33C241.65 29.35 241.71 29.38 241.77 29.4C242.11 29.54 242.64 29.76 243.2 29.46C243.89 29.1 244.38 28.07 244.77 26.14C245.07 24.67 245.26 22.77 245.46 20.93C245.64 19.2 245.88 16.87 246.15 16.34C246.65 16.29 247.83 16.44 248.87 16.56C250.43 16.75 251.9 16.92 252.67 16.76C253.06 16.68 254.09 16.47 255.96 9.53003C256.2 8.65003 255.76 7.73003 254.93 7.35003C252.58 6.28003 250.99 5.50003 250.67 5.27003C250.42 5.08003 250.18 4.69003 249.86 4.15003C249.49 3.53003 249.04 2.76003 248.28 1.84003C246.7 -0.029971 242.57 2.8994e-05 242.4 2.8994e-05C238.92 2.8994e-05 238.09 0.640029 237.14 1.69003C236.57 2.32003 233.89 4.70003 231.52 6.79003C230.41 7.77003 229.34 8.73003 228.5 9.48003C225.97 11.75 222.96 12.2 218.77 12.8L218.48 12.85C216.43 13.15 211.92 13.29 207.14 13.43C202.06 13.59 196.8 13.75 194.44 14.13C189.71 14.88 182.41 20.31 181.24 22.45C180.82 23.23 180.47 24.24 180.06 25.41C179.35 27.47 178.54 29.8 177.06 31.74C175.82 33.36 174.52 34.73 173.38 35.93C172.33 37.03 171.51 37.91 171.03 38.65C170.45 39.64 169.98 40.57 170.35 41.24C170.48 41.47 170.76 41.76 171.38 41.79C176.71 42.03 183.52 34.46 185.86 31.65L186.16 38.54L180.45 45.54C180.38 45.63 180.34 45.74 180.34 45.86L180.47 53.55C180.47 53.62 180.49 53.7 180.52 53.76C181.2 55.08 182.11 56.24 183.25 57.19C183.34 57.26 183.45 57.31 183.56 57.31H183.57V57.3ZM187.03 56.15C186.38 56.17 184.38 56.28 183.74 56.32C182.8 55.49 182.03 54.52 181.45 53.42L181.32 46.03L187.04 39.02C187.12 38.93 187.16 38.81 187.15 38.69L186.79 30.33C186.79 30.13 186.65 29.95 186.45 29.89C186.25 29.83 186.04 29.89 185.92 30.06C185.84 30.17 177.34 41.09 171.44 40.83C171.27 40.83 171.22 40.78 171.22 40.78C171.17 40.67 171.24 40.29 171.89 39.18C172.3 38.53 173.14 37.65 174.12 36.63C175.22 35.47 176.6 34.02 177.87 32.35C179.45 30.28 180.32 27.77 181.02 25.75C181.41 24.62 181.75 23.64 182.13 22.94C183.19 20.98 190.22 15.82 194.62 15.12C196.92 14.76 202.14 14.59 207.2 14.43C212.01 14.28 216.54 14.14 218.65 13.83L218.94 13.78C223.12 13.17 226.43 12.69 229.17 10.22C230.01 9.47003 231.08 8.52003 232.18 7.54003C234.68 5.32003 237.27 3.03003 237.87 2.36003C238.56 1.59003 239.11 1.00003 242.41 1.00003C243.48 1.00003 246.46 1.24003 247.52 2.53003C248.22 3.39003 248.66 4.12003 249.01 4.71003C249.37 5.33003 249.67 5.81003 250.08 6.11003C250.47 6.40003 251.96 7.13003 254.51 8.29003C254.91 8.47003 255.11 8.91003 255 9.33003C253.69 14.21 252.76 15.74 252.45 15.87C251.84 15.99 250.31 15.81 248.97 15.65C247.6 15.49 246.41 15.34 245.82 15.46C245.09 15.63 244.9 16.71 244.47 20.89C244.28 22.71 244.09 24.59 243.79 26.01C243.36 28.12 242.9 28.57 242.72 28.66C242.58 28.73 242.42 28.69 242.12 28.56C241.82 28.44 241.11 28.14 240.73 28.87C240.64 29.03 240.58 29.3 240.43 29.9C240.13 31.12 239.45 33.89 238.45 34.66C238.4 34.54 238.35 34.37 238.32 34.26C238.17 33.77 238.01 33.21 237.5 33.11C237.01 33.01 236.64 33.44 236.52 33.58C235.71 34.53 235.21 35.01 234.94 35.26C234.8 35.19 234.63 35.19 234.48 35.28C234.25 35.42 234.09 35.72 234.12 35.98C234.13 36.08 234.17 36.17 234.23 36.25L233.94 53.85C233.94 53.97 233.98 54.08 234.05 54.17L235.58 56V56.03H232.12C231.13 54.76 230.3 53.37 229.66 51.89C229.52 50.81 228.09 39.78 227.96 38.95C227.86 38.35 227.67 37.75 226.97 37.77C224.21 37.87 221.47 37.7 218.79 37.28C217.33 36.93 204.21 33.82 203.15 33.82C202.51 33.76 202.11 34.53 201.3 36.09C200.41 37.79 199.06 40.37 196.95 42.27C194.3 44.65 191.45 45.54 189.57 46.12C188.89 46.33 188.36 46.5 187.97 46.69C186.62 47.37 185.4 48 185.4 48C185.24 48.08 185.14 48.25 185.13 48.43L185.08 53.91C185.08 54.03 185.13 54.16 185.21 54.25L187.01 56.17H187.02L187.03 56.15ZM227.05 38.24V38.73C227.05 38.73 226.98 38.73 226.96 38.71L227.05 38.24Z" fill="#888888"/>
												<path class="dog-xxl" d="M241.61 12.4899C241.89 12.4899 242.17 12.4399 242.46 12.3399C243.58 12.0699 244.06 10.5899 244.49 9.28992L244.58 9.01992C244.98 7.63992 245.25 5.48992 244.7 3.91992C244.61 3.66992 244.34 3.52992 244.08 3.61992C243.83 3.70992 243.69 3.98992 243.78 4.23992C244.25 5.58992 244.01 7.49992 243.65 8.72992L243.57 8.98992C243.24 10.0099 242.82 11.2699 242.22 11.3999C242.2 11.3999 242.18 11.4099 242.15 11.4199C241.31 11.7199 240.4 11.3199 239.19 10.1099C238.2 9.11992 237.95 8.28992 237.62 7.22992L237.47 6.75992C237.39 6.50992 237.11 6.35992 236.86 6.44992C236.61 6.52992 236.46 6.80992 236.55 7.05992L236.69 7.51992C237.03 8.60992 237.35 9.64992 238.5 10.7999C239.32 11.6299 240.38 12.4999 241.59 12.4999L241.61 12.4899Z" fill="#888888"/>
												</g>
												<defs>
												<clipPath id="clip0_2081_131441">
													<rect width="256" height="58" fill="white"/>
												</clipPath>
												</defs>
											</svg>

											</div>
											<h5 id="df-dog-size">small</h5>
										</div>
										</div>
									</div>
									</div>
								</div>
								</div>
								<div class="dog-data wpb_column vc_column_container vc_col-sm-6">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
									<div class="wpb_raw_code wpb_content_element wpb_raw_html">
										<div class="wpb_wrapper">
										<p>
											<strong>平均体高: </strong>
											<span id="df-height">CM</span>
										</p>
										<p>
											<strong>平均体重: </strong>
											<span id="df-weight">1 - 10KG</span>
										</p>
										<p>
											<strong>どんな犬種？: </strong>
											<span id="df-info"></span>
										</p>
										</div>
									</div>
									</div>
								</div>
								</div>
							</div>
							</div>
						</div>
						</div>
						<div class="text-center dogfinder-right wpb_column vc_column_container">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
							<div class="wpb_raw_code wpb_content_element wpb_raw_html">
								<div class="wpb_wrapper">
								<p class="mb-10 pink-highlight">
									<strong>愛犬の理想のベッドは:</strong>
								</p>
								<h4 class="df-size mb-20" id="df-bed-size">small</h4>
								<img src="https://ambientloungedev.kinsta.cloud/wp-content/uploads/2023/07/doggeneric.svg" class="pet-bed-dim">
								<img src="https://ambientlounge.co.jp/wp-content/themes/ambientlounge-child-theme/img/sizechart/dog_xs_bed-12-new7.svg" class="pet-bed-dim bed-dim-xs" style="width:100%; max-height: 146px;">
								<img src="https://ambientlounge.co.jp/wp-content/themes/ambientlounge-child-theme/img/sizechart/dog_s_bed-12-new2.svg" class="pet-bed-dim bed-dim-small" style="width:100%;">
								<img src="https://ambientlounge.co.jp/wp-content/themes/ambientlounge-child-theme/img/sizechart/dog_m_bed-12-new2.svg" class="pet-bed-dim bed-dim-medium">
								<img src="https://ambientlounge.co.jp/wp-content/themes/ambientlounge-child-theme/img/sizechart/dog_l_bed-12-new2.svg" class="pet-bed-dim bed-dim-large">
								<img src="https://ambientlounge.co.jp/wp-content/uploads/2023/07/dog41kg.svg" class="pet-bed-dim bed-dim-xxl">
								</div> 
							</div>
							</div>
						</div>
						</div>
					</div>
					<div style="" id="sizing-cta" class="normal_height vc_row wpb_row vc_row-fluid hide hidden">
						<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
							<div class="vc_btn3-container vc_btn3-center" id="df-order-button">
								<a style="background-color:#b7a59c; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="/products/pet/pet-lounge/?attribute_pa_size=s" title="">商品ページへ</a>
							</div>
							</div>
						</div>
						</div>
					</div>
					</section>
				</div>

				<div id="content-deliverydays" class="modal-content">
					<div class="modal-title">
						お届け予定日
					</div>
					<div class="modal-subtitle">
						配送方法：通常配送
					</div>                
 
					<div class="modal-description">
						ペット商品最短2営業日〜5営業日以内に発送 / <br>
						ソファ商品最短3営業日〜10営業日以内に発送になります。<br><br>
						詳しくは <a href="https://ambientlounge.co.jp/shopping-guide/" target="_blank" title="ご利用ガイド">ご利用ガイド</a> をご確認ください
					</div>
				</div>
			</div>';			

		echo $el;			
	}

	// (Empty) Lightbox element for Template Offers
	if ( is_page_template( 'template-offers.php' ) )
	{
		$el = 
				'<div class="modal-overlay">
					<div class="modal-wrapper modal-offers-wrapper">
						<div class="modal-close">
							<svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M32.2344 32.2324L11.7688 11.7668" stroke="#888888"/>
								<path d="M11.7656 32.2324L32.2312 11.7668" stroke="#888888"/>
							</svg>
						</div>
						<div class="modal-content-wrapper">
							<div class="modal-content-inner">                        
							</div>
						</div>
					</div>
				</div>
				<div class="modal-content-list">
					<div id="modal-offers" class="modal-content">			
						<div class="modal-thumb">
						</div>	
						<div class="modal-notice">
							商品がカートに追加されました
						</div>
					</div>
				</div>';				

			echo $el;	
	}
?>

</body>
</html>
