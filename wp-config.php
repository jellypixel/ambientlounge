<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dbw_ambientlounge' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9r5qXtzDo+s{>u2`Fhx3j{3Y;L4d@8E.2:ER#Os1m.n[jx&hi~kCy-9$C`QIJ|{x' );
define( 'SECURE_AUTH_KEY',  'Wx(Iu^ 7 roz-SfFl~:!Cg8tXtwcl(4$N7g,q47E5z-+1HewnA 25U6y<q;~|yEG' );
define( 'LOGGED_IN_KEY',    'EG2q?UG*c^tsJ[3d#m% [-]Ce;Tt}XICMAHRb!W;zdi8+!d@>PobEww9z=g<^9VA' );
define( 'NONCE_KEY',        'VJGJX<zVp@&dbP17Zzz/Z#-5lWsHs<m%VMWqIfD7p/%y|;KDVRWf<]6x9|?+WA|T' );
define( 'AUTH_SALT',        'k8zX>VkFIGs70}je$]u.`HM:woV|$8U}QLYNrituO3O;NE4LQ7ozEFb[ify6{Q/2' );
define( 'SECURE_AUTH_SALT', 'pT:}-D,6N@nSBF+ft[+<+aM4qgKYy^|)KG836]E3W.m5< m,y/LJ#N`aDR{doK#_' );
define( 'LOGGED_IN_SALT',   'f;{|C<V%VJe<LJ(NTK <lC!co3-K*UK$tp+]~J{69*[A*h0{e)}29^-<*t[>g1~Z' );
define( 'NONCE_SALT',       'wiM6G(@}d>m4b*0hcVAlT/]$9<>J)CB>K&ym3,P6oBh{S8ZmTTj,_8v&R>3LtPq^' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'dbw_al_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
